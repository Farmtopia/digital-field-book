﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Microsoft.AspNetCore.Authorization;
using System.Data;
using ITC.FieldBook.Application.Infrastructure.Enums;
using ITC.FieldBook.Application.Infrastructure;
using ITC.FieldBook.Application.Infrastructure.Models;
using System.ComponentModel.DataAnnotations;
using AutoMapper;
using ITC.FieldBook.Application.Infrastructure.Tools;
using ITC.FieldBook.Application.Infrastructure.Interfaces;
using ITC.FieldBook.Application.Enums;

namespace ITC.FieldBook.Application.Features.Users
{
    public class ChangePasswordController : ApiControllerBase
    {
        [HttpPost("users/me/password")]
        [HttpPost("users/{userId}/password")]
        public Task ChangePassword([FromRoute] int? userId, [FromBody] ChangePasswordRequest data)
        {
            return Mediator.Send(new ChangePasswordCommand {  UserId = userId, Password = data.Password, PasswordConfirmation = data.PasswordConfirmation });
        }
    }
    public class ChangePasswordRequest : IRequest
    {
        [Required]
        [MinLength(8)]
        public string? Password { get; set; }

        [Required]
        public string? PasswordConfirmation { get; set; }
    }

    public class ChangePasswordCommand : IRequest
    {
        public int? UserId { get; set; }

        public string? Password { get; set; }

        public string? PasswordConfirmation { get; set; }
    }

    internal sealed class ChangePasswordCommandHandler : IRequestHandler<ChangePasswordCommand>
    {
        private readonly DbContext _ctx;
        private readonly ICurrentUserService _currentUserService;

        public ChangePasswordCommandHandler(DbContext ctx, ICurrentUserService currentUserService)
        {
            _ctx = ctx;
            _currentUserService = currentUserService;
        }

        public Task Handle(ChangePasswordCommand request, CancellationToken cancellationToken)
        {
            if (Globals.LoginType != EnumLoginType.LOCAL_LOGIN)
                throw new Exception("Not allowed");

            var currentUser = _currentUserService.GetUser()!;
            var userId = request.UserId ?? currentUser.Id!.Value;

            if (request.UserId != null && request.UserId != currentUser.Id && !currentUser.HasRole(EnumRole.Admin))
                throw new Exception("No permission");
            else if (!request.Password!.Equals(request.PasswordConfirmation))
                throw new Exception("Passwords do not match");

            var passwordHasher = new PasswordHasher();
            var passwordHash = passwordHasher.HashPasword(request.Password, out byte[] salt);
            var saltHash = Convert.ToBase64String(salt);
            SetPassword(userId, passwordHash, saltHash, _currentUserService.Id);

            return Task.CompletedTask;
        }

        public void SetPassword(int id, string password, string salt, int currentUserId)
        {
            using (var connection = _ctx.CreateConnection())
            {
                var now = DateTime.Now;
                connection.Execute("UPDATE TOP(1) FieldBook.[user] SET password = @password, password_salt = @salt, modified = @now, modified_by = @currentUserId WHERE id = @id", new { id, password, salt, now, currentUserId });
            }
        }
    }
}

