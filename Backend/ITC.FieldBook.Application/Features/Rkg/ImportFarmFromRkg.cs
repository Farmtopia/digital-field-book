﻿using ITC.FieldBook.Application.Infrastructure.Interfaces;
using ITC.FieldBook.Application.Infrastructure;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata;
using Dapper;
using ITC.FieldBook.Application.Models;
using ITC.FieldBook.Application.Infrastructure.Models;
using ITC.FieldBook.Application.Infrastructure.Enums;
using ITC.FieldBook.Application.Infrastructure.Tools;
using System.Drawing;
using NetTopologySuite.Geometries;
using ITC.FieldBook.Application.Features.Fields;
using System.Transactions;

namespace ITC.FieldBook.Application.Features.Farms
{
    public class ImportFarmFromRkgController : ApiControllerBase
    {
        [HttpPost("rkg/farm/import")]
        public Task ImportFarmFromRkg([FromBody] ImportFarmFromRkgRequest request)
        {
            return Mediator.Send(new ImportFarmFromRkgQuery { KmgMid = request.KmgMid });
        }
    }

    public class ImportFarmFromRkgRequest
    {
        public int KmgMid { get; set; }
    }

    public class ImportFarmFromRkgQuery : IRequest
    {
        public int? KmgMid { get; set; }
    }

    internal sealed class ImportFarmFromRkgQueryHandler : IRequestHandler<ImportFarmFromRkgQuery>
    {
        private readonly DbContext _ctx;
        private readonly ICurrentUserService _currentUserService;
        private readonly IConfiguration _configuration;
        private readonly IFileService _fileService;

        public ImportFarmFromRkgQueryHandler(DbContext ctx, IConfiguration configuration, ICurrentUserService currentUserService, IFileService fileService)
        {
            _ctx = ctx;
            _configuration = configuration;
            _currentUserService = currentUserService;
            _fileService = fileService;
        }

        public async Task Handle(ImportFarmFromRkgQuery request, CancellationToken cancellationToken)
        {
            var user = _currentUserService.GetUser()!;
            var currentUserId = user.Id!.Value;
            var rkgManager = new RkgManager(_configuration["Rkg:ApiUrl"], _configuration["Rkg:Username"], _configuration["Rkg:Password"]);
            var rkgData = await rkgManager.GetFarm(request.KmgMid!.Value);
            var fieldsRepository = new FieldsRepository(_ctx);
            var geoManager = new GeoManager();

            if (rkgData != null)
            {
                try
                {
                    var farm = new Farm();
                    farm.Identifier = rkgData.KmgMid!.Value.ToString();
                    farm.Name = rkgData.DomaceIme ?? ($"{rkgData.Nosilec?.Ime ?? ""} {rkgData.Nosilec?.Priimek ?? ""}");
                    if(!string.IsNullOrWhiteSpace(rkgData.Nosilec?.Ime) || !string.IsNullOrWhiteSpace(rkgData.Nosilec?.Priimek))
                        farm.Owner = $"{rkgData.Nosilec?.Ime}{(!string.IsNullOrWhiteSpace(rkgData.Nosilec?.Ime) && !string.IsNullOrWhiteSpace(rkgData.Nosilec?.Priimek) ? " " : "")}{rkgData.Nosilec?.Priimek}";

                    if (rkgData.Naslov != null)
                    {
                        farm.Address = $"{rkgData.Naslov.Naselje} {(string.IsNullOrWhiteSpace(rkgData.Naslov.Hs) ? "" : rkgData.Naslov.Hs)}{(string.IsNullOrWhiteSpace(rkgData.Naslov.Hd) ? "" : rkgData.Naslov.Hd)}";
                        farm.PostCode = $"{rkgData.Naslov.PostnaStevilka}";
                        farm.City = $"{rkgData.Naslov.Posta}";
                    }

                    NetTopologySuite.Geometries.Point? location = null;
                    if (rkgData.Naslov?.XH != null && rkgData.Naslov?.YH != null)
                    {
                        location = geoManager.TransformSLOCoordinatesToLongLat(rkgData.Naslov.YH.Value, rkgData.Naslov.XH.Value);
                    }

                    var farmId = SaveFarm(farm, location, currentUserId);

                    if (location != null)
                    {
                        var farmsBll = new FarmsBll(_ctx);
                        farmsBll.UpdateImage(_configuration, _fileService, farmId, location.Y, location.X);
                    }

                    if (rkgData.Gerks != null && rkgData.Gerks.Any())
                    {
                        var fieldsBll = new FieldsBll(_ctx);

                        foreach (var g in rkgData.Gerks)
                        {
                            try
                            {
                                var field = new Field();
                                field.Identifier = g.GerkPid!.Value.ToString();
                                field.Name = g.DomaceIme?.ToString();
                                field.Description = g.RabaOpis?.ToString();

                                if (g.RabaId != null)
                                    field.TypeId = fieldsRepository.GetFieldTypeByIdentifier(g.RabaId.Value.ToString());
                                field.Area = g.AreaAcres;

                                Geometry? fieldGeometry = null;
                                if (!string.IsNullOrEmpty(g.Geometry))
                                    fieldGeometry = geoManager.TransformWktSLOToWGS84(g.Geometry);

                                var fieldId = SaveField(farmId, field, fieldGeometry, currentUserId);

                                if (fieldGeometry != null)
                                    fieldsBll.UpdateImage(_configuration, _fileService, fieldId, fieldGeometry.Centroid.Y, fieldGeometry.Centroid.X);
                            }
                            catch (Exception e)
                            {
                                throw new Exception("Error importing field", e);
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    throw new Exception("Error importing farm", e);
                }
            }
        }

        private int SaveFarm(Farm farm, Geometry? location, int currentUserId)
        {
            var now = DateTime.Now;

            using (var conn = _ctx.CreateConnection())
            {
                return conn.ExecuteScalar<int>($@"
                    IF NOT EXISTS
                    (
                        SELECT
                            TOP(1) 1 
                        FROM
                            FieldBook.farm 
                        WHERE
                            {DbContext.GetIsDeletedIsActiveCondition()}
                            AND identifier = @identifier 
                    )
                    BEGIN
                        INSERT INTO
                            FieldBook.farm (identifier, name, owner, address, post_code, city, location, created, created_by, modified, modified_by )
                        OUTPUT INSERTED.id 
                        VALUES
                            (
                                @identifier,
                                @name,
                                @owner,
                                @address,
                                @postCode,
                                @city,
                                @location,
                                @now,
                                @currentUserId,
                                @now,
                                @currentUserId
                            )
                    END
                    ELSE
                        BEGIN
                            UPDATE
                                FieldBook.farm
                            SET
                                name = @name,
                                owner = @owner,
                                address = @address,
                                post_code = @postCode,
                                city = @city,
                                location = @location,
                                thumb_file_id = NULL,
                                modified = @now,
                                modified_by = @currentUserId
                            OUTPUT INSERTED.id
                            WHERE
                                {DbContext.GetIsDeletedIsActiveCondition()}
                                AND identifier = @identifier
                        END"
                ,
                    new { identifier = farm.Identifier, name = farm.Name, owner = farm.Owner, address = farm.Address, postCode = farm.PostCode, city = farm.City, location, now, currentUserId });
            }
        }

        private int SaveField(int farmId, Field field, Geometry? fieldGeometry, int currentUserId)
        {
            var now = DateTime.Now;

            using (var conn = _ctx.CreateConnection())
            {
                return conn.ExecuteScalar<int>($@"
                    IF NOT EXISTS
                    (
                        SELECT
                            TOP(1) 1 
                        FROM
                            FieldBook.field 
                        WHERE
                            {DbContext.GetIsDeletedIsActiveCondition()}
                            AND farm_id = @farmId
                            AND identifier = @identifier 
                    )
                    BEGIN
                        INSERT INTO
                            FieldBook.field (farm_id, identifier, name, description, type_id, area, field_geometry, location, created, created_by, modified, modified_by)
                        OUTPUT INSERTED.id 
                        VALUES
                            (
                                @farmId,
                                @identifier,
                                @name,
                                @description,
                                @typeId,
                                @area,
                                @fieldGeometry,
                                @location,
                                @now,
                                @currentUserId,
                                @now,
                                @currentUserId
                            )
                    END
                    ELSE
                        BEGIN
                            UPDATE
                                FieldBook.field
                            SET
                                name = @name,
                                description = @description,
                                type_id = @typeId,
                                area = @area,
                                field_geometry = @fieldGeometry,
                                location = @location,
                                modified = @now,
                                modified_by = @currentUserId
                            OUTPUT INSERTED.id
                            WHERE
                                {DbContext.GetIsDeletedIsActiveCondition()}
                                AND farm_id = @farmId
                                AND identifier = @identifier
                        END"
                ,
                    new { farmId, identifier = field.Identifier, name = field.Name, description = field.Description, typeId = field.TypeId, area = field.Area, fieldGeometry, location = (Geometry?)fieldGeometry?.Centroid, now, currentUserId });
            }
        }

    }
}
