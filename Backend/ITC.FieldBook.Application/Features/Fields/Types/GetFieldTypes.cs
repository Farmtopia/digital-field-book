﻿using ITC.FieldBook.Application.Infrastructure.Interfaces;
using ITC.FieldBook.Application.Infrastructure;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata;
using Dapper;
using ITC.FieldBook.Application.Models;
using ITC.FieldBook.Application.Infrastructure.Models;

namespace ITC.FieldBook.Application.Features.Fields.Types
{
    public class GetFieldTypesController : ApiControllerBase
    {
        [HttpGet("fields/field-types")]
        public Task<DbData<FieldType>> GetFieldTypes()
        {
            return Mediator.Send(new GetFieldTypesQuery { });
        }
    }

    public class GetFieldTypesQuery : IRequest<DbData<FieldType>>
    {
    }

    internal sealed class GetFieldTypesQueryHandler : IRequestHandler<GetFieldTypesQuery, DbData<FieldType>>
    {
        private readonly DbContext _ctx;
        private readonly IConfiguration _configuration;
        private readonly ICurrentUserService _currentUserService;

        public GetFieldTypesQueryHandler(DbContext ctx, IConfiguration configuration, ICurrentUserService currentUserService)
        {
            _ctx = ctx;
            _configuration = configuration;
            _currentUserService = currentUserService;
        }

        public Task<DbData<FieldType>> Handle(GetFieldTypesQuery request, CancellationToken cancellationToken)
        {
            var fieldsRepository = new FieldsRepository(_ctx);
            var data = fieldsRepository.GetFieldTypes();

            return Task.FromResult(data);
        }
    }
}
