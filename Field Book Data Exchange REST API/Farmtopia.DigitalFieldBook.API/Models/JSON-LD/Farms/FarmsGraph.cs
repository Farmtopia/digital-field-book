﻿using System.Text.Json.Serialization;

namespace Farmtopia.DigitalFieldBook.API.Models.JSON_LD.Farms
{
    public class FarmsGraph : GraphBase
    {
        [JsonPropertyName("datasetSlices")]
        public List<FarmsDatasetSlice> DatasetSlices { get; set; }
    }
}
