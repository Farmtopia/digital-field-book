﻿using ITC.FieldBook.Application.Infrastructure.Interfaces;
using ITC.FieldBook.Application.Infrastructure;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata;
using Dapper;
using ITC.FieldBook.Application.Models;
using ITC.FieldBook.Application.Infrastructure.Models;
using ITC.FieldBook.Application.Features.Fields;
using ITC.FieldBook.Application.Enums;
using ITC.FieldBook.Application.Features.Units;
using System.ComponentModel.DataAnnotations;

namespace ITC.FieldBook.Application.Features.Fertilizers
{
    public class CalculateFertilizerUsageController : ApiControllerBase
    {
        [HttpGet("fertilizers/calculate-usage")]
        public Task<CalculateUsageResponse> CalculateFertilizerUsage([FromQuery] CalculateFertilizerUsageCommand command)
        {
            return Mediator.Send(command);
        }
    }

    public class CalculateFertilizerUsageCommand : BaseDataQuery<CalculateUsageResponse>
    {
        [Required]
        public List<int>? FieldPlotIds { get; set; }

        [Required]
        public int? FertilizerId { get; set; }

        [Required]
        public decimal? Amount { get; set; }

        [Required]
        public int? AmountUnitId { get; set; }

        public int? TaskId { get; set; }

        public DateTime? Date { get; set; }
    }

    internal sealed class CalculateFertilizerUsageQueryHandler : IRequestHandler<CalculateFertilizerUsageCommand, CalculateUsageResponse>
    {
        private readonly DbContext _ctx;
        private readonly IConfiguration _configuration;
        private readonly ICurrentUserService _currentUserService;

        public CalculateFertilizerUsageQueryHandler(DbContext ctx, IConfiguration configuration, ICurrentUserService currentUserService)
        {
            _ctx = ctx;
            _configuration = configuration;
            _currentUserService = currentUserService;
        }

        public Task<CalculateUsageResponse> Handle(CalculateFertilizerUsageCommand request, CancellationToken cancellationToken)
        {
            var user = _currentUserService.GetUser()!;
            var farmId = _currentUserService.FarmId!.Value;

            if (request.FieldPlotIds == null || !request.FieldPlotIds.Any())
                return Task.FromResult(new CalculateUsageResponse());

            var response = new CalculateUsageResponse();
            var fieldsRepo = new FieldsRepository(_ctx);
            var fieldPlots = fieldsRepo.GetFieldPlots(farmId, fieldPlotIds: request.FieldPlotIds).Data;

            // Area for calculating usage
            response.Area = fieldPlots.Sum(x => x.Area ?? 0);

            // Get units for fertilizer usage input
            var unitsRepo = new UnitsRepository(_ctx);
            var fertilizerUsageUnits = unitsRepo.GetUnits(EnumUnitGroup.FERTILIZER_USAGE).Data;

            // Validate that unit exists
            var unit = fertilizerUsageUnits.FirstOrDefault(x => x.Id == request.AmountUnitId);
            if (unit == null)
                return Task.FromResult(response);

            // Amount provided
            response.Amount = request.Amount;
            response.AmountUnitId = unit.Id;
            response.AmountUnit = unit.Name;

            // Get current status of fertilizer
            var fertilizersRepository = new FertilizersRepository(_ctx);
            var fertilizer = fertilizersRepository.GetFertilizer(request.FertilizerId!.Value, true, false, _currentUserService.FarmId!.Value, request.Date ?? DateTime.Now, request.TaskId).Item;

            if (unit.IsUnitPerArea())
            {
                // Input per /ha, calculate usage...
                var baseUnitName = unit.GetBaseUnit();
                var baseUnit = !string.IsNullOrEmpty(baseUnitName) ? unitsRepo.GetUnitByName(baseUnitName).Item : null;

                if (baseUnit == null)
                    return Task.FromResult(response);

                response.UsageUnitId = baseUnit.Id;
                response.UsageUnit = baseUnit.Name;
                response.Usage = Math.Round(request.Amount!.Value * response.Area.Value / 100, 2);
            }
            else
            {
                response.Usage = request.Amount;
                response.UsageUnitId = unit.Id;
                response.UsageUnit = unit.Name;
            }

            // Remaining stock calculation
            response.Stock = fertilizer?.Stock ?? 0;
            response.StockUnit = fertilizer?.StockUnit;
            response.RemainingStock = Math.Round((fertilizer?.Stock ?? 0) - (response.Usage ?? 0), 2); 
            response.HasStock = response.RemainingStock >= 0;

            return Task.FromResult(response);
        }
    }
}
