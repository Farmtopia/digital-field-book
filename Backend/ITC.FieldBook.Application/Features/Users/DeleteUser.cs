﻿using ITC.FieldBook.Application.Infrastructure.Models;
using ITC.FieldBook.Application.Infrastructure;
using MediatR;
using Microsoft.AspNetCore.DataProtection.KeyManagement;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using ITC.FieldBook.Application.Models;
using ITC.FieldBook.Application.Infrastructure.Services;
using ITC.FieldBook.Application.Infrastructure.Interfaces;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Authorization;
using ITC.FieldBook.Application.Infrastructure.Enums;

namespace ITC.FieldBook.Application.Features.Users
{
    [Authorize(Roles = EnumRole.Admin)]
    public class DeleteUserController : ApiControllerBase
    {
        [HttpDelete("users/{userId}")]
        public Task DeleteUser([FromRoute] int? userId)
        {
            return Mediator.Send(new DeleteUserCommand { UserId = userId });
        }
    }

    public class DeleteUserCommand : IRequest
    {
        [Required]
        public int? UserId { get; set; }
    }

    internal sealed class DeleteUserCommandHandler : IRequestHandler<DeleteUserCommand>
    {
        private readonly DbContext _ctx;
        private readonly ICurrentUserService _currentUserService;

        public DeleteUserCommandHandler(DbContext ctx, ICurrentUserService currentUserService)
        {
            _ctx = ctx;
            _currentUserService = currentUserService;
        }

        public Task Handle(DeleteUserCommand request, CancellationToken cancellationToken)
        {
            var usersRepository = new UsersRepository(_ctx);
            var user = usersRepository.GetUserById(request.UserId!.Value);
            if (user != null)
            {
                var currentUser = _currentUserService.GetUser()!;
                _ctx.DeleteItem(user, currentUser);
            }

            return Task.CompletedTask;
        }
    }
}

