﻿using ITC.FieldBook.Application.Infrastructure.Interfaces;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITC.FieldBook.Application.Infrastructure.Services
{
    /// <summary>
    /// Service for handling temporary files
    /// </summary>
    public class TempFileService : ITempFileService
    {
        private IWebHostEnvironment _env;
        private string _tempPath;
        private const int _fileValidityMinutes = 1;
        private Dictionary<Guid, TempFileInfo> _tempFiles = new Dictionary<Guid, TempFileInfo>();

        public TempFileService(IWebHostEnvironment env)
        {
            // Set temporary files path
            _env = env;
            _tempPath = Path.Combine(_env.ContentRootPath, "Temp").Replace('\\', Path.DirectorySeparatorChar);
        }

        /// <summary>
        /// Initialize temp provider
        /// </summary>
        public void Initialize()
        {
            if (!Directory.Exists(_tempPath))
                Directory.CreateDirectory(_tempPath);
            else
                DeleteAllTempFiles();
        }

        /// <summary>
        /// Save temporary file
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public Guid? SaveFile(string fileName, byte[] data)
        {
            var id = Guid.NewGuid();
            var filePath = Path.Combine(_tempPath, $"{id}.{fileName}").Replace('\\', Path.DirectorySeparatorChar);

            File.WriteAllBytes(filePath, data);
            _tempFiles.Add(id, new TempFileInfo { Filename = fileName, Path = filePath });

            return id;
        }

        /// <summary>
        /// Get and remove temporary file
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Models.File? GetFile(Guid? id)
        {
            if (id == null || !_tempFiles.ContainsKey(id.Value))
                return null;

            // Get temporary file if stil valid
            var fileInfo = _tempFiles[id.Value];
            if (!File.Exists(fileInfo.Path))
                return null;

            // Expired download link
            if ((DateTime.Now - fileInfo.Created).TotalMinutes > _fileValidityMinutes)
            {
                File.Delete(fileInfo.Path!);
                return null;
            }

            // Read file content
            var fileBytes = File.ReadAllBytes(fileInfo.Path!);
            
            // Delete temporary file
            File.Delete(fileInfo.Path!);

            return new Models.File { Filename = fileInfo.Filename, Bytes = fileBytes };
        }

        /// <summary>
        /// Delete all temporary files
        /// </summary>
        private void DeleteAllTempFiles()
        {
            DirectoryInfo di = new DirectoryInfo(_tempPath);

            foreach (FileInfo file in di.GetFiles())
            {
                file.Delete();
            }

            foreach (DirectoryInfo dir in di.GetDirectories())
            {
                dir.Delete(true);
            }

            _tempFiles.Clear();
        }

        /// <summary>
        /// Class for storing temporary file info
        /// </summary>
        private class TempFileInfo
        {
            public string? Filename { get; set; }

            public string? Path { get; set; }

            public DateTime Created { get; set; } = DateTime.Now;
        }
    }
}
