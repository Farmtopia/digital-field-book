﻿using ITC.FieldBook.Application.Infrastructure.Interfaces;
using ITC.FieldBook.Application.Infrastructure;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata;
using Dapper;
using ITC.FieldBook.Application.Models;
using ITC.FieldBook.Application.Infrastructure.Models;
using ITC.FieldBook.Application.Features.Units;
using ITC.FieldBook.Application.Enums;
using Microsoft.Extensions.Azure;

namespace ITC.FieldBook.Application.Features.Machinery
{
    public class GetMachineryPerformanceUnitsController : ApiControllerBase
    {
        [HttpGet("machinery/performance-units")]
        public Task<DbData<Models.Unit>> GetMachineryPerformanceUnits()
        {
            return Mediator.Send(new GetMachineryPerformanceUnitsQuery());
        }
    }

    public class GetMachineryPerformanceUnitsQuery : BaseDataQuery<DbData<Models.Unit>>
    {
    }

    internal sealed class GetMachineryPerformanceUnitsQueryHandler : IRequestHandler<GetMachineryPerformanceUnitsQuery, DbData<Models.Unit>>
    {
        private readonly DbContext _ctx;
        private readonly IConfiguration _configuration;
        private readonly ICurrentUserService _currentUserService;

        public GetMachineryPerformanceUnitsQueryHandler(DbContext ctx, IConfiguration configuration, ICurrentUserService currentUserService)
        {
            _ctx = ctx;
            _configuration = configuration;
            _currentUserService = currentUserService;
        }

        public Task<DbData<Models.Unit>> Handle(GetMachineryPerformanceUnitsQuery request, CancellationToken cancellationToken)
        {
            var unitsRepo = new UnitsRepository(_ctx);
            var data = unitsRepo.GetUnits(EnumUnitGroup.MACHINERY_PERFORMANCE);

            return Task.FromResult(data);
        }
    }
}
