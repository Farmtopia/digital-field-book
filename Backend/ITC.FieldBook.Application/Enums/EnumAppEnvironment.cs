﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITC.FieldBook.Application.Enums
{
    public static class EnumAppEnvironment
    {
        public const string DEFAULT = "DEFAULT";
        public const string FARMTOPIA = "FARMTOPIA";
    }
}
