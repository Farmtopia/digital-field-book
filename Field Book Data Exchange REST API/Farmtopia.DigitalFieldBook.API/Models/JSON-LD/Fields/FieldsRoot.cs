﻿using Swashbuckle.AspNetCore.Annotations;
using System.Text.Json.Serialization;

namespace Farmtopia.DigitalFieldBook.API.Models.JSON_LD.Fields
{
    public class FieldsRoot
    {
        [SwaggerSchema(Nullable = false)]
        [JsonPropertyName("@context")]
        public FieldsContext Context { get; set; }

        [SwaggerSchema(Nullable = false)]
        [JsonPropertyName("@graph")]
        public virtual List<FieldsGraph> Graph { get; set; }
    }
}
