﻿using Swashbuckle.AspNetCore.Annotations;
using System.Text.Json.Serialization;

namespace Farmtopia.DigitalFieldBook.API.Models.JSON_LD.Tasks
{
    public class TasksRoot
    {
        [SwaggerSchema(Nullable = false)]
        [JsonPropertyName("@context")]
        public TasksContext Context { get; set; }

        [SwaggerSchema(Nullable = false)]
        [JsonPropertyName("@graph")]
        public virtual List<TasksGraph> Graph { get; set; }
    }
}
