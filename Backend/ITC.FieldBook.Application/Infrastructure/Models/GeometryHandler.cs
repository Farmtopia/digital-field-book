﻿using Dapper;
using NetTopologySuite.Geometries;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NetTopologySuite.IO;

namespace ITC.FieldBook.Application.Infrastructure.Models
{
    public class GeometryCollectionHandler : SqlMapper.TypeHandler<GeometryCollection>
    {
        readonly bool _geography;
        readonly SqlServerBytesWriter _writer;
        readonly SqlServerBytesReader _reader;

        public GeometryCollectionHandler(bool geography = false)
        {
            _geography = geography;
            _writer = new SqlServerBytesWriter { IsGeography = geography };
            _reader = new SqlServerBytesReader { IsGeography = geography };
        }

        public override GeometryCollection Parse(object value)
            => (GeometryCollection)_reader.Read((byte[])value);

        public override void SetValue(IDbDataParameter parameter, GeometryCollection? value)
        {
            if (value != null && value.SRID == 0)
                value.SRID = 4326;
            parameter.Value = _writer.Write(value);

            ((SqlParameter)parameter).SqlDbType = SqlDbType.Udt;
            ((SqlParameter)parameter).UdtTypeName = _geography ? "geography" : "geometry";
        }
    }

    public class GeometryHandler : SqlMapper.TypeHandler<Geometry>
    {
        readonly bool _geography;
        readonly SqlServerBytesWriter _writer;
        readonly SqlServerBytesReader _reader;

        public GeometryHandler(bool geography = false)
        {
            _geography = geography;
            _writer = new SqlServerBytesWriter { IsGeography = geography };
            _reader = new SqlServerBytesReader { IsGeography = geography };
        }

        public override Geometry Parse(object value)
            => (Geometry)_reader.Read((byte[])value);

        public override void SetValue(IDbDataParameter parameter, Geometry? value)
        {
            if (value != null && value.SRID == 0)
                value.SRID = 4326;
            parameter.Value = _writer.Write(value);

            ((SqlParameter)parameter).SqlDbType = SqlDbType.Udt;
            ((SqlParameter)parameter).UdtTypeName = _geography ? "geography" : "geometry";
        }
    }

    //public class SqlGeographyHandler : SqlMapper.TypeHandler<Microsoft.SqlServer.Types.SqlGeography>
    //{
    //    /// <summary>
    //    /// Assign the value of a parameter before a command executes.
    //    /// </summary>
    //    /// <param name="parameter">The parameter to configure.</param>
    //    /// <param name="value">Parameter value.</param>
    //    public override void SetValue(IDbDataParameter parameter, SqlGeography? value)
    //    {
    //        object? parsed = null;
    //        if (value is not null)
    //        {
    //            parsed = SqlGeography.STGeomFromWKB(value.STAsBinary(), value.STSrid.Value);
    //        }

    //        parameter.Value = parsed ?? DBNull.Value;
    //        if (parameter is SqlParameter sqlParameter)
    //        {
    //            sqlParameter.SqlDbType = SqlDbType.Udt;
    //            sqlParameter.UdtTypeName = "geography";
    //        }
    //    }

    //    public override SqlGeography? Parse(object value)
    //    {
    //        if (value is SqlGeography geo)
    //        {
    //            return geo;
    //        }
    //        return SqlGeography.Parse(value.ToString()!);
    //    }
    //}
}
