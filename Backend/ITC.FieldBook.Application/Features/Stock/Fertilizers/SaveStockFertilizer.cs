﻿using ITC.FieldBook.Application.Infrastructure.Models;
using ITC.FieldBook.Application.Infrastructure;
using MediatR;
using Microsoft.AspNetCore.DataProtection.KeyManagement;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using ITC.FieldBook.Application.Models;
using ITC.FieldBook.Application.Infrastructure.Interfaces;
using ITC.FieldBook.Application.Features.Fertilizers;

namespace ITC.FieldBook.Application.Features.Stock.Fertilizers
{
    public class SaveStockFertilizerController : ApiControllerBase
    {
        [HttpPost("stock/fertilizers")]
        public Task SaveStockFertilizer([FromBody] StockFertilizer data)
        {
            return Mediator.Send(new SaveStockFertilizerCommand { Data = data });
        }
    }

    public class SaveStockFertilizerCommand : IRequest
    {
        public StockFertilizer? Data { get; set; }
    }

    internal sealed class SaveStockFertilizerCommandHandler : IRequestHandler<SaveStockFertilizerCommand>
    {
        private readonly DbContext _ctx;
        private readonly ICurrentUserService _currentUserService;

        public SaveStockFertilizerCommandHandler(DbContext ctx, ICurrentUserService currentUserService)
        {
            _ctx = ctx;
            _currentUserService = currentUserService;
        }

        public Task Handle(SaveStockFertilizerCommand request, CancellationToken cancellationToken)
        {
            if (request.Data != null)
            {
                var data = request.Data;
                var user = _currentUserService.GetUser()!;
                var stockRepo = new StockRepository(_ctx);

                data.FarmId = _currentUserService.FarmId!.Value;
                stockRepo.SaveStockFertilizer(data, user: user);
            }

            return Task.CompletedTask;
        }
    }
}

