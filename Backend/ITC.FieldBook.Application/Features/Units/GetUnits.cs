﻿using ITC.FieldBook.Application.Infrastructure.Interfaces;
using ITC.FieldBook.Application.Infrastructure;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using ITC.FieldBook.Application.Infrastructure.Models;
using ITC.FieldBook.Application.Models;
using ITC.FieldBook.Application.Enums;

namespace ITC.FieldBook.Application.Features.Units
{
    public class GetUnitsController : ApiControllerBase
    {
        [HttpGet("units")]
        public Task<DbData<Models.Unit>> GetUnits(string? group)
        {
            return Mediator.Send(new GetUnitsQuery { Group = group });
        }
    }

    public class GetUnitsQuery : IRequest<DbData<Models.Unit>>
    {
        public string? Group { get; set; }
    }

    internal sealed class GetUnitsQueryHandler : IRequestHandler<GetUnitsQuery, DbData<Models.Unit>>
    {
        private readonly DbContext _ctx;
        private readonly IConfiguration _configuration;
        private readonly ICurrentUserService _currentUserService;

        public GetUnitsQueryHandler(DbContext ctx, IConfiguration configuration, ICurrentUserService currentUserService)
        {
            _ctx = ctx;
            _configuration = configuration;
            _currentUserService = currentUserService;
        }

        public Task<DbData<Models.Unit>> Handle(GetUnitsQuery request, CancellationToken cancellationToken)
        {
            //var displayNames = new Dictionary<string, string>
            //{
            //    { EnumUnitGroup.FERTILIZER, "Gnojila - vnos zaloge" },
            //    { EnumUnitGroup.FERTILIZER_USAGE, "Gnojila - vnos porabe" },
            //    { EnumUnitGroup.PESTICIDE, "FFS - vnos porabe" },
            //    { EnumUnitGroup.PESTICIDE_USAGE, "FFS - vnos porabe" },
            //    { EnumUnitGroup.MACHINERY_PERFORMANCE, "Mehanizacija - zmogljivost" }
            //};

            var unitsRepo = new UnitsRepository(_ctx);
            var data = unitsRepo.GetUnits(request.Group);

            //if(data.Data.Any())
            //{
            //    foreach(var d in data.Data)
            //    {
            //        if (!string.IsNullOrWhiteSpace(d.GroupsDisplayValue))
            //            d.GroupsDisplayValue = string.Join(", ", d.GroupsDisplayValue.Split(',', StringSplitOptions.RemoveEmptyEntries).Select(x =>
            //            {
            //                x = x.Trim();
            //                return displayNames.ContainsKey(x) ? displayNames[x] : x;
            //            }));
            //    }
            //}

            return Task.FromResult(data);
        }
    }
}
