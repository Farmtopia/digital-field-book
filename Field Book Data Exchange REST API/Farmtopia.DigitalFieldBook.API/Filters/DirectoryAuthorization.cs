﻿using Farmtopia.DigitalFieldBook.API.Repository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.DataProtection.KeyManagement;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Text.Json;

namespace Farmtopia.DigitalFieldBook.API.Filters
{
    public sealed class DirectoryAuthorizationAttribute : Attribute, IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationFilterContext context)
        {
            if (context != null)
            {
                // Check if [AllowAnonymous] is applied to the action or controller
                var endpoint = context.HttpContext.GetEndpoint();
                if (endpoint?.Metadata?.GetMetadata<IAllowAnonymous>() != null)
                {
                    // Skip authorization if [AllowAnonymous] is present
                    return;
                }

                // Validate user
                var authHeader = context.HttpContext.Request.Headers["Authorization"].FirstOrDefault();

                if (string.IsNullOrEmpty(authHeader))
                {
                    // No authorization header present, return unauthorized
                    context.Result = new BadRequestObjectResult("Incorrect Headers. Token Missing");
                    return;
                }

                if (!authHeader.StartsWith("Bearer ", StringComparison.OrdinalIgnoreCase))
                {
                    // Invalid authentication scheme, return unauthorized
                    context.Result = new BadRequestObjectResult("Incorrect Headers");
                    return;
                }

                var jwtTokenValue = authHeader.Substring("Bearer ".Length).Trim();
                if (string.IsNullOrWhiteSpace(jwtTokenValue))
                {
                    // Invalid authentication scheme, return unauthorized
                    context.Result = new BadRequestObjectResult("Incorrect Headers");
                    return;
                }

                // Validate provider
                var providerId = Globals.ProviderId!.Value;
                var ctx = context.HttpContext.RequestServices.GetService<DbContext>()!;
                var providerRepository = new ProviderRepository(ctx);
                var provider = providerRepository.GetProvider(providerId);

                if (string.IsNullOrWhiteSpace(provider?.SecretKey))
                {
                    // Invalid authentication scheme, return unauthorized
                    context.Result = new BadRequestObjectResult("No Api Key present in the Database");
                    return;
                }

                // Validate token
                try
                {
                    var tokenHandler = new JwtSecurityTokenHandler();
                    var key = Encoding.ASCII.GetBytes(provider.SecretKey);
                    tokenHandler.ValidateToken(jwtTokenValue, new TokenValidationParameters
                    {
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey = new SymmetricSecurityKey(key),
                        ValidateIssuer = false,
                        ValidateAudience = false,
                        RequireExpirationTime = false,
                        ClockSkew = TimeSpan.Zero
                    }, out SecurityToken validatedToken);

                    var jwtToken = (JwtSecurityToken)validatedToken;
                    var username = jwtToken.Claims.First(x => x.Type == "sub").Value;
                    int datasetId = int.Parse(jwtToken.Claims.FirstOrDefault(x => x.Type == "dataset" || x.Type == "dataset_id")?.Value!);

                    if (username == null)
                        throw new Exception("Invalid token");
                    else if(username != "farmtopia_directory_listing")
                    {
                        // Validate user
                        var user = providerRepository.GetUser(providerId, datasetId, username);
                        if (user == null || !user.Active)
                        {
                            context.Result = new UnauthorizedObjectResult("No permission");
                            return;
                        }
                    };

                    // Store token so it can be used in controller method
                    context.HttpContext.User = new ClaimsPrincipal(new ClaimsIdentity(new List<Claim>{
                        new Claim(ClaimTypes.Name, username),
                        new Claim("consumer", jwtToken.Claims.FirstOrDefault(x => x.Type == "consumer")?.Value ?? ""),
                        new Claim("dataset", datasetId.ToString())
                    }, "Custom"));

                    context.HttpContext.Items["JwtToken"] = jwtToken;
                }
                catch
                {
                    context.Result = new UnauthorizedObjectResult("Could not validate credentials");
                    return;
                }
            }
        }
    }
}
