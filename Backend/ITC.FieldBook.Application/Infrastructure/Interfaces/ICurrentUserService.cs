﻿using ITC.FieldBook.Application.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITC.FieldBook.Application.Infrastructure.Interfaces
{
    public interface ICurrentUserService
    {
        int Id { get; set; }
        string? Name { get; set; }
        bool IsAuthenticated { get; set; }
        public int? FarmId { get; set; }
        public int? Year { get; set; }
        public string? Locale { get; set; }
        public User? GetUser();
    }
}
