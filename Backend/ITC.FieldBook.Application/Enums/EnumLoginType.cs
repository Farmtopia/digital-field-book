﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITC.FieldBook.Application.Enums
{
    public static class EnumLoginType
    {
        public const string LOCAL_LOGIN = "LOCAL_LOGIN";
        public const string ITC_SSO_LOGIN = "ITC_SSO_LOGIN";
    }
}
