﻿using ITC.FieldBook.Application.Infrastructure.Interfaces;
using ITC.FieldBook.Application.Infrastructure.Models;
using ITC.FieldBook.Application.Infrastructure;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication;
using System.Text.Json;
using ITC.FieldBook.Application.Infrastructure.Tools;
using ITC.FieldBook.Application.Infrastructure.Enums;
using ITC.FieldBook.Application.Features.Farms;
using Microsoft.Extensions.Configuration;
using ITC.FieldBook.Application.Enums;
using ITC.FieldBook.Application.Models;

namespace ITC.FieldBook.Application.Features.Users
{
    [AllowAnonymous]
    public class LoginUserController : ApiControllerBase
    {
        [HttpPost("user/login")]
        public Task<LoginResponse> LoginUser([FromBody] LoginUserRequest request)
        {
            return Mediator.Send(new LoginUserCommand { Token = request.Token, Username = request.Username, Password = request.Password });
        }
    }
    public class LoginUserRequest
    {
        public string? Token { get; set; }

        public string? Username { get; set; }

        public string? Password { get; set; }
    }

    public class LoginUserCommand : IRequest<LoginResponse>
    {
        public string? Token { get; set; }

        public string? Username { get; set; }

        public string? Password { get; set; }
    }

    public class LoginResponse
    {
        public User? User { get; set; }

        public int? RegistrationStatus { get; set; }

        public bool Success { get; set; }

        public string? Token { get; set; }
    }

    internal sealed class LoginUserCommandHandler : IRequestHandler<LoginUserCommand, LoginResponse>
    {
        private readonly DbContext _ctx;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IConfiguration _configuration;

        public LoginUserCommandHandler(DbContext ctx, IHttpContextAccessor httpContextAccessor, IConfiguration configuration)
        {
            _ctx = ctx;
            _httpContextAccessor = httpContextAccessor;
            _configuration = configuration;
        }

        public async Task<LoginResponse> Handle(LoginUserCommand request, CancellationToken cancellationToken)
        {
            var usersRepository = new UsersRepository(_ctx);
            var usersBll = new UsersBll(_ctx);

            User? user;
            if (Globals.Instance.HasLoginType(EnumLoginType.ITC_SSO_LOGIN))
            {
                if (string.IsNullOrWhiteSpace(request.Token))
                    return new LoginResponse { Success = false };

                var ssoManager = new ItcSsoManager(_configuration["ItcSso:ApiUrl"]);
                var ssoUser = await ssoManager.GetUser(request.Token);

                if (ssoUser == null)
                    return new LoginResponse { Success = false };

                var uuid = ssoUser.Uuid!.Value;

                var ensureUserResult = usersBll.EnsureUser(uuid, new User
                {
                    Uuid = ssoUser.Uuid,
                    Name = ssoUser.Name,
                    Surname = ssoUser.Surname,
                    Displayname = ssoUser.Displayname,
                    Email = ssoUser.Email,
                    Role = EnumRole.User,
                });

                // Inactive user
                if(ensureUserResult.User == null)
                    return new LoginResponse { Success = false };

                user = ensureUserResult.User;
            }
            else if (Globals.Instance.HasLoginType(EnumLoginType.LOCAL_LOGIN))
            {
                var email = request.Username!;

                // Get user
                user = usersRepository.GetUserByEmail(email, true);
                if (user == null || !user.IsActive)
                    return new LoginResponse { Success = false };

                // Validate password
                var passwordSalt = usersRepository.GetUserPasswordSalt(user.Id!.Value);
                if (
                    passwordSalt == null ||
                    string.IsNullOrEmpty(passwordSalt.Password) ||
                    string.IsNullOrEmpty(passwordSalt.PasswordSalt))
                    return new LoginResponse { Success = false };

                var passwordHasher = new PasswordHasher();
                var isValid = passwordHasher.VerifyPassword(request.Password!, passwordSalt.Password, Convert.FromBase64String(passwordSalt.PasswordSalt));
                if (!isValid)
                    return new LoginResponse { Success = false };
            }
            else
                throw new Exception("Invalid login type");

            var userId = user.Id!.Value;

            // Set default farm
            var farmsBll = new FarmsBll(_ctx);
            var userFarms = farmsBll.GetFarms(userId).Data.ToList();
            var farm = userFarms.FirstOrDefault();
            user.FarmId = farm?.Id;

            // User must have at least one farm defined (admin not required)
            if (user.Role == EnumRole.User && user.FarmId == null)
                return new LoginResponse { Success = false };

            var authService = new AuthService(_ctx, _configuration);
            var token = authService.GetToken(user, farm);

            // Update user login time
            usersRepository.UpdateLastLogin(userId, DateTime.Now);

            //// Set claims
            //var claimsIdentity = usersBll.GetClaims(user, farm);
            //await _httpContextAccessor.HttpContext!.SignInAsync(
            //    CookieAuthenticationDefaults.AuthenticationScheme,
            //    new ClaimsPrincipal(claimsIdentity));

            return new LoginResponse { User = user, Success = true, Token = token };
        }
    }
}
