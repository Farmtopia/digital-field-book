﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Http.Json;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace ITC.FieldBook.Application.Infrastructure.Tools
{
    public class RkgManager
    {
        private string _apiUrl;
        private string _username;
        private string _password;
        private HttpClient _client;

        public RkgManager(string apiUrl, string username, string password)
        {
            _apiUrl = apiUrl;
            _username = username;
            _password = password;
            _client = new HttpClient() { BaseAddress = new Uri(_apiUrl) };
        }

        public async Task Login()
        {
            //_client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Token);
            var response = await _client.PostAsync("/api/authtoken", JsonContent.Create(new { Email = _username, Password = _password }));

            if (response.IsSuccessStatusCode)
            {
                var data = await response.Content.ReadFromJsonAsync<RkgLoginResponse>();
                _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", data!.Token);
            }
        }

        public async Task<RkgKmg?> GetFarm(int kmgMid)
        {
            await Login();

            var response = await _client.GetAsync($"/rkg/GetKmgResponse?kmgMid={kmgMid}");

            if (response.IsSuccessStatusCode)
            {
                var data = await response.Content.ReadFromJsonAsync<RkgKmg>(new System.Text.Json.JsonSerializerOptions { PropertyNameCaseInsensitive = true });
                return data;
            }

            return null;
        }
    }

    public class RkgLoginResponse
    {
        public string? Message { get; set; }

        public string? Token { get; set; }
    }

    public class RkgKmg
    {
        [JsonPropertyName("kmg_mid")]
        public int? KmgMid { get; set; }

        [JsonPropertyName("domace_ime")]
        public string? DomaceIme { get; set; }

        [JsonPropertyName("SubjectNosilec")]
        public RkgKmgNosilec? Nosilec { get; set; }

        [JsonPropertyName("NaslovObj")]
        public RkgKmgNaslov? Naslov { get; set; }

        [JsonPropertyName("gerks")]
        public List<RkgKmgGerk>? Gerks { get; set; }
    }

    public class RkgKmgNosilec
    {
        [JsonPropertyName("naziv")]
        public string? Naziv { get; set; }

        [JsonPropertyName("ime")]
        public string? Ime { get; set; }

        [JsonPropertyName("priimek")]
        public string? Priimek { get; set; }

        [JsonPropertyName("ds")]
        public int? Ds { get; set; }

        [JsonPropertyName("emso")]
        public string? Emso { get; set; }
    }

    public class RkgKmgNaslov
    {
        [JsonPropertyName("drzava")]
        public string? Drzava { get; set; }

        [JsonPropertyName("obcina")]
        public string? Obcina { get; set; }

        [JsonPropertyName("naselje")]
        public string? Naselje { get; set; }

        [JsonPropertyName("hs")]
        public string? Hs { get; set; }

        [JsonPropertyName("hd")]
        public string? Hd { get; set; }

        [JsonPropertyName("postna_stevilka")]
        public int? PostnaStevilka { get; set; }

        [JsonPropertyName("posta")]
        public string? Posta { get; set; }

        [JsonPropertyName("x_h")]
        public double? XH { get; set; }

        [JsonPropertyName("y_h")]
        public double? YH { get; set; }
    }

    public class RkgKmgGerk
    {
        [JsonPropertyName("kmg_mid")]
        public int? KmgMid { get; set; }

        [JsonPropertyName("gerk_pid")]
        public int? GerkPid { get; set; }

        [JsonPropertyName("raba_id")]
        public int? RabaId { get; set; }

        [JsonPropertyName("raba_opis")]
        public string? RabaOpis { get; set; }

        [JsonPropertyName("domace_ime")]
        public string? DomaceIme { get; set; }

        [JsonPropertyName("drzava")]
        public string? Drzava { get; set; }

        [JsonPropertyName("area")]
        public decimal? Area { get; set; }

        [JsonPropertyName("area_acres")]
        public decimal? AreaAcres => Area != null ? Area / 100 : null;

        [JsonPropertyName("x_c")]
        public double? XC { get; set; }

        [JsonPropertyName("y_c")]
        public double YC { get; set; }

        [JsonPropertyName("geometry")]
        public string? Geometry { get; set; }
    }
}
