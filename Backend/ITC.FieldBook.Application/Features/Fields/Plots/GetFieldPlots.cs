﻿using ITC.FieldBook.Application.Infrastructure.Interfaces;
using ITC.FieldBook.Application.Infrastructure;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata;
using Dapper;
using ITC.FieldBook.Application.Models;
using ITC.FieldBook.Application.Infrastructure.Models;
using Microsoft.VisualBasic;

namespace ITC.FieldBook.Application.Features.Fields.Crops
{
    public class GetFieldPlotsController : ApiControllerBase
    {
        [HttpGet("fields/plots")]
        public Task<DbData<FieldPlot>> GetFieldPlots([FromQuery]int? year, [FromQuery] int[]? fieldIds)
        {
            return Mediator.Send(new GetFieldPlotsQuery { Year = year, FieldIds = fieldIds?.ToList() });
        }
    }

    public class GetFieldPlotsQuery : IRequest<DbData<FieldPlot>>
    {
        public int? Year { get; set; }

        public List<int>? FieldIds { get; set; }
    }

    internal sealed class GetFieldPlotsQueryHandler : IRequestHandler<GetFieldPlotsQuery, DbData<FieldPlot>>
    {
        private readonly DbContext _ctx;
        private readonly IConfiguration _configuration;
        private readonly ICurrentUserService _currentUserService;

        public GetFieldPlotsQueryHandler(DbContext ctx, IConfiguration configuration, ICurrentUserService currentUserService)
        {
            _ctx = ctx;
            _configuration = configuration;
            _currentUserService = currentUserService;
        }

        public Task<DbData<FieldPlot>> Handle(GetFieldPlotsQuery request, CancellationToken cancellationToken)
        {
            var fieldsRepository = new FieldsRepository(_ctx);
            //var year = request.Year ?? _currentUserService.Year!.Value;
            return Task.FromResult(fieldsRepository.GetFieldPlots(_currentUserService.FarmId!.Value, request.Year, fieldIds: request.FieldIds));
        }
    }
}
