﻿using ITC.FieldBook.Application.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITC.FieldBook.Application.Models
{
    public class FieldMeasure : DbEntityWithAudit
    {
        public FieldMeasure() : base("FieldBook.field_measure") { }

        private int? _id;
        [DbColumn("id", IsKey = true, Identity = true)]
        public int? Id { get => _id; set { _id = value; RaisePropertyChanged(nameof(Id)); } }

        private int? _fieldId;
        [DbColumn("field_id")]
        public int? FieldId { get => _fieldId; set { _fieldId = value; RaisePropertyChanged(nameof(FieldId)); } }

        private int? _fieldPlotId;
        [DbColumn("field_plot_id")]
        public int? FieldPlotId { get => _fieldPlotId; set { _fieldPlotId = value; RaisePropertyChanged(nameof(FieldPlotId)); } }

        private int? _measureId;
        [DbColumn("measure_id")]
        public int? MeasureId { get => _measureId; set { _measureId = value; RaisePropertyChanged(nameof(MeasureId)); } }

        private int? _year;
        [DbColumn("year")]
        public int? Year { get => _year; set { _year = value; RaisePropertyChanged(nameof(Year)); } }

        public string? MeasureName { get; set; }

        public string? MeasureNameEn { get; set; }

        public string? MeasureDescription { get; set; }

        public string? MeasureDescriptionEn { get; set; }

        public string? MeasureCategory { get; set; }
    }
}
