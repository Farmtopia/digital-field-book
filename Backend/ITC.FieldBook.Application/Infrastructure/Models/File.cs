﻿using ITC.FieldBook.Application.Infrastructure.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITC.FieldBook.Application.Infrastructure.Models
{
    public class File
    {
        public string? Filename { get; set; }

        public byte[]? Bytes { get; set; }

        public string ContentType { get { return !string.IsNullOrEmpty(Filename) ? MimeTypeHelper.GetMimeTypeForFileExtension(Filename) : "application/octet-stream"; } }

    }
}
