﻿using System.Text.Json.Serialization;

namespace Farmtopia.DigitalFieldBook.API.Models.JSON_LD
{
    public class UnitOfMeasure
    {
        [JsonPropertyName("@id")]
        public string Id { get; set; }
    }
}
