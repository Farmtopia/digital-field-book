﻿using ITC.FieldBook.Application.Models;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITC.FieldBook.Application.Infrastructure.Models
{
    public class BaseDataQuery<T> : IRequest<T> where T: class
    {
        //public List<int>? Ids { get; set; }
    }
}
