﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Http.Json;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using System.Web;

namespace ITC.FieldBook.Application.Infrastructure.Tools
{
    public class ItcTrackingManager
    {
        string _url;
        string _apiKey;
        HttpClient _client;

        public ItcTrackingManager(string apiUrl, string apiKey)
        {
            _url = apiUrl;
            _apiKey = apiKey;
            _client = new HttpClient() { BaseAddress = new Uri(_url) };
            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _apiKey);
            _client.Timeout = TimeSpan.FromMinutes(5);
        }

        public async Task<List<DeviceTrip>> GetDeviceTrips(DateTime date, List<int> deviceIds, bool calculateDrives)
        {
            var builder = new UriBuilder(new Uri(new Uri(_url), "/tracking/trips"));
            var query = HttpUtility.ParseQueryString(builder.Query);
            query["date"] = date.ToString("yyyy-MM-dd");

            var i = 0;
            foreach (var deviceId in deviceIds) {
                query[$"deviceIds[{i}]"] = deviceId.ToString();
                i++;
            }

            query["calculateDrives"] = calculateDrives ? "true" : "false";

            builder.Query = query.ToString();
            
            string url = builder.ToString();
            var response = await _client.GetAsync(url);

            if (response.IsSuccessStatusCode)
            {
                var data = await response.Content.ReadFromJsonAsync<List<DeviceTrip>>(new System.Text.Json.JsonSerializerOptions { PropertyNameCaseInsensitive = true }) ?? new List<DeviceTrip>();
                return data;
            }
            else
            {
                var message = await response.Content.ReadAsStringAsync();
                throw new Exception($"{response.StatusCode} : {message}");
            }

        }
    }

    public class DeviceTrip
    {
        public int Id { get; set; }
        public int? DeviceId { get; set; }
        public int? GerkPid { get; set; }
        public DateTime? StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        public double? TotalMinutes { get; set; }
        public double? AverageVelocity { get; set; }
        public int? PointCount { get; set; }
        public string? BeaconId { get; set; }
        public string? BeaconName { get; set; }

        [JsonPropertyName("geoJSON")]
        public string? GeoJSON { get; set; }
    }
}
