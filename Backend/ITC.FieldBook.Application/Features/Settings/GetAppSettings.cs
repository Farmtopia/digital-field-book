﻿using ITC.FieldBook.Application.Infrastructure.Interfaces;
using ITC.FieldBook.Application.Infrastructure;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using ITC.FieldBook.Application.Infrastructure.Models;
using ITC.FieldBook.Application.Models;
using Microsoft.AspNetCore.Authorization;
using ITC.FieldBook.Application.Models.DTO;
using ITC.FieldBook.Application.Enums;

namespace ITC.FieldBook.Application.Features.Settings
{
    [AllowAnonymous]
    public class GetAppSettingsController : ApiControllerBase
    {
        [HttpGet("settings/app-settings")]
        public Task<AppSettingsDto> GetAppSettings()
        {
            return Mediator.Send(new GetAppSettingsQuery());
        }
    }

    public class GetAppSettingsQuery : IRequest<AppSettingsDto>
    {
    }

    internal sealed class GetAppSettingsQueryHandler : IRequestHandler<GetAppSettingsQuery, AppSettingsDto>
    {
        private readonly DbContext _ctx;
        private readonly IConfiguration _configuration;
        private readonly ICurrentUserService _currentUserService;

        public GetAppSettingsQueryHandler(DbContext ctx, IConfiguration configuration, ICurrentUserService currentUserService)
        {
            _ctx = ctx;
            _configuration = configuration;
            _currentUserService = currentUserService;
        }

        public Task<AppSettingsDto> Handle(GetAppSettingsQuery request, CancellationToken cancellationToken)
        {
            var appSettings = new AppSettingsDto();
            var isAuthenticated = _currentUserService.IsAuthenticated;

            appSettings.LoginType = Globals.LoginType;
            appSettings.AppEnvironment = Globals.AppEnvironment;

            if(Globals.LoginType == EnumLoginType.ITC_SSO_LOGIN)
                appSettings.ItcSsoAppId = _configuration["ItcSso:AppId"];

            if (isAuthenticated)
            {
                appSettings.MapboxAccessToken = _configuration["Mapbox:AccessToken"];
            }

            return Task.FromResult(appSettings);
        }
    }
}
