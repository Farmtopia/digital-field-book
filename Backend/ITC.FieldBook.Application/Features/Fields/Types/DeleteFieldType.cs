﻿using ITC.FieldBook.Application.Infrastructure.Models;
using ITC.FieldBook.Application.Infrastructure;
using MediatR;
using Microsoft.AspNetCore.DataProtection.KeyManagement;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using ITC.FieldBook.Application.Models;
using ITC.FieldBook.Application.Infrastructure.Services;
using ITC.FieldBook.Application.Infrastructure.Interfaces;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Authorization;
using ITC.FieldBook.Application.Infrastructure.Enums;

namespace ITC.FieldBook.Application.Features.Fields.Types
{
    [Authorize(Roles = EnumRole.Admin)]
    public class DeleteFieldTypeController : ApiControllerBase
    {
        [HttpDelete("fields/field-types/{fieldTypeId}")]
        public Task DeleteFieldType([FromRoute] int? fieldTypeId)
        {
            return Mediator.Send(new DeleteFieldTypeCommand { FieldTypeId = fieldTypeId });
        }
    }

    public class DeleteFieldTypeCommand : IRequest
    {
        [Required]
        public int? FieldTypeId { get; set; }
    }

    internal sealed class DeleteFieldTypeCommandHandler : IRequestHandler<DeleteFieldTypeCommand>
    {
        private readonly DbContext _ctx;
        private readonly ICurrentUserService _currentUserService;

        public DeleteFieldTypeCommandHandler(DbContext ctx, ICurrentUserService currentUserService)
        {
            _ctx = ctx;
            _currentUserService = currentUserService;
        }

        public Task Handle(DeleteFieldTypeCommand request, CancellationToken cancellationToken)
        {
            var fieldTypesRepository = new FieldsRepository(_ctx);
            var fieldType = fieldTypesRepository.GetFieldType(request.FieldTypeId!.Value).Item;
            if (fieldType != null)
            {
                var user = _currentUserService.GetUser()!;
                _ctx.DeleteItem(fieldType, user);
            }

            return Task.CompletedTask;
        }
    }
}

