﻿namespace Farmtopia.DigitalFieldBook.API.Models.Providers
{
    public class ProviderUser
    {
        public int? ProviderId { get; set; }

        public int? DatasetId { get; set; }

        public string? Username { get; set; }

        public bool Active { get; set; }
    }
}
