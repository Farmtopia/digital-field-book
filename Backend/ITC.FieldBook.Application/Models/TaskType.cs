﻿using ITC.FieldBook.Application.Infrastructure.Interfaces;
using ITC.FieldBook.Application.Infrastructure.Models;
using ITC.FieldBook.Application.Infrastructure.Services;
using ITC.FieldBook.Application.Infrastructure.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITC.FieldBook.Application.Models
{
    public class TaskType : DbEntityWithAudit
    {
        public TaskType() : base("FieldBook.task_type") {
        }
        
        private int? _id;
        [DbColumn("id", IsKey = true, Identity = true)]
        public int? Id { get => _id; set { _id = value; RaisePropertyChanged(nameof(Id)); } }

        private string? _identifier;
        [DbColumn("identifier")]
        public string? Identifier { get => _identifier; set { _identifier = value; RaisePropertyChanged(nameof(Identifier)); } }

        private string? _name;
        [DbColumn("name")]
        public string? Name { get => _name; set { _name = value; RaisePropertyChanged(nameof(Name)); } }

        private string? _nameEn;
        [DbColumn("name_en")]
        public string? NameEn { get => _nameEn; set { _nameEn = value; RaisePropertyChanged(nameof(NameEn)); } }

        private string? _systemType;
        [DbColumn("system_type")]
        public string? SystemType { get => _systemType; set { _systemType = value; RaisePropertyChanged(nameof(SystemType)); } }

        private string? _cropStatus;
        [DbColumn("crop_status")]
        public string? CropStatus { get => _cropStatus; set { _cropStatus = value; RaisePropertyChanged(nameof(CropStatus)); } }

        private bool _cropStatusEditable;
        [DbColumn("crop_status_editable")] public bool CropStatusEditable { get => _cropStatusEditable; set { _cropStatusEditable = value; RaisePropertyChanged(nameof(CropStatusEditable)); } }

        private bool _requiresTime;
        [DbColumn("requires_time")]
        public bool RequiresTime { get => _requiresTime; set { _requiresTime = value; RaisePropertyChanged(nameof(RequiresTime)); } }
    }
}
