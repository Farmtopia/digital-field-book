﻿using Dapper;
using ITC.FieldBook.Application.Infrastructure.Interfaces;
using ITC.FieldBook.Application.Infrastructure.Models;
using ITC.FieldBook.Application.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Dynamic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using static Dapper.SqlMapper;

namespace ITC.FieldBook.Application.Infrastructure
{
    public class DbContext
    {
        public readonly IServiceProvider _serviceProvider;
        public readonly string _connectionString;

        public DbContext(IConfiguration configuration, IServiceProvider serviceProvider)
        {

            var connectionString = configuration.GetConnectionString("Default");

            if (string.IsNullOrEmpty(connectionString))
                throw new Exception("Missing ConnectionString in config.");

            _serviceProvider = serviceProvider;
            _connectionString = connectionString;
        }

        public SqlConnection CreateConnection()
        {
            return new SqlConnection(_connectionString);
        }

        public static string GetIsDeletedIsActiveCondition(string? tablePrefix = null, bool includeInactive = false)
        {
            return $"(ISNULL({(!string.IsNullOrEmpty(tablePrefix) ? $"{tablePrefix}." : "")}is_deleted, 0) = 0{(includeInactive ? "" : $" AND ISNULL({(!string.IsNullOrEmpty(tablePrefix) ? $"{tablePrefix}." : "")}is_active, 1) = 1")})";
        }

        public DbData<T> Get<T>(string sql, object? param = null/*, List<int>? ids = null, DbQueryParameters? parameters = null*/) where T : DbEntity, new()
        {
            var result = new DbData<T>();

            // Zaenkrat ne, ker je potreben subquery, potem pa padejo vsi declare-i in order by, ker ne gre znotraj subquery-ja... rešiti drugače
            //if (ids != null && ids.Count > 0)
            //{
            //    var expando = new ExpandoObject();
            //    var dictionary = (IDictionary<string, object?>)expando!;

            //    if (param != null)
            //    {
            //        foreach (var property in param.GetType().GetProperties())
            //            dictionary.Add(property.Name, property.GetValue(param));
            //    }

            //    dictionary.Add("ids", ids);
            //    param = dictionary;

            //    sql = $"SELECT * FROM ({sql}) s WHERE s.id IN @ids";
            //}

            var data = GetRawRows<T>(sql, param);

            // TODO: pridobiti id-je, iz id-jev naložiti nazive uporabnikov in dodati k responsu, da bo prikazno ime za ModifiedByName, CreatedByName....
            //if(typeof(T).IsSubclassOf(typeof(DbEntityWithAudit)))
            //{
            //    var auditEntities = data.Where(x => x is DbEntityWithAudit).Select(x=> (x as DbEntityWithAudit)!).ToList() ?? new List<DbEntityWithAudit>();
            //    if(auditEntities.Any())
            //    {
            //        var userIds = auditEntities.Where(x => x.CreatedBy != null).Select(x => x.CreatedBy!.Value).ToList();
            //        userIds = userIds.Concat(auditEntities.Where(x=>x.ModifiedBy != null).Select(x=>x.ModifiedBy!.Value)).ToList();

            //        if(userIds.Any())
            //        {
            //            foreach(var a in auditEntities)
            //            {
            //                a.modi
            //            }
            //        }
            //    }

            //}

            result.Data = data;
            return result;
        }

        public DbItem<T> GetItem<T>(string sql, object? param = null) where T : DbEntity, new()
        {
            var result = new DbItem<T>();
            var item = GetRawItem<T>(sql, param);
            var currentUserService = _serviceProvider.GetRequiredService<ICurrentUserService>();

            result.Item = item;
            return result;
        }

        private T? GetRawItem<T>(string sql, object? param = null) where T : DbEntity, new()
        {
            using (var conn = CreateConnection())
            {
                var item = conn.QuerySingleOrDefault<T>(sql, param);
                return item;
            }
        }

        private IEnumerable<T> GetRawRows<T>(string sql, object? param = null) where T : DbEntity, new()
        {
            List<T> data = new List<T>();
            using (var conn = CreateConnection())
            {
                using (var reader = conn.ExecuteReader(sql, param))
                {

                    // Seznam columnov
                    List<string> columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
                    //bool entityTracking = typeof(T).IsSubclassOf(typeof(IEntityWithAudit));

                    // Dodamo podatke...
                    int i = 1;
                    var parser = reader.GetRowParser<T>();

                    while (reader.Read())
                    {
                        T t = parser(reader);
                        t.RowNum = i;
                        data.Add(t);
                        i++;
                    }
                }
                return data;
            }
        }

        public Dictionary<string, object?> Save<T>(T entity, List<string>? excludeColumns = null, User? user = null) where T : DbEntity
        {
            var result = new Dictionary<string, object?>();
            var sqlData = entity.GetSqlData(false);

            if (string.IsNullOrWhiteSpace(entity.TableName) || !sqlData.Any())
                return result;

            var now = DateTime.Now;
            var table = entity.TableName;
            var schema = "dbo";

            // Get table and schema name
            if (table.Contains('.'))
            {
                var tblSchema = table.Split('.');
                schema = tblSchema[0];
                table = tblSchema[1];
            }
            var tableAndSchema = $"[{schema}].[{table}]";

            var isNewRecordColumnName = "_isNewRecord";
            var keyColumns = entity.GetKeyColumns();
            var identityColumn = entity.GetIdentityColumn();
            var outputColumns = keyColumns.ToDictionary(key => key, value => "varchar(MAX)");
            var outputKeysTableSql = $"DECLARE @keys TABLE ({isNewRecordColumnName} bit, {string.Join(", ", outputColumns.Select(x => $"{x.Key} {x.Value}"))})";
            var outputKeysSql = $" OUTPUT CAST([isNewRecord] AS bit) AS {isNewRecordColumnName}, {string.Join(", ", outputColumns.Select(x => "INSERTED." + x.Key))} INTO @keys";
            var whereConditions = string.Join(" AND ", keyColumns.Select(x => x + " = @" + x));

            var insertColumns = sqlData.Where(x => x.Key != identityColumn /* Identity ne spreminjamo */ && (excludeColumns == null || !excludeColumns.Contains(x.Key)) /* Ročno excludanih tudi ne */).Select(x => x.Key).ToList(); // Brez exclude columnov
            var updateColumns = new List<string>(insertColumns.Where(x => !keyColumns.Contains(x) /* Pri update-u ne spreminjamo ključev */));

            if (entity.Audit)
            {
                if (user == null)
                    throw new ArgumentNullException(nameof(user));

                // Dodaj stolpce za change tracking
                if (!insertColumns.Contains("created_by"))
                    insertColumns.Add("created_by");
                if (updateColumns.Contains("created_by"))
                    updateColumns.Remove("created_by");
                sqlData["created_by"] = user.Id;

                if (!insertColumns.Contains("created"))
                    insertColumns.Add("created");
                if (updateColumns.Contains("created"))
                    updateColumns.Remove("created");
                sqlData["created"] = now;

                if (!insertColumns.Contains("modified_by"))
                    insertColumns.Add("modified_by");
                if (!updateColumns.Contains("modified_by"))
                    updateColumns.Add("modified_by");
                sqlData["modified_by"] = user.Id;

                if (!insertColumns.Contains("modified"))
                    insertColumns.Add("modified");
                if (!updateColumns.Contains("modified"))
                    updateColumns.Add("modified");
                sqlData["modified"] = now;

                //if (!insertColumns.Contains("sid"))
                //    insertColumns.Add("sid");

                //if (!updateColumns.Contains("sid") && updateSid)
                //    updateColumns.Add("sid");
                //podatkiZaBazo.Set("sid", sid);
            }

            string insertSql = $@"
                INSERT INTO {tableAndSchema} (
                    {string.Join(", ", insertColumns)}
                )
                {outputKeysSql.Replace("[isNewRecord]", "1")}
                SELECT TOP(1)
                    {string.Join(", ", insertColumns.Select(x => $"@{x}"))}
            ";

            string? updateSql = updateColumns.Count > 0 ? $@"UPDATE TOP(2) {tableAndSchema} SET
                                        {string.Join(", ", updateColumns.Select(x => $"{x} = @{x}"))}
                                    {outputKeysSql.Replace("[isNewRecord]", "0")}
                                    WHERE {whereConditions}" : null;

            string insertOrUpdate = $@"
                {outputKeysTableSql}
                IF NOT EXISTS(SELECT TOP(1) {keyColumns[0]} FROM {tableAndSchema} WHERE {whereConditions})
                BEGIN 
                    /* insert */
                    {insertSql}
                END 
                {(
                !string.IsNullOrEmpty(updateSql) ? @$"
                ELSE
                BEGIN 
                    /* update */
                    {updateSql}
                END" : "")}
                SELECT TOP(1) * FROM @keys";

            using (var conn = CreateConnection())
            {
                conn.Open();

                using (var transaction = conn.BeginTransaction())
                {
                    try
                    {
                        // Get DB info required for saving and parsing data
                        Dictionary<string, string> columnNameType = new Dictionary<string, string>();
                        using (var reader = conn.ExecuteReader(@"
                            SELECT
                                COLUMN_NAME,
                                DATA_TYPE
                            FROM INFORMATION_SCHEMA.COLUMNS
                            WHERE TABLE_SCHEMA = @schema AND TABLE_NAME = @table
                        ", new { schema, table }, transaction))
                        {
                            while (reader.Read())
                            {
                                var columnName = reader["COLUMN_NAME"].ToString();
                                var dataType = reader["DATA_TYPE"].ToString();

                                if (!string.IsNullOrEmpty(columnName) && !string.IsNullOrEmpty(dataType))
                                    columnNameType.Add(columnName, dataType);
                            }
                        }

                        // Insert or update record
                        var novZapis = false;
                        var insertOrUpdateParams = new DynamicParameters();
                        foreach (KeyValuePair<string, object?> data in sqlData)
                        {
                            //if (data.Value is string) - to ne deluje če je prazen string, shrani noter polno presledkov. če se bo rabilo, potem pohendlati
                            //    insertOrUpdateParams.Add("@" + data.Key, data.Value, DbType.StringFixedLength, size: ((string)data.Value).Length);
                            //else
                                insertOrUpdateParams.Add("@" + data.Key, data.Value);
                        }

                        using (var reader = conn.ExecuteReader(insertOrUpdate, insertOrUpdateParams, transaction))
                        {
                            if (reader.RecordsAffected > 1)
                                throw new Exception($"Invalid SQL - multiple rows affected ({reader.RecordsAffected})!");

                            while (reader.Read())
                            {
                                novZapis = (bool)reader[isNewRecordColumnName];
                                foreach (var outputColumnNameType in outputColumns)
                                {
                                    var outputColumn = outputColumnNameType.Key;
                                    var value = reader[outputColumn] == DBNull.Value ? null : reader[outputColumn];
                                    if (value != null && columnNameType.ContainsKey(outputColumn))
                                    {
                                        // Casting for output
                                        var columnType = columnNameType[outputColumn];
                                        if (columnType == "int")
                                            value = Convert.ToInt32(value);
                                        else if (columnType == "decimal")
                                            value = Convert.ToDecimal(value);
                                        // TODO: other types if required...
                                    }
                                    result.Add(outputColumn, value);
                                }
                            }
                        }
                        transaction.Commit();
                    }
                    catch
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            }

            return result;
        }

        public void DeleteItem<T>(T entity, User? user = null) where T : DbEntity
        {
            var table = entity.TableName;
            var schema = "dbo";

            // Get table and schema name
            if (table!.Contains('.'))
            {
                var tblSchema = table.Split('.');
                schema = tblSchema[0];
                table = tblSchema[1];
            }

            var now = DateTime.Now;
            var tableAndSchema = $"[{schema}].[{table}]";
            var keyColumns = entity.GetKeyColumns();
            var sqlData = entity.GetSqlData(false);
            var keyConditions = string.Join(" AND ", keyColumns.Select(x => x + " = @" + x));
            var sql = $"UPDATE TOP(2) {tableAndSchema} SET is_deleted = 1, is_active = 0{(entity.Audit ? ", modified = @now, modified_by = @userId" : "")} WHERE {keyConditions}"; // TOP2 zato ker če izbriše več kot 1 zapis damo rollback - če je ključ pravilen mora izbrisati samo en zapis!

            using (var conn = CreateConnection())
            {
                conn.Open();

                using (var transaction = conn.BeginTransaction())
                {
                    try
                    {
                        var sqlParams = new DynamicParameters();
                        foreach (KeyValuePair<string, object?> data in sqlData)
                        {
                            if (data.Value is string)
                                sqlParams.Add("@" + data.Key, data.Value, DbType.StringFixedLength, size: ((string)data.Value).Length);
                            else
                                sqlParams.Add("@" + data.Key, data.Value);
                        }

                        if (entity.Audit)
                        {
                            sqlParams.Add("now", now);
                            sqlParams.Add("userId", user?.Id);
                        }

                        var recordsAffected = conn.Execute(sql, sqlParams, transaction);
                        if (recordsAffected > 1)
                            throw new Exception("More than 1 record affected!");

                        transaction.Commit();
                    }
                    catch
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            }
        }

        public bool HasItem<T>(int farmId, int id) where T : DbEntity, new()
        {
            var t = new T();
            var table = t.TableName;
            var schema = "dbo";

            // Get table and schema name
            if (table!.Contains('.'))
            {
                var tblSchema = table.Split('.');
                schema = tblSchema[0];
                table = tblSchema[1];
            }

            var now = DateTime.Now;
            var tableAndSchema = $"[{schema}].[{table}]";

            return false;
        }
    }
}
