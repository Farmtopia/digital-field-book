﻿using ITC.FieldBook.Application.Infrastructure.Models;
using ITC.FieldBook.Application.Infrastructure;
using MediatR;
using Microsoft.AspNetCore.DataProtection.KeyManagement;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using ITC.FieldBook.Application.Models;
using Dapper;
using System.Transactions;
using ITC.FieldBook.Application.Infrastructure.Enums;
using Microsoft.AspNetCore.Authorization;
using ITC.FieldBook.Application.Infrastructure.Interfaces;

namespace ITC.FieldBook.Application.Features.Units
{
    [Authorize(Roles = EnumRole.Admin)]
    public class SaveUnitController : ApiControllerBase
    {
        [HttpPost("units")]
        public Task SaveUnit([FromBody] Models.Unit data)
        {
            return Mediator.Send(new SaveUnitCommand { Data = data });
        }
    }

    public class SaveUnitCommand : IRequest
    {
        public Models.Unit? Data { get; set; }
    }

    internal sealed class SaveUnitCommandHandler : IRequestHandler<SaveUnitCommand>
    {
        private readonly DbContext _ctx;
        private readonly ICurrentUserService _currentUserService;

        public SaveUnitCommandHandler(DbContext ctx, ICurrentUserService currentUserService)
        {
            _ctx = ctx;
            _currentUserService = currentUserService;
        }

        public Task Handle(SaveUnitCommand request, CancellationToken cancellationToken)
        {
            var data = request.Data;
            if (data != null)
            {
                var user = _currentUserService.GetUser()!;

                using (var scope = new TransactionScope())
                {
                    var keys = _ctx.Save(data, user: user);

                    // Save tags
                    SaveUnitGroups((int)keys["id"]!, data.Groups, user.Id!.Value);

                    scope.Complete();
                }
            }

            return Task.CompletedTask;
        }

        private void SaveUnitGroups(int unitId, IEnumerable<UnitGroup> groups, int userId)
        {
            using (var conn = _ctx.CreateConnection())
            {
                var now = DateTime.Now;
                using (var scope = new TransactionScope())
                {
                    // Delete removed groups
                    conn.Execute($@"
                        UPDATE FieldBook.unit_group 
                        SET is_deleted = 1, is_active = 0, modified = @now, modified_by = @userId
                        WHERE
                            {DbContext.GetIsDeletedIsActiveCondition()}                        
                            AND unit_id = @unitId
                        {((groups?.Any() ?? false) ? "AND group_id NOT IN @groupIds" : "")};", new { unitId, groupIds = groups?.Select(x => x.GroupId), userId, now });

                    // Save new groups
                    if (groups != null)
                    {
                        foreach (var ug in groups)
                        {
                            conn.Execute($@"
                                IF NOT EXISTS(SELECT TOP(1) 1 FROM FieldBook.unit_group WHERE {DbContext.GetIsDeletedIsActiveCondition()} AND unit_id = @unitId AND group_id = @groupId)
                                BEGIN
                                    INSERT INTO FieldBook.unit_group(unit_id, group_id, created, created_by, modified, modified_by) VALUES (@unitId, @groupId, @now, @userId, @now, @userId);
                                END", new { unitId, groupId = ug.GroupId, userId, now });
                        }
                    }

                    scope.Complete();
                }
            }
        }
    }
}

