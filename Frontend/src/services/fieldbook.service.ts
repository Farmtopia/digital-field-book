import {
  Crop,
  Farm,
  Fertilizer,
  Field,
  Machinery,
  Measure,
  Pesticide,
  StockFertilizer,
  StockPesticide,
  StockType,
  Task,
  TaskType,
  Unit,
  FieldBookInfo,
  TaskField,
  CalculateUsageResponse,
  CropType,
  UserCropSetting,
  DeviceTrip,
  FertilizerType,
} from 'src/models/fieldbook.models';
import { axiosApi } from '.';
import { DbData, DbItem } from 'src/models/core.models';
import routes from './routes';
import { RkgKmg } from 'src/models/rkg.models';
import { t } from 'src/boot/i18n';
import { computed } from 'vue';
import {
  EnumCropStatus,
  EnumFieldEnvironmentType,
  EnumUnitGroup,
} from 'src/enums/fieldbook.enums';

export const downloadFile = async function (url: string, inline: boolean) {
  axiosApi
    .get(url, {
      responseType: 'blob',
    })
    .then((response: any) => {
      const fname = response.headers['content-disposition']
        .split('filename=')[1]
        .split('.')[0];
      const ext = response.headers['content-disposition']
        .split('.')[1]
        .split(';')[0];
      const filename = `${fname.replace('"', '')}.${ext.replace('"', '')}`;

      const href = URL.createObjectURL(response.data);
      const link = document.createElement('a');

      try {
        link.href = href;

        // Inline or download
        if (inline) {
          link.target = '_blank';
        } else {
          link.setAttribute('download', filename);
        }

        document.body.appendChild(link);
        link.click();
      } finally {
        // clean up "a" element & remove ObjectURL
        document.body.removeChild(link);
        URL.revokeObjectURL(href);
      }
    });
};

export default {
  getFieldEnvironmentTypes() {
    return computed<{ id: string; name: string }[]>(() => [
      {
        id: EnumFieldEnvironmentType.OUTDOOR_CULTIVATION,
        name: t('fieldEnvironmentType.outdoorCultivation'),
      },
      {
        id: EnumFieldEnvironmentType.PROTECTED_ENVIRONMENT,
        name: t('fieldEnvironmentType.protectedEnvironment'),
      },
    ]);
  },
  getUserRoles() {
    return computed<{ id: string; name: string }[]>(() => [
      { id: '1', name: t('userRoles.admin') },
      { id: '2', name: t('userRoles.user') },
    ]);
  },
  getStockTypes() {
    return computed<StockType[]>(() => [
      { id: 1, name: t('stockType.initialStock'), type: 0 },
      { id: 2, name: t('stockType.purchase'), type: 1 },
      { id: 3, name: t('stockType.usage'), type: -1 },
      { id: 4, name: t('stockType.receipt'), type: 1 },
      { id: 5, name: t('stockType.sale'), type: -1 },
    ]);
  },
  getCropTypes() {
    return computed<CropType[]>(() => [
      { id: 1, name: t('crop.types.type1') },
      { id: 2, name: t('crop.types.type2') },
      { id: 3, name: t('crop.types.type3') },
    ]);
  },
  getFertilizerTypes() {
    return computed<FertilizerType[]>(() => [
      { id: 'MINERAL', name: t('fertilizerType.mineral') },
      { id: 'ORGANIC', name: t('fertilizerType.organic') },
    ]);
  },
  getTrackingType() {
    return computed<{ id: number; text: string }[]>(() => [
      {
        id: 1,
        text: t('trackingTypes.trackingDevice'),
      },
      {
        id: 2,
        text: t('trackingTypes.beacon'),
      },
    ]);
  },
  getSystemTaskTypes() {
    return computed<{ id: string; text: string }[]>(() => [
      {
        id: 'SOWING',
        text: t('systemTaskTypes.sowing'),
      },
      {
        id: 'HARVESTING',
        text: t('systemTaskTypes.harvesting'),
      },
      {
        id: 'FERTILIZING',
        text: t('systemTaskTypes.fertilizing'),
      },
      {
        id: 'PEST_CONTROL',
        text: t('systemTaskTypes.pestControl'),
      },
      {
        id: 'OTHER',
        text: t('systemTaskTypes.other'),
      },
    ]);
  },
  getTaskTypeCropStatuses() {
    return computed<{ id: string; text: string }[]>(() => [
      {
        id: EnumCropStatus.CROP_STAY,
        text: t('cropStatusAfterTask.cropStay'),
      },
      {
        id: EnumCropStatus.CROP_REMOVE,
        text: t('cropStatusAfterTask.cropRemove'),
      } /*,
      ...(includeUserDefined
        ? [
            {
              id: EnumCropStatus.USER_DEFINED,
              text: t('cropStatusAfterTask.userDefined'),
            },
          ]
        : []),*/,
    ]);
  },

  getUnitModuleLocations() {
    return computed<{ id: string; text: string }[]>(() => [
      {
        id: EnumUnitGroup.FERTILIZER_USAGE,
        text: t('unitModuleLocation.taskFertilizerUsage'),
      },
      {
        id: EnumUnitGroup.PESTICIDE_USAGE,
        text: t('unitModuleLocation.taskPesticideUsage'),
      },
      {
        id: EnumUnitGroup.FERTILIZER,
        text: t('unitModuleLocation.stockFertilizer'),
      },
      {
        id: EnumUnitGroup.PESTICIDE,
        text: t('unitModuleLocation.stockPesticide'),
      },
      {
        id: EnumUnitGroup.MACHINERY_PERFORMANCE,
        text: t('unitModuleLocation.machineryPerformance'),
      },
    ]);
  },

  async getFieldBookInfo() {
    return await axiosApi.get<FieldBookInfo>(routes.fieldbook.info);
  },
  async getFile(fileId: number, inline = false) {
    await downloadFile(`files/${fileId}`, inline);
  },
  async getFarms() {
    return await axiosApi.get<DbData<Farm>>(routes.farms.default);
  },
  async saveFarm(data: Farm) {
    return await axiosApi.post(routes.farms.default, data);
  },
  async saveFertilizer(data: Fertilizer) {
    return await axiosApi.post(routes.fertilizers.default, data);
  },
  async getFields(
    ids?: number[],
    images = false,
    latLong:
      | { lat: number | undefined; long: number | undefined }
      | undefined = undefined,
    date?: string
  ) {
    return await axiosApi.get<DbData<Field>>('fields', {
      params: {
        ids,
        images,
        latitude: latLong?.lat,
        longitude: latLong?.long,
        date,
      },
    });
  },
  async saveMachinery(data: Machinery) {
    return await axiosApi.post(routes.machinery.default, data);
  },
  async saveMeasure(data: Measure) {
    return await axiosApi.post(routes.measures.default, data);
  },
  async savePesticide(data: Pesticide) {
    return await axiosApi.post(routes.pesticides.default, data);
  },
  async calculatePesticideUsage(
    fieldPlotIds: number[],
    pesticideId: number | undefined,
    amount: number | undefined,
    amountUnitId: number | undefined,
    taskId: number | undefined,
    date: Date | string | undefined
  ) {
    return await axiosApi.get<CalculateUsageResponse>(
      routes.pesticides.calculateUsage,
      {
        params: {
          fieldPlotIds,
          pesticideId,
          amount,
          amountUnitId,
          taskId,
          date,
        },
      }
    );
  },

  async calculateFertilizerUsage(
    fieldPlotIds: number[],
    fertilizerId: number | undefined,
    amount: number | undefined,
    amountUnitId: number | undefined,
    taskId: number | undefined,
    date: Date | string | undefined
  ) {
    return await axiosApi.get<CalculateUsageResponse>(
      routes.fertilizers.calculateUsage,
      {
        params: {
          fieldPlotIds,
          fertilizerId,
          amount,
          amountUnitId,
          taskId,
          date,
        },
      }
    );
  },
  async saveCrop(data: Crop) {
    return await axiosApi.post(routes.crops.default, data);
  },
  async saveStockFertilizer(data: StockFertilizer) {
    return await axiosApi.post(routes.stock.fertilizers.default, data);
  },
  async saveStockPesticide(data: StockPesticide) {
    return await axiosApi.post(routes.stock.pesticides.default, data);
  },
  async saveTask(data: Task) {
    return await axiosApi.post(routes.tasks.default, data);
  },
  async saveTaskType(data: TaskType) {
    return await axiosApi.post(routes.tasks.types.default, data);
  },
  async saveUnit(data: Unit) {
    return await axiosApi.post(routes.units.default, data);
  },
  async saveUserCropSetting(data: UserCropSetting) {
    return await axiosApi.post(routes.users.settings.crop, data);
  },
  async getRkgFarm(kmgMid: number) {
    return await axiosApi.get<RkgKmg>(routes.rkg.farm.default, {
      params: { kmgMid },
    });
  },
  async importRkgFarm(kmgMid: number) {
    return await axiosApi.post<RkgKmg>(routes.rkg.farm.import, { kmgMid });
  },
  async getDeviceTrips(
    date: string,
    minTripTime?: number,
    calculateTrips?: boolean
  ) {
    return await axiosApi.get<DeviceTrip[]>(routes.tracking.trips.default, {
      params: { date, minTripTime, calculateTrips },
    });
  },
};
