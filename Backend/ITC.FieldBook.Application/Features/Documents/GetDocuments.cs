﻿using ITC.FieldBook.Application.Infrastructure.Interfaces;
using ITC.FieldBook.Application.Infrastructure;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using Dapper;
using ITC.FieldBook.Application.Models;
using ITC.FieldBook.Application.Infrastructure.Models;

namespace ITC.FieldBook.Application.Features.Documents
{
    public class GetDocumentsController : ApiControllerBase
    {
        [HttpGet("documents")]
        public Task<DbData<Document>> GetDocuments()
        {
            return Mediator.Send(new GetDocumentsQuery { });
        }
    }

    public class GetDocumentsQuery : IRequest<DbData<Document>>
    {
    }

    internal sealed class GetDocumentsQueryHandler : IRequestHandler<GetDocumentsQuery, DbData<Document>>
    {
        private readonly DbContext _ctx;
        private readonly IConfiguration _configuration;
        private readonly ICurrentUserService _currentUserService;

        public GetDocumentsQueryHandler(DbContext ctx, IConfiguration configuration, ICurrentUserService currentUserService)
        {
            _ctx = ctx;
            _configuration = configuration;
            _currentUserService = currentUserService;
        }

        public Task<DbData<Document>> Handle(GetDocumentsQuery request, CancellationToken cancellationToken)
        {
            var documentsRepository = new DocumentsRepository(_ctx);
            var data = documentsRepository.GetDocuments(_currentUserService.FarmId!.Value);

            return Task.FromResult(data);
        }
    }
}
