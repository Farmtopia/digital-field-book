﻿using ITC.FieldBook.Application.Infrastructure.Interfaces;
using ITC.FieldBook.Application.Infrastructure;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata;
using Dapper;
using ITC.FieldBook.Application.Models;
using ITC.FieldBook.Application.Infrastructure.Models;
using ITC.FieldBook.Application.Enums;
using Microsoft.Extensions.Azure;
using ITC.FieldBook.Application.Features.Units;

namespace ITC.FieldBook.Application.Features.Pesticides
{
    public class GetPesticideUnitsController : ApiControllerBase
    {
        [HttpGet("pesticides/{pesticideId}/units")]
        public Task<DbData<Models.Unit>> GetPesticideUnits(int pesticideId, bool forTaskInput)
        {
            return Mediator.Send(new GetPesticideUnitsQuery { PesticideId = pesticideId, ForTaskInput = forTaskInput });
        }
    }

    public class GetPesticideUnitsQuery : BaseDataQuery<DbData<Models.Unit>>
    {
        public int? PesticideId { get; set; }

        public bool ForTaskInput { get; set; }
    }

    internal sealed class GetPesticideUnitsQueryHandler : IRequestHandler<GetPesticideUnitsQuery, DbData<Models.Unit>>
    {
        private readonly DbContext _ctx;
        private readonly IConfiguration _configuration;
        private readonly ICurrentUserService _currentUserService;

        public GetPesticideUnitsQueryHandler(DbContext ctx, IConfiguration configuration, ICurrentUserService currentUserService)
        {
            _ctx = ctx;
            _configuration = configuration;
            _currentUserService = currentUserService;
        }

        public Task<DbData<Models.Unit>> Handle(GetPesticideUnitsQuery request, CancellationToken cancellationToken)
        {
            var unitsRepo = new UnitsRepository(_ctx);
            var pestUnit = unitsRepo.GetUnitForPesticide(_currentUserService.FarmId!.Value, request.PesticideId!.Value).Item;
            var units = unitsRepo.GetUnits(request.ForTaskInput ? EnumUnitGroup.PESTICIDE_USAGE : EnumUnitGroup.PESTICIDE);

            if (pestUnit == null)
                return Task.FromResult(units);

            var result = new List<Models.Unit>();

            // When inputing task, also include unit/ha if exists.
            if (request.ForTaskInput)
            {
                // Check if unit per area is defined
                var unitPerAreaName = pestUnit.GetUnitPerAreaName();
                var unitPerArea = units.Data.FirstOrDefault(x => x.Name!.Equals(unitPerAreaName, StringComparison.OrdinalIgnoreCase));

                if (unitPerArea != null)
                    result.Add(unitPerArea);
            }

            // Check if base unit is defined
            var baseUnit = units.Data.FirstOrDefault(x => x.Id == pestUnit.Id || x.Name == pestUnit.Name);
            if (baseUnit != null)
                result.Add(baseUnit);

            return Task.FromResult(new DbData<Models.Unit> { Data = result });
        }
    }
}
