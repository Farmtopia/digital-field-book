import { RouteRecordRaw } from 'vue-router';
import { defineAsyncComponent } from 'vue';

import BlankLayout from 'src/layouts/BlankLayout.vue';
import MainLayout from 'src/layouts/MainLayout.vue';
import RouterLayout from 'src/layouts/RouterLayout.vue';

const AdminLayout = defineAsyncComponent(
  () => import('../layouts/AdminLayout.vue')
);

const routes: RouteRecordRaw[] = [
  {
    path: '/',
    component: RouterLayout,
    children: [
      {
        path: '',
        name: 'home',
        component: () => import('pages/IndexPage.vue'),
        meta: { layout: MainLayout, titleTranslation: 'modules.fields.title' },
      },
      {
        path: 'field/:id',
        name: 'field',
        component: () => import('pages/fieldbook/field/FieldPage.vue'),
        meta: { titleTranslation: 'modules.field.title', layout: MainLayout },
      },
    ],
  },
  {
    path: '/farm',
    name: 'farm',
    component: () => import('pages/fieldbook/farm/FarmPage.vue'),
    meta: {
      titleTranslation: 'modules.farm.title',
      icon: 'warehouse',
      layout: MainLayout,
    },
  },
  {
    path: '/tasks',
    name: 'tasks',
    component: () => import('pages/fieldbook/tasks/TasksPage.vue'),
    meta: {
      titleTranslation: 'modules.tasks.title',
      icon: 'task',
      layout: MainLayout,
    },
  },
  {
    path: '/crop-rotation',
    component: RouterLayout,
    meta: { titleTranslation: 'modules.cropRotation.title' },
    children: [
      {
        path: '',
        name: 'cropRotation',
        component: () =>
          import('pages/fieldbook/crop_rotation/CropRotationPage.vue'),
        meta: {
          //titleTranslation: 'modules.cropRotation.title',
          layout: MainLayout,
        },
      },
      {
        path: 'sum',
        name: 'cropRotationSum',
        component: () =>
          import('pages/fieldbook/crop_rotation/CropRotationSumPage.vue'),
        meta: {
          titleTranslation: 'modules.cropRotationSum.title',
          layout: MainLayout,
        },
      },
    ],
  },
  {
    path: '/fertilizers',
    component: RouterLayout,
    meta: { titleTranslation: 'modules.fertilizers.title' },
    children: [
      {
        path: '',
        name: 'fertilizers',
        component: () =>
          import('pages/fieldbook/fertilizers/FertilizersPage.vue'),
        meta: {
          titleTranslation: 'modules.fertilizers.list',
          layout: MainLayout,
        },
      },
      {
        path: 'stock',
        name: 'fertilizerStock',
        component: () =>
          import('pages/fieldbook/fertilizers/FertilizerStockPage.vue'),
        meta: {
          titleTranslation: 'modules.fertilizers.stockControlTitle',
          layout: MainLayout,
        },
      },
      {
        path: 'stock-status',
        name: 'fertilizerStockStatus',
        component: () =>
          import('pages/fieldbook/fertilizers/FertilizerStockStatusPage.vue'),
        meta: {
          titleTranslation: 'modules.fertilizerStockStatus.title',
          layout: MainLayout,
        },
      },
    ],
  },
  {
    path: '/pesticides',
    component: RouterLayout,
    meta: { titleTranslation: 'modules.pesticides.title' },
    children: [
      {
        path: '',
        name: 'pesticides',
        component: () =>
          import('pages/fieldbook/pesticides/PesticidesPage.vue'),
        meta: {
          titleTranslation: 'modules.pesticides.list',
          layout: MainLayout,
        },
      },
      {
        path: 'stock',
        name: 'pesticideStock',
        component: () =>
          import('pages/fieldbook/pesticides/PesticideStockPage.vue'),
        meta: {
          titleTranslation: 'modules.pesticides.stockControlTitle',
          layout: MainLayout,
        },
      },
      {
        path: 'stock-status',
        name: 'pesticideStockStatus',
        component: () =>
          import('pages/fieldbook/pesticides/PesticideStockStatusPage.vue'),
        meta: {
          titleTranslation: 'modules.pesticideStockStatus.title',
          layout: MainLayout,
        },
      },
    ],
  },
  {
    path: '/files',
    name: 'files',
    component: () => import('pages/fieldbook/files/FilesPage.vue'),
    meta: {
      titleTranslation: 'modules.documents.title',
      icon: 'folder',
      layout: MainLayout,
    },
  },
  {
    path: '/machinery',
    name: 'machinery',
    component: () => import('pages/fieldbook/machinery/MachineryPage.vue'),
    meta: {
      titleTranslation: 'modules.machinery.title',
      icon: 'agriculture',
      layout: MainLayout,
    },
  },
  {
    path: '/reports',
    name: 'reports',
    component: () => import('pages/fieldbook/reports/ReportsPage.vue'),
    meta: {
      titleTranslation: 'modules.reports.title',
      icon: 'analytics',
      layout: MainLayout,
    },
  },
  /*{
    path: '/reports',
    component: RouterLayout,
    meta: { title: 'Poročila' },
    children: [
      {
        path: '',
        name: 'reports',
        component: () => import('pages/fieldbook/reports/ReportsPage.vue'),
        meta: { layout: MainLayout, icon: 'analytics' },
      },
      {
        path: '/reports/:slug',
        name: 'report',
        component: () => import('pages/fieldbook/reports/ReportPage.vue'),
        meta: {
          layout: MainLayout,
          title: 'Ogled poročila',
          icon: 'analytics',
        },
      },
    ],
  },*/
  {
    path: '/settings',
    name: 'settings',
    component: () => import('pages/SettingsPage.vue'),
    meta: {
      titleTranslation: 'modules.settings.title',
      icon: 'settings',
      layout: MainLayout,
    },
  },
  {
    path: '/login',
    name: 'login',
    component: () => import('pages/LoginPage.vue'),
    meta: {
      titleTranslation: 'modules.login.title',
      layout: BlankLayout,
      requiresAuth: false,
    },
  },
  {
    path: '/admin',
    meta: {
      titleTranslation: 'modules.administration.title',
      layout: AdminLayout,
    },

    children: [
      {
        path: '',
        name: 'admin',
        component: () => import('pages/admin/IndexPage.vue'),
        meta: {
          titleTranslation: 'modules.admin.farms.title',
          icon: 'home',
          layout: AdminLayout,
        },
      },
      {
        path: 'fertilizers',
        name: 'fertilizersAdmin',
        component: () =>
          import('pages/admin/fertilizers/FertilizersAdminPage.vue'),
        meta: {
          titleTranslation: 'modules.admin.fertilizers.title',
          icon: 'mdi-sack',
          layout: AdminLayout,
        },
      },
      {
        path: 'pesticides',
        name: 'pesticidesAdmin',
        component: () =>
          import('pages/admin/pesticides/PesticidesAdminPage.vue'),
        meta: {
          titleTranslation: 'modules.admin.pesticides.title',
          icon: 'science',
          layout: AdminLayout,
        },
      },
      {
        path: 'crops',
        name: 'crops',
        component: () => import('pages/admin/crops/CropsPage.vue'),
        meta: {
          titleTranslation: 'modules.admin.crops.title',
          icon: 'mdi-sprout',
          layout: AdminLayout,
        },
      },
      {
        path: 'units',
        name: 'units',
        component: () => import('pages/admin/units/UnitsPage.vue'),
        meta: {
          titleTranslation: 'modules.admin.units.title',
          icon: 'straighten',
          layout: AdminLayout,
        },
      },
      {
        path: 'measure-types',
        name: 'measure-types',
        component: () =>
          import('pages/admin/measure-types/MeasureTypesPage.vue'),
        meta: {
          titleTranslation: 'modules.admin.measureTypes.title',
          icon: 'assignment_late',
          layout: AdminLayout,
        },
      },
      {
        path: 'document-types',
        name: 'document-types',
        component: () =>
          import('pages/admin/document-types/DocumentTypesPage.vue'),
        meta: {
          titleTranslation: 'modules.admin.documentTypes.title',
          layout: AdminLayout,
        },
      },
      {
        path: 'field-types',
        name: 'field-types',
        component: () => import('pages/admin/field-types/FieldTypesPage.vue'),
        meta: {
          titleTranslation: 'modules.admin.fieldTypes.title',
          layout: AdminLayout,
        },
      },
      {
        path: 'task-types',
        name: 'task-types',
        component: () => import('pages/admin/task-types/TaskTypesPage.vue'),
        meta: {
          titleTranslation: 'modules.admin.taskTypes.title',
          icon: 'task',
          layout: AdminLayout,
        },
      },
      {
        path: 'users',
        name: 'users',
        component: () => import('pages/admin/users/UsersPage.vue'),
        meta: {
          titleTranslation: 'modules.admin.users.title',
          icon: 'person',
          layout: AdminLayout,
        },
      },
      {
        path: '/admin/settings',
        name: 'admin-settings',
        component: () => import('pages/SettingsPage.vue'),
        meta: {
          titleTranslation: 'modules.admin.settings.title',
          icon: 'settings',
          layout: AdminLayout,
        },
      },
    ],
  },
  {
    path: '/404',
    name: '404',
    component: () => import('pages/ErrorNotFound.vue'),
    meta: { title: '404', layout: BlankLayout, requiresAuth: false },
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    redirect: '/404',
  },
];

export default routes;
