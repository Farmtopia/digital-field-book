﻿using System.Text.Json.Serialization;

namespace Farmtopia.DigitalFieldBook.API.Models.JSON_LD
{
    public class Centroid : JsonLdBase
    {
        [JsonPropertyName("geoJson")]
        public CentroidGeoJson? GeoJson { get; set; }
    }
}
