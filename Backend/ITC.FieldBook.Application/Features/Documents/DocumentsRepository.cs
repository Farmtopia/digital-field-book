﻿using Dapper;
using ITC.FieldBook.Application.Features.Documents.Types;
using ITC.FieldBook.Application.Infrastructure;
using ITC.FieldBook.Application.Infrastructure.Models;
using ITC.FieldBook.Application.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITC.FieldBook.Application.Features.Documents
{
    public class DocumentsRepository
    {
        private DbContext _ctx;


        public DocumentsRepository(DbContext ctx)
        {
            _ctx = ctx;
        }

        public DbData<Document> GetDocuments(int farmId, int? taskId = null)
        {
            var data = _ctx.Get<Document>($@"
                SELECT 
                    d.id,
                    d.farm_id,
                    d.file_id,
                    d.description,
                    d.document_type_id,
                    d.task_id,
                    d.created,
                    d.created_by,
                    d.modified,
                    d.modified_by,
                    dt.name AS document_type_name,
                    dt.name_en AS document_type_name_en,
                    f.displayname AS file_displayname
                FROM FieldBook.document d
                    LEFT JOIN FieldBook.[file] f ON f.id = d.file_id
                    LEFT JOIN FieldBook.document_type dt ON dt.id = d.document_type_id
                WHERE
                    {DbContext.GetIsDeletedIsActiveCondition("d")} AND
                    d.farm_id = @farmId AND
                    (@taskId IS NULL OR d.task_id = @taskId)",
            new { farmId, taskId });

            return data;
        }

        public DbItem<Document> GetDocument(int id, int? farmId = null)
        {
            var data = _ctx.GetItem<Document>($@"
                SELECT 
                    d.id,
                    d.farm_id,
                    d.file_id,
                    d.description,
                    d.document_type_id,
                    d.task_id,
                    d.created,
                    d.created_by,
                    d.modified,
                    d.modified_by,
                    dt.name AS document_type_name,
                    dt.name_en AS document_type_name_en,
                    f.displayname AS file_displayname
                FROM FieldBook.document d
                    LEFT JOIN FieldBook.[file] f ON f.id = d.file_id
                    LEFT JOIN FieldBook.document_type dt ON dt.id = d.document_type_id
                WHERE
                    {DbContext.GetIsDeletedIsActiveCondition("d")} AND
                    {(farmId == null ? "" : "d.farm_id = @farmId AND")}
                    d.id = @id",
            new { id, farmId });

            return data;
        }

        public Dictionary<string, object?> SaveDocument(Document data, User user)
        {
            return _ctx.Save(data, user: user);
        }

        public void DeleteTaskDocuments(int farmId, int taskId, int userId, List<int>? excludeIds = null)
        {
            using (var conn = _ctx.CreateConnection())
            {
                var now = DateTime.Now;

                conn.Execute($@"
                    UPDATE td
                    SET is_deleted = 1, is_active = 0, modified = @now, modified_by = @userId
                    FROM FieldBook.document td
                        JOIN FieldBook.task t ON t.id = td.task_id
                    WHERE
                        {DbContext.GetIsDeletedIsActiveCondition("td")} AND
                        t.farm_id = @farmId
                        AND td.task_id = @taskId
                        {((excludeIds?.Any() ?? false) ? "AND td.file_id NOT IN @excludeIds" : "")}
                        ", new { farmId, taskId, userId, now, excludeIds });
            }
        }

        #region Document types

        public DbData<DocumentType> GetDocumentTypes()
        {
            var data = _ctx.Get<DocumentType>($@"
                SELECT 
                    dt.id,
                    dt.name,
                    dt.name_en
                FROM FieldBook.document_type dt
                WHERE 
                    {DbContext.GetIsDeletedIsActiveCondition("dt")}");

            return data;
        }

        public DbItem<DocumentType> GetDocumentType(int id)
        {
            var data = _ctx.GetItem<DocumentType>($@"
                SELECT TOP(1)
                    dt.id,
                    dt.name,
                    dt.name_en
                FROM FieldBook.document_type dt
                WHERE 
                    {DbContext.GetIsDeletedIsActiveCondition("dt")}
                    AND dt.id = @id", new { id });

            return data;
        }

        #endregion
    }
}
