﻿using ITC.FieldBook.Application.Infrastructure.Models;
using ITC.FieldBook.Application.Infrastructure;
using MediatR;
using Microsoft.AspNetCore.DataProtection.KeyManagement;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using ITC.FieldBook.Application.Models;
using ITC.FieldBook.Application.Infrastructure.Services;
using ITC.FieldBook.Application.Infrastructure.Interfaces;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Authorization;
using ITC.FieldBook.Application.Infrastructure.Enums;

namespace ITC.FieldBook.Application.Features.Fertilizers
{
    [Authorize(Roles = EnumRole.Admin)]
    public class DeleteFertilizerController : ApiControllerBase
    {
        [HttpDelete("fertilizers/{fertilizerId}")]
        public Task DeleteFertilizer([FromRoute] int? fertilizerId)
        {
            return Mediator.Send(new DeleteFertilizerCommand { FertilizerId = fertilizerId });
        }
    }

    public class DeleteFertilizerCommand : IRequest
    {
        [Required]
        public int? FertilizerId { get; set; }
    }

    internal sealed class DeleteFertilizerCommandHandler : IRequestHandler<DeleteFertilizerCommand>
    {
        private readonly DbContext _ctx;
        private readonly ICurrentUserService _currentUserService;

        public DeleteFertilizerCommandHandler(DbContext ctx, ICurrentUserService currentUserService)
        {
            _ctx = ctx;
            _currentUserService = currentUserService;
        }

        public Task Handle(DeleteFertilizerCommand request, CancellationToken cancellationToken)
        {
            var fertilizersRepository = new FertilizersRepository(_ctx);
            var fertilizer = fertilizersRepository.GetFertilizer(request.FertilizerId!.Value).Item;
            if (fertilizer != null)
            {
                var user = _currentUserService.GetUser()!;
                _ctx.DeleteItem(fertilizer, user);
            }

            return Task.CompletedTask;
        }
    }
}

