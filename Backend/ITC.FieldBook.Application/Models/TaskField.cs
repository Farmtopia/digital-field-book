﻿using ITC.FieldBook.Application.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITC.FieldBook.Application.Models
{
    public class TaskField : DbEntityWithAudit
    {
        public TaskField() : base("FieldBook.task_field") { }

        private int? _id;
        [DbColumn("id", IsKey = true, Identity = true)]
        public int? Id { get => _id; set { _id = value; RaisePropertyChanged(nameof(Id)); } }

        private int? _task_id;
        [DbColumn("task_id")]
        public int? TaskId { get => _task_id; set { _task_id = value; RaisePropertyChanged(nameof(TaskId)); } }

        private int? _fieldId;
        [DbColumn("field_id")]
        public int? FieldId { get => _fieldId; set { _fieldId = value; RaisePropertyChanged(nameof(FieldId)); } }

        private int? _fieldPlotId;
        [DbColumn("field_plot_id")]
        public int? FieldPlotId { get => _fieldPlotId; set { _fieldPlotId = value; RaisePropertyChanged(nameof(FieldPlotId)); } }

        private int? _duration;
        [DbColumn("duration")]
        public int? Duration { get => _duration; set { _duration = value; RaisePropertyChanged(nameof(Duration)); } }

        public string? FieldIdentifier { get; set; }

        public string? FieldName { get; set; }

        public string? CropIdentifier { get; set; }

        public string? CropName { get; set; }

        public string? CropNameEn { get; set; }

        public decimal? Area { get; set; }
    }
}
