﻿using ITC.FieldBook.Application.Infrastructure.Interfaces;
using ITC.FieldBook.Application.Infrastructure;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using ITC.FieldBook.Application.Infrastructure.Models;
using ITC.FieldBook.Application.Models;
using Microsoft.AspNetCore.Authorization;
using ITC.FieldBook.Application.Models.DTO;
using AutoMapper;
using ITC.FieldBook.Application.Features.Machinery;
using ITC.FieldBook.Application.Features.Fields;

namespace ITC.FieldBook.Application.Features.Tracking
{
    public class GetTripsController : ApiControllerBase
    {
        [HttpGet("tracking/trips")]
        public Task<List<DeviceTripDto>> GetTrips([FromQuery] GetTripsQuery query)
        {
            return Mediator.Send(query);
        }
    }

    public class GetTripsQuery : IRequest<List<DeviceTripDto>>
    {
        public DateTime Date { get; set; }

        public int? MinTripTime { get; set; }

        public int? MinPointCount { get; set; }

        public bool CalculateTrips { get; set; } = false;
    }

    internal sealed class GetTripsQueryHandler : IRequestHandler<GetTripsQuery, List<DeviceTripDto>>
    {
        private readonly DbContext _ctx;
        private readonly IConfiguration _configuration;
        private readonly ICurrentUserService _currentUserService;
        private readonly ITrackingService _trackingService;
        private readonly IMapper _mapper;

        public GetTripsQueryHandler(DbContext ctx, IConfiguration configuration, ICurrentUserService currentUserService, ITrackingService trackingService, IMapper mapper)
        {
            _ctx = ctx;
            _configuration = configuration;
            _currentUserService = currentUserService;
            _trackingService = trackingService;
            _mapper = mapper;
        }

        public async Task<List<DeviceTripDto>> Handle(GetTripsQuery request, CancellationToken cancellationToken)
        {
            var fieldsRepository = new FieldsRepository(_ctx);
            var machineryRepository = new MachineryRepository(_ctx);
            var myMachines = machineryRepository.GetMachinery(_currentUserService.FarmId!.Value).Data.Where(x => !string.IsNullOrWhiteSpace(x.TrackingId)).ToList();
            var myFields = fieldsRepository.GetFields(_currentUserService.FarmId!.Value).Data;
            
            var mainMachines = myMachines.Where(x => x.TrackingType == 1 && int.TryParse(x.TrackingId, out _)).ToList();
            if (mainMachines.Count == 0)
                return new List<DeviceTripDto>();

            var trips = await _trackingService.Instance.GetDeviceTrips(request.Date, mainMachines.Select(x => int.Parse(x.TrackingId!)).ToList(), request.CalculateTrips);
            if (request.MinTripTime != null)
                trips = trips.Where(x => x.TotalMinutes >= request.MinTripTime).ToList();
            if (request.MinPointCount != null)
                trips = trips.Where(x => x.PointCount >= request.MinPointCount).ToList();

            var tripsDto = new List<DeviceTripDto>();

            foreach(var trip in trips)
            {
                if (trip.GerkPid == null)
                    continue;

                var tripDto = _mapper.Map<DeviceTripDto>(trip);
                var machine = mainMachines.FirstOrDefault(x => int.Parse(x.TrackingId!) == trip.DeviceId);
                if (machine == null)
                    continue;

                tripDto.MachineId = machine.Id;
                tripDto.MachineName = machine.Name;

                if (trip.BeaconId != null)
                {
                    var attachment = myMachines.FirstOrDefault(x => x.TrackingType == 2 && x.TrackingId!.Equals(trip.BeaconId, StringComparison.InvariantCulture));
                    if (attachment == null)
                        continue;

                    tripDto.AttachmentId = attachment.Id;
                    tripDto.AttachmentName = attachment.Name;
                }

                var field = myFields.FirstOrDefault(x=>x.Identifier?.Equals(trip.GerkPid.ToString(), StringComparison.OrdinalIgnoreCase) ?? false);
                if (field == null)
                    continue;

                tripDto.FieldId = field.Id;
                tripDto.FieldIdentifier = field.Identifier;
                tripDto.FieldName = field.Name;


                tripsDto.Add(tripDto);
            }

            return tripsDto;
        }
    }
}
