import { EnumSystemTaskType } from 'src/enums/fieldbook.enums';
import { CheckboxItem, DbEntity, DbEntityWithAudit } from './core.models';
import { ComputedRef } from 'vue';

export interface FieldBookInfo {
  farms?: Farm[];
  defaultFarm?: Farm;
}

export interface Farm extends DbEntity {
  id?: number;
  name?: string;
  owner?: string;
  address?: string;
  postCode?: string;
  city?: string;
  identifier?: string;
  ecoFarm?: boolean;
  thumbFileId?: number;
  geoJSON?: string;
  latitude?: number;
  longitude?: number;
}

export interface Season extends DbEntity {
  farmId?: number;
  year?: number;
  active?: boolean;
}

export interface Field extends DbEntity {
  id?: number;
  identifier?: string;
  name?: string;
  description?: string;
  area?: number;
  typeId?: number;
  latitude?: number;
  longitude?: number;
  imageFileId?: number;

  displayname?: string;
  typeIdentifier?: string;
  typeName?: string;
  typeNameEn?: string;
  typeDisplayname?: string;

  activePlots?: FieldPlot[];

  lastTask?: Task | undefined;
  lastTasks?: Task[] | undefined;
  distanceFromTarget?: number;
  geoJSON?: string;
}

export interface FieldMeasure extends DbEntityWithAudit {
  id?: number;
  fieldId?: number;
  fieldPlotId?: number;
  measureId?: number;
  year?: number;

  measureName?: string;
  measureNameEn?: string;
  measureDescription?: string;
  measureDescriptionEn?: string;
  measureCategory?: string;
}

export interface FieldType extends DbEntity {
  id?: number;
  identifier?: string;
  name?: string;
  nameEn?: string;
  environmentType?: string;
  includeInCropRotation: boolean;

  displayName?: string;
}

export class FieldPlot extends DbEntity {
  id?: number;
  year?: number;
  fieldId?: number;
  cropType?: number;
  cropId?: number;
  cropVariety?: string;
  area?: number;
  sowingDate?: string | Date;
  sowingTaskId?: number;
  harvestDate?: string | Date;
  harvestTaskId?: number;

  cropIdentifier?: string;
  cropName?: string;
  cropNameEn?: string;
}

export interface TaskFieldPlot {
  taskField: TaskField;
  fieldPlots: FieldPlot[];
  fieldPlot?: FieldPlot;
  //cropId: number | undefined;
}

export class Task extends DbEntity {
  id?: number;
  farmId?: string;
  year?: number;
  dateFrom?: string;
  dateTo?: string;
  taskTypeId?: number;
  cropStatusAfter?: string | null;
  executor?: string;
  note?: string;
  equipmentId?: number;
  attachedEquipmentId?: number;
  treatmentSuccessful?: boolean | null;
  harvestAmount?: number;
  trackingTripId?: number;
  data?: string | null;

  taskTypeName?: string;
  taskTypeNameEn?: string;
  machineName?: string;
  attachmentName?: string;

  taskFields?: TaskField[];
  fertilizers?: TaskFertilizer[];
  pesticides?: TaskPesticide[];
  documents?: Document[];

  fields?: Field[];

  isLoading?: boolean = false;
}

export interface TaskByField extends DbEntity {
  taskId?: number;
  year?: number;
  dateFrom?: string;
  dateTo?: string;
  fieldId?: number;
  fieldIdentifier?: string;
  fieldName?: string;
  cropIdentifier?: string;
  cropName?: string;
  cropNameEn?: string;
  taskTypeName?: string;
  taskTypeNameEn?: string;
  machineName?: string;
  attachmentName?: string;
  executor?: string;
  note?: string;
}

export interface TaskField extends DbEntity {
  id?: number;
  taskId?: number;
  fieldId?: number;
  //fieldArea?: number;
  fieldPlotId?: number;
  duration?: number | null | undefined;
  //cropId?: number;

  fieldIdentifier?: string;
  fieldName?: string;
  cropIdentifier?: string;
  cropName?: string;
  cropNameEn?: string;
  area?: number;
}

export interface TaskData {
  fertilizers?: TaskFertilizer[];
  pesticides?: TaskPesticide[];
}

export interface TaskFertilizer {
  id?: number;
  fertilizerId?: number;
  amount?: number;
  amountUnitId?: number;
  amountUnit?: string;
  amountUsed?: number;
  amountUsedUnitId?: number;
  amountUsedUnit?: string;
}

export interface TaskPesticide {
  id?: number;
  pesticideId?: number;
  amount?: number;
  amountUnitId?: number;
  amountUnit?: string;
  amountUsed?: number;
  amountUsedUnitId?: number;
  amountUsedUnit?: string;
}

/*export interface TaskDocument extends DbEntityWithAudit {
  taskId?: number;
  fileId?: number;
  description?: string;
}*/

export interface TaskType extends DbEntity {
  id?: number;
  identifier?: string;
  name?: string;
  nameEn?: string;
  systemType?: EnumSystemTaskType;
  cropStatus?: string;
  cropStatusEditable?: string;
  requiresTime?: boolean;

  displayName?: string;
}

export interface CropType {
  id: number;
  name: string | ComputedRef;
}

export interface Crop extends DbEntity {
  id?: number;
  name?: string;
  nameEn?: string;
  identifier?: string;

  displayName?: string;
}

export interface Machinery extends DbEntity {
  id?: number;
  farmId?: number;
  name?: string;
  description?: string;
  performance?: number;
  performanceUnitId?: number;
  performanceUnit?: string;
  classificationId?: number;
  trackingId?: string;
  trackingType?: number;
  performanceDisplayValue?: string;
  farmManagerId?: number;
  farmManagerName?: string;
}

export interface MachineryClassification extends DbEntity {
  id?: number;
  code?: string;
  parent_code?: string;
  name?: string;
}

export interface FertilizerType {
  id: string;
  name: string;
}

export interface Fertilizer extends DbEntityWithAudit {
  id?: number;
  name?: string;
  nameEn?: string;
  brandName?: string;
  type?: string;
  ratioN?: number;
  ratioP?: number;
  ratioK?: number;
  isGlobal?: boolean;
  farmId?: number;

  farmName?: string;
  stock?: number;
  stockUnitId?: number;
  stockUnit?: string;

  displayName?: string;
}

export interface Pesticide extends DbEntityWithAudit {
  id?: number;
  name?: string;
  nameEn?: string;
  brandName?: string;
  isGlobal?: boolean;
  farmId?: number;

  farmName?: string;
  stock?: number;
  stockUnitId?: number;
  stockUnit?: string;
}

export interface Unit extends DbEntity {
  id?: number;
  name?: string;
  description?: string;
  descriptionEn?: string;
  groups?: UnitGroup[];
  groupsDisplayValue?: string;

  displayName?: string;
}

export interface UnitGroup extends DbEntity {
  unitId?: number;
  groupId?: string;
}

export interface MeasureGroup {
  category: string | undefined;
  items: CheckboxItem<{
    measure: Measure;
    fieldMeasure?: FieldMeasure | undefined;
  }>[];
}

export interface Measure extends DbEntity {
  id?: number;
  name?: string;
  nameEn?: string;
  description?: string;
  descriptionEn?: string;
  category?: string;
  yearFrom?: string;
  yearTo?: string;
}

export interface StockType extends DbEntity {
  id?: number;
  name?: string;
  type?: number;
}

export interface StockFertilizer extends DbEntity {
  id?: number;
  farmId?: number;
  date?: string;
  fertilizerId?: number;
  stockTypeId?: number;
  stockType?: number;
  stockName?: string;
  value?: number;
  valueUnitId?: number;
  valueUnit?: string;
  taskId?: string;
  note?: string;

  fertilizerName?: string;
  fertilizerNameEn?: string;
}

export interface FertilizersStockReportItem extends DbEntity {
  name?: string;
  nameEn?: string;
  brandName?: string;
  stock?: number;
  stockUnit?: string;
}

export interface StockPesticide extends DbEntity {
  id?: number;
  farmId?: number;
  date?: string;
  pesticideId?: number;
  stockTypeId?: number;
  stockType?: number;
  stockName?: string;
  value?: number;
  valueUnitId?: number;
  valueUnit?: string;
  taskId?: string;
  note?: string;

  pesticideName?: string;
  pesticideNameEn?: string;
}

export interface PesticidesStockReportItem extends DbEntity {
  name?: string;
  nameEn?: string;
  brandName?: string;
  stock?: number;
  stockUnit?: string;
}

export interface Document extends DbEntityWithAudit {
  id?: number;
  farmId?: number;
  fileId?: number;
  documentTypeId?: number;
  description?: string;
  taskId?: number;

  fileDisplayname?: string;
  documentTypeName?: string;
  documentTypeNameEn?: string;
}

export interface DocumentType extends DbEntity {
  id?: number;
  name?: string;
  nameEn?: string;

  displayName?: string;
}

export interface Report {
  slug?: string;
  name: string;
  component: any;
}

export interface CalculateUsageResponse {
  area?: number;
  amount?: number;
  amountUnitId?: number;
  amountUnit?: string;
  usage?: number;
  usageUnitId?: number;
  usageUnit?: string;
  stock?: number;
  stockUnitId?: number;
  stockUnit?: string;
  remainingStock?: number;
  hasStock?: boolean;
}

export interface TaskDurationForFieldPlot {
  fieldPlotId: number;
  durationMinutes: number;
}

export interface CropRotation {
  fields: CropRotationField[];
  years: number[];
  hasCropRotation: boolean;
}

export interface CropRotationGroup {
  cropType: number;
  plots: CropRotationPlot[];
}

export interface CropRotationField {
  fieldId?: number;
  fieldIdentifier?: string;
  fieldName?: string;
  fieldArea?: number;
  fieldYears: { [key: number]: CropRotationFieldYear };
}

export interface CropRotationFieldYear {
  year?: number;
  fieldId?: number;
  fieldIdentifier?: string;
  fieldName?: string;
  fieldArea?: number;
  plots: CropRotationPlot[];
  editable?: boolean;

  selected?: boolean;
}

export interface CropRotationPlot {
  fieldPlot: FieldPlot;
  fieldMeasures?: FieldMeasure[];
  editable?: boolean;
  hexColor?: string | null;

  selected?: boolean;
}

export interface CropRotationSum {
  crops: CropRotationSumCrop[];
  yearSum: { [key: number]: CropRotationYearSumDto };
  years: number[];
  hasCropRotation: boolean;
}

export interface CropRotationYearSumDto {
  sumAll: number;
  sumAssigned: number;
}

export interface CropRotationSumCrop {
  cropId?: number;
  cropIdentifier?: string;
  cropName?: string;
  cropNameEn?: string;
  hexColor?: string;
  years: { [key: number]: CropRotationPlotYearSum };
}

export interface CropRotationPlotYearSum {
  year: number;
  sum: number;
  percentage: number;
}

export interface UserCropSetting extends DbEntityWithAudit {
  userId?: number;
  cropId?: number;
  isFavorite?: boolean;
  hexColor?: string | null;
}

export interface DeviceTrip {
  id: number;
  deviceId?: number;
  gerkPid?: number;
  startTime?: string;
  endTime?: string;
  totalMinutes?: number;
  averageVelocity?: number;
  pointCount?: number;
  beaconId?: string;
  beaconName?: string;
  geoJSON?: string;

  machineId?: number;
  machineName?: string;

  attachmentId?: number;
  attachmentName?: string;

  fieldId?: number;
  fieldIdentifier?: string;
  fieldName?: string;
}

export interface FmMachinery {
  id?: number;
  ime?: string;
  displayName?: string;
}
