﻿using ITC.FieldBook.Application.Infrastructure.Models;
using Microsoft.AspNetCore.DataProtection.KeyManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Dapper.SqlMapper;

namespace ITC.FieldBook.Application.Models
{
    public class UnitGroup : DbEntityWithAudit
    {
        public UnitGroup() : base("FieldBook.unit_group") { }

        private int? _id;
        [DbColumn("id", IsKey = true, Identity = true)]
        public int? Id { get => _id; set { _id = value; RaisePropertyChanged(nameof(Id)); } }

        private int? _unitId;
        [DbColumn("unit_id")]
        public int? UnitId { get => _unitId; set { _unitId = value; RaisePropertyChanged(nameof(_unitId)); } }

        private string? _groupId;
        [DbColumn("group_id")]
        public string? GroupId { get => _groupId; set { _groupId = value; RaisePropertyChanged(nameof(GroupId)); } }
    }
}
