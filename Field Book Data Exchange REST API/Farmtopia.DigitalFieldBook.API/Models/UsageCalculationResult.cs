﻿namespace Farmtopia.DigitalFieldBook.API.Models
{
    public class UsageCalculationResult
    {
        public string? QudtUnit { get; set; }

        public decimal? Value { get; set; }
    }
}
