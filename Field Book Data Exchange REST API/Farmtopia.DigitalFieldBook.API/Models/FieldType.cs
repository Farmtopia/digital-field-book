﻿namespace Farmtopia.DigitalFieldBook.API.Models
{
    public class FieldTypeEntity
    {
        public int? Id { get; set; }

        public Guid? Uuid { get; set; }

        public string? Name { get; set; }

        public string? NameEn { get; set; }
    }

    public class FieldType
    {
        public Guid? Uuid { get; set; }

        public string? Name { get; set; }

        public string? NameEn { get; set; }
    }
}
