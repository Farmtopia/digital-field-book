﻿using ITC.FieldBook.Application.Infrastructure.Interfaces;
using ITC.FieldBook.Application.Infrastructure.Tools;
using ITC.FieldBook.Application.Infrastructure;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using ITC.FieldBook.Application.Models.Reports;
using ITC.FieldBook.Application.Features.Fields;
using ITC.FieldBook.Application.Features.Farms;
using ITC.FieldBook.Application.Features.Crops;
using ITC.FieldBook.Application.Features.Fields.Crops;
using ITC.FieldBook.Application.Infrastructure.Models;
using Microsoft.VisualBasic;
using Microsoft.Extensions.Azure;
using ITC.FieldBook.Application.Features.Users;
using ITC.FieldBook.Application.Models.DTO;

namespace ITC.FieldBook.Application.Features.CropRotation
{
    public class GetCropRotationController : ApiControllerBase
    {
        [HttpGet("crop-rotation")]
        public Task<CropRotationDto> GetCropRotation([FromQuery] GetCropRotationQuery query)
        {
            return Mediator.Send(query);
        }
    }

    public class GetCropRotationQuery : IRequest<CropRotationDto>
    {
        public int? CropType { get; set; }
    }

    internal sealed class GetCropRotationQueryHandler : IRequestHandler<GetCropRotationQuery, CropRotationDto>
    {
        private readonly DbContext _ctx;
        private readonly IConfiguration _configuration;
        private readonly ICurrentUserService _currentUserService;

        public GetCropRotationQueryHandler(DbContext ctx, IConfiguration configuration, ICurrentUserService currentUserService)
        {
            _ctx = ctx;
            _configuration = configuration;
            _currentUserService = currentUserService;
        }

        public Task<CropRotationDto> Handle(GetCropRotationQuery request, CancellationToken cancellationToken)
        {
            var data = new CropRotationDto();
            var farmId = _currentUserService.FarmId!.Value;

            var farmsRepo = new FarmsRepository(_ctx);
            var fieldsRepo = new FieldsRepository(_ctx);
            var cropRotationRepo = new CropRotationRepository(_ctx);
            var userRepository = new UsersRepository(_ctx);

            var fieldTypes = fieldsRepo.GetFieldTypes().Data.Where(x => x.IncludeInCropRotation);
            var cropRorationFieldTypeIds = fieldTypes.Select(x => x.Id!.Value).ToList();

            var farm = farmsRepo.GetFarm(farmId).Item;
            if (farm == null)
                return Task.FromResult(data);

            var fields = fieldsRepo.GetFields(farmId).Data.Where(x => x.TypeId != null && cropRorationFieldTypeIds.Contains(x.TypeId.Value)).OrderBy(x => x.Name).ToList();
            data.HasCropRotation = fields.Count > 0;

            if (!data.HasCropRotation)                
                return Task.FromResult(data);
            
            var years = new List<int>();
            for(var i = DateTime.Now.Year - 1; i < DateTime.Now.Year + 5; i++)
            {
                years.Add(i);
            }

            var minYear = years.Min();
            var maxYear = years.Max();
            var items = new List<CropRotationFieldDto>();
            var fieldPlots = fieldsRepo.GetFieldPlots(farmId, years.FirstOrDefault(), years.LastOrDefault(), request.CropType).Data.OrderBy(x=>x.CropType).ThenBy(x=>x.CropName);
            var fieldPlotsInUse = cropRotationRepo.GetCropsInUse(farmId, null, minYear, maxYear);
            var fieldMeasures = fieldsRepo.GetFieldMeasures(farmId, year: minYear, yearTo: maxYear).Data.OrderBy(x => x.MeasureCategory).ThenBy(x => x.MeasureName);
            var userCropSettings = userRepository.GetUserCropSettings(_currentUserService.Id).Data.Where(x=>!string.IsNullOrWhiteSpace(x.HexColor));

            foreach (var field in fields)
            {
                var itm = new CropRotationFieldDto
                {
                    FieldId = field.Id,
                    FieldIdentifier = field.Identifier,
                    FieldName = field.Name,
                    FieldArea = field.Area
                };

                foreach (var year in years)
                {
                    var fieldYear = new CropRotationFieldYearDto();
                    fieldYear.Year = year;
                    fieldYear.FieldId = field.Id;
                    fieldYear.FieldIdentifier = field.Identifier;
                    fieldYear.FieldName = field.Name;
                    fieldYear.FieldArea = field.Area;

                    var yearlyFieldPlots = fieldPlots.Where(x => x.CropType != null && x.FieldId == field.Id && x.Year == year).ToList();
                    var crops = yearlyFieldPlots.Select(x => 
                        new CropRotationPlotDto
                        { 
                            FieldPlot = x,
                            FieldMeasures = fieldMeasures.Where(y => y.FieldId == field.Id && y.FieldPlotId == x.Id).ToList(),
                            Editable = !fieldPlotsInUse.Any(y => y.FieldPlotId == x.Id),
                            HexColor = userCropSettings.FirstOrDefault(cs => cs.CropId == x.CropId)?.HexColor
                        }).OrderBy(x=>x.FieldPlot!.CropType).ToList();
                    
                    fieldYear.Plots = crops;
                    fieldYear.Editable = crops.Any(x => x.Editable) || !(new int[] {1, 2, 3}).All(val => crops.Select(x=>x.FieldPlot?.CropType).Contains(val)) /* Disable once primary, secondary and winter crops are defined */; // year >= DateTime.Now.Year;

                    itm.FieldYears.Add(year, fieldYear);                    
                }

                items.Add(itm);
            }

            data.Fields = items;
            data.Years = years;

            return Task.FromResult(data);
        }
    }
}
