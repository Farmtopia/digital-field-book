import { defineStore } from 'pinia';
import { computed, ref } from 'vue';
import { Cookies, LocalStorage } from 'quasar';
import { User } from 'src/models/core.models';
import authService from 'src/services/auth.service';
import { useFieldbookStore } from './fieldbook.store';
import fieldbookService from 'src/services/fieldbook.service';

export const useAuthStore = defineStore('auth', () => {
  const user = ref<User>();
  const registrationStatus = ref();
  const isAuthenticated = ref(false);
  const _jwtToken = ref<string | undefined | null>(
    //LocalStorage.getItem<string>('jwt_token')
    Cookies.get<string>('jwt_token')
  );
  const jwtToken = computed({
    get: () => {
      return _jwtToken.value;
    },
    set: (value) => {
      _jwtToken.value = value;

      if (value) {
        Cookies.set('jwt_token', value);
      } else {
        Cookies.remove('jwt_token');
      }
    },
  });

  const userDisplayname = computed(() => {
    const u = user.value;
    return u ? `${u.name} ${u.surname}` : '';
  });

  const loginLocal = async function (username: string, password: string) {
    const loginResponse = (await authService.loginLocal(username, password))
      .data;
    if (loginResponse.success && loginResponse.user) {
      user.value = loginResponse.user;
      jwtToken.value = loginResponse.token;
      isAuthenticated.value = true;
      await onAfterLogin();
      return { success: true };
    }

    clearLogin();
    return { success: false };
  };

  const loginSso = async function (accessToken: string) {
    const loginResponse = (await authService.loginSso(accessToken)).data;
    if (loginResponse.success && loginResponse.user) {
      user.value = loginResponse.user;
      jwtToken.value = loginResponse.token;
      isAuthenticated.value = true;
      await onAfterLogin();
      return { success: true };
    }

    clearLogin();
    return { success: false };
  };

  const logout = async function () {
    clearLogin();
  };

  const checkLogin = async function () {
    if (!user.value && jwtToken.value) {
      try {
        const loginResponse = (await authService.refreshToken()).data;
        if (loginResponse.success && loginResponse.user) {
          user.value = loginResponse.user;
          jwtToken.value = loginResponse.token;
          isAuthenticated.value = true;
          await onAfterLogin();
        }
      } catch (e) {
        clearLogin();
      }
    }
  };

  const refreshToken = async function () {
    if (jwtToken.value) {
      const loginResponse = (await authService.refreshToken()).data;
      if (loginResponse.success && loginResponse.user) {
        jwtToken.value = loginResponse.token;
      } else {
        jwtToken.value = undefined;
      }
    }
  };

  const onAfterLogin = async function () {
    const fieldbookInfo = (await fieldbookService.getFieldBookInfo()).data;
    const fieldbookStore = useFieldbookStore();

    fieldbookStore.farm = fieldbookInfo.defaultFarm;
    fieldbookStore.farms = fieldbookInfo.farms;

    await fieldbookStore.loadAppSetting();
  };

  const clearLogin = function () {
    const fieldbookStore = useFieldbookStore();

    user.value = undefined;
    jwtToken.value = undefined;
    registrationStatus.value = undefined;
    isAuthenticated.value = false;

    fieldbookStore.farm = undefined;
    fieldbookStore.farms = undefined;
  };

  return {
    user,
    userDisplayname,
    jwtToken,
    isAuthenticated,
    registrationStatus,
    loginLocal,
    loginSso,
    checkLogin,
    refreshToken,
    logout,
  };
});
