﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITC.FieldBook.Application.Infrastructure.Models
{
    public class DbItem<T> //where T: DbEntity
    {
        public T? Item { get; set; }

        public static DbItem<T> Empty()
        {
            return new DbItem<T>();
        }
    }
}
