﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using System.Data;
using ITC.FieldBook.Application.Infrastructure.Enums;
using ITC.FieldBook.Application.Infrastructure;
using ITC.FieldBook.Application.Infrastructure.Models;
using ITC.FieldBook.Application.Infrastructure.Interfaces;

namespace ITC.FieldBook.Application.Features.Users
{
    [Authorize(Roles = EnumRole.Admin)]
    public class GetUsersController : ApiControllerBase
    {
        [HttpGet("users")]
        public Task<DbData<User>> GetUsers([FromQuery] bool includeInactive)
        {
            return Mediator.Send(new GetUsersQuery { IncludeInactive = includeInactive });
        }
    }

    public class GetUsersQuery : IRequest<DbData<User>>
    {
        public bool IncludeInactive { get; set; }
    }

    internal sealed class GetUsersQueryHandler : IRequestHandler<GetUsersQuery, DbData<User>>
    {
        private readonly DbContext _ctx;
        private readonly IConfiguration _configuration;
        private readonly ICurrentUserService _currentUserService;
        private readonly UsersRepository _usersRepository;

        public GetUsersQueryHandler(DbContext ctx, IConfiguration configuration, ICurrentUserService currentUserService)
        {
            _ctx = ctx;
            _configuration = configuration;
            _currentUserService = currentUserService;
            _usersRepository = new UsersRepository(_ctx);
        }

        public Task<DbData<User>> Handle(GetUsersQuery request, CancellationToken cancellationToken)
        {
            var users = _usersRepository.GetUsers(request.IncludeInactive);
            return Task.FromResult(users);
        }
    }
}
