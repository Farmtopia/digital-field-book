﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using ITC.FieldBook.Application.Models;
using Dapper;
using System.Transactions;
using System.ComponentModel.DataAnnotations;
using ITC.FieldBook.Application.Infrastructure.Tools;
using System.Data;
using Microsoft.AspNetCore.Http;
using ITC.FieldBook.Application.Infrastructure.Interfaces;
using ITC.FieldBook.Application.Infrastructure.Services;

namespace ITC.FieldBook.Application.Infrastructure.Features.Export
{
    public class ExportGetGetController : ApiControllerBase
    {
        [HttpGet("export-data/{id}")]
        public async Task<IActionResult> ExportGetData([FromRoute] ExportGetCommand command)
        {
            var file = await Mediator.Send(command);

            if (file == null || file.Filename == null || file.Bytes == null)
                return NotFound();

            return File(file.Bytes, file.ContentType, file.Filename);
        }
    }

    public class ExportGetCommand : IRequest<Models.File?>
    {
        [Required]
        public Guid? Id { get; set; }
    }


    internal sealed class ExportGetCommandHandler : IRequestHandler<ExportGetCommand, Models.File?>
    {
        private ITempFileService _tempFileService;

        public ExportGetCommandHandler(ITempFileService tempFileService)
        {
            _tempFileService = tempFileService;
        }

        public Task<Models.File?> Handle(ExportGetCommand request, CancellationToken cancellationToken)
        {
            return Task.FromResult(_tempFileService.GetFile(request.Id!.Value));
        }
    }
}
