﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITC.FieldBook.Application.Models
{
    public class CalculateUsageResponse
    {
        public decimal? Area { get; set; }

        public decimal? Amount { get; set; }

        public int? AmountUnitId { get; set; }

        public string? AmountUnit { get; set; }

        public decimal? Usage { get; set; }

        public int? UsageUnitId { get; set; }

        public string? UsageUnit { get; set; }

        public decimal? Stock { get; set; }

        public int? StockUnitId { get; set; }

        public string? StockUnit { get; set; }

        public decimal? RemainingStock { get; set; }

        public bool HasStock { get; set; }
    }
}
