﻿using Azure.Core;
using ITC.FieldBook.Application.Infrastructure;
using ITC.FieldBook.Application.Infrastructure.Models;
using ITC.FieldBook.Application.Models;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITC.FieldBook.Application.Features.Pesticides
{
    public class PesticidesRepository
    {
        private DbContext _ctx;

        public PesticidesRepository(DbContext ctx) {
            _ctx = ctx;
        }
        public DbData<Pesticide> GetPesticides(bool includeStock = false, bool stockOnly = false, int? stockFarmId = null, DateTime? stockDate = null, int? taskId = null)
        {
            return _ctx.Get<Pesticide>($@"
                SELECT 
                    p.id,
                    p.name,
                    p.name_en,
                    p.brand_name,
                    p.is_global,
                    p.farm_id,
                    p.created,
                    frm.name AS farm_name
                    {(includeStock || stockOnly ? ",ISNULL(s.stock, 0) AS stock, s.stock_unit, s.stock_unit_id" : "")}
                FROM FieldBook.pesticide p
                    LEFT JOIN FieldBook.farm frm ON frm.id = p.farm_id
                    {(includeStock || stockOnly ? $"{(stockOnly ? "JOIN FieldBook.fn_pesticide_stock(@stockFarmId, NULL, @stockDate, @taskId) s ON s.pesticide_id = p.id AND s.stock > 0" : "LEFT JOIN FieldBook.fn_pesticide_stock(@stockFarmId, NULL, @stockDate, @taskId) s ON s.pesticide_id = p.id")}" : "")}
                WHERE
                    {DbContext.GetIsDeletedIsActiveCondition("p")}
                ORDER BY {(includeStock ? "(CASE WHEN s.stock > 0 THEN 1 ELSE 0 END) DESC, " : "")} p.name ASC
                ", new { stockFarmId, stockDate, taskId });
        }

        public DbItem<Pesticide> GetPesticide(int pesticideId, bool includeStock = false, bool stockOnly = false, int? stockFarmId = null, DateTime? stockDate = null, int? taskId = null)
        {
            return _ctx.GetItem<Pesticide>($@"
                SELECT TOP(1)
                    p.id,
                    p.name,
                    p.name_en,
                    p.brand_name,
                    p.is_global,
                    p.farm_id,
                    p.created,
                    frm.name AS farm_name
                    {(includeStock || stockOnly ? ",s.stock, s.stock_unit, s.stock_unit_id" : "")}
                FROM FieldBook.pesticide p
                    LEFT JOIN FieldBook.farm frm ON frm.id = p.farm_id
                    {(includeStock || stockOnly ? $"{(stockOnly ? "JOIN FieldBook.fn_pesticide_stock(@stockFarmId, NULL, @stockDate, @taskId) s ON s.pesticide_id = p.id AND s.stock > 0" : "LEFT JOIN FieldBook.fn_pesticide_stock(@stockFarmId, NULL, @stockDate, @taskId) s ON s.pesticide_id = p.id")}" : "")}
                WHERE
                    {DbContext.GetIsDeletedIsActiveCondition("p")}
                    AND p.id = @pesticideId
                ", new { stockFarmId, pesticideId, stockDate, taskId });
        }
    }
}
