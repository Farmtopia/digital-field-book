﻿using ITC.FieldBook.Application.Infrastructure.Interfaces;
using ITC.FieldBook.Application.Infrastructure.Models;
using ITC.FieldBook.Application.Infrastructure.Tools;
using NetTopologySuite.Densify;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITC.FieldBook.Application.Models
{
    public class DocumentType : DbEntityWithAudit
    {
        public DocumentType() : base("FieldBook.document_type") { }
        
        private int? _id;
        [DbColumn("id", IsKey = true, Identity = true)]
        public int? Id { get => _id; set { _id = value; RaisePropertyChanged(nameof(Id)); } }

        private string? _name;
        [DbColumn("name")]
        public string? Name { get => _name; set { _name = value; RaisePropertyChanged(nameof(Name)); } }

        private string? _nameEn;
        [DbColumn("name_en")]
        public string? NameEn { get => _nameEn; set { _nameEn = value; RaisePropertyChanged(nameof(NameEn)); } }
    }
}
