﻿using ITC.FieldBook.API.Models;
using ITC.FieldBook.Application.Models;
using Microsoft.AspNetCore.Mvc;

namespace ITC.FieldBook.API.Middleware
{
    public class ValidationErrorResult : IActionResult
    {
        public async Task ExecuteResultAsync(ActionContext context)
        {
            var keys = context.ModelState.Keys;
            var errors = context.ModelState.Where(x => x.Value?.Errors?.Any() ?? false).Select(x => $"{x.Key}: {string.Join(" ; ", x.Value!.Errors.Select(x=>x.ErrorMessage))}").ToList();
            var problemDetails = new ErrorDetails { StatusCode = StatusCodes.Status400BadRequest, Message = "Bad request", Errors = errors };
            var objectResult = new ObjectResult(problemDetails) { StatusCode = problemDetails.StatusCode };
            await objectResult.ExecuteResultAsync(context);
        }
    }
}
