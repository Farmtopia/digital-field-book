﻿using ITC.FieldBook.Application.Infrastructure.Models;
using ITC.FieldBook.Application.Infrastructure;
using MediatR;
using Microsoft.AspNetCore.DataProtection.KeyManagement;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using ITC.FieldBook.Application.Models;
using ITC.FieldBook.Application.Infrastructure.Services;
using ITC.FieldBook.Application.Infrastructure.Interfaces;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Authorization;
using ITC.FieldBook.Application.Infrastructure.Enums;

namespace ITC.FieldBook.Application.Features.Farms
{
    [Authorize(Roles = EnumRole.Admin)]
    public class DeleteFarmController : ApiControllerBase
    {
        [HttpDelete("farms/{farmId}")]
        public Task DeleteFarm([FromRoute] int? farmId)
        {
            return Mediator.Send(new DeleteFarmCommand { FarmId = farmId });
        }
    }

    public class DeleteFarmCommand : IRequest
    {
        [Required]
        public int? FarmId { get; set; }
    }

    internal sealed class DeleteFarmCommandHandler : IRequestHandler<DeleteFarmCommand>
    {
        private readonly DbContext _ctx;
        private readonly ICurrentUserService _currentUserService;

        public DeleteFarmCommandHandler(DbContext ctx, ICurrentUserService currentUserService)
        {
            _ctx = ctx;
            _currentUserService = currentUserService;
        }

        public Task Handle(DeleteFarmCommand request, CancellationToken cancellationToken)
        {
            var farmsRepository = new FarmsRepository(_ctx);
            var farm = farmsRepository.GetFarm(request.FarmId!.Value).Item;
            if (farm != null)
            {
                var user = _currentUserService.GetUser()!;
                _ctx.DeleteItem(farm, user);
            }

            return Task.CompletedTask;
        }
    }
}

