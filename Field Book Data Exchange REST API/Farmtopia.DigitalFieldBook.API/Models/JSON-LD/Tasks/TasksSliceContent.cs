﻿using Azure;
using Swashbuckle.AspNetCore.Annotations;
using System.Text.Json.Serialization;

namespace Farmtopia.DigitalFieldBook.API.Models.JSON_LD.Tasks
{
    public class TasksSliceContent : SliceContentBase
    {
        [JsonPropertyName("label")]
        public string? Label { get; set; }

        [JsonPropertyName("startDatetime")]
        public DateTime? StartDatetime { get; set; }

        [JsonPropertyName("endDatetime")]
        public DateTime? EndDatetime { get; set; }

        [JsonPropertyName("operationNotes")]
        public string? OperationNotes { get; set; }

        [JsonPropertyName("operatedOn")]
        public OperatedOn? OperatedOn { get; set; }

        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
        [JsonPropertyName("fertilizer")]
        public Fertilizer? Fertilizer { get; set; }

        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
        [JsonPropertyName("pesticide")]
        public Pesticide? Pesticide { get; set; }

        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
        [JsonPropertyName("resourceQuantityUsed")]
        public ResourceQuantityUsed? ResourceQuantityUsed { get; set; }

        
    }
}
