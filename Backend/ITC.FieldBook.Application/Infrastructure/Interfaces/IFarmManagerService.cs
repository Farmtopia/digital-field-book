﻿using ITC.FieldBook.Application.Infrastructure.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITC.FieldBook.Application.Infrastructure.Interfaces
{
    public interface IFarmManagerService
    {
        public FmManager Instance { get; set; }

    }
}
