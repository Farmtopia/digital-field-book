﻿using ITC.FieldBook.Application.Infrastructure.Interfaces;
using ITC.FieldBook.Application.Infrastructure.Models;
using ITC.FieldBook.Application.Infrastructure;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication;
using System.Text.Json;
using ITC.FieldBook.Application.Features.Farms;
using ITC.FieldBook.Application.Infrastructure.Enums;
using Microsoft.Extensions.Configuration;

namespace ITC.FieldBook.Application.Features.Users
{
    public class RefreshTokenController : ApiControllerBase
    {
        [HttpPost("user/refresh-token")]
        public Task<RefreshTokenResponse> RefreshToken()
        {
            return Mediator.Send(new RefreshTokenQuery { });
        }
    }

    public class RefreshTokenQuery : IRequest<RefreshTokenResponse>
    {
    }

    public class RefreshTokenResponse
    {
        public User? User { get; set; }

        public int? RegistrationStatus { get; set; }

        public bool Success { get; set; }

        public string? Token { get; set; }
    }

    internal sealed class RefreshTokenQueryHandler : IRequestHandler<RefreshTokenQuery, RefreshTokenResponse>
    {
        private readonly DbContext _ctx;
        private readonly ICurrentUserService _currentUserService;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IConfiguration _configuration;

        public RefreshTokenQueryHandler(DbContext ctx, ICurrentUserService currentUserService, IHttpContextAccessor httpContextAccessor, IConfiguration configuration)
        {
            _ctx = ctx;
            _currentUserService = currentUserService;
            _httpContextAccessor = httpContextAccessor;
            _configuration = configuration;
        }

        public async Task<RefreshTokenResponse> Handle(RefreshTokenQuery request, CancellationToken cancellationToken)
        {
            var usersRepository = new UsersRepository(_ctx);

            var user = _currentUserService.GetUser();
            if (user == null || !user.IsActive)
                return new RefreshTokenResponse { Success = false };

            var userId = user.Id!.Value;

            // Set default farm
            var farmsBll = new FarmsBll(_ctx);
            var userFarms = farmsBll.GetFarms(userId).Data.ToList();
            var farm = userFarms.FirstOrDefault();
            user.FarmId = farm?.Id;

            // User must have at least one farm defined (admin not required)
            if (user.Role == EnumRole.User && user.FarmId == null)
                return new RefreshTokenResponse { Success = false };

            var authService = new AuthService(_ctx, _configuration);
            var token = authService.GetToken(user, farm);

            // Update user login time
            usersRepository.UpdateLastLogin(userId, DateTime.Now);

            //// Set claims
            //var usersBll = new UsersBll(_ctx);
            //var claimsIdentity = usersBll.GetClaims(user, farm);
            //await _httpContextAccessor.HttpContext!.SignInAsync(
            //    CookieAuthenticationDefaults.AuthenticationScheme,
            //    new ClaimsPrincipal(claimsIdentity));

            return new RefreshTokenResponse { User = user, Success = true, Token = token };

        }
    }
}
