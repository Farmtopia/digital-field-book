﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITC.FieldBook.Application.Infrastructure.Interfaces
{
    public interface ITempFileService
    {
        void Initialize();

        Guid? SaveFile(string fileName, byte[] data);

        Models.File? GetFile(Guid? id);

    }
}
