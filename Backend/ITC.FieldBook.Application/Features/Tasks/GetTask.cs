﻿using ITC.FieldBook.Application.Infrastructure.Interfaces;
using ITC.FieldBook.Application.Infrastructure;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using ITC.FieldBook.Application.Infrastructure.Models;
using ITC.FieldBook.Application.Models;
using ITC.FieldBook.Application.Features.Documents;
using System.ComponentModel.DataAnnotations;

namespace ITC.FieldBook.Application.Features.Tasks
{
    public class GetTaskController : ApiControllerBase
    {
        [HttpGet("tasks/{taskId}")]
        public Task<DbItem<TaskEntity>> GetTask([FromRoute] int? taskId)
        {
            return Mediator.Send(new GetTaskQuery { TaskId = taskId });
        }
    }

    public class GetTaskQuery : IRequest<DbItem<TaskEntity>>
    {
        [Required]
        public int? TaskId { get; set; }
    }

    internal sealed class GetTaskQueryHandler : IRequestHandler<GetTaskQuery, DbItem<TaskEntity>>
    {
        private readonly DbContext _ctx;
        private readonly IConfiguration _configuration;
        private readonly ICurrentUserService _currentUserService;

        public GetTaskQueryHandler(DbContext ctx, IConfiguration configuration, ICurrentUserService currentUserService)
        {
            _ctx = ctx;
            _configuration = configuration;
            _currentUserService = currentUserService;
        }

        public Task<DbItem<TaskEntity>> Handle(GetTaskQuery request, CancellationToken cancellationToken)
        {
            var documentsRepo = new DocumentsRepository(_ctx);
            var tasksRepository = new TasksRepository(_ctx);

            var farmId = _currentUserService.FarmId!.Value;
            var taskId = request.TaskId!.Value;
            var data = tasksRepository.GetTask(farmId, taskId);

            var item = data.Item;
            if (item != null)
            {
                item.TaskFields = tasksRepository.GetTaskFields(farmId, taskId).Data?.ToList();
                item.Pesticides = tasksRepository.GetTaskPesticides(farmId, taskId).Data?.ToList();
                item.Fertilizers = tasksRepository.GetTaskFertilizers(farmId, taskId).Data?.ToList();
                item.Documents = documentsRepo.GetDocuments(farmId, taskId).Data?.ToList();
            }

            return Task.FromResult(data);
        }
    }
}
