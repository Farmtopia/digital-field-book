﻿using ITC.FieldBook.Application.Infrastructure.Interfaces;
using ITC.FieldBook.Application.Infrastructure;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata;
using Dapper;
using ITC.FieldBook.Application.Models;
using ITC.FieldBook.Application.Infrastructure.Models;
using ITC.FieldBook.Application.Features.Pesticides;
using ITC.FieldBook.Application.Infrastructure.Enums;

namespace ITC.FieldBook.Application.Features.Fertilizers
{
    public class GetFertilizerController : ApiControllerBase
    {
        [HttpGet("fertilizers")]
        public Task<DbData<Fertilizer>> GetFertilizer([FromQuery] GetFertilizerQuery query)
        {
            return Mediator.Send(query);
        }
    }

    public class GetFertilizerQuery : BaseDataQuery<DbData<Fertilizer>>
    {
        public bool All { get; set; }

        public bool IncludeStock { get; set; }

        public bool StockOnly { get; set; }

        public DateTime? StockDate { get; set; }

        public int? TaskId { get; set; }
    }

    internal sealed class GetFertilizerQueryHandler : IRequestHandler<GetFertilizerQuery, DbData<Fertilizer>>
    {
        private readonly DbContext _ctx;
        private readonly IConfiguration _configuration;
        private readonly ICurrentUserService _currentUserService;

        public GetFertilizerQueryHandler(DbContext ctx, IConfiguration configuration, ICurrentUserService currentUserService)
        {
            _ctx = ctx;
            _configuration = configuration;
            _currentUserService = currentUserService;
        }

        public Task<DbData<Fertilizer>> Handle(GetFertilizerQuery request, CancellationToken cancellationToken)
        {
            var user = _currentUserService.GetUser()!;
            var fertilizersRepository = new FertilizersRepository(_ctx);

            var data = fertilizersRepository.GetFertilizers(
                request.IncludeStock,
                request.StockOnly,
                _currentUserService.FarmId,
                request.StockDate ?? DateTime.Now,
                request.TaskId);

            if (request.All && user.HasRole(EnumRole.Admin))
                return Task.FromResult(data);
            else
                data.Data = data.Data.Where(x => x.IsGlobal || (_currentUserService.FarmId != null && x.FarmId == _currentUserService.FarmId));

            return Task.FromResult(data);
        }
    }
}
