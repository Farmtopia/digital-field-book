﻿using ITC.FieldBook.Application.Infrastructure.Interfaces;
using ITC.FieldBook.Application.Infrastructure;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Components.Forms;
using System.Transactions;
using ITC.FieldBook.Application.Models;
using ITC.FieldBook.Application.Features.Documents;
using ITC.FieldBook.Application.Features.Crops;
using ITC.FieldBook.Application.Features.Fields;
using ITC.FieldBook.Application.Features.Stock;
using System.ComponentModel.DataAnnotations;

namespace ITC.FieldBook.Application.Features.Tasks
{
    public class DeleteTaskController : ApiControllerBase
    {
        [HttpDelete("tasks/{taskId}")]
        public Task DeleteTask([FromRoute]int? taskId)
        {
            return Mediator.Send(new DeleteTaskCommand { TaskId = taskId });
        }
    }

    public class DeleteTaskCommand : IRequest
    {
        [Required]
        public int? TaskId { get; set; }
    }

    internal sealed class DeleteTaskCommandHandler : IRequestHandler<DeleteTaskCommand>
    {
        private readonly DbContext _ctx;
        private readonly IConfiguration _configuration;
        private readonly ICurrentUserService _currentUserService;

        public DeleteTaskCommandHandler(DbContext ctx, IConfiguration configuration, ICurrentUserService currentUserService)
        {
            _ctx = ctx;
            _configuration = configuration;
            _currentUserService = currentUserService;
        }

        public Task Handle(DeleteTaskCommand request, CancellationToken cancellationToken)
        {
            var user = _currentUserService.GetUser()!;
            var userId = user.Id!.Value;
            var farmId = _currentUserService.FarmId!.Value;
            var taskId = request.TaskId!.Value;
            var tasksRepository = new TasksRepository(_ctx);
            var task = tasksRepository.GetTask(farmId, taskId).Item;

            if(task != null)
            {
                var stockRepository = new StockRepository(_ctx);
                var fieldsRepo = new FieldsRepository(_ctx);
                var documentsRepo = new DocumentsRepository(_ctx);

                using (var scope = new TransactionScope())
                {
                    _ctx.DeleteItem(task, user);

                    // Delete task related data
                    tasksRepository.DeleteTaskFields(farmId, taskId, userId);
                    tasksRepository.DeleteTaskPesticides(farmId, taskId, userId);
                    tasksRepository.DeleteTaskFertilizers(farmId, taskId, userId);
                    documentsRepo.DeleteTaskDocuments(farmId, taskId, userId);

                    // Delete stock for task
                    stockRepository.DeleteFertilizerStockForTask(farmId, taskId, userId);
                    stockRepository.DeletePesticideStockForTask(farmId, taskId, userId);

                    // Clear sowing/harvest date based on this task
                    fieldsRepo.ClearSowingDateForFieldPlots(farmId, taskId);
                    fieldsRepo.ClearHarvestDateForFieldPlots(farmId, taskId);

                    scope.Complete();
                }
            }
            return Task.CompletedTask;
        }
    }
}
