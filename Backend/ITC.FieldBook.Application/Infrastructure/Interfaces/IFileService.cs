﻿using ITC.FieldBook.Application.Infrastructure.Models;

namespace ITC.FieldBook.Application.Infrastructure.Interfaces
{
    public interface IFileService
    {
        public FileData? Get(int id, bool includeFile);

        public List<FileData> Get(int[] ids);

        public int Save(string fileName, byte[] bytes, SaveFileSettings? settings = null);   
    }
}
