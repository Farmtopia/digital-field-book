# Field book 

## API documentation 

### Glossary 

 
The Farmtopia Digital FieldBook API is a web service that provides access to farm-related data. It offers endpoints to retrieve information about farms, fields, tasks, and associated metadata. This document outlines the available RESTful endpoints and their usage. 


### Swagger documentation URl

https://farmtopia-fb-api.dih-agrifood.com/swagger/index.html


 
### Authorization 

 

To use the API, you must be authorized. Use the Authorize button at the top right of the Swagger UI to input your credentials. 

Credentials are available in Bearer (API Key) format. To get API Key contact ITC administrator (miran.bunderla@itc-cluster.com).

 **It is critical to write “Bearer + APIKEY”, example:**
  
_Bearer naz4vdmmvNfAk6fkk3fpeWdddLc29HZsHBZ3FbyIGGg19i9ldPshUYRk7aY8py0TuDKhd70wgj7KbuayQELA5GBRZ83ddasCucTvx95TACVxVzWqIav3zYnn1rgcD_
 
 

### Api endpoints 

 

#### /farms 

The GET request retrieves a list of farms from the Farmtopia Digital FieldBook API. 

##### Parameters 

No parameters. 

##### Response 

###### Curl 

```
curl -X 'GET' \ 
  'https://farmtopia-fb-api.dih-agrifood.com/farms' \ 
  -H 'accept: text/plain' \ 
  -H 'Authorization: Bearer APIKEY 
```
Request URL 


https://farmtopia-fb-api.dih-agrifood.com/farms 

 
###### Code 200 

 

**Description**: Successful 

**Media type**: text/plain, application/json, text/json 

The response is a JSON array containing farm objects with the following properties: 

- uuid (string): The unique identifier of the farm,
- latitude (number): The latitude coordinate of the farm location,
- longitude (number): The longitude coordinate of the farm location, 
- ecoFarm (boolean): Indicates whether the farm is an eco-friendly farm. 


Example response: 
```json
{
  "@context": {
    "fsm": "https://gitlab.com/Farmtopia/farmtopia-semantic-model/-/raw/main/FarmtopiaSemanticModel.rdf#",
    "pcsm": "https://www.tno.nl/agrifood/ontology/ploutos/pcsm#",
    "geosparql": "http://www.opengis.net/ont/geosparql/",
    "centroid": {
      "@id": "fsm:centroid",
      "@type": "@id"
    },
    "geoJson": {
      "@id": "geosparql:asGeoJSON",
      "@type": "@json"
    },
    "isEcoFarm": "fsm:isEcoFarm",
    "sliceContents": {
      "@id": "fsm:sliceContents",
      "@type": "@json",
      "@container": "@set"
    },
    "datasetSlices": {
      "@id": "fsm:datasetSlices",
      "@type": "@json",
      "@container": "@set"
    }
  },
  "@graph": [
    {
      "@id": "ITC_farms_serviceResource_representation_instance_43728",
      "@type": "fsm:Dataset",
      "datasetSlices": [
        {
          "@id": "FarmComplexesSlice_1",
          "@type": "fsm:Slice",
          "@sliceContents": [
            {
              "@id": "01b7766b-a6f5-4a2e-a752-9ebf20183ac9",
              "@type": "fsm:FarmComplex",
              "centroid": {
                "@id": "01b7766b-a6f5-4a2e-a752-9ebf20183ac9_GeometryCentroid",
                "@type": "geosparql:Geometry",
                "geoJson": {
                  "type": "Point",
                  "coordinates": [
                    46.8525779920234,
                    16.2697863753868
                  ]
                }
              },
              "isEcoFarm": true
            },
            {
              "@id": "125ee673-34e0-4829-b059-6c75733dd6ea",
              "@type": "fsm:FarmComplex",
              "centroid": {
                "@id": "125ee673-34e0-4829-b059-6c75733dd6ea_GeometryCentroid",
                "@type": "geosparql:Geometry",
                "geoJson": {
                  "type": "Point",
                  "coordinates": [
                    46.3967035452489,
                    16.2729023766489
                  ]
                }
              },
              "isEcoFarm": false
            },
            {
              "@id": "05e2e4cf-c9e3-464d-8c92-d68606384d4a",
              "@type": "fsm:FarmComplex",
              "centroid": {
                "@id": "05e2e4cf-c9e3-464d-8c92-d68606384d4a_GeometryCentroid",
                "@type": "geosparql:Geometry",
                "geoJson": {
                  "type": "Point",
                  "coordinates": [
                    46.5561221326656,
                    16.1995861524434
                  ]
                }
              },
              "isEcoFarm": false
            },
            {
              "@id": "1aacb81d-dc49-424e-9d63-668168c644ad",
              "@type": "fsm:FarmComplex",
              "centroid": {
                "@id": "1aacb81d-dc49-424e-9d63-668168c644ad_GeometryCentroid",
                "@type": "geosparql:Geometry",
                "geoJson": {
                  "type": "Point",
                  "coordinates": [
                    35.3231514,
                    23.7200581
                  ]
                }
              },
              "isEcoFarm": false
            }
          ]
        }
      ]
    }
  ]
}
```

Example response header:  
```
content-type: text/plain; charset=utf-8  
date: Fri,26 Apr 2024 05:17:09 GMT  
request-context: appId=cid-v1:6b2a2081-a5e9-4021-81f5-2cfce7f82dde  
server: Microsoft-IIS/10.0  
transfer-encoding: chunked  
x-powered-by: ASP.NET 
```

###### Code 401 

 

**Description**: Error - Unauthorized 

**Response body**: Required authorization header 

**Response header**:  
```
content-type: text/plain; charset=utf-8  
date: Fri,26 Apr 2024 05:17:09 GMT  
request-context: appId=cid-v1:6b2a2081-a5e9-4021-81f5-2cfce7f82dde  
server: Microsoft-IIS/10.0  
transfer-encoding: chunked  
x-powered-by: ASP.NET 
```
 

 

##### /farms/{farmUUid}/fields 

 

The GET request retrieves a list of fields from the specific farm in Farmtopia Digital FieldBook API. 

###### Parameters 

**Required**


FarmUuid (string) 

**Response**

###### Curl 

```
curl -X 'GET' \ 
  'https://farmtopia-fb-api.dih-agrifood.com/farms/3fa85f64-5717-4562-b3fc-2c963f66afa6/fields' \ 
  -H 'accept: text/plain' \ 
  -H 'Authorization: Bearer APIKEY 
```
 

Request URL 

https://farmtopia-fb-api.dih-agrifood.com/farms/3fa85f64-5717-4562-b3fc-2c963f66afa6/fields 

 

##### Code 200 

**Description**: Successful 

**Media type**: text/plain, application/json, text/json 

The response is a JSON array containing objects with the following properties: 

- uuid (string): The unique identifier for the field. 
- identifier (string): The identifier for the field. 
- name (string): The name of the field. 
- description (string): The description of the field. 
- areaM2 (number): The area of the field in square meters. 
- fieldType (object): An object containing the following properties: 
    - id (number): The ID of the field type. 
    - name (string): The name of the field type. 
    - nameEn (string): The name of the field type in English. 
    - crops (array): An array of objects representing the crops in the field, each containing the following properties: 
    - year (number): The year of the crop. 
    - cropType (number): The type of the crop. 
    - crop (object): An object containing the following properties: 
    - uuid (string): The unique identifier for the crop. 
    - name (string): The name of the crop. 
    - nameEn (string): The name of the crop in English. 
    - sowingDate (string): The date when the crop was sown. 
    - harvestDate (string): The date when the crop was harvested. 
- geoJSON (object): An object containing the following properties: 
    - type (string): The type of the geoJSON. 
    - coordinates (array): An array containing the coordinates of the field. 


Example response: 

```json
{ 
    {
  "@context": {
    "fsm": "https://gitlab.com/Farmtopia/farmtopia-semantic-model/-/raw/main/FarmtopiaSemanticModel.rdf#",
    "pcsm": "https://www.tno.nl/agrifood/ontology/ploutos/pcsm#",
    "geosparql": "http://www.opengis.net/ont/geosparql/",
    "qudt": "http://qudt.org/schema/qudt/",
    "obo": "http://purl.obolibrary.org/obo/",
    "contains": {
      "@id": "fsm:contains",
      "@type": "@id",
      "@container": "@set"
    },
    "geometry": {
      "@id": "geosparql:hasGeometry",
      "@type": "@id"
    },
    "geoJson": {
      "@id": "geosparql:asGeoJSON",
      "@type": "@json"
    },
    "hasArea": {
      "@id": "fsm:hasArea",
      "@type": "@id"
    },
    "unitOfMeasure": {
      "@id": "fsm:unitOfMeasure",
      "@type": "@id"
    },
    "landType": "fsm:landType",
    "name": "saref4agri:hasName",
    "nationalIdentifier": "fsm:nationalIdentifier",
    "numericValue": "qudt:numericValue",
    "label": "fsm:label",
    "cropVariety": "pcsm:hasCropVariety",
    "agrovocCode": "fsm:agrovocCode",
    "hasPlantDate": "pcsm:hasPlantDate",
    "hasHarvestDate": "pcsm:hasHarvestDate",
    "sliceContents": {
      "@id": "fsm:sliceContents",
      "@type": "@json",
      "@container": "@set"
    },
    "datasetSlices": {
      "@id": "fsm:datasetSlices",
      "@type": "@json",
      "@container": "@set"
    }
  },
  "@graph": [
    {
      "@id": "ITC_fields_serviceResource_representation_instance_23328",
      "@type": "fsm:Dataset",
      "datasetSlices": [
        {
          "@id": "FarmSitesSlice_1",
          "@type": "fsm:Slice",
          "@sliceContents": [
            {
              "@id": "f258b3a1-ae7c-4ff5-acaa-fd5d51b845cc",
              "@type": "fsm:FarmSite",
              "name": "ADANO",
              "nationalIdentifier": "4304851",
              "hasArea": {
                "@id": "f258b3a1-ae7c-4ff5-acaa-fd5d51b845cc_FieldArea",
                "@type": "qudt:QuantityValue",
                "numericValue": 5663,
                "unitOfMeasure": "unit:M2"
              },
              "geometry": {
                "@id": "f258b3a1-ae7c-4ff5-acaa-fd5d51b845cc_FieldGeometry",
                "@type": "geosparql:Geometry",
                "geoJson": {
                  "type": "Polygon",
                  "coordinates": [
                    [
                      [
                        16.271353896215,
                        46.85256580836863
                      ],
                      [
                        16.27135490449762,
                        46.85301134569147
                      ],
                      [
                        16.271373548487805,
                        46.853123414112645
                      ],
                      [
                        16.27126517693778,
                        46.85319069671327
                      ],
                      [
                        16.27121138033754,
                        46.85325611356193
                      ],
                      [
                        16.27117784257345,
                        46.85329921912885
                      ],
                      [
                        16.271119793512707,
                        46.853378942514
                      ],
                      [
                        16.271031366954787,
                        46.853346727708775
                      ],
                      [
                        16.27097352262425,
                        46.85333787883631
                      ],
                      [
                        16.270692583177485,
                        46.853255577124706
                      ],
                      [
                        16.27052835482446,
                        46.853173148713964
                      ],
                      [
                        16.27031690826869,
                        46.85263714903659
                      ],
                      [
                        16.270299311475284,
                        46.85257216516078
                      ],
                      [
                        16.270312686068486,
                        46.85257210668006
                      ],
                      [
                        16.27034631279825,
                        46.8525855879614
                      ],
                      [
                        16.27077281090908,
                        46.8525539539526
                      ],
                      [
                        16.271134495681178,
                        46.852540446311984
                      ],
                      [
                        16.271270626009855,
                        46.85254365745919
                      ],
                      [
                        16.27135391221452,
                        46.85254426172436
                      ],
                      [
                        16.271353896215,
                        46.85256580836863
                      ]
                    ]
                  ]
                }
              },
              "contains": [
                {
                  "@id": "d0409245-fca6-4658-adce-93ab2900ed63",
                  "@type": "fsm:Plot",
                  "landType": {
                    "@id": "bf866b4d-d94f-42c9-87b7-05ee868e9e6e",
                    "@type": "obo:ENVO_01001177",
                    "label": "Extensive orchard"
                  },
                  "contains": [
                    {
                      "@id": "1fcf7a5a-31d7-440f-8b10-5cba35a75714",
                      "@type": "pcsm:Crop",
                      "cropVariety": "Bean",
                      "agrovocCode": null,
                      "hasPlantDate": null,
                      "hasHarvestDate": null
                    }
                  ]
                },
                {
                  "@id": "1181f71c-6fb9-4c30-be00-68e241ad788a",
                  "@type": "fsm:Plot",
                  "landType": {
                    "@id": "bf866b4d-d94f-42c9-87b7-05ee868e9e6e",
                    "@type": "obo:ENVO_01001177",
                    "label": "Extensive orchard"
                  },
                  "contains": [
                    {
                      "@id": "6bcb1f9c-1539-495f-8ec6-e61b2ed76c14",
                      "@type": "pcsm:Crop",
                      "cropVariety": "Horseradish",
                      "agrovocCode": null,
                      "hasPlantDate": null,
                      "hasHarvestDate": null
                    }
                  ]
                },
                {
                  "@id": "3773d981-03c2-449f-8125-befcc7a51a17",
                  "@type": "fsm:Plot",
                  "landType": {
                    "@id": "bf866b4d-d94f-42c9-87b7-05ee868e9e6e",
                    "@type": "obo:ENVO_01001177",
                    "label": "Extensive orchard"
                  },
                  "contains": [
                    {
                      "@id": "74e3ccb6-e4f8-49fb-a7ca-56cdd28054a7",
                      "@type": "pcsm:Crop",
                      "cropVariety": "Eggplant",
                      "agrovocCode": null,
                      "hasPlantDate": null,
                      "hasHarvestDate": null
                    }
                  ]
                }
              ]
            },
            {
              "@id": "cc3044c9-2f65-46b8-8409-15be4a3ebf54",
              "@type": "fsm:FarmSite",
              "name": "ADANO",
              "nationalIdentifier": "4304853",
              "hasArea": {
                "@id": "cc3044c9-2f65-46b8-8409-15be4a3ebf54_FieldArea",
                "@type": "qudt:QuantityValue",
                "numericValue": 4243,
                "unitOfMeasure": "unit:M2"
              },
              "geometry": {
                "@id": "cc3044c9-2f65-46b8-8409-15be4a3ebf54_FieldGeometry",
                "@type": "geosparql:Geometry",
                "geoJson": {
                  "type": "Polygon",
                  "coordinates": [
                    [
                      [
                        16.269178455881022,
                        46.85193649670502
                      ],
                      [
                        16.269338883760827,
                        46.851899631469976
                      ],
                      [
                        16.269357034599658,
                        46.85191305976765
                      ],
                      [
                        16.269901361089136,
                        46.85179276672025
                      ],
                      [
                        16.26990711876121,
                        46.85180615251023
                      ],
                      [
                        16.27001294947707,
                        46.85203717626407
                      ],
                      [
                        16.270054972605326,
                        46.85212869851944
                      ],
                      [
                        16.270060432287526,
                        46.85214334710901
                      ],
                      [
                        16.27013801193615,
                        46.85229960896463
                      ],
                      [
                        16.2702311592189,
                        46.852468158054684
                      ],
                      [
                        16.270248602461166,
                        46.852501611135274
                      ],
                      [
                        16.27023544786922,
                        46.85250265677491
                      ],
                      [
                        16.27018276771056,
                        46.852515341636796
                      ],
                      [
                        16.27018186098663,
                        46.8525213793122
                      ],
                      [
                        16.270185352280603,
                        46.852538748671016
                      ],
                      [
                        16.270164866446287,
                        46.852554224980814
                      ],
                      [
                        16.269972665456066,
                        46.85258397688845
                      ],
                      [
                        16.269929201543672,
                        46.8524841938743
                      ],
                      [
                        16.269824766579543,
                        46.852505432657075
                      ],
                      [
                        16.2698178174083,
                        46.852487121040106
                      ],
                      [
                        16.26966150829725,
                        46.852519640953375
                      ],
                      [
                        16.269670099874016,
                        46.852539202857244
                      ],
                      [
                        16.269514099994513,
                        46.85257372534342
                      ],
                      [
                        16.269385610535878,
                        46.85231545934952
                      ],
                      [
                        16.269384770754662,
                        46.85231376833694
                      ],
                      [
                        16.269178455881022,
                        46.85193649670502
                      ]
                    ],
                    [
                      [
                        16.269526576667747,
                        46.85220171923462
                      ],
                      [
                        16.269490366809773,
                        46.85225110647675
                      ],
                      [
                        16.269545946750167,
                        46.85228912967213
                      ],
                      [
                        16.26960706589266,
                        46.85229232016575
                      ],
                      [
                        16.269636935080538,
                        46.852240979027044
                      ],
                      [
                        16.269603169067924,
                        46.85220766193826
                      ],
                      [
                        16.269526576667747,
                        46.85220171923462
                      ]
                    ],
                    [
                      [
                        16.269517703409374,
                        46.852356601356554
                      ],
                      [
                        16.269561201444347,
                        46.852385537209976
                      ],
                      [
                        16.269618609260867,
                        46.85232862768496
                      ],
                      [
                        16.269572660579662,
                        46.852301428360285
                      ],
                      [
                        16.269517703409374,
                        46.852356601356554
                      ]
                    ]
                  ]
                }
              },
              "contains": [
                {
                  "@id": "0a8da224-e3bc-44f3-91af-341b58ba8011",
                  "@type": "fsm:Plot",
                  "landType": {
                    "@id": "b888f841-4bf1-4958-84c6-e60ce6d24d28",
                    "@type": "obo:ENVO_01001177",
                    "label": "Meadows and pastures"
                  },
                  "contains": [
                    {
                      "@id": "7df12034-72fd-40f3-b12f-f59a02d6aae2",
                      "@type": "pcsm:Crop",
                      "cropVariety": "Wheat (spring)",
                      "agrovocCode": null,
                      "hasPlantDate": null,
                      "hasHarvestDate": null
                    }
                  ]
                }
              ]
            },
            {
              "@id": "6db936ec-a9aa-47b4-a7a9-fb0eb5662fcb",
              "@type": "fsm:FarmSite",
              "name": "ADANO",
              "nationalIdentifier": "4309024",
              "hasArea": {
                "@id": "6db936ec-a9aa-47b4-a7a9-fb0eb5662fcb_FieldArea",
                "@type": "qudt:QuantityValue",
                "numericValue": 5619,
                "unitOfMeasure": "unit:M2"
              },
              "geometry": {
                "@id": "6db936ec-a9aa-47b4-a7a9-fb0eb5662fcb_FieldGeometry",
                "@type": "geosparql:Geometry",
                "geoJson": {
                  "type": "Polygon",
                  "coordinates": [
                    [
                      [
                        16.269514099994513,
                        46.85257372534342
                      ],
                      [
                        16.26955340265962,
                        46.85265277290328
                      ],
                      [
                        16.269598825785007,
                        46.85272720935522
                      ],
                      [
                        16.270275904346278,
                        46.852572200042644
                      ],
                      [
                        16.270299311475284,
                        46.85257216516078
                      ],
                      [
                        16.27031690826869,
                        46.85263714903659
                      ],
                      [
                        16.27052835482446,
                        46.853173148713964
                      ],
                      [
                        16.270456924524975,
                        46.85314983118871
                      ],
                      [
                        16.270192225163072,
                        46.853119392358295
                      ],
                      [
                        16.270026041536372,
                        46.85314030899181
                      ],
                      [
                        16.269877400302047,
                        46.853176639563394
                      ],
                      [
                        16.269634640787864,
                        46.85323133218682
                      ],
                      [
                        16.269043810150194,
                        46.85214373825814
                      ],
                      [
                        16.268951713952013,
                        46.85200014165783
                      ],
                      [
                        16.268950211583416,
                        46.85200038322584
                      ],
                      [
                        16.26894477260572,
                        46.851992166806475
                      ],
                      [
                        16.269178455881022,
                        46.85193649670502
                      ],
                      [
                        16.269384770754662,
                        46.85231376833694
                      ],
                      [
                        16.269385610535878,
                        46.85231545934952
                      ],
                      [
                        16.269514099994513,
                        46.85257372534342
                      ]
                    ]
                  ]
                }
              },
              "contains": [
                {
                  "@id": "00fc1f0c-07a8-4e1a-b9ef-2a6e3508d029",
                  "@type": "fsm:Plot",
                  "landType": {
                    "@id": "61e3a73b-fa33-405d-a76e-ebe6950fa492",
                    "@type": "obo:ENVO_01001177",
                    "label": "Arable land"
                  },
                  "contains": [
                    {
                      "@id": "be2631af-36a5-4d3d-b80c-928d897f4fb2",
                      "@type": "pcsm:Crop",
                      "cropVariety": "Potatoe (early season)",
                      "agrovocCode": null,
                      "hasPlantDate": "2023-05-01T00:00:00",
                      "hasHarvestDate": null
                    }
                  ]
                },
                {
                  "@id": "99f40f16-7723-4f08-b551-c272b146a907",
                  "@type": "fsm:Plot",
                  "landType": {
                    "@id": "61e3a73b-fa33-405d-a76e-ebe6950fa492",
                    "@type": "obo:ENVO_01001177",
                    "label": "Arable land"
                  },
                  "contains": [
                    {
                      "@id": "9e5d43df-f25c-486a-aafe-02bdf1fa3034",
                      "@type": "pcsm:Crop",
                      "cropVariety": "Buckwheat",
                      "agrovocCode": null,
                      "hasPlantDate": null,
                      "hasHarvestDate": null
                    }
                  ]
                },
                {
                  "@id": "e483ba9e-8fc3-405e-b174-2fd576ec5245",
                  "@type": "fsm:Plot",
                  "landType": {
                    "@id": "61e3a73b-fa33-405d-a76e-ebe6950fa492",
                    "@type": "obo:ENVO_01001177",
                    "label": "Arable land"
                  },
                  "contains": [
                    {
                      "@id": "7fbd47c5-2f53-4be3-9df9-33da1590c090",
                      "@type": "pcsm:Crop",
                      "cropVariety": "Crimson clover",
                      "agrovocCode": null,
                      "hasPlantDate": null,
                      "hasHarvestDate": null
                    }
                  ]
                }
              ]
            },
            {
              "@id": "dd9f466d-500a-47b8-a353-112d48f20739",
              "@type": "fsm:FarmSite",
              "name": "ADANO_1",
              "nationalIdentifier": "5854375",
              "hasArea": {
                "@id": "dd9f466d-500a-47b8-a353-112d48f20739_FieldArea",
                "@type": "qudt:QuantityValue",
                "numericValue": 2687,
                "unitOfMeasure": "unit:M2"
              },
              "geometry": {
                "@id": "dd9f466d-500a-47b8-a353-112d48f20739_FieldGeometry",
                "@type": "geosparql:Geometry",
                "geoJson": {
                  "type": "Polygon",
                  "coordinates": [
                    [
                      [
                        16.271353896215,
                        46.85256580836863
                      ],
                      [
                        16.271468015993634,
                        46.852555994031874
                      ],
                      [
                        16.27153374031889,
                        46.85251815356611
                      ],
                      [
                        16.271837101520273,
                        46.852500478556436
                      ],
                      [
                        16.272115902586425,
                        46.85245901025752
                      ],
                      [
                        16.272193401108158,
                        46.852508843770174
                      ],
                      [
                        16.27219216602723,
                        46.8525564936336
                      ],
                      [
                        16.272138962427274,
                        46.85259419531001
                      ],
                      [
                        16.271820143809748,
                        46.852837538575756
                      ],
                      [
                        16.27178879831422,
                        46.85292069908947
                      ],
                      [
                        16.271582893262185,
                        46.85301434572978
                      ],
                      [
                        16.271503009374612,
                        46.85315512780945
                      ],
                      [
                        16.271436341654123,
                        46.85315303455651
                      ],
                      [
                        16.271373548487805,
                        46.853123414112645
                      ],
                      [
                        16.27135490449762,
                        46.85301134569147
                      ],
                      [
                        16.271353896215,
                        46.85256580836863
                      ]
                    ]
                  ]
                }
              },
              "contains": [
                {
                  "@id": "f21173f3-6a93-405d-b6b8-a8af2943ef57",
                  "@type": "fsm:Plot",
                  "landType": {
                    "@id": "b888f841-4bf1-4958-84c6-e60ce6d24d28",
                    "@type": "obo:ENVO_01001177",
                    "label": "Meadows and pastures"
                  },
                  "contains": [
                    {
                      "@id": "1fcf7a5a-31d7-440f-8b10-5cba35a75714",
                      "@type": "pcsm:Crop",
                      "cropVariety": "Bean",
                      "agrovocCode": null,
                      "hasPlantDate": null,
                      "hasHarvestDate": null
                    }
                  ]
                },
                {
                  "@id": "ed8272d5-c91b-4814-a48c-090f6d41eea3",
                  "@type": "fsm:Plot",
                  "landType": {
                    "@id": "b888f841-4bf1-4958-84c6-e60ce6d24d28",
                    "@type": "obo:ENVO_01001177",
                    "label": "Meadows and pastures"
                  },
                  "contains": [
                    {
                      "@id": "6bcb1f9c-1539-495f-8ec6-e61b2ed76c14",
                      "@type": "pcsm:Crop",
                      "cropVariety": "Horseradish",
                      "agrovocCode": null,
                      "hasPlantDate": null,
                      "hasHarvestDate": null
                    }
                  ]
                },
                {
                  "@id": "3af6b9c8-7238-4569-a185-1faef1823042",
                  "@type": "fsm:Plot",
                  "landType": {
                    "@id": "b888f841-4bf1-4958-84c6-e60ce6d24d28",
                    "@type": "obo:ENVO_01001177",
                    "label": "Meadows and pastures"
                  },
                  "contains": [
                    {
                      "@id": "74e3ccb6-e4f8-49fb-a7ca-56cdd28054a7",
                      "@type": "pcsm:Crop",
                      "cropVariety": "Eggplant",
                      "agrovocCode": null,
                      "hasPlantDate": null,
                      "hasHarvestDate": null
                    }
                  ]
                }
              ]
            }
          ]
        }
      ]
    }
  ]
}
```
Response header:  

```
content-type: text/plain; charset=utf-8  
date: Fri,26 Apr 2024 05:17:09 GMT  
request-context: appId=cid-v1:6b2a2081-a5e9-4021-81f5-2cfce7f82dde  
server: Microsoft-IIS/10.0  
transfer-encoding: chunked  
x-powered-by: ASP.NET 
```
 
##### Code 401 

 

**Description**: Error - Unauthorized 

**Response body**: Required authorization header 

**Response header**:  
```
content-type: text/plain; charset=utf-8  
date: Fri,26 Apr 2024 05:17:09 GMT  
request-context: appId=cid-v1:6b2a2081-a5e9-4021-81f5-2cfce7f82dde  
server: Microsoft-IIS/10.0  
transfer-encoding: chunked  
x-powered-by: ASP.NET 
```

 

##### /farms/{farmUUid}/tasks 

The GET request retrieves a list of tasks from the specific farm in in specific time in Farmtopia Digital FieldBook API. 

##### Parameters

**Required**

FarmUuid (string) 

**Optional**

Date from (date-time; YYYY-MM-DD) 

Date to (date-time; YYYY-MM-DD) 

**Response** 

##### Curl 

 
```
curl -X 'GET' \ 
  'https://farmtopia-fb-api.dih-agrifood.com/farms/01b7766b-a6f5-4a2e-a752-9ebf20183ac9/tasks?dateFrom=2024-01-26&dateTo=2024-04-26' \ 
  -H 'accept: text/plain' \ 
  -H 'Authorization: Bearer APIKEY' 
```
 
Request URL 

https://farmtopia-fb-api.dih-agrifood.com/farms/01b7766b-a6f5-4a2e-a752-9ebf20183ac9/tasks?dateFrom=2024-01-26&dateTo=2024-04-26 

 

##### Code 200 

**Description**: Successful 

**Media type**: text/plain, application/json, text/json 

The response is a JSON array containing task details, including UUID, task type, date range, notes, and field details.  

The response of the given request contains the following fields: 

- uuid: A unique identifier for the task. 
- taskType: An object containing the id, name, and nameEn of the task type. 
- dateFrom: The start date of the task. 
- dateTo: The end date of the task. 
- note: Additional notes or details related to the task. 
- fields: An array of objects, each representing a field involved in the task, with fieldUuid, fieldIdentifier, and fieldName attributes. 

 
```json
{
  "@context": {
    "fsm": "https://gitlab.com/Farmtopia/farmtopia-semantic-model/-/raw/main/FarmtopiaSemanticModel.rdf#",
    "pcsm": "https://www.tno.nl/agrifood/ontology/ploutos/pcsm#",
    "operatedOn": {
      "@id": "pcsm:isOperatedOn",
      "@type": "@id"
    },
    "label": "fsm:label",
    "startDatetime": "pcsm:hasStartDatetime",
    "endDatetime": "pcsm:hasEndDatetime",
    "operationNotes": "fsm:operationNotes",
    "sliceContents": {
      "@id": "fsm:sliceContents",
      "@type": "@json",
      "@container": "@set"
    },
    "datasetSlices": {
      "@id": "fsm:datasetSlices",
      "@type": "@json",
      "@container": "@set"
    }
  },
  "@graph": [
    {
      "@id": "ITC_tasks_serviceResource_representation_instance_43421",
      "@type": "fsm:Dataset",
      "datasetSlices": [
        {
          "@id": "TasksSlice_1",
          "@type": "fsm:Slice",
          "@sliceContents": [
            {
              "@id": "b9ba775f-6558-4386-9fb4-bdb42b8452c4",
              "@type": "pcsm:CropProtectionOperation",
              "label": "Removal of invasive plants",
              "startDatetime": "2024-04-24T00:00:00",
              "endDatetime": "2024-04-24T00:00:00",
              "operationNotes": null,
              "operatedOn": {
                "@id": "f258b3a1-ae7c-4ff5-acaa-fd5d51b845bd"
              }
            }
          ]
        }
      ]
    }
  ]
}
```
 
Response header:  

```
content-type: application/json;  
charset=utf-8 date: Fri,26 Apr 2024 06:47:58 GMT  
request-context: appId=cid-v1:6b2a2081-a5e9-4021-81f5-2cfce7f82dde  
server: Microsoft-IIS/10.0  
transfer-encoding: chunked  
x-powered-by: ASP.NET 
```
 

##### Code 401 

 

**Description**: Error - Unauthorized 

Response body: Required authorization header 

Response header:  

```
content-type: text/plain; charset=utf-8  
date: Fri,26 Apr 2024 05:17:09 GMT  
request-context: appId=cid-v1:6b2a2081-a5e9-4021-81f5-2cfce7f82dde  
server: Microsoft-IIS/10.0  
transfer-encoding: chunked  
x-powered-by: ASP.NET 
```
 

 