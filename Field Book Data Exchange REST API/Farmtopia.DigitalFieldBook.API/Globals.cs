﻿using Farmtopia.DigitalFieldBook.API.Models.Providers;
using Farmtopia.DigitalFieldBook.API.Repository;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Farmtopia.DigitalFieldBook.API
{
    public sealed class Globals
    {
        private static readonly Lazy<Globals> instance = new Lazy<Globals>(() => new Globals());

        public static Globals Instance
        {
            get
            {
                return instance.Value;
            }
        }

        //public static void Initialize(IConfiguration configuration, DbContext ctx)
        //{
        //    ProviderId = configuration.GetValue<int?>("ProviderId");

        //    if(ProviderId != null)
        //    {
        //        var providerRepository = new ProviderRepository(ctx);
        //        Settings = providerRepository.GetProviderSettings(ProviderId.Value);
        //    }
        //}

        public static int? ProviderId { get; set; }
    }
}
