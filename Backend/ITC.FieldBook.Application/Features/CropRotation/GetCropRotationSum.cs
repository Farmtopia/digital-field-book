﻿using ITC.FieldBook.Application.Infrastructure.Interfaces;
using ITC.FieldBook.Application.Infrastructure.Tools;
using ITC.FieldBook.Application.Infrastructure;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using ITC.FieldBook.Application.Models.Reports;
using ITC.FieldBook.Application.Features.Fields;
using ITC.FieldBook.Application.Features.Farms;
using ITC.FieldBook.Application.Features.Crops;
using ITC.FieldBook.Application.Features.Fields.Crops;
using ITC.FieldBook.Application.Infrastructure.Models;
using Microsoft.VisualBasic;
using Microsoft.Extensions.Azure;
using ITC.FieldBook.Application.Features.Users;
using System.ComponentModel.DataAnnotations;
using static ITC.FieldBook.Application.Features.CropRotation.GetCropRotationSumQueryHandler;
using ITC.FieldBook.Application.Models.DTO;

namespace ITC.FieldBook.Application.Features.CropRotation
{
    public class GetCropRotationSumController : ApiControllerBase
    {
        [HttpGet("crop-rotation/sum")]
        public Task<CropRotationSumDto> GetCropRotationSum([FromQuery] GetCropRotationSumQuery query)
        {
            return Mediator.Send(query);
        }
    }

    public class GetCropRotationSumQuery : IRequest<CropRotationSumDto>
    {
        [Required]
        public int? CropType { get; set; }
    }

    internal sealed class GetCropRotationSumQueryHandler : IRequestHandler<GetCropRotationSumQuery, CropRotationSumDto>
    {
        private readonly DbContext _ctx;
        private readonly IConfiguration _configuration;
        private readonly ICurrentUserService _currentUserService;

        public GetCropRotationSumQueryHandler(DbContext ctx, IConfiguration configuration, ICurrentUserService currentUserService)
        {
            _ctx = ctx;
            _configuration = configuration;
            _currentUserService = currentUserService;
        }
                
        public Task<CropRotationSumDto> Handle(GetCropRotationSumQuery request, CancellationToken cancellationToken)
        {
            var data = new CropRotationSumDto();
            var farmId = _currentUserService.FarmId!.Value;
            var cropType = request.CropType!.Value;

            var fieldsRepo = new FieldsRepository(_ctx);
            var userRepository = new UsersRepository(_ctx);

            var years = new List<int>();
            for (var i = DateTime.Now.Year - 1; i < DateTime.Now.Year + 5; i++)
            {
                years.Add(i);
            }

            var fieldTypes = fieldsRepo.GetFieldTypes().Data.Where(x => x.IncludeInCropRotation);
            var cropRorationFieldTypeIds = fieldTypes.Select(x => x.Id!.Value).ToList();

            var fields = fieldsRepo.GetFields(farmId).Data.Where(x => x.TypeId != null && cropRorationFieldTypeIds.Contains(x.TypeId.Value)).ToList();          
            var cropRotationFieldIds = fields.Select(x => x.Id!.Value).ToList();

            data.HasCropRotation = fields.Count > 0;

            if (!data.HasCropRotation)
                return Task.FromResult(data);

            var fieldPlots = fieldsRepo.GetFieldPlots(farmId, years.FirstOrDefault(), years.LastOrDefault(), cropType).Data.Where(x => x.FieldId != null && cropRotationFieldIds.Contains(x.FieldId.Value)).OrderBy(x => x.CropName);
            var userCropSettings = userRepository.GetUserCropSettings(_currentUserService.Id).Data.Where(x => !string.IsNullOrWhiteSpace(x.HexColor));
            
            var cropTypes = fieldPlots.Where(x => x.CropType != null).Select(x => x.CropType!.Value).Distinct().ToList();
            var cropsDistinct = fieldPlots.GroupBy(x => x.CropId).Select(x =>
            {
                var first = x.First();
                return new CropRotationSumCropDto
                {
                    CropId = first.CropId,
                    CropIdentifier = first.CropIdentifier, 
                    CropName = first.CropName,
                    CropNameEn = first.CropNameEn,
                    HexColor = userCropSettings.FirstOrDefault(cs => cs.CropId == first.CropId)?.HexColor
                };
            }).OrderBy(x=>x.CropName).ToList();

            foreach (var y in years)
            {
                var sum = new CropRotationYearSumDto();
                sum.SumAll = fields.Sum(x => Math.Floor(x.Area ?? 0));
                sum.SumAssigned = fieldPlots.Where(x => x.Year == y).Sum(x => Math.Floor(x.Area ?? 0));
                data.YearSum.Add(y, sum);
            }

            foreach (var c in cropsDistinct)
            {
                foreach (var y in years)
                {                    
                    var yearlyCropStatus = new CropRotationCropYearSumDto();
                    yearlyCropStatus.Year = y;

                    var fieldPlotsYear = fieldPlots.Where(x => x.CropId == c.CropId && x.Year == y).ToList();
                    yearlyCropStatus.Sum = fieldPlotsYear.Sum(x => Math.Floor(x.Area ?? 0));

                    if(yearlyCropStatus.Sum != 0)
                        yearlyCropStatus.Percentage = Math.Round(yearlyCropStatus.Sum * 100 / data.YearSum[y].SumAll, 2);
                    c.Years.Add(y, yearlyCropStatus);
                }
            }

            data.Crops = cropsDistinct;
            data.Years = years;

            return Task.FromResult(data);
        }
    }
}
