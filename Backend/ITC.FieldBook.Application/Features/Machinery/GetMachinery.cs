﻿using ITC.FieldBook.Application.Infrastructure.Interfaces;
using ITC.FieldBook.Application.Infrastructure;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata;
using Dapper;
using ITC.FieldBook.Application.Models;
using ITC.FieldBook.Application.Infrastructure.Models;

namespace ITC.FieldBook.Application.Features.Machinery
{
    public class GetMachineryController : ApiControllerBase
    {
        [HttpGet("machinery")]
        public Task<DbData<Models.Machinery>> GetMachinery()
        {
            return Mediator.Send(new GetMachineryQuery { });
        }
    }

    public class GetMachineryQuery : IRequest<DbData<Models.Machinery>>
    {
    }

    internal sealed class GetMachineryQueryHandler : IRequestHandler<GetMachineryQuery, DbData<Models.Machinery>>
    {
        private readonly DbContext _ctx;
        private readonly IConfiguration _configuration;
        private readonly ICurrentUserService _currentUserService;

        public GetMachineryQueryHandler(DbContext ctx, IConfiguration configuration, ICurrentUserService currentUserService)
        {
            _ctx = ctx;
            _configuration = configuration;
            _currentUserService = currentUserService;
        }

        public Task<DbData<Models.Machinery>> Handle(GetMachineryQuery request, CancellationToken cancellationToken)
        {
            var machineryRepository = new MachineryRepository(_ctx);
            var data = machineryRepository.GetMachinery(_currentUserService.FarmId!.Value);

            return Task.FromResult(data);
        }
    }
}
