﻿using ITC.FieldBook.Application.Infrastructure.Models;
using Microsoft.AspNetCore.DataProtection.KeyManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Dapper.SqlMapper;

namespace ITC.FieldBook.Application.Models
{
    public class MachineryClassification : DbEntityWithAudit
    {
        public MachineryClassification() : base("FieldBook.machinery_classification") { }

        private int? _id;
        [DbColumn("id", IsKey = true, Identity = true)]
        public int? Id { get => _id; set { _id = value; RaisePropertyChanged(nameof(Id)); } }

        private string? _code;
        [DbColumn("code")]
        public string? Code { get => _code; set { _code = value; RaisePropertyChanged(nameof(Code)); } }

        private string? _parentCode;
        [DbColumn("parent_code")]
        public string? ParentCode { get => _parentCode; set { _parentCode = value; RaisePropertyChanged(nameof(ParentCode)); } }

        private string? _name;
        [DbColumn("name")]
        public string? Name { get => _name; set { _name = value; RaisePropertyChanged(nameof(Name)); } }

        public List<MachineryClassification> Parents { get; set; } = new List<MachineryClassification>();

        public bool HasChildren { get; set; } = false;

        public string? DisplayName { get => $"{Code} {Name}" + (Parents.Count > 0 ? $"({string.Join(" | ", Parents.Select(x => $"{x.Code} {x.Name}"))})" : ""); }
    }
}
