using ITC.FieldBook.API.Middleware;
using ITC.FieldBook.Application;
using ITC.FieldBook.Application.Infrastructure.Interfaces;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System.Diagnostics;
using System.Globalization;
using System.Text;
using CustomRequestCultureProvider = ITC.FieldBook.API.Middleware.CustomRequestCultureProvider;

namespace ITC.FieldBook.API
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            // Initialize global settings
            Globals.Initialize(builder.Configuration);


            // Add CORS
            builder.Services.AddCors(options =>
            {
                options.AddPolicy(name: "CorsPolicy",
                    policyBuilder =>
                    {
                        policyBuilder
                        .AllowAnyOrigin()
                        .AllowAnyHeader()
                        .AllowAnyMethod();
                    });
            });

            // Add services to the container.
            builder.Services
                .AddApplication()
                .AddInfrastructure();

            builder.Services.AddControllers();
            builder.Services.AddHttpContextAccessor();

            // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
            builder.Services.AddEndpointsApiExplorer();
            builder.Services.AddSwaggerGen(c =>
            {
                c.DescribeAllParametersInCamelCase();

                c.TagActionsBy(api =>
                {
                    if (api.GroupName != null)
                    {
                        return new[] { api.GroupName };
                    }
                    if (api.ActionDescriptor is ControllerActionDescriptor controllerActionDescriptor)
                    {
                        var ns = controllerActionDescriptor.MethodInfo?.DeclaringType?.Namespace;
                        return new[] { ns ?? controllerActionDescriptor.ControllerName };
                    }
                    throw new InvalidOperationException("Unable to determine tag for endpoint.");
                });
            });

            // Po vpisu v AzureAD, koristimo na� token za nadaljnjo avtentikacijo.
            builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme).AddJwtBearer(/*"JwtTokenAuth"*/JwtBearerDefaults.AuthenticationScheme, options =>
            {
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true,
                    ValidIssuer = builder.Configuration.GetValue<string>("Jwt:Issuer"),
                    ValidAudience = builder.Configuration.GetValue<string>("Jwt:Issuer"),
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(builder.Configuration.GetValue<string>("Jwt:Key")))
                };

                // Add event to check for token in cookie
                options.Events = new JwtBearerEvents
                {
                    OnMessageReceived = context =>
                    {
                        // Check if token is present in Authorization header
                        var token = context.Request.Headers["Authorization"].FirstOrDefault()?.Split(" ").Last();

                        // If not present in header, check cookie
                        if (string.IsNullOrEmpty(token))
                            token = context.Request.Cookies["jwt_token"];
                        
                        // If not present in header or cookie, check query parameters
                        if (string.IsNullOrEmpty(token))
                            token = context.Request.Query["jwt_token"].FirstOrDefault();

                        context.Token = token;

                        return Task.CompletedTask;
                    }
                };
            });

            //builder.Services.AddAuthorization();
            builder.Services.Configure<ApiBehaviorOptions>(x => x.InvalidModelStateResponseFactory = ctx => new ValidationErrorResult());

            // Localization
            builder.Services.AddLocalization();

            var app = builder.Build();

            //// Define the supported cultures
            //var supportedCultures = new[] { new CultureInfo("en-US"), new CultureInfo("sl") };
            //// Configure the Request Localization options
            //var requestLocalizationOptions = new RequestLocalizationOptions
            //{
            //    DefaultRequestCulture = new RequestCulture("sl"),  // Default culture
            //    SupportedCultures = supportedCultures,
            //    SupportedUICultures = supportedCultures
            //};

            // Add the Request Localization middleware
            app.UseRequestLocalization((options) =>
            {
                var supportedCultures = new List<CultureInfo>
                    {
                        new CultureInfo("en-US"),
                        new CultureInfo("sl"),
                    };

                options.DefaultRequestCulture = new RequestCulture(culture: "sl", uiCulture: "sl");
                options.SupportedCultures = supportedCultures;
                options.SupportedUICultures = supportedCultures;
                options.RequestCultureProviders.Insert(0, new CustomRequestCultureProvider());
            });

            app.UseRouting();

            // Allow Cors
            app.UseCors("CorsPolicy");

            // Initialize temp file service
            var tempService = app.Services.GetService<ITempFileService>();
            if(tempService != null)
                tempService.Initialize();

            // Configure the HTTP request pipeline.
            app.UseSwagger();
            app.UseSwaggerUI();

            app.UseMiddleware<ExceptionMiddleware>();
            app.UseHttpsRedirection();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseMiddleware<AuthorizeFarmMiddleware>();

            app.MapControllers();

            app.Run();
        }
    }
}