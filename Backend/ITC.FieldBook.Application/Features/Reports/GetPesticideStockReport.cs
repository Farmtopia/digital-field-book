﻿using ITC.FieldBook.Application.Infrastructure.Interfaces;
using ITC.FieldBook.Application.Infrastructure.Tools;
using ITC.FieldBook.Application.Infrastructure;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using ITC.FieldBook.Application.Models;
using ITC.FieldBook.Application.Models.Reports;
using ITC.FieldBook.Application.Features.Fields;
using ITC.FieldBook.Application.Features.Farms;
using ITC.FieldBook.Application.Features.Crops;
using ITC.FieldBook.Application.Features.Fields.Crops;
using ITC.FieldBook.Application.Infrastructure.Models;

namespace ITC.FieldBook.Application.Features.Reports
{
    public class GetPesticideStockReportController : ApiControllerBase
    {
        [HttpGet("reports/stock/pesticides")]
        public Task<DbData<PesticidesStockReportItem>> GetPesticideStockReport(DateTime? date)
        {
            return Mediator.Send(new GetPesticideStockReportQuery { Date = date });
        }
    }

    public class GetPesticideStockReportQuery : IRequest<DbData<PesticidesStockReportItem>>
    {

        public DateTime? Date { get; set; }
    }

    internal sealed class GetPesticideStockReportQueryHandler : IRequestHandler<GetPesticideStockReportQuery, DbData<PesticidesStockReportItem>>
    {
        private readonly DbContext _ctx;
        private readonly IConfiguration _configuration;
        private readonly ICurrentUserService _currentUserService;

        public GetPesticideStockReportQueryHandler(DbContext ctx, IConfiguration configuration, ICurrentUserService currentUserService)
        {
            _ctx = ctx;
            _configuration = configuration;
            _currentUserService = currentUserService;
        }

        public Task<DbData<PesticidesStockReportItem>> Handle(GetPesticideStockReportQuery request, CancellationToken cancellationToken)
        {
            var date = request.Date ?? DateTime.Now;
            var data = _ctx.Get<PesticidesStockReportItem>(@"                
                SELECT
                    p.id,
	                p.name,
	                p.name_en,
	                p.brand_name,
	                s.stock,
	                s.stock_unit
                FROM FieldBook.fn_pesticide_stock(@farmId, NULL, @date, null) s
	                JOIN FieldBook.pesticide p ON p.id = s.pesticide_id
                ", new { farmId = _currentUserService.FarmId, date });

            return Task.FromResult(data);
        }
    }
}
