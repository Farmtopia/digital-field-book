﻿using Swashbuckle.AspNetCore.Annotations;
using System.Text.Json.Serialization;

namespace Farmtopia.DigitalFieldBook.API.Models.JSON_LD
{
    public class JsonLdBase
    {
        [SwaggerSchema(Nullable = false)]
        [JsonPropertyOrder(-1)]
        [JsonPropertyName("@id")]
        public string Id { get; set; }

        [SwaggerSchema(Nullable = false)]
        [JsonPropertyOrder(-1)]
        [JsonPropertyName("@type")]
        public string Type { get; set; }
    }
}
