﻿using System.Text.Json.Serialization;

namespace Farmtopia.DigitalFieldBook.API.Models.JSON_LD
{
    public class ResourceQuantityUsed : JsonLdBase
    {
        [JsonPropertyName("numericValue")]
        public string? NumericValue { get; set; }

        [JsonPropertyName("unitOfMeasure")]
        public UnitOfMeasure UnitOfMeasure { get; set; }
    }
}
