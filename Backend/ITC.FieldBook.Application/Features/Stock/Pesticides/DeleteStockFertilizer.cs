﻿using ITC.FieldBook.Application.Infrastructure.Models;
using ITC.FieldBook.Application.Infrastructure;
using MediatR;
using Microsoft.AspNetCore.DataProtection.KeyManagement;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using ITC.FieldBook.Application.Models;
using ITC.FieldBook.Application.Infrastructure.Services;
using ITC.FieldBook.Application.Infrastructure.Interfaces;
using System.ComponentModel.DataAnnotations;

namespace ITC.FieldBook.Application.Features.Stock.Pesticides
{
    public class DeleteStockPesticideController : ApiControllerBase
    {
        [HttpDelete("stock/pesticides/{stockPesticideId}")]
        public Task DeleteStockPesticide([FromRoute] int? stockPesticideId)
        {
            return Mediator.Send(new DeleteStockPesticideCommand { StockPesticideId = stockPesticideId });
        }
    }

    public class DeleteStockPesticideCommand : IRequest
    {
        [Required]
        public int? StockPesticideId { get; set; }
    }

    internal sealed class DeleteStockPesticideCommandHandler : IRequestHandler<DeleteStockPesticideCommand>
    {
        private readonly DbContext _ctx;
        private readonly ICurrentUserService _currentUserService;

        public DeleteStockPesticideCommandHandler(DbContext ctx, ICurrentUserService currentUserService)
        {
            _ctx = ctx;
            _currentUserService = currentUserService;
        }

        public Task Handle(DeleteStockPesticideCommand request, CancellationToken cancellationToken)
        {
            var stockRepository = new StockRepository(_ctx);
            var stockPesticide = stockRepository.GetStockPesticide(request.StockPesticideId!.Value, _currentUserService.FarmId!.Value).Item;
            if (stockPesticide != null)
            {
                var user = _currentUserService.GetUser()!;
                _ctx.DeleteItem(stockPesticide, user);
            }

            return Task.CompletedTask;
        }
    }
}

