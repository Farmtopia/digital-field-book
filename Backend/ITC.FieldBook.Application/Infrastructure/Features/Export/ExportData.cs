﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using ITC.FieldBook.Application.Models;
using Dapper;
using System.Transactions;
using System.ComponentModel.DataAnnotations;
using ITC.FieldBook.Application.Infrastructure.Tools;
using System.Data;
using Microsoft.AspNetCore.Http;
using ITC.FieldBook.Application.Infrastructure.Interfaces;
using ITC.FieldBook.Application.Infrastructure.Services;

namespace ITC.FieldBook.Application.Infrastructure.Features.Export
{
    public class ExportController : ApiControllerBase
    {
        [HttpPost("export-data")]
        public Task<Guid?> ExportData([FromBody] ExportCommand command)
        {
            return Mediator.Send(command);
        }
    }

    public class ExportCommand : IRequest<Guid?>
    {
        [Required]
        public List<ExportColumn>? Columns { get; set; }

        [Required]
        public List<Dictionary<string, object>>? Rows { get; set; }
    }

    public class ExportColumn
    {
        public string? Name { get; set; }

        public string? Label { get; set; }
    }

    internal sealed class SaveUnitCommandHandler : IRequestHandler<ExportCommand, Guid?>
    {
        private ITempFileService _tempFileService;

        public SaveUnitCommandHandler(ITempFileService tempFileService)
        {
            _tempFileService = tempFileService;
        }

        public Task<Guid?> Handle(ExportCommand request, CancellationToken cancellationToken)
        {
            var exportManager = new ExcelExportManager();
            var dt = new DataTable();

            foreach (var c in request.Columns!)
            {
                dt.Columns.Add(c.Label);
            }

            foreach(var r in request.Rows!)
            {
                var rowData = new List<object?>();
                foreach (var c in request.Columns!)
                {

                    rowData.Add(c.Name != null && r.ContainsKey(c.Name) ? r[c.Name] : null);
                }
                
                dt.Rows.Add(rowData.ToArray());
            }

            var bytes = exportManager.Export(dt);
            var id = _tempFileService.SaveFile($"Export_{DateTime.Now:dd.MM.yyyy HHmmss}.xlsx", bytes);

            return Task.FromResult(id);
        }
    }
}
