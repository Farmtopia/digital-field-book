import enUS from './en-US';
import slSI from './sl';

export default {
  'en-US': enUS,
  sl: slSI,
};
