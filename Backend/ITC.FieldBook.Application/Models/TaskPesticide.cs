﻿using ITC.FieldBook.Application.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITC.FieldBook.Application.Models
{
    public class TaskPesticide : DbEntityWithAudit
    {
        public TaskPesticide() :base("FieldBook.task_pesticide")
        {

        }

        private int? _id;
        [DbColumn("id", IsKey = true, Identity = true)]
        public int? Id { get => _id; set { _id = value; RaisePropertyChanged(nameof(Id)); } }

        private int? _taskId;
        [DbColumn("task_id")]
        public int? TaskId { get => _taskId; set { _taskId = value; RaisePropertyChanged(nameof(TaskId)); } }

        private int? _pesticideId;
        [DbColumn("pesticide_id", IsKey = true)]
        public int? PesticideId { get => _pesticideId; set { _pesticideId = value; RaisePropertyChanged(nameof(PesticideId)); } }

        private decimal? _amount;
        [DbColumn("amount")]
        public decimal? Amount { get => _amount; set { _amount = value; RaisePropertyChanged(nameof(Amount)); } }

        private int? _amountUnitId;
        [DbColumn("amount_unit_id")]
        public int? AmountUnitId { get => _amountUnitId; set { _amountUnitId = value; RaisePropertyChanged(nameof(AmountUnitId)); } }

        private string? _amountUnit;
        [DbColumn("amount_unit")]
        public string? AmountUnit { get => _amountUnit; set { _amountUnit = value; RaisePropertyChanged(nameof(AmountUnit)); } }

        private decimal? _amountUsed;
        [DbColumn("amount_used")]
        public decimal? AmountUsed { get => _amountUsed; set { _amountUsed = value; RaisePropertyChanged(nameof(AmountUsed)); } }

        private int? _amountUsedUnitId;
        [DbColumn("amount_used_unit_id")]
        public int? AmountUsedUnitId { get => _amountUsedUnitId; set { _amountUsedUnitId = value; RaisePropertyChanged(nameof(AmountUsedUnitId)); } }

        private string? _amountUsedUnit;
        [DbColumn("amount_used_unit")]
        public string? AmountUsedUnit { get => _amountUsedUnit; set { _amountUsedUnit = value; RaisePropertyChanged(nameof(AmountUsedUnit)); } }

    }
}
