﻿using System.Text.Json.Serialization;

namespace Farmtopia.DigitalFieldBook.API.Models.JSON_LD.Fields
{
    public class FieldsDatasetSlice : DatasetSliceBase
    {
        [JsonPropertyName("@sliceContents")]
        public List<FieldsSliceContent> SliceContents { get; set; }
    }
}
