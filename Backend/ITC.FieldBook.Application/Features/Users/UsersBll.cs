﻿using ITC.FieldBook.Application.Features.Farms;
using ITC.FieldBook.Application.Infrastructure;
using ITC.FieldBook.Application.Infrastructure.Models;
using ITC.FieldBook.Application.Models;
using Microsoft.AspNetCore.Authentication.Cookies;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace ITC.FieldBook.Application.Features.Users
{
    public class UsersBll
    {
        private readonly DbContext _ctx;
        private readonly UsersRepository _userRepo;

        public UsersBll(DbContext ctx) {
            _ctx = ctx;
            _userRepo = new UsersRepository(_ctx);
        }
        //public ClaimsIdentity GetClaims(User user, Farm? farm)
        //{
        //    var claims = new List<Claim>
        //    {
        //        new Claim(ClaimTypes.Name, user.Displayname ?? ""),
        //        new Claim(ClaimTypes.NameIdentifier, user.Id.ToString() ?? ""),
        //        new Claim(ClaimTypes.GivenName, user.Name ?? ""),
        //        new Claim(ClaimTypes.Surname, user.Surname ?? ""),
        //        new Claim(ClaimTypes.Email, user.Email ?? ""),
        //        new Claim(ClaimTypes.Role, user.Role ?? ""),
        //        new Claim("Uuid", user.Uuid?.ToString() ?? ""),
        //        new Claim("FarmId", farm?.Id?.ToString() ?? ""),
        //    };

        //    var claimsIdentity = new ClaimsIdentity(
        //    claims, CookieAuthenticationDefaults.AuthenticationScheme, ClaimTypes.Name, ClaimTypes.Role);

        //    return claimsIdentity;
        //}

        public EnsureUserResult EnsureUser(Guid uuid, User newUser)
        {
            var isNew = false;
            var user = _userRepo.GetUserByUuid(uuid, true);

            // Check if user alredy exists and is inactive
            if(user != null && !user.IsActive)
            {
                if (!user.IsActive)
                    return new EnsureUserResult(null, false);
            }

            // If displayname is not set on Azure AD B2C, we set it...
            if (string.IsNullOrWhiteSpace(newUser.Displayname))
            {
                newUser.Displayname = newUser.Name;
                if (!string.IsNullOrWhiteSpace(newUser.Surname))
                    newUser.Displayname += $" {newUser.Surname}";
            }

            // Create/update user
            if (user != null)
            {
                var userId = user.Id!.Value;
                _userRepo.UpdateUserData(newUser); // Update with new data from Azure AD B2C
                user = _userRepo.GetUserById(userId);
            }
            else
            {
                var userId = _userRepo.CreateUser(newUser);
                user = _userRepo.GetUserById(userId);
                isNew = true;
            }

            return new EnsureUserResult(user, isNew);
        }

        //public void LoadAdditionalUserData(ref User? user)
        //{
        //    if (user == null)
        //        return;

        //    var u = user;
        //    var farmsRepo = new FarmsRepository(_ctx);
        //    user.Farms = farmsRepo.GetFarmsForUser(u.Id!.Value).Data.ToList();

        //    if (u.Farms.Any()) {
        //        if (u.FarmId != null)
        //            u.Farm = user.Farms.FirstOrDefault(x => x.Id == u.FarmId);
        //        else
        //            u.Farm = user.Farms.FirstOrDefault();
        //    }
        //}
    }
}
