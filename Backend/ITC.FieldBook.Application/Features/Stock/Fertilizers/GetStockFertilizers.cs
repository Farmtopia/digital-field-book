﻿using ITC.FieldBook.Application.Infrastructure.Interfaces;
using ITC.FieldBook.Application.Infrastructure;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata;
using Dapper;
using ITC.FieldBook.Application.Models;
using ITC.FieldBook.Application.Infrastructure.Models;

namespace ITC.FieldBook.Application.Features.Stock.Fertilizers
{
    public class GetStockFertilizersController : ApiControllerBase
    {
        [HttpGet("stock/fertilizers")]
        public Task<DbData<StockFertilizer>> GetStockFertilizers()
        {
            return Mediator.Send(new GetStockFertilizersQuery { });
        }
    }

    public class GetStockFertilizersQuery : IRequest<DbData<StockFertilizer>>
    {
    }

    internal sealed class GetStockFertilizersQueryHandler : IRequestHandler<GetStockFertilizersQuery, DbData<StockFertilizer>>
    {
        private readonly DbContext _ctx;
        private readonly IConfiguration _configuration;
        private readonly ICurrentUserService _currentUserService;

        public GetStockFertilizersQueryHandler(DbContext ctx, IConfiguration configuration, ICurrentUserService currentUserService)
        {
            _ctx = ctx;
            _configuration = configuration;
            _currentUserService = currentUserService;
        }

        public Task<DbData<StockFertilizer>> Handle(GetStockFertilizersQuery request, CancellationToken cancellationToken)
        {
            var stockRepository = new StockRepository(_ctx);
            var data = stockRepository.GetStockFertilizers(_currentUserService.FarmId!.Value);

            return Task.FromResult(data);
        }
    }
}
