﻿using Swashbuckle.AspNetCore.Annotations;
using System.Text.Json.Serialization;

namespace Farmtopia.DigitalFieldBook.API.Models.JSON_LD.Farms
{
    public class FarmsSliceContent : SliceContentBase
    {
        [JsonPropertyName("centroid")]
        public Centroid Centroid { get; set; }

        [JsonPropertyName("isEcoFarm")]
        public bool IsEcoFarm { get; set; }
    }
}
