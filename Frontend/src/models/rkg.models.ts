export interface RkgKmg {
  kmg_mid?: number;
  domace_ime?: string;
  SubjectNosilec?: RkgKmgNosilec;
  NaslovObj?: RkgKmgNaslov;
  gerks?: RkgKmgGerk[];
}

export interface RkgKmgNosilec {
  naziv?: string;
  ime?: string;
  priimek?: string;
  ds?: number;
  emso?: string;
}

export interface RkgKmgNaslov {
  drzava?: string;
  obcina?: string;
  naselje?: string;
  hs?: string;
  hd?: string;
  postna_stevilka?: number;
  posta?: string;
}

export interface RkgKmgGerk {
  kmg_mid?: number;
  gerk_pid?: number;
  raba_id?: number;
  raba_opis?: string;
  domace_ime?: string;
  drzava?: string;
  area?: number;
  area_acres?: number;
}
