﻿using ITC.FieldBook.Application.Infrastructure.Interfaces;
using ITC.FieldBook.Application.Infrastructure;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata;
using Dapper;
using ITC.FieldBook.Application.Models;
using ITC.FieldBook.Application.Infrastructure.Models;
using ITC.FieldBook.Application.Infrastructure.Services;
using ITC.FieldBook.Application.Infrastructure.Enums;
using System.ComponentModel.DataAnnotations;

namespace ITC.FieldBook.Application.Features.Farms
{
    public class GetFarmController : ApiControllerBase
    {
        [HttpGet("farms/{farmId}")]
        public Task<DbItem<Farm>> GetFarm([FromRoute] int? farmId, bool loadThumb = false)
        {
            return Mediator.Send(new GetFarmQuery { FarmId = farmId, LoadThumb = loadThumb });
        }
    }

    public class GetFarmQuery : IRequest<DbItem<Farm>>
    {
        [Required]
        public int? FarmId { get; set; }

        public bool LoadThumb { get; set; }
    }

    internal sealed class GetFarmQueryHandler : IRequestHandler<GetFarmQuery, DbItem<Farm>>
    {
        private readonly DbContext _ctx;
        private readonly IConfiguration _configuration;
        private readonly ICurrentUserService _currentUserService;
        private readonly IFileService _fileService;

        public GetFarmQueryHandler(DbContext ctx, IConfiguration configuration, ICurrentUserService currentUserService, IFileService fileService)
        {
            _ctx = ctx;
            _configuration = configuration;
            _currentUserService = currentUserService;
            _fileService = fileService;
        }

        public Task<DbItem<Farm>> Handle(GetFarmQuery request, CancellationToken cancellationToken)
        {
            var user = _currentUserService.GetUser()!;
            var farmsRepo = new FarmsRepository(_ctx);
            var data = farmsRepo.GetFarm(request.FarmId!.Value, user.HasRole(EnumRole.Admin) ? null : user.Id!.Value);

            if (request.LoadThumb && data.Item is Farm f && f.ThumbFileId == null && f.Latitude != null && f.Longitude != null)
            {
                var farmsBll = new FarmsBll(_ctx);
                data.Item.ThumbFileId = farmsBll.UpdateImage(_configuration, _fileService, data.Item.Id!.Value, f.Latitude.Value, f.Longitude.Value);
            }

            return Task.FromResult(data);
        }        
    }
}
