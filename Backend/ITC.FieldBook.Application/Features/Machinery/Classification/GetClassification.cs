﻿using ITC.FieldBook.Application.Infrastructure.Interfaces;
using ITC.FieldBook.Application.Infrastructure;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata;
using Dapper;
using ITC.FieldBook.Application.Models;
using ITC.FieldBook.Application.Infrastructure.Models;

namespace ITC.FieldBook.Application.Features.Machinery.Classification
{
    public class GetMachineryClassificationController : ApiControllerBase
    {
        [HttpGet("machinery/classification")]
        public Task<DbData<MachineryClassification>> GetMachineryClassification(bool flat)
        {
            return Mediator.Send(new GetMachineryClassificationQuery { Flat = flat });
        }
    }

    public class GetMachineryClassificationQuery : IRequest<DbData<MachineryClassification>>
    {
        public bool Flat { get; set; }
    }

    internal sealed class GetMachineryClassificationQueryHandler : IRequestHandler<GetMachineryClassificationQuery, DbData<MachineryClassification>>
    {
        private readonly DbContext _ctx;

        public GetMachineryClassificationQueryHandler(DbContext ctx)
        {
            _ctx = ctx;
        }

        public Task<DbData<MachineryClassification>> Handle(GetMachineryClassificationQuery request, CancellationToken cancellationToken)
        {
            var data = _ctx.Get<MachineryClassification>($@"
                SELECT 
                    c.id,
                    c.code,
                    c.parent_code,
                    c.name
                FROM FieldBook.machinery_classification c
                ORDER BY c.code");

            var response = new DbData<MachineryClassification>();
            var allClassifications = data.Data.ToList();

            if (request.Flat)
                response.Data = GetLastRank(allClassifications);
            else
                response.Data = allClassifications;

            return Task.FromResult(response);
        }

        private List<MachineryClassification> GetLastRank(List<MachineryClassification> allClassifications)
        {
            foreach (var d in allClassifications)
            {
                d.HasChildren = allClassifications.Any(x => x.ParentCode == d.Code);
            }
            var lastRankClassifications = allClassifications.Where(x => !x.HasChildren).ToList();

            foreach (var c in lastRankClassifications)
            {
                var classification = c;
                GetParents(ref classification, allClassifications);
            }

            return lastRankClassifications;
        }

        private void GetParents(ref MachineryClassification classification, List<MachineryClassification> data)
        {
            var parents = new List<MachineryClassification>();

            MachineryClassification? current = classification;

            while (current != null && !string.IsNullOrEmpty(current.ParentCode))
            {
                current = data.FirstOrDefault(x => x.Code == current.ParentCode);
                if (current != null)
                    parents.Insert(0, current);
            }

            classification.Parents = parents;
        }
    }
}
