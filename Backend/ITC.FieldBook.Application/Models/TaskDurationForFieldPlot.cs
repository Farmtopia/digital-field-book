﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITC.FieldBook.Application.Models
{
    public class TaskDurationForFieldPlot
    {
        public int? FieldPlotId { get; set; }

        public int DurationMinutes { get; set; }
    }
}
