﻿using DocumentFormat.OpenXml.Wordprocessing;
using ITC.FieldBook.Application.Infrastructure.Models;
using Microsoft.AspNetCore.DataProtection.KeyManagement;
using NetTopologySuite.Densify;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITC.FieldBook.Application.Models
{
    public class FieldPlot : DbEntityWithAudit
    {
        public FieldPlot() : base("FieldBook.field_plot") { }

        private int? _id;
        [DbColumn("id", IsKey = true, Identity = true)]
        public int? Id { get => _id; set { _id = value; RaisePropertyChanged(nameof(Id)); } }

        private int? _fieldId;
        [DbColumn("field_id")]
        public int? FieldId { get => _fieldId; set { _fieldId = value; RaisePropertyChanged(nameof(FieldId)); } }

        private int? _year;
        [DbColumn("year")]
        public int? Year { get => _year; set { _year = value; RaisePropertyChanged(nameof(Year)); } }

        private int? _cropType;
        [DbColumn("crop_type")]
        public int? CropType { get => _cropType; set { _cropType = value; RaisePropertyChanged(nameof(CropType)); } }

        private string? _cropVariety;
        [DbColumn("crop_variety")]
        public string? CropVariety { get => _cropVariety; set { _cropVariety = value; RaisePropertyChanged(nameof(CropVariety)); } }

        private decimal? _area;
        [DbColumn("area")]
        public decimal? Area { get => _area; set { _area = value; RaisePropertyChanged(nameof(Area)); } }

        private int? _cropId;
        [DbColumn("crop_id")]
        public int? CropId { get => _cropId; set { _cropId = value; RaisePropertyChanged(nameof(CropId)); } }

        private DateTime? _sowingDate;
        [DbColumn("sowing_date")]
        public DateTime? SowingDate { get => _sowingDate; set { _sowingDate = value; RaisePropertyChanged(nameof(SowingDate)); } }

        private DateTime? _harvestDate;
        [DbColumn("harvest_date")]
        public DateTime? HarvestDate { get => _harvestDate; set { _harvestDate = value; RaisePropertyChanged(nameof(HarvestDate)); } }

        /// <summary>
        /// Only modified programaticly
        /// </summary>
        public int? SowingTaskId { get; set; }

        /// <summary>
        /// Only modified programaticly
        /// </summary>
        public int? HarvestTaskId { get; set; }

        public string? CropIdentifier { get; set; }

        public string? CropName { get; set; }

        public string? CropNameEn { get; set; }
    }
}
