import { EnumAppEnvironment, EnumLoginType } from 'src/enums/core.enums';
import { Ref } from 'vue';

export interface AppSettings {
  appEnvironment?: EnumAppEnvironment;
  loginType?: EnumLoginType;
  itcSsoAppId?: string;
  mapboxAccessToken?: string;
}

export interface ErrorDetails {
  statusCode?: number;
  message?: string;
  errors?: string[];
}

export interface LoginResponse {
  user?: User;
  success?: boolean;
  token?: string;
}

export interface RefreshTokenResponse {
  user?: User;
  success?: boolean;
  token?: string;
}

export interface MenuItem {
  id?: string;
  icon?: string;
  text?: string;
  translation?: string;
  path?: string;
  group?: string;
  badge?: Ref<number | undefined>;

  expanded?: boolean;
  children?: MenuItem[];
}

export interface User extends DbEntityWithAudit {
  id: number;
  uuid: string;
  name: string;
  surname: string;
  email: string;
  role: string;
  lastLogin: Date;
  status: number;
  farmId?: number;
}

export interface DbEntity {
  rowNum?: number;
  isActive?: boolean;
  isDeleted?: boolean;
}

export interface DbEntityWithAudit extends DbEntity {
  created?: string;
  createdBy?: number;
  modified?: string;
  modifiedBy?: number;
}

export interface DbData<T extends DbEntity> {
  data: T[];
}

export interface DbItem<T> {
  item: T;
}

export abstract class DbEntity {
  rowNum?: number;
}

export interface DialogCloseEventArgs {
  changes: boolean;
  data: any;
}

export class ForItem<T> {
  constructor(itm: T) {
    this.item = itm;
  }
  key = crypto.randomUUID();
  item: T;
}

export class CheckboxItem<T> {
  constructor(itm: T) {
    this.item = itm;
  }
  key = crypto.randomUUID();
  checked = false;
  item: T;
}

export interface ExportRequest {
  columns?: ExportColumn[];
  rows?: { [k: string]: any }[];
}

export interface ExportColumn {
  name?: string;
  label?: string;
}
