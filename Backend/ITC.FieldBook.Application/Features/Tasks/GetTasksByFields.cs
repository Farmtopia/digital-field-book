﻿//using ITC.FieldBook.Application.Infrastructure.Interfaces;
//using ITC.FieldBook.Application.Infrastructure.Tools;
//using ITC.FieldBook.Application.Infrastructure;
//using MediatR;
//using Microsoft.AspNetCore.Mvc;
//using Microsoft.Extensions.Configuration;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
//using Dapper;
//using ITC.FieldBook.Application.Models;
//using ITC.FieldBook.Application.Features.Fields;
//using ITC.FieldBook.Application.Features.Farms;
//using ITC.FieldBook.Application.Features.Crops;
//using ITC.FieldBook.Application.Features.Fields.Crops;
//using ITC.FieldBook.Application.Infrastructure.Models;

//namespace ITC.FieldBook.Application.Features.Tasks
//{
//    public class GetTasksByFieldController : ApiControllerBase
//    {
//        [HttpGet("tasks/tasks-by-field")]
//        public Task<DbData<TaskByField>> GetTasksByField(int? fieldId, DateTime? dateFrom, DateTime? dateTo)
//        {
//            return Mediator.Send(new GetTasksByFieldQuery { FieldId = fieldId, DateFrom = dateFrom, DateTo = dateTo });
//        }
//    }

//    public class GetTasksByFieldQuery : IRequest<DbData<TaskByField>>
//    {
//        public int? FieldId { get; set; }

//        public DateTime? DateFrom { get; set; }

//        public DateTime? DateTo { get; set; }
//    }

//    internal sealed class GetTasksByFieldQueryHandler : IRequestHandler<GetTasksByFieldQuery, DbData<TaskByField>>
//    {
//        private readonly DbContext _ctx;
//        private readonly IConfiguration _configuration;
//        private readonly ICurrentUserService _currentUserService;

//        public GetTasksByFieldQueryHandler(DbContext ctx, IConfiguration configuration, ICurrentUserService currentUserService)
//        {
//            _ctx = ctx;
//            _configuration = configuration;
//            _currentUserService = currentUserService;
//        }

//        public Task<DbData<TaskByField>> Handle(GetTasksByFieldQuery request, CancellationToken cancellationToken)
//        {
//            var data = new DbData<TaskByField>();
//            var farmId = _currentUserService.FarmId!.Value;
//            var farmsRepo = new FarmsRepository(_ctx);

//            var farm = farmsRepo.GetFarm(farmId).Item;

//            if (farm == null)
//                return Task.FromResult(data);

//            //var report = new TasksReport();
//            //report.FarmIdentifier = farm.Identifier;
//            //report.FarmName = farm.Name;
//            data.Data = GetTasks(_currentUserService.FarmId!.Value, _currentUserService.Year!.Value, request.FieldId, request.DateFrom, request.DateTo).ToList();

//            return Task.FromResult(data);
//        }

//        private IEnumerable<TaskByField> GetTasks(int farmId, int year, int? fieldId = null, DateTime? dateFrom = null, DateTime? dateTo = null)
//        {
//            using (var conn = _ctx.CreateConnection())
//            {
//                return conn.Query<TaskByField>($@"
//                    SELECT
//                        t.id AS task_id,
//	                    t.year,
//	                    t.date_from,
//	                    t.date_to,
//                        f.id AS field_id,
//	                    f.identifier AS field_identifier,
//	                    f.name AS  field_name,
//	                    c.identifier AS crop_identifier,
//	                    c.name AS crop_name,
//	                    c.name_en AS crop_name_en,
//	                    tt.name AS task_type_name,
//	                    tt.name_en AS task_type_name_en,
//	                    m.name AS machine_name,
//	                    att.name AS attachment_name,
//	                    t.executor,
//	                    t.note
//                    FROM FieldBook.task t
//	                    LEFT JOIN FieldBook.task_type tt ON tt.id = t.task_type_id
//	                    LEFT JOIN FieldBook.task_field tf ON tf.task_id = t.id
//                        LEFT JOIN FieldBook.field f ON f.id = tf.field_id
//                        LEFT JOIN FieldBook.field_plot fp ON fp.id = tf.field_plot_id
//                        LEFT JOIN FieldBook.crop c ON c.id = fp.crop_id
//	                    LEFT JOIN FieldBook.machinery m ON m.id = t.equipment_id
//	                    LEFT JOIN FieldBook.machinery att ON att.id = t.attached_equipment_id
//                    WHERE
//                        {DbContext.GetIsDeletedIsActiveCondition("t")} AND
//                        t.farm_id = @farmId AND
//                        t.year = @year
//                        {(fieldId != null ? "AND tf.field_id = @fieldId" : "")}
//                        {(dateFrom != null ? "AND t.date_from >= @dateFrom" : "")}
//                        {(dateTo != null ? "AND t.date_to <= @dateTo" : "")}
//                    ORDER BY t.date_from DESC
//                ", new { farmId, year, fieldId, dateFrom, dateTo });
//            }
//        }
//    }
//}
