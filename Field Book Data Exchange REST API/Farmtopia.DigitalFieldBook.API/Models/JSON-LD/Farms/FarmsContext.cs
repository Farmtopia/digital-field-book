﻿using Swashbuckle.AspNetCore.Annotations;
using System.Text.Json.Nodes;
using System.Text.Json.Serialization;

namespace Farmtopia.DigitalFieldBook.API.Models.JSON_LD.Farms
{
    public class FarmsContext
    {
        [SwaggerSchema(Nullable = false)]
        [JsonPropertyName("fsm")]
        public Uri Fsm { get; set; }

        [SwaggerSchema(Nullable = false)]
        [JsonPropertyName("pcsm")]
        public Uri Pcsm { get; set; }

        [SwaggerSchema(Nullable = false)]
        [JsonPropertyName("geosparql")]
        public Uri Geosparql { get; set; }

        [SwaggerSchema(Nullable = false)]
        [JsonPropertyName("centroid")]
        public TermDefinition Centroid { get; set; }

        [SwaggerSchema(Nullable = false)]
        [JsonPropertyName("geoJson")]
        public TermDefinition GeoJson { get; set; }

        [SwaggerSchema(Nullable = false)]
        [JsonPropertyName("isEcoFarm")]
        public string IsEcoFarm { get; set; }

        [SwaggerSchema(Nullable = false)]
        [JsonPropertyName("sliceContents")]
        public TermDefinition SliceContents { get; set; }

        [SwaggerSchema(Nullable = false)]
        [JsonPropertyName("datasetSlices")]
        public TermDefinition DatasetSlices { get; set; }
    }
}
