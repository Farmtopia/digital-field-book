﻿using ITC.FieldBook.Application.Infrastructure.Models;
using Microsoft.AspNetCore.DataProtection.KeyManagement;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITC.FieldBook.Application.Models
{
    public class Machinery : DbEntityWithAudit
    {
        public Machinery() : base("FieldBook.machinery") { }

        private int? _id;
        [DbColumn("id", IsKey = true, Identity = true)]
        public int? Id { get => _id; set { _id = value; RaisePropertyChanged(nameof(Id)); } }

        private int? _farmId;
        [DbColumn("farm_id")]
        public int? FarmId { get => _farmId; set { _farmId = value; RaisePropertyChanged(nameof(FarmId)); } }

        private string? _name;
        [DbColumn("name")]
        public string? Name { get => _name; set { _name = value; RaisePropertyChanged(nameof(Name)); } }

        private string? _description;
        [DbColumn("description")]
        public string? Description { get => _description; set { _description = value; RaisePropertyChanged(nameof(Description)); } }

        private decimal? _performance;
        [DbColumn("performance")]
        public decimal? Performance { get => _performance; set { _performance = value; RaisePropertyChanged(nameof(Performance)); } }

        private int? _performanceUnitId;
        [DbColumn("performance_unit_id")]
        public int? PerformanceUnitId { get => _performanceUnitId; set { _performanceUnitId = value; RaisePropertyChanged(nameof(PerformanceUnitId)); } }

        private string? _performanceUnit;
        [DbColumn("performance_unit")]
        public string? PerformanceUnit { get => _performanceUnit; set { _performanceUnit = value; RaisePropertyChanged(nameof(PerformanceUnit)); } }

        private int? _classificationId;
        [DbColumn("classification_id")]
        public int? ClassificationId { get => _classificationId; set { _classificationId = value; RaisePropertyChanged(nameof(ClassificationId)); } }

        private string? _trackingId;
        [DbColumn("tracking_id")]
        public string? TrackingId { get => _trackingId; set { _trackingId = value; RaisePropertyChanged(nameof(TrackingId)); } }

        private int? _trackingType;
        [DbColumn("tracking_type")]
        public int? TrackingType { get => _trackingType; set { _trackingType = value; RaisePropertyChanged(nameof(TrackingType)); } }

        private int? _farmManagerId;
        [DbColumn("farm_manager_id")]
        public int? FarmManagerId { get => _farmManagerId; set { _farmManagerId = value; RaisePropertyChanged(nameof(FarmManagerId)); } }

        private string? _farmManagerName;
        [DbColumn("farm_manager_name")]
        public string? FarmManagerName { get => _farmManagerName; set { _farmManagerName = value; RaisePropertyChanged(nameof(FarmManagerName)); } }

        public string? ClassificationDisplayName { get; set; }

        public string? PerformanceDisplayValue => $"{Performance?.ToString("#,##0.00", CultureInfo.GetCultureInfo("sl-SI"))} {PerformanceUnit}";
    }
}
