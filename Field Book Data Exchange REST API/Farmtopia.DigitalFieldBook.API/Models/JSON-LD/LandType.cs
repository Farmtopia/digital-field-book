﻿using System.Text.Json.Serialization;

namespace Farmtopia.DigitalFieldBook.API.Models.JSON_LD
{
    public class LandType : JsonLdBase
    {
        [JsonPropertyName("label")]
        public string Label { get; set; }
    }
}
