﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITC.FieldBook.Application.Models.DTO
{
    public class AppSettingsDto
    {
        public string? AppEnvironment { get; set; }

        public string? LoginType { get; set; }

        public string? MapboxAccessToken { get; set; }

        public string? ItcSsoAppId { get; set; }
    }
}
