﻿using Swashbuckle.AspNetCore.Annotations;

namespace Farmtopia.DigitalFieldBook.API.Models
{
    public class FieldCropEntity
    {
        public int? Id  { get; set; }

        public Guid? Uuid { get; set; }

        public int? Year { get; set; }

        public int? CropType { get; set; }

        public Guid? CropUuid { get; set; }

        public DateTime? SowingDate { get; set; }

        public DateTime? HarvestDate { get; set; }
    }

    public class CropGeneric
    {
        public string? Title { get; set; } // TEST

        public int? Year { get; set; }

        public Guid? Uuid { get; set; }

        public int? CropType { get; set; }

        public Crop? Crop { get; set; }

        public DateTime? SowingDate { get; set; }

        public DateTime? HarvestDate { get; set; }
    }
}
