﻿using ITC.FieldBook.Application.Infrastructure;
using ITC.FieldBook.Application.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITC.FieldBook.Application.Features.Machinery
{
    public class MachineryRepository
    {
        private DbContext _ctx;

        public MachineryRepository(DbContext ctx)
        {
            _ctx = ctx;
        }

        public DbData<Models.Machinery> GetMachinery(int farmId)
        {
            var data = _ctx.Get<Models.Machinery>($@"
                SELECT 
                    m.id,
                    m.name,
                    m.description,
                    m.performance,
                    m.performance_unit_id,
                    ISNULL(m.performance_unit, u.name) AS performance_unit,
                    m.classification_id,
                    m.tracking_id,
                    m.tracking_type,
                    m.farm_manager_id,
                    m.farm_manager_name,
                    c.code + ' ' + c.name AS classification_displayname
                FROM FieldBook.machinery m
		            LEFT JOIN FieldBook.unit u ON u.id = m.performance_unit_id
                    LEFT JOIN FieldBook.machinery_classification c ON m.classification_id = c.id
                WHERE
                    {DbContext.GetIsDeletedIsActiveCondition("m")} AND
                    m.farm_id = @farmId",
            new { farmId });

            return data;
        }

        public DbItem<Models.Machinery> GetMachine(int id, int? farmId = null)
        {
            var data = _ctx.GetItem<Models.Machinery>($@"
                SELECT TOP(1)
                    m.id,
                    m.name,
                    m.description,
                    m.performance,
                    m.performance_unit_id,
                    ISNULL(m.performance_unit, u.name) AS performance_unit,
                    m.classification_id,
                    m.tracking_id,
                    m.tracking_type,
                    m.farm_manager_id,
                    m.farm_manager_name,
                    c.code + ' ' + c.name AS classification_displayname
                FROM FieldBook.machinery m
		            LEFT JOIN FieldBook.unit u ON u.id = m.performance_unit_id
                    LEFT JOIN FieldBook.machinery_classification c ON m.classification_id = c.id
                WHERE
                    {DbContext.GetIsDeletedIsActiveCondition("m")} AND
                    {(farmId == null ? "" : "m.farm_id = @farmId AND")}
                    m.id = @id",
            new { id, farmId });

            return data;
        }
    }
}
