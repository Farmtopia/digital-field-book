﻿using ITC.FieldBook.Application.Infrastructure.Models;
using ITC.FieldBook.Application.Infrastructure;
using MediatR;
using Microsoft.AspNetCore.DataProtection.KeyManagement;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using ITC.FieldBook.Application.Models;
using ITC.FieldBook.Application.Infrastructure.Services;
using ITC.FieldBook.Application.Infrastructure.Interfaces;
using System.ComponentModel.DataAnnotations;

namespace ITC.FieldBook.Application.Features.Documents
{
    public class DeleteDocumentController : ApiControllerBase
    {
        [HttpDelete("documents/{documentId}")]
        public Task DeleteDocument([FromRoute] int? documentId)
        {
            return Mediator.Send(new DeleteDocumentCommand { DocumentId = documentId });
        }
    }

    public class DeleteDocumentCommand : IRequest
    {
        [Required]
        public int? DocumentId { get; set; }
    }

    internal sealed class DeleteDocumentCommandHandler : IRequestHandler<DeleteDocumentCommand>
    {
        private readonly DbContext _ctx;
        private readonly ICurrentUserService _currentUserService;

        public DeleteDocumentCommandHandler(DbContext ctx, ICurrentUserService currentUserService)
        {
            _ctx = ctx;
            _currentUserService = currentUserService;
        }

        public Task Handle(DeleteDocumentCommand request, CancellationToken cancellationToken)
        {
            var documentsRepository = new DocumentsRepository(_ctx);
            var document = documentsRepository.GetDocument(request.DocumentId!.Value, _currentUserService.FarmId!.Value).Item;
            if (document != null)
            {
                var user = _currentUserService.GetUser()!;
                _ctx.DeleteItem(document, user);
            }

            return Task.CompletedTask;
        }
    }
}

