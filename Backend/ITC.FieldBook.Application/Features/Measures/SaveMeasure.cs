﻿using ITC.FieldBook.Application.Infrastructure.Models;
using ITC.FieldBook.Application.Infrastructure;
using MediatR;
using Microsoft.AspNetCore.DataProtection.KeyManagement;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using ITC.FieldBook.Application.Models;
using ITC.FieldBook.Application.Infrastructure.Interfaces;
using ITC.FieldBook.Application.Infrastructure.Enums;
using Microsoft.AspNetCore.Authorization;

namespace ITC.FieldBook.Application.Features.Measures
{
    [Authorize(Roles = EnumRole.Admin)]
    public class SaveMeasureController : ApiControllerBase
    {
        [HttpPost("measures")]
        public Task SaveMeasure([FromBody] Measure data)
        {
            return Mediator.Send(new SaveMeasureCommand { Data = data });
        }
    }

    public class SaveMeasureCommand : IRequest
    {
        public Measure? Data { get; set; }
    }

    internal sealed class SaveMeasureCommandHandler : IRequestHandler<SaveMeasureCommand>
    {
        private readonly DbContext _ctx;
        private readonly ICurrentUserService _currentUserService;

        public SaveMeasureCommandHandler(DbContext ctx, ICurrentUserService currentUserService)
        {
            _ctx = ctx;
            _currentUserService = currentUserService;
        }

        public Task Handle(SaveMeasureCommand request, CancellationToken cancellationToken)
        {
            if (request.Data != null)
            {
                var user = _currentUserService.GetUser()!;
                _ctx.Save(request.Data, user: user);
            }

            return Task.CompletedTask;
        }
    }
}

