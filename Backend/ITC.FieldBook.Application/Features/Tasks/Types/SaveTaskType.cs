﻿using ITC.FieldBook.Application.Infrastructure.Models;
using ITC.FieldBook.Application.Infrastructure;
using MediatR;
using Microsoft.AspNetCore.DataProtection.KeyManagement;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using ITC.FieldBook.Application.Models;
using ITC.FieldBook.Application.Infrastructure.Interfaces;
using ITC.FieldBook.Application.Infrastructure.Enums;
using Microsoft.AspNetCore.Authorization;

namespace ITC.FieldBook.Application.Features.Tasks.Types
{
    [Authorize(Roles = EnumRole.Admin)]
    public class SaveTaskTypeController : ApiControllerBase
    {
        [HttpPost("tasks/task-types")]
        public Task SaveTaskType([FromBody] TaskType data)
        {
            return Mediator.Send(new SaveTaskTypeCommand { Data = data });
        }
    }

    public class SaveTaskTypeCommand : IRequest
    {
        public TaskType? Data { get; set; }
    }

    internal sealed class SaveTaskTypeCommandHandler : IRequestHandler<SaveTaskTypeCommand>
    {
        private readonly DbContext _ctx;
        private readonly ICurrentUserService _currentUserService;

        public SaveTaskTypeCommandHandler(DbContext ctx, ICurrentUserService currentUserService)
        {
            _ctx = ctx;
            _currentUserService = currentUserService;
        }

        public Task Handle(SaveTaskTypeCommand request, CancellationToken cancellationToken)
        {
            if (request.Data != null)
            {
                var user = _currentUserService.GetUser()!;
                _ctx.Save(request.Data, user: user);
            }

            return Task.CompletedTask;
        }
    }
}

