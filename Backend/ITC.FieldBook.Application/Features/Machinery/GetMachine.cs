﻿using ITC.FieldBook.Application.Infrastructure.Interfaces;
using ITC.FieldBook.Application.Infrastructure;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata;
using Dapper;
using ITC.FieldBook.Application.Models;
using ITC.FieldBook.Application.Infrastructure.Models;
using System.ComponentModel.DataAnnotations;

namespace ITC.FieldBook.Application.Features.Machinery
{
    public class GetMachineController : ApiControllerBase
    {
        [HttpGet("machinery/{machineryId}")]
        public Task<DbItem<Models.Machinery>> GetMachine([FromRoute] int? machineryId)
        {
            return Mediator.Send(new GetMachineQuery { MachineryId = machineryId });
        }
    }

    public class GetMachineQuery : IRequest<DbItem<Models.Machinery>>
    {
        [Required]
        public int? MachineryId { get; set; }
    }

    internal sealed class GetMachineQueryHandler : IRequestHandler<GetMachineQuery, DbItem<Models.Machinery>>
    {
        private readonly DbContext _ctx;
        private readonly IConfiguration _configuration;
        private readonly ICurrentUserService _currentUserService;

        public GetMachineQueryHandler(DbContext ctx, IConfiguration configuration, ICurrentUserService currentUserService)
        {
            _ctx = ctx;
            _configuration = configuration;
            _currentUserService = currentUserService;
        }

        public Task<DbItem<Models.Machinery>> Handle(GetMachineQuery request, CancellationToken cancellationToken)
        {;
            var machineryRepoisitory = new MachineryRepository(_ctx);
            var data = machineryRepoisitory.GetMachine(request.MachineryId!.Value, _currentUserService.FarmId!.Value);

            return Task.FromResult(data);
        }
    }
}
