﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITC.FieldBook.Application.Infrastructure.Models
{
    public class DbData<T>// where T: DbEntity
    {
        public IEnumerable<T> Data { get; set; } = Enumerable.Empty<T>();
    }
}
