﻿using System.Text.Json.Serialization;

namespace Farmtopia.DigitalFieldBook.API.Models.JSON_LD
{
    public class HasArea : JsonLdBase
    {
        [JsonPropertyName("numericValue")]
        public decimal? NumericValue { get; set; }

        [JsonPropertyName("unitOfMeasure")]
        public string UnitOfMeasure { get; set; }
    }
}
