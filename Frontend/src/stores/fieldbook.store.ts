import { defineStore } from 'pinia';
import { Cookies } from 'quasar';
import geolocation from 'src/helpers/geolocation';
import { AppSettings } from 'src/models/core.models';
import { Farm, Season } from 'src/models/fieldbook.models';
import { axiosApi } from 'src/services';
import routes from 'src/services/routes';
import { computed, ref } from 'vue';

export const useFieldbookStore = defineStore('fieldbook', () => {
  const year = ref(new Date().getFullYear());
  const farms = ref<Farm[]>();
  const farm = ref<Farm>();
  const season = ref<Season>();
  const appSettings = ref<AppSettings>({});
  const _locationEnabled = ref<boolean>(Cookies.get('geolocation') == 'true');

  const loadAppSetting = async function () {
    appSettings.value = (
      await axiosApi.get<AppSettings>(routes.settings.appSettings)
    ).data;
  };

  const locationEnabled = computed({
    get() {
      return _locationEnabled.value;
    },
    async set(val) {
      Cookies.set('geolocation', val ? 'true' : 'false');
      _locationEnabled.value = val;

      if (val) {
        geolocation.requestLocation((response) => {
          if (!response.success) {
            _locationEnabled.value = false;

            Cookies.set('geolocation', 'false');
            _locationEnabled.value = false;
          }
        });
      }
      //if (!(await geolocation.requestLocation()).success) {
      //  val = false;
      //}

      //Cookies.set('geolocation', val ? 'true' : 'false');
    },
  });

  const getLocation = async function (): Promise<{
    success: boolean;
    location?: GeolocationPosition;
  }> {
    if (_locationEnabled.value) {
      return new Promise((resolve) => {
        geolocation.requestLocation((response) => {
          resolve(response);
        });
      });
    }
    return { success: false };
  };

  return {
    year,
    farms,
    farm,
    season,
    loadAppSetting,
    appSettings,
    getLocation,
    locationEnabled,
  };
});
