﻿using ITC.FieldBook.Application.Infrastructure.Models;
using ITC.FieldBook.Application.Infrastructure;
using MediatR;
using Microsoft.AspNetCore.DataProtection.KeyManagement;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using ITC.FieldBook.Application.Models;
using Microsoft.AspNetCore.Authorization;
using ITC.FieldBook.Application.Infrastructure.Enums;
using System.Transactions;
using Dapper;
using NetTopologySuite.Geometries;
using ITC.FieldBook.Application.Infrastructure.Interfaces;
using System.Text.Json;
using DocumentFormat.OpenXml.Spreadsheet;
using ITC.FieldBook.Application.Features.Fields;
using Microsoft.Extensions.Configuration;

namespace ITC.FieldBook.Application.Features.Farms
{
    [Authorize]
    public class SaveFarmController : ApiControllerBase
    {
        [HttpPost("farms")]
        public Task SaveFarm([FromBody] Farm data)
        {
            return Mediator.Send(new SaveFarmCommand { Data = data });
        }
    }

    public class SaveFarmCommand : IRequest
    {
        public Farm? Data { get; set; }
    }

    internal sealed class SaveFarmCommandHandler : IRequestHandler<SaveFarmCommand>
    {
        private readonly DbContext _ctx;
        private readonly ICurrentUserService _currentUserService;
        private readonly IConfiguration _configuration;
        private readonly IFileService _fileService;

        public SaveFarmCommandHandler(DbContext ctx, IConfiguration configuration, ICurrentUserService currentUserService, IFileService fileService)
        {
            _ctx = ctx;
            _configuration = configuration;
            _currentUserService = currentUserService;
            _fileService = fileService;
        }

        public Task Handle(SaveFarmCommand request, CancellationToken cancellationToken)
        {
            var data = request.Data;
            if (data != null)
            {
                var user = _currentUserService.GetUser()!;
                if (data.Id != null)
                {
                    var farmRepository = new FarmsRepository(_ctx);
                    var farm = farmRepository.GetFarm(data.Id.Value, user.HasRole(EnumRole.Admin) ? null : user.Id!.Value);
                    if (farm == null)
                        return Task.CompletedTask;
                }
                else
                {
                    // For now only admin can add new farms
                    if(!user.HasRole(EnumRole.Admin))
                        return Task.CompletedTask;
                }

                using (var scope = new TransactionScope())
                {
                    // Save farm
                    var ids = _ctx.Save(data, user: user);

                    var farmId = Convert.ToInt32(ids["id"]);

                    // Update location
                    Point? location = data.Longitude != null && data.Latitude != null ? new Point(data.Longitude.Value, data.Latitude.Value) : null;
                    UpdateFarmLocation(farmId, location);

                    if (location != null)
                    {
                        var farmsBll = new FarmsBll(_ctx);
                        farmsBll.UpdateImage(_configuration, _fileService, farmId, location.Y, location.X);
                    }

                    scope.Complete();
                }
            }
            return Task.CompletedTask;
        }

        private void UpdateFarmLocation(int farmId, Geometry? location)
        {
            using (var conn = _ctx.CreateConnection())
            {
                conn.Execute(@"
                UPDATE
                    FieldBook.farm
                SET
                    location = @location
                WHERE
                    id = @farmId"
                , new { farmId, location });
            }
        }
    }
}

