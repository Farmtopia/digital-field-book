﻿using Dapper;
using ITC.FieldBook.Application.Infrastructure;
using ITC.FieldBook.Application.Infrastructure.Interfaces;
using ITC.FieldBook.Application.Infrastructure.Models;
using ITC.FieldBook.Application.Infrastructure.Services;
using ITC.FieldBook.Application.Models.DTO;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITC.FieldBook.Application.Features.Fields
{
    public class FieldsBll
    {
        private readonly DbContext _ctx;

        public FieldsBll( DbContext ctx)
        {
            _ctx = ctx;
        }

        public int? UpdateImage(IConfiguration configuration, IFileService fileService, int fieldId, double latitude, double longitude)
        {
            var imageUrl = configuration!["Mapbox:Field:ImageUrl"];
            var accessToken = configuration["Mapbox:AccessToken"];

            var downloadUrl = imageUrl.Replace("{access_token}", accessToken).Replace("{latitude}", latitude.ToString("0.#########", System.Globalization.CultureInfo.InvariantCulture)).Replace("{longitude}", longitude.ToString("0.#########", System.Globalization.CultureInfo.InvariantCulture));

            using (HttpClient client = new HttpClient())
            {
                using (var response = client.GetAsync(downloadUrl).Result)
                {
                    if (response.IsSuccessStatusCode)
                    {
                        var bytes = response.Content.ReadAsByteArrayAsync().Result;
                        var fileId = fileService.Save("Image.jpg", bytes, new SaveFileSettings { Folder = @$"Fields\Images\{fieldId}" });
                        UpdateFieldImageFileId(fieldId, fileId);

                        return fileId;
                    }
                }
            }

            return null;
        }

        private void UpdateFieldImageFileId(int fieldId, int fileId)
        {
            using (var conn = _ctx.CreateConnection())
            {
                conn.Execute(@"UPDATE TOP(1) FieldBook.[field] SET image_file_id = @fileId WHERE id = @fieldId", new { fieldId, fileId });
            }
        }
    }
}
