﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITC.FieldBook.Application.Infrastructure.Enums
{
    public static class EnumRole
    {
        public const string Admin = "1";
        public const string User = "2";
    }
}
