﻿//using MediatR;
//using Microsoft.AspNetCore.Mvc;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
//using Microsoft.AspNetCore.Authorization;
//using ITC.FieldBook.Application.Infrastructure;
//using ITC.FieldBook.Application.Models;
//using ITC.FieldBook.Application.Infrastructure.Interfaces;
//using ITC.FieldBook.Application.Infrastructure.Models;

//namespace ITC.FieldBook.Application.Features.Tasks
//{
//    public class SaveTaskDocumentController : ApiControllerBase
//    {
//        [HttpPost("tasks/{taskId}/documents")]
//        public Task SaveTaskDocument(int taskId, [FromBody] TaskDocument data)
//        {
//            return Mediator.Send(new SaveTaskDocumentCommand { Data = data });
//        }
//    }

//    public class SaveTaskDocumentCommand : IRequest
//    {
//        public TaskDocument? Data { get; set; }
//    }

//    internal sealed class SaveTaskDocumentCommandHandler : IRequestHandler<SaveTaskDocumentCommand>
//    {
//        private readonly DbContext _ctx;
//        private readonly ICurrentUserService _currentUserService;
//        private readonly User _currentUser;

//        public SaveTaskDocumentCommandHandler(DbContext ctx, ICurrentUserService currentUserService)
//        {
//            _ctx = ctx;
//            _currentUserService = currentUserService;
//            _currentUser = currentUserService.GetUser()!;
//        }

//        public Task Handle(SaveTaskDocumentCommand request, CancellationToken cancellationToken)
//        {
//            if (request.Data != null)
//            {
//                _ctx.Save(request.Data, user: _currentUser);
//            }

//            return Task.CompletedTask;
//        }
//    }
//}
