﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITC.FieldBook.Application.Models
{
    public class CropInUse
    {
        public int FieldId { get; set; }

        public int CropYear { get; set; }

        //public int CropId { get; set; }

        public int FieldPlotId { get; set; }
    }
}
