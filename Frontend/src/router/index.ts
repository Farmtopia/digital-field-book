import { route } from 'quasar/wrappers';
import {
  createMemoryHistory,
  createRouter,
  createWebHashHistory,
  createWebHistory,
  Router,
} from 'vue-router';

import routes from './routes';
import { useAuthStore } from 'src/stores/auth.store';
import { EnumRole } from 'src/enums/core.enums';
import { useFieldbookStore } from 'src/stores/fieldbook.store';

/*
 * If not building with SSR mode, you can
 * directly export the Router instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Router instance.
 */
declare module 'vue-router' {
  interface RouteMeta {
    layout?: any;
    title?: string;
    titleTranslation?: string;
    icon?: string;
    requiresAuth?: boolean;
  }
}

let router: Router;

export default route(function (/* { store, ssrContext } */) {
  const createHistory = process.env.SERVER
    ? createMemoryHistory
    : process.env.VUE_ROUTER_MODE === 'history'
    ? createWebHistory
    : createWebHashHistory;

  router = createRouter({
    scrollBehavior: () => ({ left: 0, top: 0 }),
    routes,

    // Leave this as is and make changes in quasar.conf.js instead!
    // quasar.conf.js -> build -> vueRouterMode
    // quasar.conf.js -> build -> publicPath
    history: createHistory(process.env.VUE_ROUTER_BASE),
  });

  router.beforeEach(async (to, from, next) => {
    // Workaround za Azure B2C avtentikacijo. Token si pošlje s hashom, ne smemo ga redirectati na 404 dokler ne validira tokena (potem se popup samodejno zapre, zato je dovolj return).
    if (
      to.fullPath == '/404' &&
      (to.redirectedFrom?.fullPath.includes('/state=') ?? false)
    ) {
      return;
    }

    const authStore = useAuthStore();

    // Verify auth token before each page change
    const requiresAuth = to.matched.some(
      (record) => record.meta.requiresAuth !== false
    );

    if (!requiresAuth || authStore.isAuthenticated) {
      // Authenticated user or pages that don't require authentication
      if (to.name == 'login' && authStore.isAuthenticated) {
        // Is already logged in, prevent going to login page again.
        next({ name: 'home' });
      } else {
        // If admin and does not have farm, open admin portal by default instead
        if (to.name == 'home' && authStore.user?.role == EnumRole.Admin) {
          const fieldbookStore = useFieldbookStore();
          if (!fieldbookStore.farm) {
            next({ name: 'admin' });
            return;
          }
        }

        // Go to selected page
        next();
      }
    } else {
      // Not logged in
      next({
        name: 'login',
        query: { nextUrl: to.fullPath == '/' ? undefined : to.fullPath },
      });
    }
  });

  return router;
});

export { router };
