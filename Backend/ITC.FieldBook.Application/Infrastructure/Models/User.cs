﻿using ITC.FieldBook.Application.Features.Farms;
using ITC.FieldBook.Application.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITC.FieldBook.Application.Infrastructure.Models
{
    public class User : DbEntityWithAudit
    {
        public User() : base("FieldBook.user") { }


        private int? _id;
        [DbColumn("id", IsKey = true, Identity = true)]
        public int? Id { get => _id; set { _id = value; RaisePropertyChanged(nameof(Id)); } }


        private string? _name;
        [DbColumn("name")]
        public string? Name { get => _name; set { _name = value; RaisePropertyChanged(nameof(Name)); } }

        private string? _surname;
        [DbColumn("surname")]
        public string? Surname { get => _surname; set { _surname = value; RaisePropertyChanged(nameof(Surname)); } }

        private string? _displayname;
        [DbColumn("displayname")]
        public string? Displayname { get => _displayname; set { _displayname = value; RaisePropertyChanged(nameof(Displayname)); } }

        private string? _email;
        [DbColumn("email")]
        public string? Email { get => _email; set { _email = value; RaisePropertyChanged(nameof(Email)); } }

        private string? _role;
        [DbColumn("role")]
        public string? Role { get => _role; set { _role = value; RaisePropertyChanged(nameof(Role)); } }

        private int? _farmId;
        [DbColumn("farm_id")]
        public int? FarmId { get => _farmId; set { _farmId = value; RaisePropertyChanged(nameof(FarmId)); } }

        public DateTime? LastLogin { get; set; }

        public Guid? Uuid { get; set; }

        public bool HasRole(string roleId) => Role == roleId;
    }
}
