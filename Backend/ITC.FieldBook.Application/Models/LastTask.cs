﻿using ITC.FieldBook.Application.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITC.FieldBook.Application.Models
{
    public class LastTask : DbEntity
    {
        public int? Id { get; set; }
        
        public int? FarmId { get; set; }

        public int? FieldId { get; set; }

        public DateTime? DateFrom { get; set; }

        public DateTime? DateTo { get; set; }

        public string? TaskTypeName { get; set; }
    }
}
