import { boot } from 'quasar/wrappers';

import itcSso from 'src/libs/itc-sso';
import DefaultPageLayout from 'src/layouts/DefaultPageLayout.vue';

import CBtn from 'src/components/CBtn.vue';
import CBtnDropdown from 'src/components/CBtnDropdown.vue';
import CCard from 'src/components/CCard.vue';
import CCheckbox from 'src/components/CCheckbox.vue';
import CDate from 'src/components/CDate.vue';
import CDialog from 'src/components/CDialog.vue';
import CField from 'src/components/CField.vue';
import CFile from 'src/components/CFile.vue';
import CFileDownload from 'src/components/CFileDownload.vue';
import CForm from 'src/components/CForm.vue';
import CIcon from 'src/components/CIcon.vue';
import CInput from 'src/components/CInput.vue';
import CLabel from 'src/components/CLabel.vue';
import CList from 'src/components/CList.vue';
import CMarkupTable from 'src/components/CMarkupTable.vue';
import COptionGroup from 'src/components/COptionGroup.vue';
import CSectionTitle from 'src/components/CSectionTitle.vue';
import CSelect from 'src/components/CSelect.vue';
import CTable from 'src/components/CTable.vue';
import CTabs from 'src/components/CTabs.vue';
import CToggle from 'src/components/CToggle.vue';
import CTooltip from 'src/components/CTooltip.vue';
import CMap from 'src/components/CMap.vue';
import { useFieldbookStore } from 'src/stores/fieldbook.store';

import { Loading } from 'quasar';
import { t } from 'boot/i18n';
import { EnumLoginType } from 'src/enums/core.enums';
import { useAuthStore } from 'src/stores/auth.store';

declare global {
  interface Date {
    toDate: () => Date;
    dateOnly: () => Date;
  }
  interface String {
    toDate: () => Date;
  }
}

export default boot(async ({ app, store }) => {
  const authStore = useAuthStore();
  const fieldbookStore = useFieldbookStore(store);

  Loading.show({
    delay: 250,
    backgroundColor: 'transparent',
    //boxClass: 'bg-secondary border-radius shadow-6',
  });

  // Register components
  app.component('DefaultPageLayout', DefaultPageLayout);
  app.component('CBtn', CBtn);
  app.component('CBtnDropdown', CBtnDropdown);
  app.component('CCard', CCard);
  app.component('CCheckbox', CCheckbox);
  app.component('CDate', CDate);
  app.component('CDialog', CDialog);
  app.component('CField', CField);
  app.component('CFile', CFile);
  app.component('CFileDownload', CFileDownload);
  app.component('CForm', CForm);
  app.component('CIcon', CIcon);
  app.component('CInput', CInput);
  app.component('CLabel', CLabel);
  app.component('CList', CList);
  app.component('CMap', CMap);
  app.component('CMarkupTable', CMarkupTable);
  app.component('COptionGroup', COptionGroup);
  app.component('CSectionTitle', CSectionTitle);
  app.component('CSelect', CSelect);
  app.component('CTable', CTable);
  app.component('CTabs', CTabs);
  app.component('CToggle', CToggle);
  app.component('CTooltip', CTooltip);

  // Prototypes
  Date.prototype.toJSON = function () {
    const leto = this.getFullYear();
    const mesec = this.getMonth() + 1;
    const dan = this.getDate();
    const ura = this.getHours();
    const minute = this.getMinutes();

    return (
      leto +
      '-' +
      (mesec < 10 ? '0' : '') +
      mesec +
      '-' +
      (dan < 10 ? '0' : '') +
      dan +
      'T' +
      (ura < 10 ? '0' : '') +
      ura +
      ':' +
      (minute < 10 ? '0' : '') +
      minute
    );
  };

  Date.prototype.toDate = function () {
    return new Date(this.toDateString());
  };

  Date.prototype.dateOnly = function () {
    return new Date(this.getFullYear(), this.getMonth(), this.getDate());
  };

  String.prototype.toDate = function () {
    const [YYYY, MM, DD] = this.split('-').map((x) => parseInt(x));

    let hoursSplitted = this.split('T');
    if (hoursSplitted.length == 1) {
      hoursSplitted = this.split(' ');
    }

    const [HH, MIN, SS] =
      hoursSplitted.length > 1
        ? hoursSplitted[1].split(':').map((x) => parseInt(x))
        : [];
    return new Date(YYYY, MM - 1, DD, HH ?? 0, MIN ?? 0, SS ?? 0);
  };

  // Reload page if page doesen't load within 1 minute
  const timeout = setTimeout(() => {
    window.location.reload();
  }, 60 * 1000);

  // Check if user is logged in
  await authStore.checkLogin();

  setInterval(() => {
    // Refresh token every 15 minutes
    authStore.refreshToken();
  }, 1000 * 60 * 15);

  // Load global app settings
  await fieldbookStore.loadAppSetting();
  const appSettings = fieldbookStore.appSettings;

  // Initialize ITC SSO
  if (
    appSettings.loginType == EnumLoginType.ITC_SSO_LOGIN &&
    appSettings.itcSsoAppId
  ) {
    if (appSettings.itcSsoAppId) {
      itcSso.init(appSettings.itcSsoAppId);
    } else {
      alert('Missing ITC SSO App ID');
    }
  }

  // App loaded, clear timeout and hide loading
  clearTimeout(timeout);
  Loading.hide();
});
