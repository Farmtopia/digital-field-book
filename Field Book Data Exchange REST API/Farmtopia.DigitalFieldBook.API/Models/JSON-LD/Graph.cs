﻿using Farmtopia.DigitalFieldBook.API.Models.JSON_LD.Farms;
using Swashbuckle.AspNetCore.Annotations;
using System.Text.Json.Serialization;

namespace Farmtopia.DigitalFieldBook.API.Models.JSON_LD
{
    public abstract class GraphBase : JsonLdBase
    {
    }
}
