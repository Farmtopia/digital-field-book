import { RefreshTokenResponse, LoginResponse } from 'src/models/core.models';
import { axiosApi } from '.';

export default {
  async registerUser() {
    return await axiosApi.post<string>('user/register');
  },
  async loginSso(token: string) {
    return await axiosApi.post<LoginResponse>('user/login', { token });
  },
  async loginLocal(username: string, password: string) {
    return await axiosApi.post<LoginResponse>('user/login', {
      username,
      password,
    });
  },
  async refreshToken() {
    return await axiosApi.post<RefreshTokenResponse>('user/refresh-token');
  },
};
