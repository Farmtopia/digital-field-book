﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Microsoft.AspNetCore.Authorization;
using System.Data;
using ITC.FieldBook.Application.Infrastructure.Enums;
using ITC.FieldBook.Application.Infrastructure;
using ITC.FieldBook.Application.Infrastructure.Models;
using DocumentFormat.OpenXml.Spreadsheet;
using System.Transactions;
using ITC.FieldBook.Application.Infrastructure.Interfaces;

namespace ITC.FieldBook.Application.Features.Users
{
    [Authorize(Roles = EnumRole.Admin)]
    public class SaveUserController : ApiControllerBase
    {
        [HttpPost("users")]
        public Task<Dictionary<string, object?>> SaveUser([FromBody] User data)
        {
            return Mediator.Send(new SaveUserCommand { Data = data });
        }
    }

    public class SaveUserCommand : IRequest<Dictionary<string, object?>>
    {
        public User? Data { get; set; }
    }

    internal sealed class SaveUserCommandHandler : IRequestHandler<SaveUserCommand, Dictionary<string, object?>>
    {
        private readonly DbContext _ctx;
        private readonly ICurrentUserService _currentUserService;

        public SaveUserCommandHandler(DbContext ctx, ICurrentUserService currentUserService)
        {
            _ctx = ctx;
            _currentUserService = currentUserService;
        }

        public Task<Dictionary<string, object?>> Handle(SaveUserCommand request, CancellationToken cancellationToken)
        {
            var data = request.Data;
            if (data != null)
            {
                var user = _currentUserService.GetUser()!;
                using (var scope = new TransactionScope())
                {
                    data.Email = data.Email!.Trim();

                    if (EmailExists(data.Email, data.Id))
                        throw new Exception("User with same email already exists");

                    var keys = _ctx.Save(data, user: user);
                    var userId = Convert.ToInt32(keys["id"]);

                    if (data.HasChanged(nameof(data.FarmId)))
                        UpdateFarm(userId, data.FarmId, user.Id!.Value);

                    scope.Complete();

                    return Task.FromResult(keys);
                }
            }

            return Task.FromResult(new Dictionary<string, object?>());
        }

        public bool EmailExists(string email, int? userId)
        {
            using (var connection = _ctx.CreateConnection())
            {
                return connection.ExecuteScalar<bool>("SELECT CAST(CASE WHEN EXISTS(SELECT TOP(1) 1 FROM FieldBook.[user] WHERE (@userId IS NULL OR id <> @userId) AND email = @email) THEN 1 ELSE 0 END AS BIT)", new { email, userId });
            }
        }


        public void UpdateFarm(int userId, int? farmId, int currentUserId)
        {
            using (var connection = _ctx.CreateConnection())
            {
                var now = DateTime.Now;
                connection.Execute($@"
                    IF NOT EXISTS(SELECT TOP(1) farm_id FROM FieldBook.user_farm WHERE {DbContext.GetIsDeletedIsActiveCondition()} AND user_id = @userId AND farm_id = @farmId)
                    BEGIN
                        UPDATE FieldBook.user_farm
                        SET is_deleted = 1, is_active = 0, modified = @now, modified_by = @currentUserId
                        WHERE {DbContext.GetIsDeletedIsActiveCondition()} AND user_id = @userId;

                        UPDATE FieldBook.[user] SET farm_id = @farmId WHERE id = @userId;

                        IF @farmId IS NOT NULL
                        BEGIN
                            INSERT INTO FieldBook.user_farm(user_id, farm_id, created, created_by, modified, modified_by) VALUES(@userId, @farmId, @now, @currentUserId, @now, @currentUserId);
                        END
                    END", new { userId, farmId, currentUserId, now });
            }
        }
    }
}

