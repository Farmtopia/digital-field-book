﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITC.FieldBook.Application.Infrastructure.Tools
{
    public static class Helpers
    {
        public static string? GetLocalizedValue(string? locale, string? value, string? valueEn)
        {
            if (locale == "en-US" && !string.IsNullOrEmpty(valueEn))
                return valueEn;
            return value;
        }
    }
}
