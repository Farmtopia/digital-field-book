﻿namespace Farmtopia.DigitalFieldBook.API.Models
{
    public class TaskFertilizerEntity
    {
        public Guid? FertilizerUuid { get; set; }

        public string? FertilizerName { get; set; }

        public string? FertilizerNameEn { get; set; }

        public decimal? AmountUsed { get; set; }

        public string? AmountUsedUnit { get; set; }

        public string? AmountUsedUnitDescription { get; set; }

        public string? AmountUsedUnitDescriptionEn { get; set; }

        public int? RatioN { get; set; }

        public int? RatioP { get; set; }

        public int? RatioK { get; set; }
    }

    public class TaskFertilizer
    {
        public Guid? FertilizerUuid { get; set; }

        public string? FertilizerName { get; set; }

        public string? FertilizerNameEn { get; set; }

        public decimal? AmountUsed { get; set; }

        public string? AmountUsedUnit { get; set; }

        public string? AmountUsedUnitDescription { get; set; }

        public string? AmountUsedUnitDescriptionEn { get; set; }
    }
}
