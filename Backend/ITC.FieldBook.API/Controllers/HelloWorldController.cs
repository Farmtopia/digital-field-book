﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;

namespace ITC.FieldBook.API.Controllers
{
    [AllowAnonymous]
    [ApiController]
    public class HelloWorldController : ControllerBase
    {
        [HttpGet("/hello-world")]
        public string Index()
        {
            return "Hello world!";
        }
    }
}
