﻿using System.Text.Json.Serialization;

namespace Farmtopia.DigitalFieldBook.API.Models.JSON_LD.Fields
{
    public class FieldGeometry : JsonLdBase
    {
        [JsonPropertyName("geoJson")]
        public object? GeoJson { get; set; }
    }
}
