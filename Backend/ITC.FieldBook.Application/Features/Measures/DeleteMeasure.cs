﻿using ITC.FieldBook.Application.Infrastructure.Models;
using ITC.FieldBook.Application.Infrastructure;
using MediatR;
using Microsoft.AspNetCore.DataProtection.KeyManagement;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using ITC.FieldBook.Application.Models;
using ITC.FieldBook.Application.Infrastructure.Services;
using ITC.FieldBook.Application.Infrastructure.Interfaces;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Authorization;
using ITC.FieldBook.Application.Infrastructure.Enums;

namespace ITC.FieldBook.Application.Features.Measures
{
    [Authorize(Roles = EnumRole.Admin)]
    public class DeleteMeasureController : ApiControllerBase
    {
        [HttpDelete("measures/{measureId}")]
        public Task DeleteMeasure([FromRoute] int? measureId)
        {
            return Mediator.Send(new DeleteMeasureCommand { MeasureId = measureId });
        }
    }

    public class DeleteMeasureCommand : IRequest
    {
        [Required]
        public int? MeasureId { get; set; }
    }

    internal sealed class DeleteMeasureCommandHandler : IRequestHandler<DeleteMeasureCommand>
    {
        private readonly DbContext _ctx;
        private readonly ICurrentUserService _currentUserService;

        public DeleteMeasureCommandHandler(DbContext ctx, ICurrentUserService currentUserService)
        {
            _ctx = ctx;
            _currentUserService = currentUserService;
        }

        public Task Handle(DeleteMeasureCommand request, CancellationToken cancellationToken)
        {
            var measuresRepository = new MeasuresRepository(_ctx);
            var measure = measuresRepository.GetMeasure(request.MeasureId!.Value).Item;
            if (measure != null)
            {
                var user = _currentUserService.GetUser()!;
                _ctx.DeleteItem(measure, user);
            }

            return Task.CompletedTask;
        }
    }
}

