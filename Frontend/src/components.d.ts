// components.d.ts

// declare module '@vue/runtime-core' works for vue 3
// declare module 'vue' works for vue2 + vue 3
import CBtn from 'components/CBtn.vue';
import CBtnDropdown from 'components/CBtnDropdown.vue';
import CCard from 'components/CCard.vue';
import CCheckbox from 'components/CCheckbox.vue';
import CDate from 'components/CDate.vue';
import CDialog from 'components/CDialog.vue';
import CField from 'components/CField.vue';
import CFile from 'components/CFile.vue';
import CFileDownload from 'components/CFileDownload.vue';
import CForm from 'components/CForm.vue';
import CIcon from 'components/CIcon.vue';
import CInput from 'components/CInput.vue';
import CLabel from 'components/CLabel.vue';
import CList from 'components/CList.vue';
import CMap from 'components/CMap.vue';
import CMarkupTable from 'components/CMarkupTable.vue';
import COptionGroup from 'components/COptionGroup.vue';
import CSectionTitle from 'components/CSectionTitle.vue';
import CSelect from 'components/CSelect.vue';
import CTable from 'components/CTable.vue';
import CTabs from 'components/CTabs.vue';
import CToggle from 'components/CToggle.vue';
import CTooltip from 'components/CTooltip.vue';
import DefaultPageLayout from 'layouts/DefaultPageLayout.vue';

declare module '@vue/runtime-core' {
  export interface GlobalComponents {
    CBtn: typeof CBtn;
    CBtnDropdown: typeof CBtnDropdown;
    CCard: typeof CCard;
    CCheckbox: typeof CCheckbox;
    CDate: typeof CDate;
    CDialog: typeof CDialog;
    CField: typeof CField;
    CFile: typeof CFile;
    CFileDownload: typeof CFileDownload;
    CForm: typeof CForm;
    CIcon: typeof CIcon;
    CInput: typeof CInput;
    CLabel: typeof CLabel;
    CList: typeof CList;
    CMap: typeof CMap;
    CMarkupTable: typeof CMarkupTable;
    COptionGroup: typeof COptionGroup;
    CSectionTitle: typeof CSectionTitle;
    CSelect: typeof CSelect;
    CTable: typeof CTable;
    CTabs: typeof CTabs;
    CToggle: typeof CToggle;
    CTooltip: typeof CTooltip;
    DefaultPageLayout: typeof DefaultPageLayout;
  }
}
export {};
