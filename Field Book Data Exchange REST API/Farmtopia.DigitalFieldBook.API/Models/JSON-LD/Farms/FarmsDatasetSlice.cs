﻿using System.Text.Json.Serialization;

namespace Farmtopia.DigitalFieldBook.API.Models.JSON_LD.Farms
{
    public class FarmsDatasetSlice : DatasetSliceBase
    {
        [JsonPropertyName("@sliceContents")]
        public List<FarmsSliceContent> SliceContents { get; set; }
    }
}
