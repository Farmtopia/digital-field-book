﻿using Azure.Storage.Blobs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITC.FieldBook.Application.Infrastructure.Tools
{
    public class AzureFileStorageManager
    {
        private readonly string _connectionString;
        private readonly string _containerName;

        public AzureFileStorageManager(string connectionString, string containerName) {
            _connectionString = connectionString;
            _containerName = containerName;

        }

        public void UploadBlob(string filename, byte[] bytes, bool uploadIfAlreadyExists = true)
        {
            var serviceClient = new BlobServiceClient(_connectionString);
            var containerClient = serviceClient.GetBlobContainerClient(_containerName);
            var blobClient = containerClient.GetBlobClient(filename);
            var save = uploadIfAlreadyExists || !blobClient.Exists().Value;

            if (save)
            {
                var stream = new MemoryStream(bytes);
                blobClient.Upload(stream, true);
                stream.Close();
            }
        }

        public byte[] DownloadBlob(string name)
        {
            var serviceClient = new BlobServiceClient(_connectionString);
            var containerClient = serviceClient.GetBlobContainerClient(_containerName);

            var blobClient = containerClient.GetBlobClient(name);

            using (var ms = new MemoryStream())
            {
                var response = blobClient.DownloadTo(ms);
                return ms.ToArray();
            }
        }
    }
}
