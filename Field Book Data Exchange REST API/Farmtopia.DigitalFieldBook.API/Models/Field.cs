﻿using System.Text.Json;
using System.Text.Json.Serialization;

namespace Farmtopia.DigitalFieldBook.API.Models
{
    public class FieldEntity
    {
        public Guid? Uuid { get; set; }

        public string? Identifier { get; set; }

        public string? Name { get; set; }

        public string? Description { get; set; }

        public decimal? AreaM2 { get; set; }

        public int? FieldTypeId { get; set; }

        public string? GeoJSON { get; set; }
    }

    public class Field
    {
        public Guid? Uuid { get; set; }

        public string? Identifier { get; set; }

        public string? Name { get; set; }

        public string? Description { get; set; }

        public decimal? AreaM2 { get; set; }

        public FieldType? FieldType { get; set; }

        public List<CropGeneric> Crops { get; set; }


        [JsonPropertyName("geoJSON")]
        public Dictionary<string, object>? GeoJSONObj { get; set; }

        [JsonIgnore]
        public string? GeoJSON
        {
            set
            {
                if (!string.IsNullOrWhiteSpace(value))
                {
                    try
                    {
                        GeoJSONObj = JsonSerializer.Deserialize<Dictionary<string, object>>(value);
                    }
                    catch
                    {
                        GeoJSONObj = null;
                    }
                }
                else
                    GeoJSONObj = null;
            }
        }
    }
}
