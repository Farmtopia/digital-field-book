﻿using Azure.Core;
using ITC.FieldBook.Application.Infrastructure;
using ITC.FieldBook.Application.Infrastructure.Models;
using ITC.FieldBook.Application.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITC.FieldBook.Application.Features.Measures
{
    public class MeasuresRepository
    {
        private DbContext _ctx;

        public MeasuresRepository(DbContext ctx)
        {
            _ctx = ctx;
        }

        public DbData<Measure> GetMeasures(int? year = null)
        {
            var data = _ctx.Get<Measure>($@"
                SELECT 
                    m.id,
                    m.name,
                    m.name_en,
                    m.description,
                    m.description_en,
                    m.category,
                    m.year_from,
                    m.year_to
                FROM FieldBook.measure m
                WHERE
                        {DbContext.GetIsDeletedIsActiveCondition("m")}
                        AND (
                            @year IS NULL 
                            OR (
                                ISNULL(m.year_from, @year) <= @year
                                AND ISNULL(m.year_to, @year) >= @year
                            )
                        )
                ORDER BY m.category, m.name", new { year });

            return data;
        }

        public DbItem<Measure> GetMeasure(int id)
        {
            var data = _ctx.GetItem<Measure>($@"
                SELECT TOP(1)
                    m.id,
                    m.name,
                    m.name_en,
                    m.description,
                    m.description_en,
                    m.category,
                    m.year_from,
                    m.year_to
                FROM FieldBook.measure m
                WHERE
                        {DbContext.GetIsDeletedIsActiveCondition("m")}
                        AND m.id = @id",
                new { id });

            return data;
        }
    }
}
