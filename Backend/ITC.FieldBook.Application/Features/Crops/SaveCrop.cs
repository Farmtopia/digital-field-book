﻿using ITC.FieldBook.Application.Infrastructure.Models;
using ITC.FieldBook.Application.Infrastructure;
using MediatR;
using Microsoft.AspNetCore.DataProtection.KeyManagement;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using ITC.FieldBook.Application.Models;
using ITC.FieldBook.Application.Infrastructure.Enums;
using Microsoft.AspNetCore.Authorization;
using ITC.FieldBook.Application.Infrastructure.Interfaces;

namespace ITC.FieldBook.Application.Features.Crops
{
    [Authorize(Roles = EnumRole.Admin)]
    public class SaveCropController : ApiControllerBase
    {
        [HttpPost("crops")]
        public Task SaveCrop([FromBody] Crop data)
        {
            return Mediator.Send(new SaveCropCommand { Data = data });
        }
    }

    public class SaveCropCommand : IRequest
    {
        public Crop? Data { get; set; }
    }

    internal sealed class SaveCropCommandHandler : IRequestHandler<SaveCropCommand>
    {
        private readonly DbContext _ctx;
        private readonly ICurrentUserService _currentUserService;

        public SaveCropCommandHandler(DbContext ctx, ICurrentUserService currentUserService)
        {
            _ctx = ctx;
            _currentUserService = currentUserService;
        }

        public Task Handle(SaveCropCommand request, CancellationToken cancellationToken)
        {
            if (request.Data != null)
            {
                var user = _currentUserService.GetUser()!;
                _ctx.Save(request.Data, user: user);
            }

            return Task.CompletedTask;
        }
    }
}

