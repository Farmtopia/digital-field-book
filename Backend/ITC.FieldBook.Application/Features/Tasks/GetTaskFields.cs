﻿using ITC.FieldBook.Application.Infrastructure.Interfaces;
using ITC.FieldBook.Application.Infrastructure;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using ITC.FieldBook.Application.Infrastructure.Models;
using ITC.FieldBook.Application.Models;
using System.Threading.Tasks;
using ITC.FieldBook.Application.Features.Tasks;

namespace ITC.FieldBook.Application.Features.Tasks
{
    public class GetTaskFieldsController : ApiControllerBase
    {
        [HttpGet("tasks/{taskId}/fields")]
        public Task<DbData<TaskField>> GetTaskFIelds(int taskId)
        {
            return Mediator.Send(new GetTaskFIeldsQuery { TaskId = taskId });
        }
    }

    public class GetTaskFIeldsQuery : IRequest<DbData<TaskField>>
    {
        public int TaskId { get; set; }
    }

    internal sealed class GetTaskFIeldsQueryHandler : IRequestHandler<GetTaskFIeldsQuery, DbData<TaskField>>
    {
        private readonly DbContext _ctx;
        private readonly IConfiguration _configuration;
        private readonly ICurrentUserService _currentUserService;
        private readonly TasksRepository _tasksRepository;

        public GetTaskFIeldsQueryHandler(DbContext ctx, IConfiguration configuration, ICurrentUserService currentUserService)
        {
            _ctx = ctx;
            _configuration = configuration;
            _currentUserService = currentUserService;
            _tasksRepository = new TasksRepository(ctx);
        }

        public Task<DbData<TaskField>> Handle(GetTaskFIeldsQuery request, CancellationToken cancellationToken)
        {
            var user = _currentUserService.GetUser()!;
            var data = _tasksRepository.GetTaskFields(_currentUserService.FarmId!.Value, request.TaskId);

            return Task.FromResult(data);
        }
    }
}
