﻿using ITC.FieldBook.Application.Infrastructure.Interfaces;
using ITC.FieldBook.Application.Infrastructure;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata;
using Dapper;
using ITC.FieldBook.Application.Models;
using ITC.FieldBook.Application.Infrastructure.Models;
using ITC.FieldBook.Application.Infrastructure.Services;
using ITC.FieldBook.Application.Infrastructure.Enums;
using ITC.FieldBook.Application.Models.DTO;
using AutoMapper;

namespace ITC.FieldBook.Application.Features.Fields
{
    public class GetFieldsController : ApiControllerBase
    {
        [HttpGet("fields")]
        public Task<DbData<FieldDTO>> GetFields([FromQuery]int[]? ids, [FromQuery]bool images = false, decimal? latitude = null, decimal? longitude = null, DateTime? date = null)
        {
            return Mediator.Send(new GetFieldsQuery { FieldIds = ids?.ToList(), Images = images, Latitude = latitude, Longitude = longitude, Date = date });
        }
    }

    public class GetFieldsQuery : IRequest<DbData<FieldDTO>>
    {
        public List<int>? FieldIds { get; set; }

        public bool Images { get; set; } = false;

        public decimal? Latitude { get; set; }

        public decimal? Longitude { get; set; }

        public DateTime? Date { get; set; }
    }

    internal sealed class GetFieldsQueryHandler : IRequestHandler<GetFieldsQuery, DbData<FieldDTO>>
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly DbContext _ctx;
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;
        private readonly ICurrentUserService _currentUserService;

        public GetFieldsQueryHandler(IServiceProvider serviceProvider, DbContext ctx, IConfiguration configuration, ICurrentUserService currentUserService, IMapper mapper)
        {
            _serviceProvider = serviceProvider;
            _ctx = ctx;
            _configuration = configuration;
            _currentUserService = currentUserService;
            _mapper = mapper;
        }

        public Task<DbData<FieldDTO>> Handle(GetFieldsQuery request, CancellationToken cancellationToken)
        {
            var fieldsRepository = new FieldsRepository(_ctx);
            var farmId = _currentUserService.FarmId!.Value;
            var rawData = fieldsRepository.GetFields(farmId, request.FieldIds, request.Latitude, request.Longitude);
            var data = _mapper.Map<DbData<FieldDTO>>(rawData);
            var date = request.Date ?? DateTime.Now.Date;

            if (data.Data?.Count() > 0)
            {
                var lastTasks = GetLastTasks(data.Data.Select(x => x.Id!.Value).ToArray()).Data.OrderByDescending(x => x.DateFrom).ThenByDescending(x => x.Id).ToList();
                var activePlots = fieldsRepository.GetActiveFieldPlots(farmId, date, date).Data.ToList();
                if (lastTasks.Count > 0 || activePlots.Count > 0)
                {
                    foreach (var fi in data.Data)
                    {
                        if (lastTasks.Any())
                        {
                            fi.LastTasks = lastTasks.Where(x => x.FieldId == fi.Id).ToList();
                            if (fi.LastTasks.Count() > 0)
                            {
                                var lastTask = fi.LastTasks.FirstOrDefault()!;
                                fi.LastTask = lastTask;
                            }
                        }

                        if (activePlots.Any())
                        {
                            fi.ActivePlots = activePlots.Where(x => x.FieldId == fi.Id).ToList();
                        }
                    }
                }
            }

            return Task.FromResult(data);
        }

        private DbData<TaskEntity> GetLastTasks(int[] fieldIds)
        {
            return _ctx.Get<TaskEntity>($@"
                SELECT * FROM (
	                SELECT
		                t.id,
		                t.farm_id,
		                tf.field_id,
		                t.date_from,
		                t.date_to,
		                tt.name AS task_type_name,
		                tt.name_en AS task_type_name_en,
		                ROW_NUMBER() OVER (PARTITION BY t.farm_id, tf.field_id ORDER BY t.date_from DESC, t.id DESC) AS rownum 
	                FROM FieldBook.task t
		                JOIN FieldBook.task_field tf ON tf.task_id = t.id
		                LEFT JOIN FieldBook.task_type tt ON tt.id = t.task_type_id
                    WHERE
                        {DbContext.GetIsDeletedIsActiveCondition("t")}
                ) t
                WHERE
                    rownum <= 3
                    AND field_id IN @fieldIds
                ORDER BY field_id, rownum",
            new { fieldIds });
        }
    }
}
