﻿using ITC.FieldBook.Application.Infrastructure.Tools;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITC.FieldBook.Application.Infrastructure.Interfaces
{
    public class FarmManagerService : IFarmManagerService
    {
        public FmManager Instance { get; set; }

        public FarmManagerService(IConfiguration configuration)
        {
            Instance = new FmManager(configuration["FarmManager:ApiUrl"]);
        }
    }
}
