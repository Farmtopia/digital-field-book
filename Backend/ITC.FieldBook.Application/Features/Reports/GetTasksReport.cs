﻿using ITC.FieldBook.Application.Infrastructure.Interfaces;
using ITC.FieldBook.Application.Infrastructure.Tools;
using ITC.FieldBook.Application.Infrastructure;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using ClosedXML.Excel;
using DocumentFormat.OpenXml.Spreadsheet;
using Microsoft.AspNetCore.Authorization;
using System.Data;
using ITC.FieldBook.Application.Models;
using Dapper;
using DocumentFormat.OpenXml.Wordprocessing;
using Microsoft.Extensions.Localization;
using Irony;
using ITC.FieldBook.Application.Resources;

namespace ITC.FieldBook.Application.Features.Reports
{
    public class GetTasksReportController : ApiControllerBase
    {
        [HttpGet("reports/tasks")]
        public async Task<IActionResult> GetTasksReport([FromQuery] int[]? fieldIds, int? year, bool exportCrops, bool exportMeasures, bool exportTasks)
        {
            var file = await Mediator.Send(new GetTasksReportQuery() { FieldIds = fieldIds?.ToList(), Year = year, ExportCrops = exportCrops, ExportMeasures = exportMeasures, ExportTasks = exportTasks });

            if (file == null || file.Filename == null || file.Bytes == null)
                return NotFound();

            return File(file.Bytes, file.ContentType, file.Filename);
        }
    }

    public class GetTasksReportQuery : IRequest<Infrastructure.Models.File>
    {
        public List<int>? FieldIds { get; set; }

        public int? Year { get; set; }

        public bool ExportCrops { get; set; }

        public bool ExportMeasures { get; set; }

        public bool ExportTasks { get; set; }
    }

    internal sealed class GetTasksReportQueryHandler : IRequestHandler<GetTasksReportQuery, Infrastructure.Models.File>
    {
        private readonly DbContext _ctx;
        private readonly IConfiguration _configuration;
        private readonly ICurrentUserService _currentUserService;
        private readonly IStringLocalizer<Localization> _localizer;


        public GetTasksReportQueryHandler(DbContext ctx, IConfiguration configuration, ICurrentUserService currentUserService, IStringLocalizer<Localization> localizer)
        {
            _ctx = ctx;
            _configuration = configuration;
            _currentUserService = currentUserService;
            _localizer = localizer;
        }

        public Task<Infrastructure.Models.File> Handle(GetTasksReportQuery request, CancellationToken cancellationToken)
        {
            var locale = _currentUserService.Locale;
            var year = request.Year ?? DateTime.Now.Year;
            var exportManager = new ExcelExportManager();
            XLWorkbook wb = new XLWorkbook();

            if (request.ExportCrops)
            {

                // Crops
                IXLWorksheet wsCrops = wb.Worksheets.Add(_localizer["TasksReport.Crops.TabName"]);
                wsCrops.Unprotect();

                var dtCrops = new DataTable();
                dtCrops.Columns.Add(_localizer["TasksReport.Crops.Columns.FieldIdentifier"]);
                dtCrops.Columns.Add(_localizer["TasksReport.Crops.Columns.FieldName"]);
                dtCrops.Columns.Add(_localizer["TasksReport.Crops.Columns.Year"]);
                dtCrops.Columns.Add(_localizer["TasksReport.Crops.Columns.PrimaryCrop"]);
                dtCrops.Columns.Add(_localizer["TasksReport.Crops.Columns.SecondaryCrop"]);
                dtCrops.Columns.Add(_localizer["TasksReport.Crops.Columns.WinterCrop"]);

                var crops = GetCrops(_currentUserService.FarmId!.Value, year, request.FieldIds).OrderBy(x => x.FieldName).ThenBy(x => x.Year);
                var cropsGrouped = crops.GroupBy(x => new { x.FieldIdentifier, x.Year }).OrderBy(x => x.First().FieldName);

                foreach (var mg in cropsGrouped)
                {
                    var first = mg.First();
                    dtCrops.Rows.Add(
                        first.FieldIdentifier,
                        first.FieldName,
                        first.Year,
                        string.Join(", ", mg.Where(x => x.CropType == 1).Select(x => Helpers.GetLocalizedValue(locale, x.CropName, x.CropNameEn))),
                        string.Join(", ", mg.Where(x => x.CropType == 2).Select(x => Helpers.GetLocalizedValue(locale, x.CropName, x.CropNameEn))),
                        string.Join(", ", mg.Where(x => x.CropType == 3).Select(x => Helpers.GetLocalizedValue(locale, x.CropName, x.CropNameEn))));
                }

                IXLTable tblCrops = wsCrops.FirstCell().InsertTable(dtCrops);
                tblCrops.Theme = ExcelExportManager.DefaultTheme;

                exportManager.AdjustToContents(wsCrops);

            }

            if (request.ExportMeasures)
            {
                // Measures
                IXLWorksheet wsMeasures = wb.Worksheets.Add(_localizer["TasksReport.Measures.TabName"]);
                wsMeasures.Unprotect();

                var dtMeasures = new DataTable();
                dtMeasures.Columns.Add(_localizer["TasksReport.Measures.Columns.FieldIdentifier"]);
                dtMeasures.Columns.Add(_localizer["TasksReport.Measures.Columns.FieldName"]);
                dtMeasures.Columns.Add(_localizer["TasksReport.Measures.Columns.Year"]);
                dtMeasures.Columns.Add(_localizer["TasksReport.Measures.Columns.Measures"]);

                var measures = GetMeasures(_currentUserService.FarmId!.Value, year, request.FieldIds);
                var measuresGrouped = measures.GroupBy(x => new { x.FieldIdentifier, x.Year }).OrderBy(x => x.First().FieldName).ThenBy(x => x.Key.Year);

                foreach (var mg in measuresGrouped)
                {
                    var first = mg.First();
                    dtMeasures.Rows.Add(
                        first.FieldIdentifier,
                        first.FieldName,
                        first.Year,
                        string.Join(", ", mg.Select(x => $"{Helpers.GetLocalizedValue(locale, x.MeasureName, x.MeasureNameEn)}")));
                }

                IXLTable tblMeasures = wsMeasures.FirstCell().InsertTable(dtMeasures);
                tblMeasures.Theme = ExcelExportManager.DefaultTheme;

                exportManager.AdjustToContents(wsMeasures);
            }

            // Tasks
            if (request.ExportTasks)
            {
                IXLWorksheet wsTasks = wb.Worksheets.Add(_localizer["TasksReport.Tasks.TabName"]);
                wsTasks.Unprotect();

                var dtTasks = new DataTable();
                dtTasks.Columns.Add(_localizer["TasksReport.Tasks.Columns.Date"], typeof(DateTime));
                dtTasks.Columns.Add(_localizer["TasksReport.Tasks.Columns.FieldIdentifier"]);
                dtTasks.Columns.Add(_localizer["TasksReport.Tasks.Columns.FieldName"]);
                dtTasks.Columns.Add(_localizer["TasksReport.Tasks.Columns.Area"]);
                dtTasks.Columns.Add(_localizer["TasksReport.Tasks.Columns.Crop"]);
                dtTasks.Columns.Add(_localizer["TasksReport.Tasks.Columns.TaskType"]);
                dtTasks.Columns.Add(_localizer["TasksReport.Tasks.Columns.Executor"]);
                dtTasks.Columns.Add(_localizer["TasksReport.Tasks.Columns.Machine"]);
                dtTasks.Columns.Add(_localizer["TasksReport.Tasks.Columns.Attachment"]);
                dtTasks.Columns.Add(_localizer["TasksReport.Tasks.Columns.Note"]);

                var tasks = GetTasks(_currentUserService.FarmId!.Value, year, request.FieldIds);
                foreach (var task in tasks)
                {
                    dtTasks.Rows.Add(
                        task.DateFrom,
                        task.FieldIdentifier,
                        task.FieldName,
                        task.FieldArea,
                        Helpers.GetLocalizedValue(locale, task.CropName, task.CropNameEn),
                        Helpers.GetLocalizedValue(locale, task.TaskTypeName, task.TaskTypeNameEn),
                        task.Executor,
                        task.MachineName,
                        task.AttachmentName,
                        task.Note);
                }

                IXLTable tblTask = wsTasks.FirstCell().InsertTable(dtTasks);
                tblTask.Column(1).Style.DateFormat.Format = "dd.MM.yyyy";
                tblTask.Theme = ExcelExportManager.DefaultTheme;
                tblTask.ShowAutoFilter = true;

                exportManager.AdjustToContents(wsTasks);
            }

            var bytes = exportManager.Export(wb);

            return Task.FromResult(new Infrastructure.Models.File { Bytes = bytes, Filename = exportManager.GetExportFilename() });
        }

        public IEnumerable<TaskReportTaskItem> GetTasks(int farmId, int year, List<int>? fieldIds = null)
        {
            using (var conn = _ctx.CreateConnection())
            {
                var data = conn.Query<TaskReportTaskItem>($@"
                SELECT 
                    t.id,
                    t.farm_id,
                    t.date_from,
                    t.date_to,
                    t.executor,
                    t.note,
                    t.treatment_successful,
                    t.harvest_amount,
                    tt.name AS task_type_name,
                    tt.name_en AS task_type_name_en,
                    tf.duration,
                    f.identifier AS field_identifier,
                    f.name AS field_name,
                    c.identifier AS crop_identifier,
                    c.name AS crop_name,
                    c.name_en AS crop_name_en,
	                ISNULL(fp.area, f.area) AS field_area,
                    m.name AS machine_name,
                    att.name AS attachment_name
                FROM FieldBook.task t
	                JOIN FieldBook.task_field tf ON tf.task_id = t.id AND {DbContext.GetIsDeletedIsActiveCondition("tf")}
	                LEFT JOIN FieldBook.field_plot fp ON tf.field_plot_id = fp.id
                    LEFT JOIN FieldBook.crop c ON c.id = fp.crop_id
	                JOIN FieldBook.field f ON tf.field_id = f.id
                    LEFT JOIN FieldBook.task_type tt ON tt.id = t.task_type_id
                    LEFT JOIN FieldBook.machinery m ON m.id = t.equipment_id
                    LEFT JOIN FieldBook.machinery att ON att.id = t.attached_equipment_id
                WHERE
                    {DbContext.GetIsDeletedIsActiveCondition("t")}
                    AND t.farm_id = @farmId
                    AND YEAR(t.date_from) = @year
                    {((fieldIds?.Any() ?? false) ? "AND f.id IN @fieldIds" : "")}
                ORDER BY t.date_from DESC",
                new { farmId, year, fieldIds });

                return data;
            }
        }

        public IEnumerable<TaskReportMeasureItem> GetMeasures(int farmId, int year, List<int>? fieldIds = null)
        {
            using (var conn = _ctx.CreateConnection())
            {
                var data = conn.Query<TaskReportMeasureItem>($@"
                SELECT
                    fm.year,
	                f.identifier AS field_identifier,
	                f.name AS field_name,
	                c.identifier AS crop_identifier,
	                c.name AS crop_name,
                    c.name_en AS crop_name_en,
	                m.name AS measure_name,
                    m.name_en AS measure_name_en,
	                m.description AS measure_description,
	                m.description_en AS measure_description_en
                FROM FieldBook.field_measure fm
	                JOIN FieldBook.field_plot fp ON fm.field_plot_id = fp.id
	                JOIN FieldBook.field f ON fp.field_id = f.id
	                JOIN FieldBook.crop c ON fp.crop_id = c.id
	                JOIN FieldBook.measure m ON fm.measure_id = m.id
                WHERE
                    {DbContext.GetIsDeletedIsActiveCondition("fm")}
                    AND f.farm_id = @farmId
                    AND fm.year = @year
                    {((fieldIds?.Any() ?? false) ? "AND f.id IN @fieldIds" : "")}
                ORDER BY f.name ASC",
                new { farmId, year, fieldIds });

                return data;
            }
        }


        public IEnumerable<TaskReportCropItem> GetCrops(int farmId, int year, List<int>? fieldIds = null)
        {
            using (var conn = _ctx.CreateConnection())
            {
                var data = conn.Query<TaskReportCropItem>($@"
                 SELECT
	                fp.year,
	                fp.crop_type,
	                fp.area,
	                f.identifier AS field_identifier,
	                f.name AS field_name,
	                c.identifier AS crop_identifier,
	                c.name AS crop_name,
                    c.name_en AS crop_name_en
                 FROM FieldBook.field_plot fp
                     JOIN FieldBook.field f ON fp.field_id = f.id
                     JOIN FieldBook.crop c ON fp.crop_id = c.id
                WHERE
                    {DbContext.GetIsDeletedIsActiveCondition("fp")}
                    AND f.farm_id = @farmId
                    AND fp.year = @year
                    {((fieldIds?.Any() ?? false) ? "AND f.id IN @fieldIds" : "")}
                ORDER BY f.name ASC",
                new { farmId, year, fieldIds });

                return data;
            }
        }
    }

    public class TaskReportTaskItem
    {
        public string? Id { get; set; }
        public int? FarmId { get; set; }
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
        public string? Executor { get; set; }
        public string? Note { get; set; }
        public bool TreatmentSuccessful { get; set; }
        public int? HarvestAmount { get; set; }
        public string? TaskTypeName { get; set; }
        public string? TaskTypeNameEn { get; set; }
        public int? Duration { get; set; }
        public string? FieldIdentifier { get; set; }
        public string? FieldName { get; set; }
        public string? CropIdentifier { get; set; }
        public string? CropName { get; set; }
        public string? CropNameEn { get; set; }
        public decimal? FieldArea { get; set; }
        public string? MachineName { get; set; }
        public string? AttachmentName { get; set; }
    }

    public class TaskReportCropItem
    {
        public int? Year { get; set; }
        public string? FieldIdentifier { get; set; }
        public string? FieldName { get; set; }
        public string? CropIdentifier { get; set; }
        public string? CropName { get; set; }
        public string? CropNameEn { get; set; }
        public decimal? Area { get; set; }
        public int? CropType { get; set; }
    }

    public class TaskReportMeasureItem
    {
        public int? Year { get; set; }
        public string? FieldIdentifier { get; set; }
        public string? FieldName { get; set; }
        public string? CropIdentifier { get; set; }
        public string? CropName { get; set; }
        public string? CropNameEn { get; set; }
        public string? MeasureName { get; set; }
        public string? MeasureNameEn { get; set; }
        public string? MeasureDescription { get; set; }
        public string? MeasureDescriptionEn { get; set; }
    }
}