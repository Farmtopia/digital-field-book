﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITC.FieldBook.Application.Models.DTO
{

    public class CropRotationDto
    {
        public bool HasCropRotation { get; set; }

        public List<CropRotationFieldDto> Fields { get; set; } = new List<CropRotationFieldDto>();

        public List<int> Years { get; set; } = new List<int>();
    }

    public class CropRotationFieldDto
    {
        public int? FieldId { get; set; }

        public string? FieldIdentifier { get; set; }

        public string? FieldName { get; set; }

        public decimal? FieldArea { get; set; }

        public Dictionary<int, CropRotationFieldYearDto> FieldYears { get; set; } = new Dictionary<int, CropRotationFieldYearDto>();
    }

    public class CropRotationFieldYearDto
    {
        public int? FieldId { get; set; }

        public string? FieldIdentifier { get; set; }

        public string? FieldName { get; set; }

        public decimal? FieldArea { get; set; }

        public int? Year { get; set; }

        public bool Editable { get; set; }

        public List<CropRotationPlotDto> Plots { get; set; } = new List<CropRotationPlotDto>();
    }

    public class CropRotationPlotDto
    {
        public FieldPlot? FieldPlot { get; set; }

        public List<FieldMeasure> FieldMeasures { get; set; } = new List<FieldMeasure>();

        public bool Editable { get; set; }

        public string? HexColor { get; set; }
    }
}
