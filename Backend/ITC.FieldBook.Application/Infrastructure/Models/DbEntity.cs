﻿using ITC.FieldBook.Application.Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace ITC.FieldBook.Application.Infrastructure.Models
{
    public abstract class DbEntityWithAudit : DbEntity
    {
        public DbEntityWithAudit(string tableName) : base(tableName, true)
        {

        }

        public DateTime? Created { get; set; }

        public int? CreatedBy { get; set; }

        public DateTime? Modified { get; set; }

        public int? ModifiedBy { get; set; }
    }

    public abstract class DbEntity
    {
        public DbEntity()
        {
            PropertyChanged += DBEntity_PropertyChanged;
        }

        public DbEntity(string? tableName, bool audit = false)
        {
            Audit = audit;
            TableName = tableName;
            PropertyChanged += DBEntity_PropertyChanged;
        }

        private bool _isActive = true;
        [DbColumn("is_active")]
        public bool IsActive { get => _isActive; set { _isActive = value; RaisePropertyChanged(nameof(IsActive)); } }

        private bool _isDeleted = false;
        [DbColumn("is_deleted")]
        public bool IsDeleted { get => _isDeleted; set { _isDeleted = value; RaisePropertyChanged(nameof(IsDeleted)); } }

        /// <summary>
        /// Log changes (requires audit columns in table - created (int), created_by, modified, modified_by)
        /// </summary>
        internal bool Audit { get; set; }

        public int? RowNum { get; set; }

        //[JsonIgnore]
        internal string? TableName { get; set; }

        //[JsonIgnore]
        internal List<string> ChangedProperties { get; set; } = new List<string>();

        //public virtual void Initialize(ICurrentUserService currentUserService)
        //{

        //}

        internal Dictionary<string, object?> GetSqlData(bool includeUnchanged, bool includeVirtual = false)
        {
            Dictionary<string, object?> sqlData = new Dictionary<string, object?>();
            var properties = GetType().GetProperties();
            var propertyInfo = properties?.Where(prop => Attribute.IsDefined(prop, typeof(DbColumn)))?.ToArray();

            if (propertyInfo != null)
            {
                foreach (PropertyInfo p in propertyInfo)
                {
                    DbColumn? attr = p.GetCustomAttribute(typeof(DbColumn), true) as DbColumn;
                    if (attr != null)
                    {
                        // Spremenjene in ključe dodamo za SQL update
                        bool hasChanged = ChangedProperties.Any(x => x == p.Name);
                        var name = !string.IsNullOrEmpty(attr.Name) ? attr.Name : p.Name;
                        if (!sqlData.ContainsKey(name) && p.CanRead && (!attr.Virtual || includeVirtual) && (includeUnchanged || hasChanged || attr.IsKey))
                            sqlData.Add(name, p.GetValue(this, null));
                    }
                }
            }

            return sqlData;
        }


        public virtual List<string> GetKeyColumns()
        {
            List<string> keyColumns = new List<string>();
            var properties = GetType().GetProperties();
            var propertyInfo = properties?.Where(
                prop => (prop.GetCustomAttribute(typeof(DbColumn), true) as DbColumn)?.IsKey ?? false)?.ToArray();

            if (propertyInfo != null)
            {
                foreach (PropertyInfo p in propertyInfo)
                {
                    var attr = p.GetCustomAttribute(typeof(DbColumn), true) as DbColumn;
                    if (attr != null)
                    {
                        var name = !string.IsNullOrEmpty(attr.Name) ? attr.Name : p.Name;
                        if (!keyColumns.Contains(name))
                            keyColumns.Add(name);
                    }
                }
            }

            return keyColumns;
        }

        public virtual string? GetIdentityColumn()
        {
            var properties = GetType().GetProperties();
            var propertyInfo = properties?.Where(
                prop => (prop.GetCustomAttribute(typeof(DbColumn), true) as DbColumn)?.Identity ?? false)?.FirstOrDefault();

            if (propertyInfo != null)
            {
                var attr = propertyInfo.GetCustomAttribute(typeof(DbColumn), true) as DbColumn;
                if (attr != null)
                {
                    var name = !string.IsNullOrEmpty(attr.Name) ? attr.Name : propertyInfo.Name;
                    return name;
                }
            }
            return null;
        }

        #region Property changes

        public event PropertyChangedEventHandler PropertyChanged;

        private void DBEntity_PropertyChanged(object? sender, PropertyChangedEventArgs e)
        {
            // Dodaj med spremenjene propertyje
            if (e.PropertyName != null && !ChangedProperties.Contains(e.PropertyName))
                ChangedProperties.Add(e.PropertyName);
        }

        public void RaisePropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public bool HasChanged(string propertyName)
        {
            if (ChangedProperties?.Contains(propertyName) ?? false)
                return true;
            return false;
        }

        #endregion
    }

    public class DbColumn : Attribute
    {
        public DbColumn() { }

        public DbColumn(string name)
        {
            Name = name;
        }

        public string? Name { get; set; }

        public bool Virtual { get; set; } = false;

        public bool IsKey { get; set; } = false;

        public bool Identity { get; set; } = false;
    }
}
