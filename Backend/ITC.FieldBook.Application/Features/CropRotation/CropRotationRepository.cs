﻿using Dapper;
using ITC.FieldBook.Application.Features.Fields.Crops;
using ITC.FieldBook.Application.Features.Tasks;
using ITC.FieldBook.Application.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ITC.FieldBook.Application.Models;
using DocumentFormat.OpenXml.Bibliography;

namespace ITC.FieldBook.Application.Features.CropRotation
{
    public class CropRotationRepository
    {
        private readonly DbContext _ctx;
        public CropRotationRepository(DbContext ctx) {
            _ctx = ctx;
        }

		/// <summary>
		/// Gets crops in use (if any task has been added for this crop, or has been sowed/haversted/...)
		/// </summary>
		/// <param name="farmId"></param>
		/// <param name="fieldId"></param>
		/// <param name="yearFrom"></param>
		/// <param name="yearTo"></param>
		/// <returns></returns>
        public IEnumerable<CropInUse> GetCropsInUse(int farmId, int? fieldId = null, int? year = null, int? yearTo = null)
        {
            using (var cmd = _ctx.CreateConnection())
            {
				//to preveri če še dela ok... ker je malo spremenjen pogoj spodaj
                return cmd.Query<CropInUse>($@"
					SELECT DISTINCT * FROM (
						SELECT							
							fp.field_id,
							fp.[year] AS crop_year,
							fp.crop_id,
							fp.id AS field_plot_id
						FROM FieldBook.task_field tf
							JOIN FieldBook.field_plot fp ON fp.id = tf.field_plot_id
						WHERE 
							{DbContext.GetIsDeletedIsActiveCondition("tf")}
							AND (@fieldId IS NULL OR tf.field_id = @fieldId)
							AND (@year IS NULL OR (fp.year >= @year AND fp.year <= ISNULL(@yearTo, @year)))
							AND tf.field_plot_id IS NOT NULL

						UNION ALL 

						SELECT DISTINCT
							fp.field_id,
							fp.[year] AS crop_year,
							fp.crop_id,
							fp.id AS field_plot_id
						FROM FieldBook.field_plot fp 
						WHERE
							{DbContext.GetIsDeletedIsActiveCondition("fp")}
							AND (@fieldId IS NULL OR field_id = @fieldId)
							AND (@year IS NULL OR (fp.year >= @year AND fp.year <= ISNULL(@yearTo, @year)))
							AND (fp.sowing_date IS NOT NULL OR fp.harvest_date IS NOT NULL)
					) s ORDER BY field_id, crop_year
					", new { farmId, fieldId, year, yearTo });
            }
        }
    }
}
