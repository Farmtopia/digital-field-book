﻿using ITC.FieldBook.Application.Features.Users;
using ITC.FieldBook.Application.Infrastructure.Interfaces;
using ITC.FieldBook.Application.Infrastructure.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace ITC.FieldBook.Application.Infrastructure.Services
{
    public class CurrentUserService : ICurrentUserService
    {
        private DbContext _ctx;
        private User? _user = null;

        public int Id { get; set; }
        public string? Name { get; set; }
        public bool IsAuthenticated { get; set; } = false;
        public int? FarmId { get; set; }
        public int? Year { get; set; }
        public string? Locale { get; set; }

        public CurrentUserService(IHttpContextAccessor httpContextAccessor, DbContext ctx)
        {
            _ctx = ctx;

            var claim = httpContextAccessor?.HttpContext?.User;
            if (claim != null && claim.Identity != null)
            {
                IsAuthenticated = claim.Identity.IsAuthenticated;
                Id = Convert.ToInt32(claim.FindFirstValue(ClaimTypes.NameIdentifier));
                Name = claim.Identity.Name;

                if (httpContextAccessor != null && httpContextAccessor.HttpContext != null) {
                    var request = httpContextAccessor.HttpContext.Request;

                    // Farm ID
                    if (request.Headers.TryGetValue("FarmId", out var farmIdHeaderVals) && farmIdHeaderVals.Count > 0 && int.TryParse(farmIdHeaderVals[0], out int farmIdHeader))
                        FarmId = farmIdHeader;
                    else if (request.Query.TryGetValue("farmId", out var farmIdQueryVals) && farmIdQueryVals.Count > 0 && int.TryParse(farmIdQueryVals[0], out int farmIdQuery))
                        FarmId = farmIdQuery;

                    // Locale
                    if (request.Headers.TryGetValue("Accept-Language", out var localeHeaderVals) && localeHeaderVals.Count > 0 && localeHeaderVals[0] is string localeHeader && !string.IsNullOrEmpty(localeHeader))
                        Locale = localeHeaderVals;
                    else if (request.Query.TryGetValue("lang", out var localeQueryVals) && localeQueryVals.Count > 0 && localeQueryVals[0] is string localeQuery && !string.IsNullOrEmpty(localeQuery))
                        Locale = localeQuery;
                }

                // If null, read deafult from claims
                if(FarmId == null)
                    FarmId = claim.FindFirstValue("FarmId") is string f && !string.IsNullOrEmpty(f) ? Convert.ToInt32(f) : null;

                Year = DateTime.Now.Year;
                //if (httpContextAccessor != null && httpContextAccessor.HttpContext != null && httpContextAccessor.HttpContext.Request.Headers.TryGetValue("Year", out var yearHeaderVals) && yearHeaderVals.Count > 0 && int.TryParse(yearHeaderVals[0], out int yearHeader))
                //    Year = yearHeader;
                //else
                //    Year = claim.FindFirstValue("Year") is string f && !string.IsNullOrEmpty(f) ? Convert.ToInt32(claim.FindFirstValue("Year")) : null;
            }

        }

        public User? GetUser()
        {
            if (_user == null)
            {
                var usersBll = new UsersRepository(_ctx);
                _user = usersBll.GetUserById(Id);
            }

            return _user;
        }
    }
}
