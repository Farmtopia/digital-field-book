﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITC.FieldBook.Application.Infrastructure.Models
{
    public class EnsureUserResult
    {
        public EnsureUserResult(User? user, bool isNew)
        {
            User = user;
            IsNew = isNew;
        }

        public User? User { get; set; }

        public bool IsNew { get; set; } = false;
    }
}
