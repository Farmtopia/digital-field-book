﻿using ITC.FieldBook.Application.Infrastructure;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using ITC.FieldBook.Application.Infrastructure.Models;
using Microsoft.AspNetCore.Http;
using System.Text.Json;
using MediatR;

namespace ITC.FieldBook.Application.Features.Users
{
    public class LogoutUserController : ApiControllerBase
    {
        [HttpPost("user/logout")]
        public Task LogoutUser()
        {
            return Mediator.Send(new LogoutUserCommand { });
        }
    }

    public class LogoutUserCommand : IRequest
    {
    }

    internal sealed class LogoutUserCommandHandler : IRequestHandler<LogoutUserCommand>
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public LogoutUserCommandHandler(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public async Task Handle(LogoutUserCommand request, CancellationToken cancellationToken)
        {
            //await _httpContextAccessor.HttpContext!.SignOutAsync();
        }
    }
}
