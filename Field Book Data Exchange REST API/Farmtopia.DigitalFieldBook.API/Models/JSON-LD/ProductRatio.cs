﻿using System.Text.Json.Serialization;

namespace Farmtopia.DigitalFieldBook.API.Models.JSON_LD
{
    public class ProductRatio : JsonLdBase
    {
        [JsonPropertyName("numericValue")]
        public string? NumericValue { get; set; }

        [JsonPropertyName("unitOfMeasure")]
        public UnitOfMeasure UnitOfMeasure { get; set; }

        [JsonPropertyName("relatedConcept")]
        public RelatedConcept RelatedConcept { get; set; }
    }
}
