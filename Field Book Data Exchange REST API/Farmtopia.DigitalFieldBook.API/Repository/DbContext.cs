﻿using Microsoft.Data.SqlClient;

namespace Farmtopia.DigitalFieldBook.API.Repository
{
    public class DbContext
    {
        public readonly string ConnectionString;

        public DbContext(IConfiguration configuration)
        {
            var connectionString = configuration.GetConnectionString("Default");

            if (string.IsNullOrEmpty(connectionString))
                throw new Exception("Missing ConnectionString in config.");

            ConnectionString = connectionString;
        }

        public SqlConnection CreateConnection()
        {
            return new SqlConnection(ConnectionString);
        }
    }
}
