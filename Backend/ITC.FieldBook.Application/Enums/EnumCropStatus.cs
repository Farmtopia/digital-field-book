﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITC.FieldBook.Application.Enums
{
    public static class EnumCropStatus
    {
        public const string CROP_STAY = "CROP_STAY";
        public const string CROP_REMOVE = "CROP_REMOVE";
        //public const string USER_DEFINED = "USER_DEFINED";
    }
}
