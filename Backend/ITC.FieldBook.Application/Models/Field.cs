﻿using ITC.FieldBook.Application.Infrastructure.Models;

namespace ITC.FieldBook.Application.Models
{
    public class Field : DbEntityWithAudit
    {
        public Field() : base("FieldBook.field") { }

        private int? _id;
        [DbColumn("id", IsKey = true, Identity = true)]
        public int? Id { get => _id; set { _id = value; RaisePropertyChanged(nameof(Id)); } }

        private int? _farmId;
        [DbColumn("farm_id")]
        public int? FarmId { get => _farmId; set { _farmId = value; RaisePropertyChanged(nameof(FarmId)); } }

        private string? _name;
        [DbColumn("name")]
        public string? Name { get => _name; set { _name = value; RaisePropertyChanged(nameof(Name)); } }

        private string? _identifier;
        [DbColumn("identifier")]
        public string? Identifier { get => _identifier; set { _identifier = value; RaisePropertyChanged(nameof(Identifier)); } }

        private string? _description;
        [DbColumn("description")]
        public string? Description { get => _description; set { _description = value; RaisePropertyChanged(nameof(Description)); } }

        private int? _typeId;
        [DbColumn("type_id")]
        public int? TypeId { get => _typeId; set { _typeId = value; RaisePropertyChanged(nameof(TypeId)); } }

        private decimal? _area;
        [DbColumn("area")]
        public decimal? Area { get => _area; set { _area = value; RaisePropertyChanged(nameof(Area)); } }

        private int? _imageFileId;
        [DbColumn("image_file_id")]
        public int? ImageFileId { get => _imageFileId; set { _imageFileId = value; RaisePropertyChanged(nameof(ImageFileId)); } }

        public string? FieldGeometryWkt { get; set; }

        public string? TypeIdentifier { get; set; }

        public string? TypeName { get; set; }

        public string? TypeNameEn { get; set; }

        public string? ActiveCropIdentifier { get; set; }

        public string? ActiveCropName { get; set; }

        public TaskEntity? LastTask { get; set; }

        public double? Latitude { get; set; }

        public double? Longitude { get; set; }

        public decimal? DistanceFromTarget { get; set; }

        public string? GeoJSON { get; set; }
    }
}
