﻿using System.ComponentModel.DataAnnotations;

namespace Farmtopia.DigitalFieldBook.API.Models.Requests
{
    public class SetSharedSecretKeyDto
    {
        [Required]
        public string? Key { get; set; }

        [Required]
        public string? Token { get; set; }
    }
}
