﻿using ITC.FieldBook.Application.Infrastructure.Interfaces;
using ITC.FieldBook.Application.Infrastructure;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata;
using Dapper;
using ITC.FieldBook.Application.Models;
using ITC.FieldBook.Application.Infrastructure.Models;
using ITC.FieldBook.Application.Features.Farms;
using ITC.FieldBook.Application.Infrastructure.Enums;

namespace ITC.FieldBook.Application.Features.Fields
{
    public class GetFieldbookInfoController : ApiControllerBase
    {
        [HttpGet("fieldbook/info")]
        public Task<FieldBookInfo> GetFieldbookInfo()
        {
            return Mediator.Send(new GetFieldbookInfoQuery { });
        }
    }

    public class GetFieldbookInfoQuery : IRequest<FieldBookInfo>
    {
    }

    internal sealed class GetFieldbookInfoQueryHandler : IRequestHandler<GetFieldbookInfoQuery, FieldBookInfo>
    {
        private readonly DbContext _ctx;
        private readonly IConfiguration _configuration;
        private readonly ICurrentUserService _currentUserService;

        public GetFieldbookInfoQueryHandler(DbContext ctx, IConfiguration configuration, ICurrentUserService currentUserService)
        {
            _ctx = ctx;
            _configuration = configuration;
            _currentUserService = currentUserService;
        }

        public Task<FieldBookInfo> Handle(GetFieldbookInfoQuery request, CancellationToken cancellationToken)
        {
            var response = new FieldBookInfo();
            var user = _currentUserService.GetUser()!;
            var farmId = _currentUserService.FarmId;

            var farmsBll = new FarmsBll(_ctx);
            response.Farms = farmsBll.GetFarms(user.HasRole(EnumRole.Admin) ? null : user.Id!.Value).Data.ToList();

            if(response.Farms.Any() && farmId != null)
                response.DefaultFarm = response.Farms.FirstOrDefault(x => x.Id == farmId);

            return Task.FromResult(response);
        }
    }
}
