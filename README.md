# Digital Field Book

The “Farmtopia Digital Field Book” addresses the data capture and reporting needs of small farms
including the manual recording of: farmers activities (planting, irrigation, phytochemical applications,
harvesting, etc.), farmers observations (phenological growth stages, scouting for pests infestation, soil
sampling, etc.), parcels properties (geo coordinates, soil type, climate, etc.) and assets (e.g.
equipment, machinery, stock of phytochemicals, etc.). The Farmtopia Digital Field Book aims to ensure
data interoperability through the use of standardised data models, access control and APIs. Data
collected with the Fieldbook is accessible and available for reuse through the APIs. The retrieved data
objects are modelled following standardised (when available) semantics formulated in JSON-LD.

## Prerequisites

To deploy Farmtopia Digital Field Book, ensure the following requirements are met:
- **Docker**: Docker Desktop or Docker Compose must be installed.
- **Mapbox API**: A valid Mapbox account with API credentials.

## Mapbox API Credential Generation

To integrate Mapbox, the administrator must obtain a Mapbox API token by following these steps:

- Navigate to the Mapbox website ([MapBox](https://www.mapbox.com/))
- Create an account or log in if you already have one.
- Go to the **Tokens** page and generate a new **Access Token**.

## Installation Procedure

1. Clone the repository to your local machine and navigate to the project directory in the terminal (Linux) or Command Prompt (Windows).
2. Edit the **`docker-compose.yml`** file. Locate line 26, which specifies the **`Mapbox__AccessToken`** environment variable. Uncomment this line and insert your Mapbox API token.
3. Execute the following command to bring up the services:

```
docker-compose up
```
This step will initialize the services and the database, which typically takes between 5 to 15 minutes, depending on system performance.

## Verifying Service Status

To verify that all services are running, use the following command:

```
docker ps
```

Ensure that the following four containers are listed with a status of Up:
- **`itc-fieldbook-web`**
- **`farmtopia-api`**
- **`itc-fieldbook-db`**
- **`itc-fieldbook-api`**

A status of **Up** for all containers indicates successful initialization.

## Service Endpoints

Once all services are running, access the following endpoints:
- **Web Interface**: localhost:9100 (itc-fieldbook-web)
- **Farmtopia API Documentation**: localhost:9102/swagger/index.html (farmtopia-api)
- **ITC Fieldbook API Documentation**: localhost:9101/swagger/index.html (itc-fieldbook-api)

## Default User Credentials

Use the following credentials for the initial login:
- **Username**: **`admin@farmtopia.eu`**
- **Password**: **`admin12345`**

These credentials allow access to the administration interface for further configuration.

Link to the live version of the Farmtopia Digital FieldBook API: ([Farmtopia Digital FieldBook API](https://farmtopia-fb-api.dih-agrifood.com/swagger/index.html)).