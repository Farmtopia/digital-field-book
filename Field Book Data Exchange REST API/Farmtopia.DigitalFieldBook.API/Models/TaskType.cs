﻿namespace Farmtopia.DigitalFieldBook.API.Models
{
    public class TaskTypeEntity
    {
        public int? Id { get; set; }

        public string? Name { get; set; }

        public string? NameEn { get; set; }

        public string? PloutosType { get; set; }
    }

    public class TaskType
    {
        public int? Id { get; set; }

        public string? Name { get; set; }

        public string? NameEn { get; set; }
    }
}
