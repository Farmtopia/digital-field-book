﻿using ITC.FieldBook.Application.Infrastructure.Interfaces;
using ITC.FieldBook.Application.Infrastructure.Models;
using ITC.FieldBook.Application.Infrastructure.Tools;
using Microsoft.AspNetCore.DataProtection.KeyManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Dapper.SqlMapper;

namespace ITC.FieldBook.Application.Models
{
    public class Pesticide : DbEntityWithAudit
    {
        public Pesticide() : base("FieldBook.pesticide") { }

        private int? _id;
        [DbColumn("id", IsKey = true, Identity = true)]
        public int? Id { get => _id; set { _id = value; RaisePropertyChanged(nameof(Id)); } }

        private string? _name;
        [DbColumn("name")]
        public string? Name { get => _name; set { _name = value; RaisePropertyChanged(nameof(Name)); } }

        private string? _nameEn;
        [DbColumn("name_en")]
        public string? NameEn { get => _nameEn; set { _nameEn = value; RaisePropertyChanged(nameof(NameEn)); } }

        private string? _brandName;
        [DbColumn("brand_name")]
        public string? BrandName { get => _brandName; set { _brandName = value; RaisePropertyChanged(nameof(BrandName)); } }

        private bool _isGlobal;
        [DbColumn("is_global")] public bool IsGlobal { get => _isGlobal; set { _isGlobal = value; RaisePropertyChanged(nameof(IsGlobal)); } }

        private int? _farmId;
        [DbColumn("farm_id")] public int? FarmId { get => _farmId; set { _farmId = value; RaisePropertyChanged(nameof(FarmId)); } }

        public string? FarmName { get; set; }

        public decimal? Stock { get; set; }

        public int? StockUnitId { get; set; }

        public string? StockUnit { get; set; }
    }
}
