﻿using System.Text.Json.Serialization;

namespace Farmtopia.DigitalFieldBook.API.Models
{
    public class TaskEntity
    {
        public Guid? Uuid { get; set; }

        public int? TaskTypeId { get; set; }

        public DateTime? DateFrom { get; set; }

        public DateTime? DateTo { get; set; }

        public string? Note { get; set; }
    }

    public class Task
    {
        public Guid? Uuid { get; set; }

        public TaskType? TaskType { get; set; }

        public DateTime? DateFrom { get; set; }

        public DateTime? DateTo { get; set; }

        public string? Note { get; set; }
        
        public List<TaskField> Fields { get; set; } = new List<TaskField>();

        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
        public List<TaskFertilizer>? Fertilizers { get; set; }

        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
        public List<TaskPesticide>? Pesticides { get; set; }
    }
}
