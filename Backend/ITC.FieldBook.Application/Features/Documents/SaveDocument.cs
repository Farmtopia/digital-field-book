﻿using ITC.FieldBook.Application.Infrastructure.Models;
using ITC.FieldBook.Application.Infrastructure;
using MediatR;
using Microsoft.AspNetCore.DataProtection.KeyManagement;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using ITC.FieldBook.Application.Models;
using ITC.FieldBook.Application.Infrastructure.Services;
using ITC.FieldBook.Application.Infrastructure.Interfaces;

namespace ITC.FieldBook.Application.Features.Documents
{
    public class SaveDocumentController : ApiControllerBase
    {
        [HttpPost("documents")]
        public Task SaveDocument([FromBody] Document data)
        {
            return Mediator.Send(new SaveDocumentCommand { Data = data });
        }
    }

    public class SaveDocumentCommand : IRequest
    {
        public Document? Data { get; set; }
    }

    internal sealed class SaveDocumentCommandHandler : IRequestHandler<SaveDocumentCommand>
    {
        private readonly DbContext _ctx;
        private readonly ICurrentUserService _currentUserService;

        public SaveDocumentCommandHandler(DbContext ctx, ICurrentUserService currentUserService)
        {
            _ctx = ctx;
            _currentUserService = currentUserService;
        }

        public Task Handle(SaveDocumentCommand request, CancellationToken cancellationToken)
        {
            if (request.Data != null)
            {
                var user = _currentUserService.GetUser()!;
                var documentsRepository = new DocumentsRepository(_ctx);
                request.Data.FarmId = _currentUserService.FarmId!.Value;
                documentsRepository.SaveDocument(request.Data, user);
            }

            return Task.CompletedTask;
        }
    }
}

