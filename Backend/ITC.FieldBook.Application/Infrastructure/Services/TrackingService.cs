﻿using ITC.FieldBook.Application.Infrastructure.Tools;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITC.FieldBook.Application.Infrastructure.Interfaces
{
    public class TrackingService : ITrackingService
    {
        public ItcTrackingManager Instance { get; set; }

        public TrackingService(IConfiguration configuration)
        {
            Instance = new ItcTrackingManager(configuration["Tracking:ApiUrl"], configuration["Tracking:ApiKey"]);
        }
    }
}
