﻿using ITC.FieldBook.Application.Infrastructure.Models;
using ITC.FieldBook.Application.Infrastructure;
using MediatR;
using Microsoft.AspNetCore.DataProtection.KeyManagement;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using ITC.FieldBook.Application.Models;
using ITC.FieldBook.Application.Infrastructure.Interfaces;
using System.Runtime.InteropServices;
using Microsoft.AspNetCore.Components.Forms;
using System.Transactions;
using ITC.FieldBook.Application.Features.Stock;
using Microsoft.AspNetCore.Mvc.ApplicationModels;
using ITC.FieldBook.Application.Features.Fields;
using ITC.FieldBook.Application.Features.Crops;
using ITC.FieldBook.Application.Enums;
using ITC.FieldBook.Application.Features.Fields.Crops;
using ITC.FieldBook.Application.Features.Units;
using ITC.FieldBook.Application.Features.Pesticides;
using ITC.FieldBook.Application.Features.Fertilizers;
using DocumentFormat.OpenXml.Drawing.Spreadsheet;
using ITC.FieldBook.Application.Features.Documents;

namespace ITC.FieldBook.Application.Features.Tasks
{
    public class SaveTaskController : ApiControllerBase
    {
        [HttpPost("tasks")]
        public Task<Dictionary<string, object?>> SaveTask([FromBody] TaskEntity data)
        {
            return Mediator.Send(new SaveTaskCommand { Data = data });
        }
    }

    public class SaveTaskCommand : IRequest<Dictionary<string, object?>>
    {
        public TaskEntity? Data { get; set; }
    }

    internal sealed class SaveTaskCommandHandler : IRequestHandler<SaveTaskCommand, Dictionary<string, object?>>
    {
        private readonly DbContext _ctx;
        private readonly ICurrentUserService _currentUserService;

        public SaveTaskCommandHandler(DbContext ctx, ICurrentUserService currentUserService)
        {
            _ctx = ctx;
            _currentUserService = currentUserService;
        }

        public Task<Dictionary<string, object?>> Handle(SaveTaskCommand request, CancellationToken cancellationToken)
        {
            var user = _currentUserService.GetUser()!;
            var data = request.Data;
            if (data != null)
            {
                var newItem = data.Id == null;
                using (var scope = new TransactionScope())
                {
                    var farmId = _currentUserService.FarmId!.Value;
                    var year = _currentUserService.Year!.Value;
                    var userId = user.Id!.Value;
                    data.FarmId = farmId;
                    data.Year = year;

                    // Save base task
                    var keys = _ctx.Save(data, user: user);
                    var taskId = Convert.ToInt32(keys["id"]);

                    // Required repositories
                    var tasksRepository = new TasksRepository(_ctx);
                    var stockRepository = new StockRepository(_ctx);
                    var fieldsRepo = new FieldsRepository(_ctx);
                    var documentsRepo = new DocumentsRepository(_ctx);

                    // Task type
                    var taskType = tasksRepository.GetTaskType(data.TaskTypeId!.Value).Item;
                    if (taskType == null)
                        throw new Exception("Invalid task type");

                    // Updating existing task
                    if(!newItem)
                    {
                        // Delete task related data
                        tasksRepository.DeleteTaskFields(farmId, taskId, userId, data.Fields?.Where(x => x.Id != null).Select(x => x.Id!.Value).ToList());
                        tasksRepository.DeleteTaskFertilizers(farmId, taskId, userId, data.Fertilizers?.Where(x => x.Id != null).Select(x => x.Id!.Value).ToList());
                        tasksRepository.DeleteTaskPesticides(farmId, taskId, userId, data.Pesticides?.Where(x => x.Id != null).Select(x => x.Id!.Value).ToList());
                        documentsRepo.DeleteTaskDocuments(farmId, taskId, userId, data.Documents?.Where(x => x.FileId != null).Select(x => x.FileId!.Value).ToList());

                        // Delete stock for task
                        stockRepository.DeleteFertilizerStockForTask(farmId, taskId, userId);
                        stockRepository.DeletePesticideStockForTask(farmId, taskId, userId);

                        // Clear sowing/harvest date based on this task
                        fieldsRepo.ClearSowingDateForFieldPlots(farmId, taskId);
                        fieldsRepo.ClearHarvestDateForFieldPlots(farmId, taskId);
                    }

                    // ----------------------------------------- Fields
                    foreach (var field in data.TaskFields!)
                    {
                        // Field is required
                        if (field.FieldId == null)
                            continue;

                        //// Field is required
                        //var fieldId = field.FieldId!.Value;
                        //var fld = fieldsRepo.GetField(farmId, fieldId).Item;
                        //if (fld == null)
                        //    throw new Exception("Invalid field");

                        field.TaskId = taskId;

                        if (field.FieldPlotId != null)
                        {

                            // Update sowing/harvesting date
                            if (taskType.SystemType == EnumSystemTaskType.SOWING)
                                fieldsRepo.UpdateSowingDateForFieldPlot(farmId, field.FieldPlotId.Value, data.DateFrom, taskId);
                            else if (
                                (taskType.SystemType == EnumSystemTaskType.HARVESTING && string.IsNullOrEmpty(data.CropStatusAfter) /* If harvesting, and different crop status is not specified */)
                                || data.CropStatusAfter == EnumCropStatus.CROP_REMOVE)
                            {
                                fieldsRepo.UpdateHarvestDateForFieldPlot(farmId, field.FieldPlotId.Value, data.DateFrom, taskId);
                            }
                        }

                        tasksRepository.SaveTaskField(field, user);
                    }

                    // ----------------------------------------- Fertilizers
                    if (data.Fertilizers != null)
                    {
                        var fertilizersRepository = new FertilizersRepository(_ctx);

                        foreach (var f in data.Fertilizers)
                        {
                            if (f.FertilizerId == null)
                                continue;

                            var fertilizer = fertilizersRepository.GetFertilizer(f.FertilizerId.Value).Item;
                            if (fertilizer == null)
                                continue;

                            f.TaskId = taskId;
                            tasksRepository.SaveTaskFertilizer(f, user);

                            // Handle stock
                            stockRepository.SaveStockFertilizer(new StockFertilizer { FarmId = farmId, TaskId = taskId, Date = data.DateFrom, FertilizerId = f.FertilizerId, StockType = -1, StockName = taskType.Name, Value = f.AmountUsed, ValueUnitId = f.AmountUsedUnitId, ValueUnit = f.AmountUsedUnit }, user);
                        }
                    }

                    // ----------------------------------------- Pesticides
                    if (data.Pesticides != null)
                    {
                        var pesticidesRepository = new PesticidesRepository(_ctx);

                        foreach (var p in data.Pesticides)
                        {
                            if (p.PesticideId == null)
                                continue;

                            var pesticide = pesticidesRepository.GetPesticide(p.PesticideId.Value).Item;
                            if (pesticide == null)
                                continue;
                            
                            p.TaskId = taskId;
                            tasksRepository.SaveTaskPesticide(p, user);

                            // Handle stock
                            stockRepository.SaveStockPesticide(new StockPesticide { FarmId = farmId, TaskId = taskId, Date = data.DateFrom, PesticideId = p.PesticideId, StockType = -1, StockName = taskType.Name, Value = p.AmountUsed, ValueUnitId = p.AmountUsedUnitId, ValueUnit = p.AmountUsedUnit }, user);
                        }
                    }

                    // ----------------------------------------- Documents
                    if (data.Documents != null)
                    {
                        foreach (var doc in data.Documents)
                        {
                            if (doc.FileId == null)
                                continue;
                            doc.FarmId = farmId;
                            doc.TaskId = taskId;
                            documentsRepo.SaveDocument(doc, user);
                        }
                    }

                    // Success
                    scope.Complete();

                    return Task.FromResult(keys);
                }
            }

            return Task.FromResult(new Dictionary<string, object?>());
        }
    }
}

