﻿using ITC.FieldBook.Application.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITC.FieldBook.Application.Models
{
    public class Document : DbEntityWithAudit
    {
        public Document() : base("FieldBook.document") { }

        private int? _id;
        [DbColumn("id", IsKey = true, Identity = true)]
        public int? Id { get => _id; set { _id = value; RaisePropertyChanged(nameof(Id)); } }

        private int? _farmId;
        [DbColumn("farm_id")]
        public int? FarmId { get => _farmId; set { _farmId = value; RaisePropertyChanged(nameof(FarmId)); } }

        private int? _fileId;
        [DbColumn("file_id")]
        public int? FileId { get => _fileId; set { _fileId = value; RaisePropertyChanged(nameof(FileId)); } }

        private string? _description;
        [DbColumn("description")]
        public string? Description { get => _description; set { _description = value; RaisePropertyChanged(nameof(Description)); } }

        private int? _documentTypeId;
        [DbColumn("document_type_id")]
        public int? DocumentTypeId { get => _documentTypeId; set { _documentTypeId = value; RaisePropertyChanged(nameof(DocumentTypeId)); } }

        private int? _taskId;
        [DbColumn("task_id")]
        public int? TaskId { get => _taskId; set { _taskId = value; RaisePropertyChanged(nameof(TaskId)); } }

        public string? FileDisplayname { get; set; }

        public string? DocumentTypeName { get; set; }

        public string? DocumentTypeNameEn { get; set; }
    }
}
