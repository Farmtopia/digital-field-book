﻿using ITC.FieldBook.Application.Infrastructure;
using ITC.FieldBook.Application.Infrastructure.Models;
using ITC.FieldBook.Application.Models;
using ITC.FieldBook.Application.Features.Tasks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using ITC.FieldBook.Application.Features.Tasks.Types;
using ITC.FieldBook.Application.Features.Units;
using System.Transactions;

namespace ITC.FieldBook.Application.Features.Tasks
{
    internal class TasksRepository
    {
        private DbContext _ctx;

        private readonly string baseTasksQuery = $@"
            SELECT 
                t.id,
                t.farm_id,
                t.year,
                t.date_from,
                t.date_to,
                t.task_type_id,
                t.crop_status_after,
                t.executor,
                t.note,
                t.equipment_id,
                t.attached_equipment_id,
                t.treatment_successful,
                t.harvest_amount,
                t.tracking_trip_id,
                t.data,
                tt.name AS task_type_name,
                tt.name_en AS task_type_name_en,
	            m.name AS machine_name,
	            att.name AS attachment_name,
                (
                    SELECT STRING_AGG(f.identifier + ' ' + f.name, ', ')
                    FROM FieldBook.task_field tf
                        JOIN FieldBook.field f ON f.id = tf.field_id
                    WHERE
                        {DbContext.GetIsDeletedIsActiveCondition("tf")}
                        AND tf.task_id = t.id
                ) AS fields_displayvalue
            FROM FieldBook.task t
                LEFT JOIN FieldBook.task_type tt ON tt.id = t.task_type_id
	            LEFT JOIN FieldBook.machinery m ON m.id = t.equipment_id
	            LEFT JOIN FieldBook.machinery att ON att.id = t.attached_equipment_id
        ";

        public TasksRepository(DbContext ctx) { 
            _ctx = ctx;
        }

        public DbData<TaskEntity> GetTasks(int farmId, int? fieldId = null, DateTime? dateFrom = null, DateTime? dateTo = null)
        {
            var data = _ctx.Get<TaskEntity>($@"
                {baseTasksQuery}
                WHERE
                    {DbContext.GetIsDeletedIsActiveCondition("t")}
                    AND t.farm_id = @farmId
                    {(dateFrom != null ? "AND t.date_from >= @dateFrom" : "")}
                    {(dateTo != null ? "AND t.date_to <= @dateTo" : "")}
                    {(fieldId == null ? "" : "AND t.id IN(SELECT task_id FROM FieldBook.task_field WHERE field_id = @fieldId)")}
                ORDER BY t.date_from DESC, t.modified DESC",
            new { farmId, dateFrom, dateTo, fieldId });

            return data;
        }

        public DbItem<TaskEntity> GetTask(int farmId, int taskId)
        {
            var data = _ctx.GetItem<TaskEntity>($@"
                {baseTasksQuery}
                WHERE
                    {DbContext.GetIsDeletedIsActiveCondition("t")}
                    AND t.farm_id = @farmId
                    AND t.id = @taskId",
            new { farmId, taskId });

            return data;
        }

        public DbData<TaskField> GetTaskFields(int farmId, params int[] taskIds)
        {
            var data = _ctx.Get<TaskField>($@"                
                SELECT 
                    tf.id,
                    tf.task_id,
                    tf.field_id,
                    tf.field_plot_id,
                    tf.duration,
                    f.identifier AS field_identifier,
                    f.name AS field_name,
                    c.identifier AS crop_identifier,
                    c.name AS crop_name,
                    c.name_en AS crop_name_en,
                    fp.area
                FROM FieldBook.task_field tf
                    JOIN FieldBook.field f ON f.id = tf.field_id
                    LEFT JOIN FieldBook.field_plot fp ON tf.field_plot_id = fp.id
                    LEFT JOIN FieldBook.crop c ON c.id = fp.crop_id
                WHERE
                    {DbContext.GetIsDeletedIsActiveCondition("tf")}
                    AND f.farm_id = @farmId
                    {(taskIds != null && taskIds.Length > 0 ? "AND tf.task_id IN @taskIds" : "")}",
            new { farmId, taskIds });

            return data;
        }

        public Dictionary<string, object?> SaveTaskField(TaskField data, User user)
        {
            return _ctx.Save(data, user: user);
        }

        public void DeleteTaskFields(int farmId, int taskId, int userId, List<int>? excludeIds = null)
        {
            using (var conn = _ctx.CreateConnection())
            {
                var now = DateTime.Now;

                conn.Execute($@"
                    UPDATE tf
                    SET is_deleted = 1, is_active = 0, modified = @now, modified_by = @userId
                    FROM FieldBook.task_field tf
                        JOIN FieldBook.task t ON t.id = tf.task_id
                    WHERE
                        {DbContext.GetIsDeletedIsActiveCondition("tf")}
                        AND t.farm_id = @farmId
                        AND tf.task_id = @taskId
                        {((excludeIds?.Any() ?? false) ? "AND tf.id NOT IN @excludeIds" : "")}
                    ", new { farmId, taskId, userId, now, excludeIds });
            }
        }

        public DbData<TaskFertilizer> GetTaskFertilizers(int farmId, int taskId)
        {
            var data = _ctx.Get<TaskFertilizer>($@"                
                SELECT 
                    tf.id,
                    tf.task_id,
                    tf.fertilizer_id,
                    tf.amount,
                    tf.amount_unit_id,
                    tf.amount_unit,
                    tf.amount_used,
                    tf.amount_used_unit_id,
                    tf.amount_used_unit
                FROM FieldBook.task_fertilizer tf
                    JOIN FieldBook.task t ON t.id = tf.task_id
                WHERE
                    {DbContext.GetIsDeletedIsActiveCondition("tf")}
                    AND t.farm_id = @farmId
                    AND tf.task_id = @taskId",
            new { farmId, taskId });

            return data;
        }

        public Dictionary<string, object?> SaveTaskFertilizer(TaskFertilizer data, User user)
        {
            if (data.AmountUnitId != null)
            {
                var unitRepo = new UnitsRepository(_ctx);
                var unit = unitRepo.GetUnit(data.AmountUnitId.Value).Item;
                if (unit != null)
                    data.AmountUnit = unit.Name;
            }

            if (data.AmountUsedUnitId != null)
            {
                var unitRepo = new UnitsRepository(_ctx);
                var unit = unitRepo.GetUnit(data.AmountUsedUnitId.Value).Item;
                if (unit != null)
                    data.AmountUsedUnit = unit.Name;
            }

            return _ctx.Save(data, user: user);
        }

        public void DeleteTaskFertilizers(int farmId, int taskId, int userId, List<int>? excludeIds = null)
        {            
            using(var conn = _ctx.CreateConnection())
            {
                var now = DateTime.Now;

                conn.Execute($@"
                    UPDATE tf
                    SET is_deleted = 1, is_active = 0, modified = @now, modified_by = @userId
                    FROM FieldBook.task_fertilizer tf
                        JOIN FieldBook.task t ON t.id = tf.task_id
                    WHERE
                        {DbContext.GetIsDeletedIsActiveCondition("tf")}
                        AND t.farm_id = @farmId
                        AND tf.task_id = @taskId
                        {((excludeIds?.Any() ?? false) ? "AND tf.id NOT IN @excludeIds" : "")}
                    ", new { farmId, taskId, userId, now, excludeIds });
            }
        }

        public DbData<TaskPesticide> GetTaskPesticides(int farmId, int taskId)
        {
            var data = _ctx.Get<TaskPesticide>($@"                
                SELECT 
                    tp.id,
                    tp.task_id,
                    tp.pesticide_id,
                    tp.amount,
                    tp.amount_unit_id,
                    tp.amount_unit,
                    tp.amount_used,
                    tp.amount_used_unit_id,
                    tp.amount_used_unit
                FROM FieldBook.task_pesticide tp
                    JOIN FieldBook.task t ON t.id = tp.task_id
                WHERE
                    {DbContext.GetIsDeletedIsActiveCondition("tp")}
                    AND t.farm_id = @farmId
                    AND tp.task_id = @taskId",
            new { farmId, taskId });

            return data;
        }

        public Dictionary<string, object?> SaveTaskPesticide(TaskPesticide data, User user)
        {
            if (data.AmountUnitId != null)
            {
                var unitRepo = new UnitsRepository(_ctx);
                var unit = unitRepo.GetUnit(data.AmountUnitId.Value).Item;
                if (unit != null)
                    data.AmountUnit = unit.Name;
            }

            if (data.AmountUsedUnitId != null)
            {
                var unitRepo = new UnitsRepository(_ctx);
                var unit = unitRepo.GetUnit(data.AmountUsedUnitId.Value).Item;
                if (unit != null)
                    data.AmountUsedUnit = unit.Name;
            }

            return _ctx.Save(data, user: user);
        }

        public void DeleteTaskPesticides(int farmId, int taskId, int userId, List<int>? excludeIds = null)
        {
            using (var conn = _ctx.CreateConnection())
            {
                var now = DateTime.Now;

                conn.Execute($@"
                    UPDATE tp
                    SET is_deleted = 1, is_active = 0, modified = @now, modified_by = @userId
                    FROM FieldBook.task_pesticide tp
                        JOIN FieldBook.task t ON t.id = tp.task_id
                    WHERE
                        {DbContext.GetIsDeletedIsActiveCondition("tp")}
                        AND t.farm_id = @farmId
                        AND tp.task_id = @taskId                        
                        {((excludeIds?.Any() ?? false) ? "AND tp.id NOT IN @excludeIds" : "")}
                    ", new { farmId, taskId, userId, now, excludeIds });
            }
        }

        #region Task types

        public DbData<TaskType> GetTaskTypes()
        {
            var data = _ctx.Get<TaskType>($@"
                SELECT 
                    t.id,
                    t.identifier,
                    t.name,
                    t.name_en,
                    t.system_type,
                    t.crop_status,
                    t.crop_status_editable,
                    t.requires_time
                FROM FieldBook.task_type t
                WHERE
                    {DbContext.GetIsDeletedIsActiveCondition("t")}");

            return data;
        }

        public DbItem<TaskType> GetTaskType(int id)
        {
            var data = _ctx.GetItem<TaskType>($@"
                SELECT 
                    t.id,
                    t.identifier,
                    t.name,
                    t.name_en,
                    t.system_type,
                    t.crop_status,
                    t.crop_status_editable,
                    t.requires_time
                FROM FieldBook.task_type t
                WHERE
                    {DbContext.GetIsDeletedIsActiveCondition("t")}
                    AND t.id = @id", new { id });

            return data;
        }

        #endregion
    }
}
