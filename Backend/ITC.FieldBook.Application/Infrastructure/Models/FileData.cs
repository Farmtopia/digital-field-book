﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITC.FieldBook.Application.Infrastructure.Models
{
    public class FileData
    {
        public int? Id { get; set; }

        public string? Displayname { get; set; }

        public string? Filename { get; set; }

        public int? Size { get; set; }

        public string? Extension { get; set; }
        
        public string? StorageType { get; set; }

        public byte[]? Bytes { get; set; }

        public DateTime? Created { get; set; }

        public int? CreatedBy { get; set; }

        public DateTime? Modified { get; set; }

        public int? ModifiedBy { get; set; }
    }
}
