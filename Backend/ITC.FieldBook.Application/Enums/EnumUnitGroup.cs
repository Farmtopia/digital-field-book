﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITC.FieldBook.Application.Enums
{
    public static class EnumUnitGroup
    {
        public const string FERTILIZER = "FERTILIZER";
        public const string FERTILIZER_USAGE = "FERTILIZER_USAGE";

        public const string PESTICIDE = "PESTICIDE";
        public const string PESTICIDE_USAGE = "PESTICIDE_USAGE";

        public const string MACHINERY_PERFORMANCE = "MACHINERY_PERFORMANCE";
    }
}
