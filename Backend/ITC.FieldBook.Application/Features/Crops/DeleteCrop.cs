﻿using ITC.FieldBook.Application.Infrastructure.Models;
using ITC.FieldBook.Application.Infrastructure;
using MediatR;
using Microsoft.AspNetCore.DataProtection.KeyManagement;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using ITC.FieldBook.Application.Models;
using ITC.FieldBook.Application.Infrastructure.Services;
using ITC.FieldBook.Application.Infrastructure.Interfaces;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Authorization;
using ITC.FieldBook.Application.Infrastructure.Enums;

namespace ITC.FieldBook.Application.Features.Crops
{
    [Authorize(Roles = EnumRole.Admin)]
    public class DeleteCropController : ApiControllerBase
    {
        [HttpDelete("crops/{cropId}")]
        public Task DeleteCrop([FromRoute] int? cropId)
        {
            return Mediator.Send(new DeleteCropCommand { CropId = cropId });
        }
    }

    public class DeleteCropCommand : IRequest
    {
        [Required]
        public int? CropId { get; set; }
    }

    internal sealed class DeleteCropCommandHandler : IRequestHandler<DeleteCropCommand>
    {
        private readonly DbContext _ctx;
        private readonly ICurrentUserService _currentUserService;

        public DeleteCropCommandHandler(DbContext ctx, ICurrentUserService currentUserService)
        {
            _ctx = ctx;
            _currentUserService = currentUserService;
        }

        public Task Handle(DeleteCropCommand request, CancellationToken cancellationToken)
        {
            var cropsRepository = new CropsRepository(_ctx);
            var crop = cropsRepository.GetCrop(request.CropId!.Value).Item;
            if (crop != null)
            {
                var user = _currentUserService.GetUser()!;
                _ctx.DeleteItem(crop, user);
            }

            return Task.CompletedTask;
        }
    }
}

