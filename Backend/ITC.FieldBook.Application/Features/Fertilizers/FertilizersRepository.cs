﻿using Azure.Core;
using ITC.FieldBook.Application.Infrastructure;
using ITC.FieldBook.Application.Infrastructure.Models;
using ITC.FieldBook.Application.Models;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITC.FieldBook.Application.Features.Fertilizers
{
    public class FertilizersRepository
    {
        private DbContext _ctx;

        public FertilizersRepository(DbContext ctx) {
            _ctx = ctx;
        }

        //public DbItem<Pesticide> GetFertilizer(int id)
        //{
        //    var data = _ctx.GetItem<Pesticide>($@"
        //        SELECT TOP(1)
        //            f.id,
        //            f.name,
        //            f.brand_name,
        //            f.data,
        //            f.created,
        //            f.created_by,
        //            f.modified,
        //            f.modified_by
        //        FROM FieldBook.fertilizer f
        //        WHERE id = @id
        //    ", new { id });

        //    return data;
        //}

        public DbData<Fertilizer> GetFertilizers(bool includeStock = false, bool stockOnly = false, int? stockFarmId = null, DateTime? stockDate = null, int? taskId = null)
        {
            return _ctx.Get<Fertilizer>($@"
                SELECT 
                    f.id,
                    f.name,
                    f.name_en,
                    f.brand_name,
                    f.[type],
                    f.ratio_n,
                    f.ratio_p,
                    f.ratio_k,
                    f.data,
                    f.is_global,
                    f.farm_id,
                    f.created,
                    frm.name AS farm_name
                    {(includeStock || stockOnly ? ",ISNULL(s.stock, 0) AS stock, s.stock_unit, s.stock_unit_id" : "")}
                FROM FieldBook.fertilizer f
                    LEFT JOIN FieldBook.farm frm ON frm.id = f.farm_id
                    {(includeStock || stockOnly ? $"{(stockOnly ? "JOIN FieldBook.fn_fertilizer_stock(@stockFarmId, NULL, @stockDate, @taskId) s ON s.fertilizer_id = f.id AND s.stock > 0" : "LEFT JOIN FieldBook.fn_fertilizer_stock(@stockFarmId, NULL, @stockDate, @taskId) s ON s.fertilizer_id = f.id")}" : "")}
                WHERE
                    {DbContext.GetIsDeletedIsActiveCondition("f")}
                ORDER BY {(includeStock ? "(CASE WHEN s.stock > 0 THEN 1 ELSE 0 END) DESC, " : "")} f.name ASC
                ",
                new { stockFarmId, stockDate, taskId });
        }

        public DbItem<Fertilizer> GetFertilizer(int fertilizerId, bool includeStock = false, bool stockOnly = false, int? stockFarmId = null, DateTime? stockDate = null, int? taskId = null)
        {
            return _ctx.GetItem<Fertilizer>($@"
                SELECT TOP(1)
                    f.id,
                    f.name,
                    f.name_en,
                    f.brand_name,
                    f.[type],
                    f.ratio_n,
                    f.ratio_p,
                    f.ratio_k,
                    f.data,
                    f.is_global,
                    f.farm_id,
                    f.created,
                    frm.name AS farm_name
                    {(includeStock || stockOnly ? ",s.stock, s.stock_unit, s.stock_unit_id" : "")}
                FROM FieldBook.fertilizer f
                    LEFT JOIN FieldBook.farm frm ON frm.id = f.farm_id
                    {(includeStock || stockOnly ? $"{(stockOnly ? "JOIN FieldBook.fn_fertilizer_stock(@stockFarmId, NULL, @stockDate, @taskId) s ON s.fertilizer_id = f.id AND s.stock > 0" : "LEFT JOIN FieldBook.fn_fertilizer_stock(@stockFarmId, NULL, @stockDate, @taskId) s ON s.fertilizer_id = f.id")}" : "")}
                WHERE
                    {DbContext.GetIsDeletedIsActiveCondition("f")}
                    AND f.id = @fertilizerId
                ", new { stockFarmId, fertilizerId, stockDate, taskId });
        }
    }
}
