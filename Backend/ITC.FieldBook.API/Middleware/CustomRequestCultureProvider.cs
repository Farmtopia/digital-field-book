﻿using Microsoft.AspNetCore.Localization;

namespace ITC.FieldBook.API.Middleware
{
    public class CustomRequestCultureProvider : RequestCultureProvider
    {
        public override Task<ProviderCultureResult?> DetermineProviderCultureResult(HttpContext httpContext)
        {
            var culture = httpContext.Request.Query["lang"].ToString() ??
                          httpContext.Request.Headers["Accept-Language"].ToString();

            if (string.IsNullOrWhiteSpace(culture))
            {
                return Task.FromResult<ProviderCultureResult?>(null);
            }

            var providerResultCulture = new ProviderCultureResult(culture, culture);
            return Task.FromResult<ProviderCultureResult?>(providerResultCulture);
        }
        //public override Task<ProviderCultureResult> DetermineProviderCultureResult(HttpContext httpContext)
        //{
        //}
    }
}
