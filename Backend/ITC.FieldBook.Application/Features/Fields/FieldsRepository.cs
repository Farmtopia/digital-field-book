﻿using Dapper;
using ITC.FieldBook.Application.Infrastructure;
using ITC.FieldBook.Application.Infrastructure.Models;
using ITC.FieldBook.Application.Infrastructure.Services;
using ITC.FieldBook.Application.Models;
using ITC.FieldBook.Application.Models.DTO;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITC.FieldBook.Application.Features.Fields
{
    internal class FieldsRepository
    {
        private DbContext _ctx;

        public FieldsRepository(DbContext ctx)
        {
            _ctx = ctx;
        }

        public DbItem<Field> GetField(int farmId, int fieldId)
        {
            var data = _ctx.GetItem<Field>($@"
                    SELECT TOP(1)
                        f.id,
                        f.identifier,
                        f.farm_id,
                        f.name,
                        f.description,
                        f.area,
				        f.type_id,
                        f.location.Lat AS latitude,
                        f.location.Long AS longitude,
                        f.field_geometry.STAsText() AS field_geometry_wkt,
                        f.location,
                        f.image_file_id,
				        ft.identifier AS type_identifier,
				        ft.name AS type_name,
				        ft.name_en AS type_name_en
                    FROM FieldBook.field f
                        LEFT JOIN FieldBook.field_type ft ON f.type_id = ft.id
                    WHERE
                        {DbContext.GetIsDeletedIsActiveCondition("f")}
                        AND f.farm_id = @farmId
                        AND f.id = @fieldId",
                new { farmId, fieldId });
            return data;
        }

        public DbData<Field> GetFields(int farmId)
        {
            var data = _ctx.Get<Field>($@"
                SELECT 
                    f.id,
                    f.identifier,
                    f.farm_id,
                    f.name,
                    f.description,
                    f.area,
				    f.type_id,
                    f.location.Lat AS latitude,
                    f.location.Long AS longitude,
                    f.image_file_id
                FROM FieldBook.field f
                WHERE
                    {DbContext.GetIsDeletedIsActiveCondition("f")}
                    AND (@farmId IS NULL OR f.farm_id = @farmId)
                ORDER BY f.name ASC",
                new { farmId });
            return data;
        }

        public DbData<Field> GetFields(int? farmId, List<int>? fieldIds = null, decimal? latitude = null, decimal? longitude = null)
        {
            var hasLatLong = latitude != null && longitude != null;

            var data = _ctx.Get<Field>($@"
                {(hasLatLong ? "DECLARE @target geography = geography::Point(@latitude, @longitude, 4326);" : "")}

                SELECT * FROM (
                    SELECT 
                        f.id,
                        f.identifier,
                        f.farm_id,
                        f.name,
                        f.description,
                        f.area,
				        f.type_id,
                        f.location.Lat AS latitude,
                        f.location.Long AS longitude,
                        f.image_file_id,
				        ft.identifier AS type_identifier,
				        ft.name AS type_name,
				        ft.name_en AS type_name_en,
                        {(hasLatLong ? "ROUND(f.location.STDistance(@target)/1000, 2) as distance_from_target" : "NULL AS distance_from_target")}
                    FROM FieldBook.field f
                        LEFT JOIN FieldBook.field_type ft ON f.type_id = ft.id
                        WHERE
                            {DbContext.GetIsDeletedIsActiveCondition("f")}
                            AND f.farm_id = @farmId
                            {((fieldIds?.Any() ?? false) ? "AND f.id IN @fieldIds" : "")}
                ) s
                ORDER BY {(hasLatLong ? "distance_from_target ASC" : "name ASC")}",
                new { farmId, fieldIds, latitude, longitude });
            return data;
        }

        #region Field types

        public DbData<FieldType> GetFieldTypes()
        {
            return _ctx.Get<FieldType>($@"
                SELECT 
                    ft.id,
                    ft.identifier,
                    ft.name,
                    ft.name_en,
                    ft.environment_type,
                    ft.include_in_crop_rotation
                FROM FieldBook.field_type ft
                WHERE
                    {DbContext.GetIsDeletedIsActiveCondition("ft")}");
        }

        public DbItem<FieldType> GetFieldType(int id)
        {
            return _ctx.GetItem<FieldType>($@"
                SELECT TOP(1)
                    ft.id,
                    ft.identifier,
                    ft.name,
                    ft.name_en,
                    ft.environment_type,
                    ft.include_in_crop_rotation
                FROM FieldBook.field_type ft
                WHERE
                    {DbContext.GetIsDeletedIsActiveCondition("ft")}
                    AND ft.id = @id", new { id });
        }

        public int? GetFieldTypeByIdentifier(string identifier)
        {
            using(var connection = _ctx.CreateConnection())
            {
                return connection.ExecuteScalar<int?>("SELECT TOP(1) id FROM FieldBook.field_type WHERE identifier = @identifier", new { identifier });
            }
        }

        #endregion

        #region Field Crops

        public DbData<FieldPlot> GetFieldPlots(int farmId, int? year = null, int? yearTo = null, int? cropType = null, List<int>? fieldIds = null, List<int>? fieldPlotIds = null)
        {
            var data = _ctx.Get<FieldPlot>($@"
                SELECT 
                    fp.id,
                    fp.field_id,
                    fp.year,
                    fp.crop_type,
                    fp.crop_id,
                    fp.crop_variety,
                    fp.area,
                    fp.sowing_date,
                    fp.sowing_task_id,
                    fp.harvest_date,
                    fp.harvest_task_id,
                    c.identifier AS crop_identifier,
                    c.name AS crop_name,
                    c.name_en AS crop_name_en
                FROM FieldBook.field_plot fp
	                JOIN FieldBook.field f ON f.id = fp.field_id
	                JOIN FieldBook.crop c ON c.id = fp.crop_id
                WHERE
                    {DbContext.GetIsDeletedIsActiveCondition("fp")}
                    AND f.farm_id = @farmId 
                    {(year != null ? "AND [year] >= @year AND [year] <= ISNULL(@yearTo, @year)" : "")}
                    {(cropType != null ? "AND fp.crop_type = @cropType" : "")}
                    {(fieldIds != null && fieldIds.Any() ? "AND field_id IN @fieldIds" : "")}
                    {((fieldPlotIds?.Any() ?? false) ? "AND fp.id IN @fieldPlotIds" : "")}
                ORDER BY fp.field_id, fp.crop_type", new { farmId, year, yearTo, cropType, fieldIds, fieldPlotIds });

            return data;
        }

        public DbData<FieldPlot> GetActiveFieldPlots(int farmId, DateTime? dateFrom, DateTime dateTo, List<int>? fieldIds = null)
        {
            var data = _ctx.Get<FieldPlot>($@"
                SELECT 
                    fp.id,
                    fp.field_id,
                    fp.year,
                    fp.crop_type,
                    fp.crop_id,
                    fp.area,
                    fp.sowing_date,
                    fp.sowing_task_id,
                    fp.harvest_date,
                    fp.harvest_task_id,
                    c.identifier AS crop_identifier,
                    c.name AS crop_name,
                    c.name_en AS crop_name_en
                FROM FieldBook.fn_active_crops(@farmId, NULL, @dateFrom, @dateTo) ac
                    JOIN FieldBook.field_plot fp ON ac.field_plot_id = fp.id
	                JOIN FieldBook.field f ON f.id = fp.field_id
	                JOIN FieldBook.crop c ON c.id = fp.crop_id
                WHERE
                    {DbContext.GetIsDeletedIsActiveCondition("fp")}
                    AND f.farm_id = @farmId 
                    {(fieldIds != null && fieldIds.Any() ? "AND fp.field_id IN @fieldIds" : "")}
                ORDER BY fp.field_id, fp.crop_type", new { farmId, dateFrom, dateTo, fieldIds });

            return data;
        }

        public IEnumerable<FieldPlot> GetFieldPlotsInUse(int? fieldId = null, int? year = null)
        {
            using (var connection = _ctx.CreateConnection())
            {
                return connection.Query<FieldPlot>($@"
                    SELECT
                        fp.id,
                        fp.field_id,
                        fp.year,
                        fp.crop_type,
                        fp.crop_id,
                        fp.area,
                        fp.sowing_date,
                        fp.sowing_task_id,
                        fp.harvest_date,
                        fp.harvest_task_id,
                    FROM FieldBook.field_plot fp
                    WHERE
                        {DbContext.GetIsDeletedIsActiveCondition("fp")}
                        AND (@fieldId IS NULL OR field_id = @fieldId)
                        AND (@year IS NULL OR year = @year)
                        AND (sowing_date IS NOT NULL OR harvest_date IS NOT NULL)", new { year, fieldId });
            }
        }

        public void UpdateSowingDateForFieldPlot(int farmId, int fieldPlotId, DateTime? date, int? taskId = null)
        {
            using (var conn = _ctx.CreateConnection())
            {
                conn.Execute($@"
                    UPDATE TOP(1) fp
                    SET
                        sowing_date = @date,
                        sowing_task_id = @taskId
                    FROM FieldBook.field_plot fp
                        JOIN FieldBook.field f ON f.id = fp.field_id
                    WHERE
                        {DbContext.GetIsDeletedIsActiveCondition("fp")}
                        AND f.farm_id = @farmId
                        AND fp.id = @fieldPlotId
                        ", new { farmId, fieldPlotId, date, taskId });
            }
        }

        public void ClearSowingDateForFieldPlots(int farmId, int taskId)
        {
            using (var conn = _ctx.CreateConnection())
            {
                conn.Execute($@"
                    UPDATE fp
                    SET
                        sowing_date = NULL,
                        sowing_task_id = NULL
                    FROM FieldBook.field_plot fp
                        JOIN FieldBook.field f ON f.id = fp.field_id
                    WHERE
                        {DbContext.GetIsDeletedIsActiveCondition("fp")}
                        AND f.farm_id = @farmId
                        AND fp.sowing_task_id = @taskId
                    ", new { farmId, taskId });
            }
        }

        public void UpdateHarvestDateForFieldPlot(int farmId, int fieldPlotId, DateTime? date, int? taskId = null)
        {
            using (var conn = _ctx.CreateConnection())
            {
                conn.Execute($@"
                    UPDATE TOP(1) fp
                    SET
                        harvest_date = @date,
                        harvest_task_id = @taskId
                    FROM FieldBook.field_plot fp
                        JOIN FieldBook.field f ON f.id = fp.field_id
                    WHERE
                        {DbContext.GetIsDeletedIsActiveCondition("fp")}
                        AND fp.sowing_date <= @date
                        AND f.farm_id = @farmId
                        AND fp.id = @fieldPlotId
                        ", new { farmId, fieldPlotId, date, taskId });
            }
        }

        public void ClearHarvestDateForFieldPlots(int farmId, int taskId)
        {
            using (var conn = _ctx.CreateConnection())
            {
                conn.Execute($@"
                    UPDATE fp
                    SET
                        harvest_date = NULL,
                        harvest_task_id = NULL
                    FROM FieldBook.field_plot fp
                        JOIN FieldBook.field f ON f.id = fp.field_id
                    WHERE
                        {DbContext.GetIsDeletedIsActiveCondition("fp")}
                        AND f.farm_id = @farmId
                        AND fp.harvest_task_id = @taskId
                    ", new { farmId, taskId });
            }
        }

        public void DeleteFieldPlots(int farmId, int fieldId, int userId, List<int>? excludeIds = null)
        {
            using (var conn = _ctx.CreateConnection())
            {
                var now = DateTime.Now;

                conn.Execute($@"
                    UPDATE tf
                    SET is_deleted = 1, is_active = 0, modified = @now, modified_by = @userId
                    FROM FieldBook.field_plot fp
                        JOIN FieldBook.field f ON f.id = fp.field_id
                    WHERE
                        {DbContext.GetIsDeletedIsActiveCondition("fp")}
                        AND t.farm_id = @farmId
                        AND fp.field_id = @fieldId
                        {((excludeIds?.Any() ?? false) ? "AND fp.id NOT IN @excludeIds" : "")}
                    ", new { farmId, fieldId, userId, now, excludeIds });
            }
        }

        #endregion

        #region Field Measures

        public DbData<FieldMeasure> GetFieldMeasures(int farmId, int? fieldId = null, int? fieldPlotId = null, int? year = null, int? yearTo = null)
        {
            var data = _ctx.Get<FieldMeasure>($@"
                SELECT 
                    fm.id,
                    fm.field_id,
                    fm.field_plot_id,
                    fm.measure_id,
                    fm.year,
                    m.name AS measure_name,
                    m.name_en AS measure_name_en,
                    m.description AS measure_description,
                    m.description_en AS measure_description_en,
                    m.category AS measure_category
                FROM FieldBook.field_measure fm
	                JOIN FieldBook.field f ON f.id = fm.field_id
	                JOIN FieldBook.measure m ON m.id = fm.measure_id
                WHERE
                    {DbContext.GetIsDeletedIsActiveCondition("fm")}
                    AND f.farm_id = @farmId 
                    {(year != null ? "AND [year] >= @year AND [year] <= ISNULL(@yearTo, @year)" : "")}
                    {(fieldId != null ? "AND fm.field_id = @fieldId" : "")}
                    {(fieldPlotId != null ? "AND fm.field_plot_id = @fieldPlotId" : "")}
                ORDER BY m.category, m.name", new { farmId, year, yearTo, fieldId, fieldPlotId });

            return data;
        }

        public void DeleteFieldMeasures(int farmId, int fieldId, int userId, List<int>? excludeIds = null)
        {
            using (var conn = _ctx.CreateConnection())
            {
                var now = DateTime.Now;

                conn.Execute($@"
                    UPDATE tf
                    SET is_deleted = 1, is_active = 0, modified = @now, modified_by = @userId
                    FROM FieldBook.field_measure fm
                        JOIN FieldBook.field f ON f.id = fm.field_id
                    WHERE
                        {DbContext.GetIsDeletedIsActiveCondition("fm")}
                        AND t.farm_id = @farmId
                        AND fm.field_id = @fieldId
                        {((excludeIds?.Any() ?? false) ? "AND fm.id NOT IN @excludeIds" : "")}
                    ", new { farmId, fieldId, userId, now, excludeIds });
            }
        }

        #endregion
    }
}
