﻿using ITC.FieldBook.Application.Infrastructure.Interfaces;
using ITC.FieldBook.Application.Infrastructure;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using ITC.FieldBook.Application.Infrastructure.Models;
using ITC.FieldBook.Application.Models;
using System.ComponentModel.DataAnnotations;
using ITC.FieldBook.Application.Infrastructure.Tools;

namespace ITC.FieldBook.Application.Features.Tasks.Types
{
    public class GetTaskTypeController : ApiControllerBase
    {
        [HttpGet("tasks/task-types/{taskTypeId}")]
        public Task<DbItem<TaskType>> GetTaskType([FromRoute] int? taskTypeId)
        {
            return Mediator.Send(new GetTaskTypeQuery { TaskTypeId = taskTypeId });
        }
    }

    public class GetTaskTypeQuery : IRequest<DbItem<TaskType>>
    {
        [Required]
        public int? TaskTypeId { get; set; }
    }

    internal sealed class GetTaskTypeQueryHandler : IRequestHandler<GetTaskTypeQuery, DbItem<TaskType>>
    {
        private readonly DbContext _ctx;
        private readonly IConfiguration _configuration;
        private readonly ICurrentUserService _currentUserService;

        public GetTaskTypeQueryHandler(DbContext ctx, IConfiguration configuration, ICurrentUserService currentUserService)
        {
            _ctx = ctx;
            _configuration = configuration;
            _currentUserService = currentUserService;
        }

        public Task<DbItem<TaskType>> Handle(GetTaskTypeQuery request, CancellationToken cancellationToken)
        {
            var tasksRepo = new TasksRepository(_ctx);
            return Task.FromResult(tasksRepo.GetTaskType(request.TaskTypeId!.Value));
        }
    }
}
