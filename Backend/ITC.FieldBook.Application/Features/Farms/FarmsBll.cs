﻿using Dapper;
using ITC.FieldBook.Application.Infrastructure;
using ITC.FieldBook.Application.Infrastructure.Interfaces;
using ITC.FieldBook.Application.Infrastructure.Models;
using ITC.FieldBook.Application.Infrastructure.Services;
using ITC.FieldBook.Application.Models;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITC.FieldBook.Application.Features.Farms
{
    public class FarmsBll
    {
        private readonly DbContext _ctx;
        private readonly FarmsRepository _repository;

        public FarmsBll(DbContext ctx)
        {
            _ctx = ctx;
            _repository = new FarmsRepository(_ctx);
        }

        public DbData<Farm> GetFarms(int? userId = null)
        {
            var farms = _repository.GetFarms(userId);
            return farms;
        }

        public int? UpdateImage(IConfiguration configuration, IFileService fileService, int farmId, double latitude, double longitude)
        {
            var imageUrl = configuration!["Mapbox:Field:ImageUrl"];
            var accessToken = configuration["Mapbox:AccessToken"];

            var downloadUrl = imageUrl.Replace("{access_token}", accessToken).Replace("{latitude}", latitude.ToString("0.#########", System.Globalization.CultureInfo.InvariantCulture)).Replace("{longitude}", longitude.ToString("0.#########", System.Globalization.CultureInfo.InvariantCulture));

            using (HttpClient client = new HttpClient())
            {
                using (var response = client.GetAsync(downloadUrl).Result)
                {
                    if (response.IsSuccessStatusCode)
                    {
                        var bytes = response.Content.ReadAsByteArrayAsync().Result;
                        var fileId = fileService.Save("Image.jpg", bytes, new SaveFileSettings { Folder = @$"Farms\Images\{farmId}" });
                        UpdateFarmThumbFileId(farmId, fileId);

                        return fileId;
                    }
                }
            }

            return null;
        }

        private void UpdateFarmThumbFileId(int farmId, int fileId)
        {
            using (var conn = _ctx.CreateConnection())
            {
                conn.Execute(@"UPDATE TOP(1) FieldBook.farm SET thumb_file_id = @fileId WHERE id = @farmId", new { farmId, fileId });
            }
        }
    }
}
