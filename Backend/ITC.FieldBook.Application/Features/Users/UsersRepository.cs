﻿using ITC.FieldBook.Application.Infrastructure.Interfaces;
using ITC.FieldBook.Application.Infrastructure.Models;
using ITC.FieldBook.Application.Infrastructure;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ITC.FieldBook.Application.Models;
using Azure.Core;

namespace ITC.FieldBook.Application.Features.Users
{

    internal class UsersRepository
    {
        private DbContext _ctx;

        public UsersRepository(DbContext ctx)
        {
            _ctx = ctx;
        }

        //public bool UserExists(int id)
        //{
        //    using (var connection = _ctx.CreateConnection())
        //    {
        //        return connection.ExecuteScalar<bool>($@"
        //            SELECT 
        //                CAST(CASE WHEN EXISTS(
        //                    SELECT TOP(1) 1
        //                    FROM FieldBook.[user] u
        //                    WHERE
        //                        {DbContext.GetIsDeletedIsActiveCondition("u")}
        //                        AND u.id = @id
        //                ) THEN 1 ELSE 0 END AS BIT)
        //        ", new { id });
        //    }
        //}

        //public int? GetUserStatusById(int id)
        //{
        //    using (var connection = _ctx.CreateConnection())
        //    {
        //        return connection.ExecuteScalar<int?>($@"
        //            SELECT TOP(1) CASE WHEN ISNULL(is_deleted, 0) = 1 THEN 0 ELSE ISNULL(is_active, 1) END
        //            FROM FieldBook.[user] u
        //            WHERE u.id = @id
        //        ", new { id });
        //    }
        //}

        //public int? GetUserStatusByUuid(Guid uuid)
        //{
        //    using (var connection = _ctx.CreateConnection())
        //    {
        //        return connection.ExecuteScalar<int?>($@"
        //            SELECT TOP(1) CASE WHEN ISNULL(is_deleted, 0) = 1 THEN 0 ELSE ISNULL(is_active, 1) END
        //            FROM FieldBook.[user] u
        //            WHERE u.uuid = @uuid
        //        ", new { uuid });
        //    }
        //}

        //public int? GetUserStatusByEmail(string email)
        //{
        //    using (var connection = _ctx.CreateConnection())
        //    {
        //        return connection.ExecuteScalar<int?>($@"
        //            SELECT TOP(1) CASE WHEN ISNULL(is_deleted, 0) = 1 THEN 0 ELSE ISNULL(is_active, 1) END
        //            FROM FieldBook.[user] u
        //            WHERE u.email = @email
        //        ", new { email });
        //    }
        //}

        public User? GetUserById(int id, bool includeInactive = false)
        {
            using (var connection = _ctx.CreateConnection())
            {
                return connection.QuerySingleOrDefault<User?>($@"
                    SELECT TOP(1)
                        u.id,
                        u.uuid,
                        u.name,
                        u.surname,
                        u.displayname,
                        u.email,
                        u.role,
                        u.last_login,
                        u.is_active,
                        (SELECT TOP(1) farm_id FROM FieldBook.user_farm WHERE {DbContext.GetIsDeletedIsActiveCondition()} AND user_id = u.id) AS farm_id /* TODO: multiple farms... */
                    FROM FieldBook.[user] u
                    WHERE
                        {DbContext.GetIsDeletedIsActiveCondition("u", includeInactive)}
                        AND u.id = @id
                ", new { id });
            }
        }

        public User? GetUserByUuid(Guid uuid, bool includeInactive = false)
        {
            using (var connection = _ctx.CreateConnection())
            {
                return connection.QuerySingleOrDefault<User>($@"
                    SELECT TOP(1)
                        u.id,
                        u.uuid,
                        u.name,
                        u.surname,
                        u.displayname,
                        u.email,
                        u.role,
                        u.last_login,
                        u.is_active,
                        (SELECT TOP(1) farm_id FROM FieldBook.user_farm WHERE {DbContext.GetIsDeletedIsActiveCondition()} AND user_id = u.id) AS farm_id /* TODO: multiple farms... */
                    FROM FieldBook.[user] u
                    WHERE
                        {DbContext.GetIsDeletedIsActiveCondition("u", includeInactive)}
                        AND u.uuid = @uuid
                ", new { uuid });
            }
        }

        public User? GetUserByEmail(string email, bool includeInactive = false)
        {
            using (var connection = _ctx.CreateConnection())
            {
                return connection.QuerySingleOrDefault<User?>($@"
                    SELECT TOP(1)
                        u.id,
                        u.uuid,
                        u.name,
                        u.surname,
                        u.displayname,
                        u.email,
                        u.role,
                        u.last_login,
                        u.is_active,
                        (SELECT TOP(1) farm_id FROM FieldBook.user_farm WHERE {DbContext.GetIsDeletedIsActiveCondition()} AND user_id = u.id) AS farm_id /* TODO: multiple farms... */
                    FROM FieldBook.[user] u
                    WHERE
                        {DbContext.GetIsDeletedIsActiveCondition("u", includeInactive)}
                        AND u.email = @email
                ", new { email });
            }
        }

        public DbData<User> GetUsers(bool includeInactive = false)
        {
            return _ctx.Get<User>($@"
                SELECT
                    u.id,
                    u.uuid,
                    u.name,
                    u.surname,
                    u.displayname,
                    u.email,
                    u.role,
                    u.last_login,
                    u.is_active,
                    (SELECT TOP(1) farm_id FROM FieldBook.user_farm WHERE {DbContext.GetIsDeletedIsActiveCondition()} AND user_id = u.id) AS farm_id /* TODO: multiple farms... */
                FROM FieldBook.[user] u
                WHERE
                    {DbContext.GetIsDeletedIsActiveCondition("u", includeInactive)}");
        }
        public int CreateUser(User user)
        {
            using (var connection = _ctx.CreateConnection())
            {
                var now = DateTime.Now;
                return connection.ExecuteScalar<int>(@"
                    INSERT INTO FieldBook.[user] (uuid, name, surname, displayname, email, role, created, modified)
                    OUTPUT INSERTED.id
                    VALUES (@uuid, @name, @surname, @displayname, @email, @role, @created, @modified)",
                    new { uuid = user.Uuid, name = user.Name, surname = user.Surname, displayname = user.Displayname, email = user.Email, role = user.Role, created = now, modified = now });
            }
        }

        public int UpdateUserData(User user)
        {
            using (var connection = _ctx.CreateConnection())
            {
                return connection.ExecuteScalar<int>(@"
                    UPDATE TOP(1) FieldBook.[user] 
                    SET name = @name, surname = @surname, displayname = @displayname, email = @email
                    OUTPUT INSERTED.id
                    WHERE uuid = @uuid",
                    new { uuid = user.Uuid, name = user.Name, surname = user.Surname, displayname = user.Displayname, email = user.Email });
            }
        }


        public void UpdateLastLogin(int userId, DateTime lastLogin)
        {
            using (var connection = _ctx.CreateConnection())
            {
                connection.Execute(@"
                    UPDATE TOP(1) FieldBook.[user]
                    SET last_login = @last_login
                    WHERE id = @id",
                    new { id = userId, last_login = lastLogin });
            }
        }

        public DbData<UserCropSetting> GetUserCropSettings(int userId)
        {
            return _ctx.Get<UserCropSetting>(@"
                SELECT
                    user_id,
                    crop_id,
                    is_favorite,
                    hex_color
                FROM FieldBook.user_crop_setting
                WHERE user_id = @userId
                ", new { userId });
        }

        public UserPasswordSalt? GetUserPasswordSalt(int userId)
        {
            using (var connection = _ctx.CreateConnection())
            {
                return connection.QueryFirstOrDefault<UserPasswordSalt>(@"
                    SELECT TOP(1) 
                        password, 
                        password_salt 
                    FROM FieldBook.[user] u
                    WHERE 
                        u.id = @id", new { id = userId });
            }
        }
    }
}
