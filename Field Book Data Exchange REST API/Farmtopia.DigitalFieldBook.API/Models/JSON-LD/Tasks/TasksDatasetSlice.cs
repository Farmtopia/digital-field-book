﻿using System.Text.Json.Serialization;

namespace Farmtopia.DigitalFieldBook.API.Models.JSON_LD.Tasks
{
    public class TasksDatasetSlice : DatasetSliceBase
    {
        [JsonPropertyName("@sliceContents")]
        public List<TasksSliceContent> SliceContents { get; set; }
    }
}
