﻿using ITC.FieldBook.Application.Infrastructure.Models;
using ITC.FieldBook.Application.Infrastructure;
using MediatR;
using Microsoft.AspNetCore.DataProtection.KeyManagement;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using ITC.FieldBook.Application.Models;
using ITC.FieldBook.Application.Infrastructure.Services;
using ITC.FieldBook.Application.Infrastructure.Interfaces;
using System.ComponentModel.DataAnnotations;

namespace ITC.FieldBook.Application.Features.Machinery
{
    public class DeleteMachineController : ApiControllerBase
    {
        [HttpDelete("machinery/{machineryId}")]
        public Task DeleteMachine([FromRoute] int? machineryId)
        {
            return Mediator.Send(new DeleteMachineCommand { MachineryId = machineryId });
        }
    }

    public class DeleteMachineCommand : IRequest
    {
        [Required]
        public int? MachineryId { get; set; }
    }

    internal sealed class DeleteMachineCommandHandler : IRequestHandler<DeleteMachineCommand>
    {
        private readonly DbContext _ctx;
        private readonly ICurrentUserService _currentUserService;

        public DeleteMachineCommandHandler(DbContext ctx, ICurrentUserService currentUserService)
        {
            _ctx = ctx;
            _currentUserService = currentUserService;
        }

        public Task Handle(DeleteMachineCommand request, CancellationToken cancellationToken)
        {
            var machineryRepo = new MachineryRepository(_ctx);
            var machine = machineryRepo.GetMachine(request.MachineryId!.Value, _currentUserService.FarmId!.Value).Item;
            if (machine != null)
            {
                var user = _currentUserService.GetUser()!;
                _ctx.DeleteItem(machine, user);
            }

            return Task.CompletedTask;
        }
    }
}

