﻿using ITC.FieldBook.Application.Infrastructure.Models;
using ITC.FieldBook.Application.Infrastructure;
using MediatR;
using Microsoft.AspNetCore.DataProtection.KeyManagement;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using ITC.FieldBook.Application.Models;
using ITC.FieldBook.Application.Infrastructure.Interfaces;
using ITC.FieldBook.Application.Features.Units;

namespace ITC.FieldBook.Application.Features.Machinery
{
    public class SaveMachineryController : ApiControllerBase
    {
        [HttpPost("machinery")]
        public Task SaveMachinery([FromBody] Models.Machinery data)
        {
            return Mediator.Send(new SaveMachineryCommand { Data = data });
        }
    }

    public class SaveMachineryCommand : IRequest
    {
        public Models.Machinery? Data { get; set; }
    }

    internal sealed class SaveMachineryCommandHandler : IRequestHandler<SaveMachineryCommand>
    {
        private readonly DbContext _ctx;
        private readonly ICurrentUserService _currentUserService;

        public SaveMachineryCommandHandler(DbContext ctx, ICurrentUserService currentUserService)
        {
            _ctx = ctx;
            _currentUserService = currentUserService;
        }

        public Task Handle(SaveMachineryCommand request, CancellationToken cancellationToken)
        {
            var data = request.Data;
            if (data != null)
            {
                var user = _currentUserService.GetUser()!;
                data.FarmId = _currentUserService.FarmId!.Value;


                if (data.PerformanceUnitId != null)
                {
                    var unitRepo = new UnitsRepository(_ctx);
                    var unit = unitRepo.GetUnit(data.PerformanceUnitId.Value).Item;
                    if (unit != null)
                    {
                        data.PerformanceUnit = unit.Name;
                    }
                }

                _ctx.Save(data, user: user);
            }

            return Task.CompletedTask;
        }
    }
}

