﻿using System.Text.Json.Serialization;

namespace Farmtopia.DigitalFieldBook.API.Models.JSON_LD.Fields
{
    public class FieldsGraph : GraphBase
    {
        [JsonPropertyName("datasetSlices")]
        public List<FieldsDatasetSlice> DatasetSlices { get; set; }
    }
}
