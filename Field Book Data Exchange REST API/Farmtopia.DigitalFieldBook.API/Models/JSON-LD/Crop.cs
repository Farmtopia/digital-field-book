﻿using System.Text.Json.Serialization;

namespace Farmtopia.DigitalFieldBook.API.Models.JSON_LD
{
    public class Crop : JsonLdBase
    {
        [JsonPropertyName("cropVariety")]
        public string CropVariety { get; set; }

        [JsonPropertyName("agrovocCode")]
        public string AgrovocCode { get; set; }

        [JsonPropertyName("hasPlantDate")]
        public DateTimeOffset? HasPlantDate { get; set; }

        [JsonPropertyName("hasHarvestDate")]
        public DateTime? HasHarvestDate { get; set; }
    }
}
