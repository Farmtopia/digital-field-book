﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Data;
using Microsoft.Extensions.Configuration;
using static System.Net.Mime.MediaTypeNames;
using Microsoft.AspNetCore.Authorization;
using ITC.FieldBook.Application.Infrastructure;
using ITC.FieldBook.Application.Infrastructure.Tools;
using ITC.FieldBook.Application.Infrastructure.Models;
using ITC.FieldBook.Application.Infrastructure.Interfaces;
using ITC.FieldBook.Application.Infrastructure.Enums;

namespace ITC.FieldBook.Application.Features.Files
{
    public class GetFileController : ApiControllerBase
    {
        [HttpGet("files/{fileId}")]
        public async Task<IActionResult> GetFile(int fileId, bool inline = false)
        {
            var file = await Mediator.Send(new GetFileQuery { FileId = fileId });
            if(file == null)
                return NotFound();

            var mimeType = MimeTypeHelper.GetMimeTypeForFileExtension(file.Filename!);

            // Response...
            System.Net.Mime.ContentDisposition cd = new System.Net.Mime.ContentDisposition
            {
                FileName = file.Filename,
                Inline = inline // false = prompt the user for downloading;  true = browser to try to show the file inline
            };
            Response.Headers.Add("Content-Disposition", cd.ToString());
            Response.Headers.Add("X-Content-Type-Options", "nosniff");

            return File(file.Bytes!, mimeType, file.Displayname);
        }
    }

    public class GetFileQuery : IRequest<FileData?>
    {
        public int FileId { get; set; }
    }
    internal sealed class GetFileQueryHandler : IRequestHandler<GetFileQuery, FileData?>
    {
        private readonly DbContext _ctx;
        private readonly IConfiguration _configuration;
        private readonly ICurrentUserService _currentUserService;
        private readonly IFileService _fileService;

        public GetFileQueryHandler(DbContext ctx, IConfiguration configuration, ICurrentUserService currentUserService, IFileService fileService)
        {
            _ctx = ctx;
            _configuration= configuration;
            _currentUserService = currentUserService;
            _fileService = fileService;
        }

        public Task<FileData?> Handle(GetFileQuery request, CancellationToken cancellationToken)
        {
            //Urediti da bo dobil datoteke za kmetijo, ne uporabnika, kot drugje... _currentUserService.FarmId
            var currentUser = _currentUserService.GetUser()!;
            var file = _fileService.Get(request.FileId, true);
            if(file == null /*|| (file.CreatedBy != currentUser.Id && currentUser.Role != EnumRole.Admin)*/)
                return Task.FromResult<FileData?>(null);                
            return Task.FromResult<FileData?>(file);
        }
    }

}
