﻿using ITC.FieldBook.Application.Infrastructure.Interfaces;
using ITC.FieldBook.Application.Infrastructure;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using Dapper;
using ITC.FieldBook.Application.Models;
using ITC.FieldBook.Application.Infrastructure.Models;

namespace ITC.FieldBook.Application.Features.Documents.Types
{
    public class GetDocumentTypesController : ApiControllerBase
    {
        [HttpGet("documents/document-types")]
        public Task<DbData<DocumentType>> GetDocumentTypes()
        {
            return Mediator.Send(new GetDocumentTypesQuery { });
        }
    }

    public class GetDocumentTypesQuery : IRequest<DbData<DocumentType>>
    {
    }

    internal sealed class GetDocumentTypesQueryHandler : IRequestHandler<GetDocumentTypesQuery, DbData<DocumentType>>
    {
        private readonly DbContext _ctx;

        public GetDocumentTypesQueryHandler(DbContext ctx)
        {
            _ctx = ctx;
        }

        public Task<DbData<DocumentType>> Handle(GetDocumentTypesQuery request, CancellationToken cancellationToken)
        {
            var documentsRepository = new DocumentsRepository(_ctx);
            var data = documentsRepository.GetDocumentTypes();

            return Task.FromResult(data);
        }
    }
}
