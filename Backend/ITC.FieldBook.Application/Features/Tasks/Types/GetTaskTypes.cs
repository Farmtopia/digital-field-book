﻿using ITC.FieldBook.Application.Infrastructure.Interfaces;
using ITC.FieldBook.Application.Infrastructure;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using ITC.FieldBook.Application.Infrastructure.Models;
using ITC.FieldBook.Application.Models;
using ITC.FieldBook.Application.Infrastructure.Tools;

namespace ITC.FieldBook.Application.Features.Tasks.Types
{
    public class GetTaskTypesController : ApiControllerBase
    {
        [HttpGet("tasks/task-types")]
        public Task<DbData<TaskType>> GetTaskTypes()
        {
            return Mediator.Send(new GetTaskTypesQuery());
        }
    }

    public class GetTaskTypesQuery : BaseDataQuery<DbData<TaskType>>
    {
    }

    internal sealed class GetTaskTypesQueryHandler : IRequestHandler<GetTaskTypesQuery, DbData<TaskType>>
    {
        private readonly DbContext _ctx;
        private readonly IConfiguration _configuration;
        private readonly ICurrentUserService _currentUserService;

        public GetTaskTypesQueryHandler(DbContext ctx, IConfiguration configuration, ICurrentUserService currentUserService)
        {
            _ctx = ctx;
            _configuration = configuration;
            _currentUserService = currentUserService;
        }

        public Task<DbData<TaskType>> Handle(GetTaskTypesQuery request, CancellationToken cancellationToken)
        {
            var tasksRepo = new TasksRepository(_ctx);
            return Task.FromResult(tasksRepo.GetTaskTypes());
        }
    }
}
