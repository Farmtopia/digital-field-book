﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITC.FieldBook.Application.Enums
{
    public static class EnumStorageType
    {
        public const string FILESYSTEM = "FILESYSTEM";
        public const string AZURE = "AZURE";
    }
}
