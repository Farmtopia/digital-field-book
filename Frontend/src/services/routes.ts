export const routes = {
  cropRotation: {
    default: 'crop-rotation',
    sum: 'crop-rotation/sum',
  },
  documents: {
    default: 'documents',
    single: 'documents/{documentId}',
    types: {
      default: 'documents/document-types',
      single: 'documenst/document-types/{documentTypeId}',
    },
  },
  fieldbook: {
    info: 'fieldbook/info',
  },
  files: {
    get: '/api/files/{fileId}',
  },
  farmManager: {
    machinery: {
      default: 'farm-manager/machinery',
      getByCode: 'farm-manager/machinery/get-by-code/{code}',
    },
  },
  farms: {
    default: 'farms',
    single: 'farms/{farmId}',
  },
  fertilizers: {
    default: 'fertilizers',
    single: 'fertilizers/{fertilizerId}',
    units: 'fertilizers/{fertilizerId}/units',
    calculateUsage: 'fertilizers/calculate-usage',
  },
  fields: {
    default: 'fields',
    single: 'fields/{fieldId}',
    info: 'fields/info',
    plots: {
      default: 'fields/plots',
      batch: 'fields/plots-batch',
    },
    measures: {
      default: 'fields/{fieldId}/measures',
      batch: 'fields/measures-batch',
    },
    types: {
      default: 'fields/field-types',
      single: 'fields/field-types/{fieldTypeId}',
    },
  },
  machinery: {
    default: 'machinery',
    single: 'machinery/{machineryId}',
    classification: {
      default: 'machinery/classification',
    },
    performanceUnits: {
      default: 'machinery/performance-units',
    },
  },
  measures: {
    default: 'measures',
    single: 'measures/{measureId}',
  },
  pesticides: {
    default: 'pesticides',
    single: 'pesticides/{pesticideId}',
    units: 'pesticides/{pesticideId}/units',
    calculateUsage: 'pesticides/calculate-usage',
  },
  crops: {
    default: 'crops',
    single: 'crops/{cropId}',
  },
  reports: {
    cropsRotation: 'reports/crop-rotation',
    tasks: 'reports/tasks',
    kopop: 'reports/kopop',
    stock: {
      fertilizers: 'reports/stock/fertilizers',
      pesticides: 'reports/stock/pesticides',
    },
  },
  rkg: {
    farm: {
      default: 'rkg/farm',
      import: 'rkg/farm/import',
    },
  },
  stock: {
    fertilizers: {
      default: 'stock/fertilizers',
      single: 'stock/fertilizers/{stockFertilizerId}',
    },
    pesticides: {
      default: 'stock/pesticides',
      single: 'stock/pesticides/{stockPesticideId}',
    },
    types: {
      default: 'stock/types',
    },
  },
  tasks: {
    default: 'tasks',
    byField: 'tasks/tasks-by-field',
    single: 'tasks/{taskId}',
    fields: {
      default: 'tasks/{taskId}/fields',
      calculateDuration: 'tasks/fields/calculate-duration',
    },
    types: {
      default: 'tasks/task-types',
      single: 'tasks/task-types/{taskTypeId}',
    },
  },
  tracking: {
    trips: {
      default: 'tracking/trips',
    },
  },
  units: {
    default: 'units',
    single: 'units/{unitId}',
  },
  users: {
    default: 'users',
    single: 'users/{userId}',
    settings: {
      crop: 'user/settings/crop-settings',
    },
    password: {
      setMyPassword: 'users/me/password',
      setUserPassword: 'users/{userId}/password',
    },
  },

  settings: {
    appSettings: 'settings/app-settings',
  },
};

export default routes;
