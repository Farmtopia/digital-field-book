﻿namespace Farmtopia.DigitalFieldBook.API.Models
{
    public class FarmEntity
    {
        public Guid? Uuid { get; set; }

        public decimal? Latitude { get; set; }

        public decimal? Longitude { get; set; }

        public bool EcoFarm { get; set; }
    }

    public class Farm
    {
        public Guid? Uuid { get; set; }

        public decimal? Latitude { get; set; }

        public decimal? Longitude { get; set; }

        public bool EcoFarm { get; set; }
    }
}
