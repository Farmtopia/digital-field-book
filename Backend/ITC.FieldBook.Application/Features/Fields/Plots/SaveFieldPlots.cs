﻿using ITC.FieldBook.Application.Infrastructure.Models;
using ITC.FieldBook.Application.Infrastructure;
using MediatR;
using Microsoft.AspNetCore.DataProtection.KeyManagement;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using ITC.FieldBook.Application.Models;
using ITC.FieldBook.Application.Infrastructure.Interfaces;
using System.Transactions;
using Dapper;
using static System.Formats.Asn1.AsnWriter;
using ITC.FieldBook.Application.Features.Fields;
using System.ComponentModel.DataAnnotations;
using ITC.FieldBook.Application.Features.Crops;
using Microsoft.Extensions.Azure;
using ITC.FieldBook.Application.Models.DTO;
using ITC.FieldBook.Application.Features.CropRotation;
using System.Text.Json;
using DocumentFormat.OpenXml.Bibliography;
using DocumentFormat.OpenXml.Spreadsheet;

namespace ITC.FieldBook.Application.Features.Fields.Crops
{
    public class SaveCropRotationFieldPlotsController : ApiControllerBase
    {
        [HttpPost("fields/plots-batch")]
        public Task<IEnumerable<FieldPlot>> SaveCropRotationFieldPlots([FromBody] SaveCropRotationFieldPlotsCommand command)
        {
            return Mediator.Send(command);
        }
    }


    public class SaveCropRotationFieldPlotsCommand : IRequest<IEnumerable<FieldPlot>>
    {
        public FieldPlot[]? FieldPlots { get; set; }

        public int? FieldId { get; set; }

        public int? Year { get; set; }
    }

    internal sealed class SaveCropRotationFieldPlotsCommandHandler : IRequestHandler<SaveCropRotationFieldPlotsCommand, IEnumerable<FieldPlot>>
    {
        private readonly DbContext _ctx;
        private readonly ICurrentUserService _currentUserService;

        public SaveCropRotationFieldPlotsCommandHandler(DbContext ctx, ICurrentUserService currentUserService)
        {
            _ctx = ctx;
            _currentUserService = currentUserService;
        }

        public Task<IEnumerable<FieldPlot>> Handle(SaveCropRotationFieldPlotsCommand request, CancellationToken cancellationToken)
        {
            var user = _currentUserService.GetUser()!;
            var userId = user.Id!.Value;
            var farmId = _currentUserService.FarmId!.Value;
            var fieldPlots = request.FieldPlots?.ToList() ?? new List<FieldPlot>();

            var fieldsRepository = new FieldsRepository(_ctx);
            var cropsRepository = new CropsRepository(_ctx);
            var cropRotationRepository = new CropRotationRepository(_ctx);

            if (fieldPlots.Count == 0)
            {
                var fieldId = request.FieldId!.Value;
                var year = request.Year!.Value;

                using (var scope = new TransactionScope())
                {
                    // Field exists
                    var field = fieldsRepository.GetField(farmId, fieldId).Item;
                    if (field == null)
                        return Task.FromResult(Enumerable.Empty<FieldPlot>());

                    // Exclude field crops in use
                    var fieldPlotsInUse = cropRotationRepository.GetCropsInUse(farmId, fieldId, year);
                    var excludeFieldPlotIds = fieldPlotsInUse.Select(x => x.FieldPlotId).ToList();

                    // Delete existing field plots
                    DeleteFieldPlots(fieldId, year, userId, excludeFieldPlotIds);

                    // Delete measures related to field crops
                    DeleteFieldPlotsMeasures(fieldId, year, userId, excludeFieldPlotIds);

                    scope.Complete();
                }
                // Delete existing
                return Task.FromResult(Enumerable.Empty<FieldPlot>());
            }

            var crops = cropsRepository.GetCrops().Data;
            var fieldPlotsGrouped = fieldPlots.GroupBy(x => new { x.FieldId, x.Year }).ToList();

            using (var scope = new TransactionScope())
            {
                foreach (var fmg in fieldPlotsGrouped)
                {
                    var fieldId = fmg.Key.FieldId!.Value;
                    var year = fmg.Key.Year!.Value;
                    var fieldPlotsGroup = fmg.ToList();

                    // Field exists
                    var field = fieldsRepository.GetField(farmId, fieldId).Item;
                    if (field == null)
                        return Task.FromResult(Enumerable.Empty<FieldPlot>());

                    // Delete previous field crops
                    var fieldPlotsInUse = cropRotationRepository.GetCropsInUse(farmId, fieldId, year);

                    // Exclude field crops in use
                    var excludeFieldPlotIds = fieldPlots.Where(x => x.Id != null).Select(x => x.Id!.Value).Concat(fieldPlotsInUse.Select(x=>x.FieldPlotId)).Distinct().ToList();

                    // Delete existing field plots
                    DeleteFieldPlots(fieldId, year, userId, excludeFieldPlotIds);

                    // Delete measures related to field crops
                    DeleteFieldPlotsMeasures(fieldId, year, userId, excludeFieldPlotIds);

                    foreach (var fp in fieldPlotsGroup)
                    {
                        // Missing required data
                        if (fp.CropType == null || fp.CropId == null)
                            continue;

                        // In use
                        if (fp.Id != null && fieldPlotsInUse.Any(x => x.FieldPlotId == fp.Id))
                            continue;

                        // Invalid crop
                        var crop = crops.FirstOrDefault(x => x.Id == fp.CropId);
                        if (crop == null)
                            continue;

                        // Default field area
                        if (fp.Area == null && field.Area != null)
                            fp.Area = decimal.Round(field.Area!.Value, 2);

                        fp.CropIdentifier = crop.Identifier;
                        fp.CropName = crop.Name;

                        var keys = _ctx.Save(fp, user: user);

                        fp.Id = Convert.ToInt32(keys["id"]);
                    }
                }
                scope.Complete();
            }

            return Task.FromResult(fieldPlots.AsEnumerable());
        }

        private void DeleteFieldPlots(int fieldId, int year, int userId, List<int>? excludeFieldPlotIds = null)
        {
            using (var conn = _ctx.CreateConnection())
            {
                var now = DateTime.Now;
                conn.Execute($@"
                    UPDATE FieldBook.field_plot
                    SET is_deleted = 1, is_active = 0, modified = @now, modified_by = @userId
                    WHERE
                        {DbContext.GetIsDeletedIsActiveCondition()}
                        AND field_id = @fieldId
                        AND year = @year
                        {(excludeFieldPlotIds?.Any() ?? false ? "AND id NOT IN @excludeFieldPlotIds" : "")}
                ", new { year, fieldId, userId, now, excludeFieldPlotIds });
            }
        }

        private void DeleteFieldPlotsMeasures(int fieldId, int year, int userId, List<int>? excludeFieldPlotIds = null)
        {
            using (var conn = _ctx.CreateConnection())
            {
                var now = DateTime.Now;
                conn.Execute($@"
                    UPDATE FieldBook.field_measure
                    SET is_deleted = 1, is_active = 0, modified = @now, modified_by = @userId
                    WHERE
                        {DbContext.GetIsDeletedIsActiveCondition()}
                        AND field_id = @fieldId
                        AND year = @year
                        {(excludeFieldPlotIds?.Any() ?? false ? "AND field_plot_id NOT IN @excludeFieldPlotIds" : "")}
                ", new { year, fieldId, userId, now, excludeFieldPlotIds });
            }
        }
    }
}

