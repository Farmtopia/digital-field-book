﻿using ITC.FieldBook.Application.Infrastructure.Models;
using ITC.FieldBook.Application.Infrastructure;
using MediatR;
using Microsoft.AspNetCore.DataProtection.KeyManagement;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using ITC.FieldBook.Application.Models;
using ITC.FieldBook.Application.Infrastructure.Enums;
using Microsoft.AspNetCore.Authorization;
using ITC.FieldBook.Application.Infrastructure.Interfaces;
using Dapper;

namespace ITC.FieldBook.Application.Features.Pesticides
{
    [Authorize]
    public class SavePesticideController : ApiControllerBase
    {
        [HttpPost("pesticides")]
        public Task SavePesticide([FromBody] Pesticide data)
        {
            return Mediator.Send(new SavePesticideCommand { Data = data });
        }
    }

    public class SavePesticideCommand : IRequest
    {
        public Pesticide? Data { get; set; }
    }

    internal sealed class SavePesticideCommandHandler : IRequestHandler<SavePesticideCommand>
    {
        private readonly DbContext _ctx;
        private readonly ICurrentUserService _currentUserService;

        public SavePesticideCommandHandler(DbContext ctx, ICurrentUserService currentUserService)
        {
            _ctx = ctx;
            _currentUserService = currentUserService;
        }

        public Task Handle(SavePesticideCommand request, CancellationToken cancellationToken)
        {
            if (request.Data != null)
            {
                var data = request.Data;
                var user = _currentUserService.GetUser()!;
                var isAdmin = user.HasRole(EnumRole.Admin);

                if (!isAdmin && data.Id != null)
                {
                    // Only if not global and for my farm
                    var pesticidesRepository = new PesticidesRepository(_ctx);
                    var pest = pesticidesRepository.GetPesticide(data.Id!.Value).Item;
                    if(pest == null || pest.IsGlobal || pest.FarmId != _currentUserService.FarmId)
                        return Task.CompletedTask;
                }

                if (!isAdmin || !data.IsGlobal)
                {
                    data.IsGlobal = false;

                    if (data.FarmId == null)
                        data.FarmId = _currentUserService.FarmId;
                }

                _ctx.Save(data, user: user);
            }

            return Task.CompletedTask;
        }
    }
}

