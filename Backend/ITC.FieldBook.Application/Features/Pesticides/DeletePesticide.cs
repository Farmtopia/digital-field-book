﻿using ITC.FieldBook.Application.Infrastructure.Models;
using ITC.FieldBook.Application.Infrastructure;
using MediatR;
using Microsoft.AspNetCore.DataProtection.KeyManagement;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using ITC.FieldBook.Application.Models;
using ITC.FieldBook.Application.Infrastructure.Services;
using ITC.FieldBook.Application.Infrastructure.Interfaces;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Authorization;
using ITC.FieldBook.Application.Infrastructure.Enums;

namespace ITC.FieldBook.Application.Features.Pesticides
{
    [Authorize(Roles = EnumRole.Admin)]
    public class DeletePesticideController : ApiControllerBase
    {
        [HttpDelete("pesticides/{pesticideId}")]
        public Task DeletePesticide([FromRoute] int? pesticideId)
        {
            return Mediator.Send(new DeletePesticideCommand { PesticideId = pesticideId });
        }
    }

    public class DeletePesticideCommand : IRequest
    {
        [Required]
        public int? PesticideId { get; set; }
    }

    internal sealed class DeletePesticideCommandHandler : IRequestHandler<DeletePesticideCommand>
    {
        private readonly DbContext _ctx;
        private readonly ICurrentUserService _currentUserService;

        public DeletePesticideCommandHandler(DbContext ctx, ICurrentUserService currentUserService)
        {
            _ctx = ctx;
            _currentUserService = currentUserService;
        }

        public Task Handle(DeletePesticideCommand request, CancellationToken cancellationToken)
        {
            var pesticidesRepository = new PesticidesRepository(_ctx);
            var pesticide = pesticidesRepository.GetPesticide(request.PesticideId!.Value).Item;
            if (pesticide != null)
            {
                var user = _currentUserService.GetUser()!;
                _ctx.DeleteItem(pesticide, user);
            }

            return Task.CompletedTask;
        }
    }
}

