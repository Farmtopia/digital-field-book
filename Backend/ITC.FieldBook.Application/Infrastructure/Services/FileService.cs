﻿using Dapper;
using ITC.FieldBook.Application.Enums;
using ITC.FieldBook.Application.Infrastructure.Interfaces;
using ITC.FieldBook.Application.Infrastructure.Models;
using ITC.FieldBook.Application.Infrastructure.Tools;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace ITC.FieldBook.Application.Infrastructure.Services
{
    public class FileService : IFileService
    {
        private readonly DbContext _ctx;
        private readonly AzureFileStorageManager _storageManager;
        private readonly ICurrentUserService _currentUserService;
        private readonly string? _storageType;
        private readonly string? _localStoragePath;

        public FileService(IWebHostEnvironment env, IConfiguration configuration, ICurrentUserService currentUserService, DbContext ctx)
        {
            _storageType = configuration["FileStorage:Type"];

            var connString = configuration["FileStorage:Azure:ConnectionString"];
            var containerName = configuration["FileStorage:Azure:ContainerName"];

            if (!string.IsNullOrEmpty(connString) && !string.IsNullOrEmpty(containerName))
                _storageManager = new AzureFileStorageManager(connString, containerName);

            else if(_storageType == EnumStorageType.FILESYSTEM)
            {
                var folder = configuration["FileStorage:FileSystem:Folder"];

                // Default if not defined in config
                if (string.IsNullOrEmpty(folder))
                    folder = @"Files";

                //If relative path, append to current location of API
                if (!Path.IsPathRooted(folder))
                    _localStoragePath = Path.Combine(env.ContentRootPath, folder);
                else
                    _localStoragePath = folder;

                _localStoragePath = _localStoragePath.Replace('\\', Path.DirectorySeparatorChar);

                // Create temp location for files, if not exists
                if (!Directory.Exists(_localStoragePath))
                    Directory.CreateDirectory(_localStoragePath);
                
            }

            _ctx = ctx;
            _currentUserService = currentUserService;
        }

        public FileData? Get(int id, bool includeFile)
        {
            var fd = GetFileData(id)?.FirstOrDefault();
            if (fd != null && includeFile)
            {
                if (fd.StorageType == EnumStorageType.FILESYSTEM)
                    fd.Bytes = System.IO.File.ReadAllBytes(fd.Filename!);
                else if(fd.StorageType == EnumStorageType.AZURE)
                    fd.Bytes = _storageManager.DownloadBlob(fd.Filename!);
            }

            return fd;
        }

        public List<FileData> Get(int[] ids)
        {
            var fd = GetFileData(ids);
            return fd;
        }

        public int Save(string fileName, byte[] bytes, SaveFileSettings? settings = null)
        {
            var saveFileName = $"{Guid.NewGuid().ToString("N")}_{fileName}";

            if (settings != null)
            {
                if (!string.IsNullOrEmpty(settings.Folder))
                    saveFileName = $"{settings.Folder}{(settings.Folder.EndsWith('/') ? "" : '/')}{saveFileName}";
            }

            if (_storageType == EnumStorageType.FILESYSTEM)
            {
                saveFileName = Path.Combine(_localStoragePath, saveFileName).Replace('\\', Path.DirectorySeparatorChar);

                FileInfo file = new FileInfo(saveFileName);
                file.Directory?.Create();
                System.IO.File.WriteAllBytes(saveFileName, bytes);
            }
            else if (_storageType == EnumStorageType.AZURE)
                _storageManager.UploadBlob(saveFileName, bytes);

            var id = SaveFileData(new FileData
            {
                Filename = saveFileName,
                Displayname = Path.GetFileName(fileName),
                Extension = Path.GetExtension(fileName).Substring(1),
                StorageType = _storageType,
                Size = bytes.Length
            });

            return id;
        }

        private List<FileData> GetFileData(params int[] ids)
        {
            using (var conn = _ctx.CreateConnection())
            {
                return conn.Query<FileData>(@"
                    SELECT
                        f.id, f.displayname, f.filename, f.size, f.extension, f.storage_type, f.created, f.created_by, f.modified, f.modified_by
                    FROM FieldBook.[file] f
                    WHERE f.id IN @ids",
                new { ids }).ToList();
            }
        }

        public int SaveFileData(FileData fd)
        {
            using (var conn = _ctx.CreateConnection())
            {
                return conn.ExecuteScalar<int>(@"
                    IF (@id IS NULL)
                    BEGIN
	                    INSERT INTO FieldBook.[file] (
		                    displayname
		                    ,filename
		                    ,size
		                    ,extension
                            ,storage_type
		                    ,created
		                    ,created_by
		                    ,modified
		                    ,modified_by
	                    )
	                    OUTPUT INSERTED.id
	                    VALUES (
		                    @displayname
		                    ,@filename
		                    ,@size
		                    ,@extension
                            ,@storage_type
		                    ,@now
		                    ,@user_id
		                    ,@now
		                    ,@user_id
	                    )
                    END
                    ELSE
                    BEGIN
	                    UPDATE FieldBook.[file]
	                    SET 
                            displayname = @displayname
		                    ,filename = @filename
		                    ,size = @size
		                    ,extension = @extension
		                    ,storage_type = @storage_type
		                    ,modified = @now
		                    ,modified_by = @user_id
	                    WHERE id = @id
                    END",
                new { id = fd.Id, displayname = fd.Displayname, filename = fd.Filename, size = fd.Size, extension = fd.Extension, storage_type = fd.StorageType, now = DateTime.Now, user_id = _currentUserService.GetUser()!.Id });
            }
        }
    }
}
