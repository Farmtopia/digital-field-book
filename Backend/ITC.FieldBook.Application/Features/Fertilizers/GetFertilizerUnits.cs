﻿using ITC.FieldBook.Application.Infrastructure.Interfaces;
using ITC.FieldBook.Application.Infrastructure;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata;
using Dapper;
using ITC.FieldBook.Application.Models;
using ITC.FieldBook.Application.Infrastructure.Models;
using ITC.FieldBook.Application.Enums;
using Microsoft.Extensions.Azure;
using ITC.FieldBook.Application.Features.Units;

namespace ITC.FieldBook.Application.Features.Fertilizers
{
    public class GetFertilizerUnitsController : ApiControllerBase
    {
        [HttpGet("fertilizers/{fertilizerId}/units")]
        public Task<DbData<Models.Unit>> GetFertilizerUnits(int fertilizerId, bool forTaskInput)
        {
            return Mediator.Send(new GetFertilizerUnitsQuery { FertilizerId = fertilizerId, ForTaskInput = forTaskInput });
        }
    }

    public class GetFertilizerUnitsQuery : BaseDataQuery<DbData<Models.Unit>>
    {
        public int? FertilizerId { get; set; }

        public bool ForTaskInput { get; set; }
    }

    internal sealed class GetFertilizerUnitsQueryHandler : IRequestHandler<GetFertilizerUnitsQuery, DbData<Models.Unit>>
    {
        private readonly DbContext _ctx;
        private readonly IConfiguration _configuration;
        private readonly ICurrentUserService _currentUserService;

        public GetFertilizerUnitsQueryHandler(DbContext ctx, IConfiguration configuration, ICurrentUserService currentUserService)
        {
            _ctx = ctx;
            _configuration = configuration;
            _currentUserService = currentUserService;
        }

        public Task<DbData<Models.Unit>> Handle(GetFertilizerUnitsQuery request, CancellationToken cancellationToken)
        {
            var unitsRepo = new UnitsRepository(_ctx);
            var fertUnit = unitsRepo.GetUnitForFertilizer(_currentUserService.FarmId!.Value, request.FertilizerId!.Value).Item;
            var units = unitsRepo.GetUnits(request.ForTaskInput ? EnumUnitGroup.FERTILIZER_USAGE : EnumUnitGroup.FERTILIZER);

            // Not defined (maybe first input of pest?)
            if (fertUnit == null)
                return Task.FromResult(units);

            var result = new List<Models.Unit>();

            // When inputing task, also include unit/ha if exists.
            if (request.ForTaskInput)
            {
                // Check if unit per area is defined
                var unitPerAreaName = fertUnit.GetUnitPerAreaName();
                var unitPerArea = units.Data.FirstOrDefault(x => x.Name!.Equals(unitPerAreaName, StringComparison.OrdinalIgnoreCase));

                if (unitPerArea != null)
                    result.Add(unitPerArea);
            }

            // Check if base unit is defined
            var baseUnit = units.Data.FirstOrDefault(x => x.Id == fertUnit.Id || x.Name == fertUnit.Name);
            if (baseUnit != null)
                result.Add(baseUnit);

            return Task.FromResult(new DbData<Models.Unit> { Data = result });
        }
    }
}
