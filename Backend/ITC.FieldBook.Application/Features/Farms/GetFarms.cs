﻿using ITC.FieldBook.Application.Infrastructure.Interfaces;
using ITC.FieldBook.Application.Infrastructure;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata;
using Dapper;
using ITC.FieldBook.Application.Models;
using ITC.FieldBook.Application.Infrastructure.Models;
using ITC.FieldBook.Application.Infrastructure.Enums;

namespace ITC.FieldBook.Application.Features.Farms
{
    public class GetFarmsController : ApiControllerBase
    {
        [HttpGet("farms")]
        public Task<DbData<Farm>> GetFarms()
        {
            return Mediator.Send(new GetFarmsQuery { });
        }
    }

    public class GetFarmsQuery : IRequest<DbData<Farm>>
    {
    }

    internal sealed class GetFarmsQueryHandler : IRequestHandler<GetFarmsQuery, DbData<Farm>>
    {
        private readonly DbContext _ctx;
        private readonly IConfiguration _configuration;
        private readonly ICurrentUserService _currentUserService;

        public GetFarmsQueryHandler(DbContext ctx, IConfiguration configuration, ICurrentUserService currentUserService)
        {
            _ctx = ctx;
            _configuration = configuration;
            _currentUserService = currentUserService;
        }

        public Task<DbData<Farm>> Handle(GetFarmsQuery request, CancellationToken cancellationToken)
        {
            var user = _currentUserService.GetUser()!;
            var farmsRepo = new FarmsRepository(_ctx);
            var data = farmsRepo.GetFarms(user.HasRole(EnumRole.Admin) ? null : user.Id!.Value);

            return Task.FromResult(data);
        }
    }
}
