﻿using Dapper;
using Farmtopia.DigitalFieldBook.API.Models;

namespace Farmtopia.DigitalFieldBook.API.Repository
{
    public class FieldbookRepository
    {
        private DbContext _ctx;

        public FieldbookRepository(DbContext ctx)
        {
            _ctx = ctx;
        }

        /// <summary>
        /// In specific cases we want to include only specific farms. We can do this so we include them in Farmtopia.farm table.
        /// If table does not exist or empty, include all farms.
        /// </summary>
        /// <returns></returns>
        public bool IncludeAllFarms()
        {
            using (var conn = _ctx.CreateConnection())
            {
                return conn.ExecuteScalar<bool>(@"
                    DECLARE @count INT;

                    -- Check if the table exists and contains rows
                    IF EXISTS (SELECT TOP(1) 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'Farmtopia' AND TABLE_NAME = 'farm')
                    BEGIN
                        -- Execute the dynamic SQL to get the count from the table
                        EXEC sp_executesql N'SELECT @result = COUNT(*) FROM Farmtopia.farm', N'@result INT OUTPUT', @result = @count OUTPUT;
                    END
                    ELSE
                    BEGIN
                        -- If the table doesn't exist, set count to 0
                        SET @count = 0;
                    END

                    -- Return the result based on the count
                    SELECT CAST(CASE WHEN @count > 0 THEN 0 ELSE 1 END AS BIT) AS include_all_farms;
                    ");
            }
        }

        public IEnumerable<CropEntity> GetCrops()
        {
            using (var conn = _ctx.CreateConnection())
            {
                var data = conn.Query<CropEntity>($@"
                    SELECT
                        c.uuid,
                        c.name,
                        c.name_en,
                        c.agrovoc_code
                    FROM FieldBook.crop c
                    WHERE
                        {GetIsDeletedIsActiveCondition("c")}");

                return data;
            }
        }

        public IEnumerable<FarmEntity> GetFarms()
        {
            using (var conn = _ctx.CreateConnection())
            {
                var includeAllFarms = IncludeAllFarms();
                var data = conn.Query<FarmEntity>($@"
                    SELECT
                        f.uuid,
                        f.location.Lat AS latitude,
                        f.location.Long AS longitude,
                        f.eco_farm
                    FROM FieldBook.farm f
                        {(includeAllFarms ? "" : "JOIN Farmtopia.farm ff ON ff.uuid = f.uuid")}
                    WHERE
                        {GetIsDeletedIsActiveCondition("f")}");

                return data;
            }
        }

        public IEnumerable<FieldTypeEntity> GetFieldTypes()
        {
            using (var conn = _ctx.CreateConnection())
            {
                var data = conn.Query<FieldTypeEntity>($@"
                    SELECT
                        id,
                        uuid,
                        identifier,
                        name,
                        name_en
                    FROM FieldBook.field_type
                    WHERE
                        {GetIsDeletedIsActiveCondition()}");

                return data;
            }
        }

        public IEnumerable<FieldEntity> GetFields(Guid farmUuid)
        {
            using (var conn = _ctx.CreateConnection())
            {
                var includeAllFarms = IncludeAllFarms();
                var data = conn.Query<FieldEntity>($@"
                    SELECT
                        fld.uuid,
                        fld.identifier,
                        fld.name,
                        fld.description,
                        fld.area * 100 AS area_m2,
						ft.id AS field_type_id,
                        dbo.GetGeoJSON(fld.field_geometry) AS geoJSON
                    FROM FieldBook.field fld
                        JOIN FieldBook.farm f ON f.id = fld.farm_id AND {GetIsDeletedIsActiveCondition("f")}
                        {(includeAllFarms ? "" : "JOIN Farmtopia.farm ff ON ff.uuid = f.uuid")}
						LEFT JOIN FieldBook.field_type ft ON fld.type_id = ft.id
                    WHERE
                        {GetIsDeletedIsActiveCondition("fld")}                        
                        AND f.uuid = @farmUuid", new { farmUuid });

                return data;
            }
        }

        public IEnumerable<FieldCropEntity> GetFieldCrops(Guid fieldUuid)
        {
            using (var conn = _ctx.CreateConnection())
            {
                var data = conn.Query<FieldCropEntity>($@"
                    SELECT
                        fp.id,
                        fp.uuid,
	                    fp.[year],
	                    fp.crop_type,
	                    c.uuid AS crop_uuid,
	                    fp.sowing_date,
	                    fp.harvest_date
                    FROM FieldBook.field_plot fp
	                    JOIN FieldBook.field fld ON fld.id = fp.field_id AND {GetIsDeletedIsActiveCondition("fld")}
                        JOIN FieldBook.farm f ON f.id = fld.farm_id AND {GetIsDeletedIsActiveCondition("f")}
	                    JOIN FieldBook.crop c ON c.id = fp.crop_id
                    WHERE
                        {GetIsDeletedIsActiveCondition("fp")}
                        AND fld.uuid = @fieldUuid", new { fieldUuid });

                return data;
            }
        }

        public IEnumerable<TaskTypeEntity> GetTaskTypes()
        {
            using (var conn = _ctx.CreateConnection())
            {
                var data = conn.Query<TaskTypeEntity>($@"
                    SELECT
                        id,
                        name,
                        name_en,
                        ploutos_type
                    FROM FieldBook.task_type
                    WHERE
                        {GetIsDeletedIsActiveCondition()}");

                return data;
            }
        }

        public IEnumerable<TaskEntity> GetTasks(Guid farmUuid, DateTime? dateFrom = null, DateTime? dateTo = null)
        {
            using (var conn = _ctx.CreateConnection())
            {
                var includeAllFarms = IncludeAllFarms();
                var data = conn.Query<TaskEntity>($@"
                    SELECT
                        t.uuid,
                        t.task_type_id,
                        t.date_from,
                        t.date_to,
                        t.note
                    FROM FieldBook.task t
	                    JOIN FieldBook.farm f ON f.id = t.farm_id AND {GetIsDeletedIsActiveCondition("f")}
                        {(includeAllFarms ? "" : "JOIN Farmtopia.farm ff ON ff.uuid = f.uuid")}
                        JOIN FieldBook.task_type tt ON t.task_type_id = tt.id
                    WHERE
                        {GetIsDeletedIsActiveCondition("t")}
                        AND f.uuid = @farmUuid
                        AND (@dateFrom IS NULL OR t.date_from >= @dateFrom)
                        AND (@dateTo IS NULL OR t.date_to <= @dateTo)", new { farmUuid, dateFrom, dateTo });

                return data;
            }
        }

        public IEnumerable<TaskFieldEntity> GetTaskFields(Guid taskUuid)
        {
            using (var conn = _ctx.CreateConnection())
            {
                var data = conn.Query<TaskFieldEntity>($@"
                    SELECT
                        fld.uuid AS field_uuid,
	                    fld.identifier AS field_identifier,
	                    fld.name AS field_name,
                        fld.area * 100 AS field_area_m2,
	                    c.uuid AS crop_uuid,	
	                    c.name AS crop_name,
	                    c.name_en AS crop_name_en
                    FROM FieldBook.task_field tf
	                    JOIN FieldBook.task t ON t.id = tf.task_id AND {GetIsDeletedIsActiveCondition("t")}
	                    JOIN FieldBook.farm f ON f.id = t.farm_id AND {GetIsDeletedIsActiveCondition("f")}
                        LEFT JOIN FieldBook.field fld ON fld.id = tf.field_id
                        LEFT JOIN FieldBook.field_plot fp ON fp.id = tf.field_plot_id
                        LEFT JOIN FieldBook.crop c ON c.id = fp.crop_id
                    WHERE
                        {GetIsDeletedIsActiveCondition("tf")}
                        AND t.uuid = @taskUuid", new { taskUuid });

                return data;
            }
        }

        public IEnumerable<TaskFertilizerEntity> GetTaskFertilizers(Guid taskUuid)
        {
            using (var conn = _ctx.CreateConnection())
            {
                var data = conn.Query<TaskFertilizerEntity>($@"
                    SELECT
                        frt.uuid AS fertilizer_uuid,
	                    frt.name AS fertilizer_name,
	                    frt.name_en AS fertilizer_name_en,
	                    tf.amount_used,
	                    tf.amount_used_unit,
                        u.description AS amount_used_unit_description,
                        u.description_en AS amount_used_unit_description_en,
                        frt.ratio_n,
                        frt.ratio_p,
                        frt.ratio_k
                    FROM FieldBook.task_fertilizer tf
	                    JOIN FieldBook.task t ON t.id = tf.task_id AND {GetIsDeletedIsActiveCondition("t")}
	                    JOIN FieldBook.farm f ON f.id = t.farm_id AND {GetIsDeletedIsActiveCondition("f")}
	                    LEFT JOIN FieldBook.fertilizer frt ON frt.id = tf.fertilizer_id
                        LEFT JOIN FieldBook.unit u ON u.id = tf.amount_used_unit_id
                    WHERE
                        {GetIsDeletedIsActiveCondition("tf")}
                        AND t.uuid = @taskUuid", new { taskUuid });

                return data;
            }
        }

        public IEnumerable<TaskPesticideEntity> GetTaskPesticides(Guid taskUuid)
        {
            using (var conn = _ctx.CreateConnection())
            {
                var data = conn.Query<TaskPesticideEntity>($@"
                    SELECT
                        p.uuid AS pesticide_uuid,
	                    p.name AS pesticide_name,
	                    p.name_en AS pesticide_name_en,
	                    tp.amount_used,
	                    tp.amount_used_unit,
                        u.description AS amount_used_unit_description,
                        u.description_en AS amount_used_unit_description_en
                    FROM FieldBook.task_pesticide tp
	                    JOIN FieldBook.task t ON t.id = tp.task_id AND {GetIsDeletedIsActiveCondition("t")}
	                    JOIN FieldBook.farm f ON f.id = t.farm_id AND {GetIsDeletedIsActiveCondition("f")}
	                    LEFT JOIN FieldBook.pesticide p ON p.id = tp.pesticide_id
                        LEFT JOIN FieldBook.unit u ON u.id = tp.amount_used_unit_id
                    WHERE
                        {GetIsDeletedIsActiveCondition("tp")}
                        AND t.uuid = @taskUuid", new { taskUuid });

                return data;
            }
        }

        private string GetIsDeletedIsActiveCondition(string? tablePrefix = null, bool includeInactive = false)
        {
            return $"(ISNULL({(!string.IsNullOrEmpty(tablePrefix) ? $"{tablePrefix}." : "")}is_deleted, 0) = 0{(includeInactive ? "" : $" AND ISNULL({(!string.IsNullOrEmpty(tablePrefix) ? $"{tablePrefix}." : "")}is_active, 1) = 1")})";
        }
    }
}
