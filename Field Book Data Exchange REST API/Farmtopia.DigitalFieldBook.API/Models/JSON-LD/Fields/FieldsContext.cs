﻿using Farmtopia.DigitalFieldBook.API.Models.JSON_LD.Farms;
using Microsoft.OpenApi.Any;
using Swashbuckle.AspNetCore.Annotations;
using System.ComponentModel;
using System.Text.Json.Serialization;

namespace Farmtopia.DigitalFieldBook.API.Models.JSON_LD.Fields
{
    public class FieldsContext
    {
        [SwaggerSchema(Nullable = false)]
        [JsonPropertyName("fsm")]
        public Uri Fsm { get; set; }

        [SwaggerSchema(Nullable = false)]
        [JsonPropertyName("pcsm")]
        public Uri Pcsm { get; set; }

        [SwaggerSchema(Nullable = false)]
        [JsonPropertyName("geosparql")]
        public Uri Geosparql { get; set; }

        [SwaggerSchema(Nullable = false)]
        [JsonPropertyName("qudt")]
        public Uri Qudt { get; set; }

        [SwaggerSchema(Nullable = false)]
        [JsonPropertyName("obo")]
        public Uri Obo { get; set; }

        [SwaggerSchema(Nullable = false)]
        [JsonPropertyName("contains")]
        public TermDefinition Contains { get; set; }

        [SwaggerSchema(Nullable = false)]
        [JsonPropertyName("geometry")]
        public TermDefinition Geometry { get; set; }

        [SwaggerSchema(Nullable = false)]
        [JsonPropertyName("geoJson")]
        public TermDefinition GeoJson { get; set; }

        [SwaggerSchema(Nullable = false)]
        [JsonPropertyName("hasArea")]
        public TermDefinition HasArea { get; set; }

        [JsonPropertyName("unitOfMeasure")]
        public TermDefinition UnitOfMeasure { get; set; }

        [SwaggerSchema(Nullable = false)]
        [JsonPropertyName("landType")]
        public string LandType { get; set; }

        [SwaggerSchema(Nullable = false)]
        [JsonPropertyName("name")]
        public string Name { get; set; }

        [SwaggerSchema(Nullable = false)]
        [JsonPropertyName("nationalIdentifier")]
        public string NationalIdentifier { get; set; }

        [SwaggerSchema(Nullable = false)]
        [JsonPropertyName("numericValue")]
        public string NumericValue { get; set; }

        [SwaggerSchema(Nullable = false)]
        [JsonPropertyName("label")]
        public string Label { get; set; }

        [SwaggerSchema(Nullable = false)]
        [JsonPropertyName("cropVariety")]
        public string CropVariety { get; set; }

        [SwaggerSchema(Nullable = false)]
        [JsonPropertyName("agrovocCode")]
        public string AgrovocCode { get; set; }

        [SwaggerSchema(Nullable = false)]
        [JsonPropertyName("hasPlantDate")]
        public string HasPlantDate { get; set; }

        [SwaggerSchema(Nullable = false)]
        [JsonPropertyName("hasHarvestDate")]
        public string HasHarvestDate { get; set; }

        [SwaggerSchema(Nullable = false)]
        [JsonPropertyName("sliceContents")]
        public TermDefinition SliceContents { get; set; }

        [SwaggerSchema(Nullable = false)]
        [JsonPropertyName("datasetSlices")]
        public TermDefinition DatasetSlices { get; set; }
    }
}
