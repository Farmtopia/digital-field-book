﻿using ITC.FieldBook.API.Models;
using ITC.FieldBook.Application.Infrastructure.Enums;
using ITC.FieldBook.Application.Infrastructure.Interfaces;
using ITC.FieldBook.Application.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Net;
using System.Security.Claims;

namespace ITC.FieldBook.API.Middleware
{
    /// <summary>
    /// Check if user has access to farm on authorization.
    /// </summary>
    public class AuthorizeFarmMiddleware
    {
        private readonly RequestDelegate _next;

        public AuthorizeFarmMiddleware(RequestDelegate next)
        {
            _next = next;
        }
        public async Task InvokeAsync(HttpContext httpContext)
        {
            string? role = httpContext.User.FindFirstValue(ClaimTypes.Role);
            if (role != EnumRole.Admin)
            {
                if (httpContext != null && httpContext.Request.Headers.TryGetValue("FarmId", out var farmIdHeaderVals) && farmIdHeaderVals.Count > 0 && int.TryParse(farmIdHeaderVals[0], out int farmIdHeader))
                {
                    int? userFarms = httpContext.User.FindFirstValue("FarmId") is string f && !string.IsNullOrEmpty(f) ? Convert.ToInt32(f) : null;

                    if (farmIdHeader != userFarms)
                    {
                        httpContext.Response.ContentType = "application/json";
                        httpContext.Response.StatusCode = (int)HttpStatusCode.Forbidden;
                        await httpContext.Response.WriteAsync(new ErrorDetails()
                        {
                            StatusCode = httpContext.Response.StatusCode,
                            Message = $"No permission to access farm."
                        }.ToString());
                        return;
                    }
                }
            }

            await _next(httpContext!);
        }
    }
}
