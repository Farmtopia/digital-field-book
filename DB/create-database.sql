PRINT 'CREATING DATABASE & TABLES...'
GO

CREATE DATABASE [fieldbook-db]
 WITH CATALOG_COLLATION = SQL_Latin1_General_CP1_CI_AS
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [fieldbook-db].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [fieldbook-db] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [fieldbook-db] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [fieldbook-db] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [fieldbook-db] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [fieldbook-db] SET ARITHABORT OFF 
GO
ALTER DATABASE [fieldbook-db] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [fieldbook-db] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [fieldbook-db] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [fieldbook-db] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [fieldbook-db] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [fieldbook-db] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [fieldbook-db] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [fieldbook-db] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [fieldbook-db] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [fieldbook-db] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [fieldbook-db] SET ALLOW_SNAPSHOT_ISOLATION ON 
GO
ALTER DATABASE [fieldbook-db] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [fieldbook-db] SET READ_COMMITTED_SNAPSHOT ON 
GO
ALTER DATABASE [fieldbook-db] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [fieldbook-db] SET  MULTI_USER 
GO
ALTER DATABASE [fieldbook-db] SET DB_CHAINING OFF 
GO
ALTER DATABASE [fieldbook-db] SET ENCRYPTION ON
GO
ALTER DATABASE [fieldbook-db] SET QUERY_STORE = ON
GO
ALTER DATABASE [fieldbook-db] SET QUERY_STORE (OPERATION_MODE = READ_WRITE, CLEANUP_POLICY = (STALE_QUERY_THRESHOLD_DAYS = 30), DATA_FLUSH_INTERVAL_SECONDS = 900, INTERVAL_LENGTH_MINUTES = 60, MAX_STORAGE_SIZE_MB = 100, QUERY_CAPTURE_MODE = AUTO, SIZE_BASED_CLEANUP_MODE = AUTO, MAX_PLANS_PER_QUERY = 200, WAIT_STATS_CAPTURE_MODE = ON)
GO
USE [fieldbook-db]
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 8;
GO
USE [fieldbook-db]
GO
/****** Object:  Schema [FieldBook]    Script Date: 26/08/2024 10:46:17 ******/
CREATE SCHEMA [FieldBook]
GO
/****** Object:  Schema [FieldBook]    Script Date: 26/08/2024 10:46:17 ******/
CREATE SCHEMA [Farmtopia]
GO
USE [fieldbook-db]
GO
/****** Object:  Table [FieldBook].[stock_fertilizer]    Script Date: 09/09/2024 11:20:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [FieldBook].[stock_fertilizer](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[uuid] [uniqueidentifier] NULL,
	[farm_id] [int] NULL,
	[task_id] [int] NULL,
	[date] [datetime] NULL,
	[fertilizer_id] [int] NULL,
	[stock_type_id] [int] NULL,
	[stock_type] [smallint] NULL,
	[stock_name] [varchar](250) NULL,
	[value] [decimal](18, 2) NULL,
	[value_unit_id] [int] NULL,
	[value_unit] [varchar](50) NULL,
	[note] [varchar](max) NULL,
	[created] [datetime] NULL,
	[created_by] [int] NULL,
	[modified] [datetime] NULL,
	[modified_by] [int] NULL,
	[is_deleted] [bit] NULL,
	[is_active] [bit] NULL,
 CONSTRAINT [PK_stock_fertilizer] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [FieldBook].[unit]    Script Date: 09/09/2024 11:20:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [FieldBook].[unit](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[uuid] [uniqueidentifier] NULL,
	[name] [varchar](50) NULL,
	[description] [varchar](250) NULL,
	[description_en] [varchar](250) NULL,
	[created] [datetime] NULL,
	[created_by] [int] NULL,
	[modified] [datetime] NULL,
	[modified_by] [int] NULL,
	[is_deleted] [bit] NULL,
	[is_active] [bit] NULL,
 CONSTRAINT [PK_unit] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [FieldBook].[v_fertilizer_unit]    Script Date: 09/09/2024 11:20:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO







CREATE VIEW [FieldBook].[v_fertilizer_unit] AS
WITH tmp AS(
	SELECT DISTINCT p.farm_id, p.fertilizer_id, p.value_unit_id, ISNULL(p.value_unit, u.name) AS value_unit, DENSE_RANK() OVER(PARTITION BY farm_id, fertilizer_id ORDER BY [date] ASC) AS rnk
	FROM FieldBook.stock_fertilizer p
		LEFT JOIN FieldBook.unit u ON u.id = p.value_unit_id
	WHERE
		(ISNULL(p.is_deleted, 0) = 0 AND ISNULL(p.is_active, 1) = 1)
		AND p.value_unit_id IS NOT NULL
)
SELECT
	farm_id,
	fertilizer_id,
	value_unit_id AS unit_id,
	value_unit AS unit
FROM tmp
WHERE rnk = 1
GO
/****** Object:  UserDefinedFunction [FieldBook].[fn_fertilizer_stock]    Script Date: 09/09/2024 11:20:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [FieldBook].[fn_fertilizer_stock]
(	
	-- Add the parameters for the function here
	@farmId int,
	@fertilizerId int,
	@date date,
	@excludeTaskId int
)
RETURNS TABLE 
AS
RETURN 
(
	WITH myStock AS (
		SELECT id, date, fertilizer_id, value, stock_type, stock_type_id
		FROM FieldBook.stock_fertilizer
		WHERE			
			(ISNULL(is_deleted, 0) = 0 AND ISNULL(is_active, 1) = 1)
			AND farm_id = @farmId
			AND (@fertilizerId IS NULL OR fertilizer_id = @fertilizerId)
			AND (@excludeTaskId IS NULL OR task_id IS NULL OR task_id <> @excludeTaskId)
			AND [date] <= @date
	),
	myFertilizers AS (
		SELECT DISTINCT fertilizer_id FROM myStock
	),
	initialStockAll AS (
		SELECT 
			sf.date, sf.fertilizer_id, sf.[value], ROW_NUMBER() OVER (PARTITION BY sf.fertilizer_id ORDER BY sf.[date] DESC, sf.id DESC) AS rn
		FROM myStock sf
		WHERE sf.stock_type = 0 AND sf.[date] <= @date
	),
	initialStock AS (
		SELECT date, fertilizer_id, [value]
		FROM initialStockAll
		WHERE rn = 1
	),
	sumStock AS (
		SELECT 
			sf.fertilizer_id, SUM(CASE WHEN sf.stock_type = -1 THEN (sf.[value] * -1) ELSE sf.[value] END) AS [value]
		FROM myStock sf
				LEFT JOIN initialStock i ON i.fertilizer_id = sf.fertilizer_id
		WHERE sf.stock_type <> 0 AND sf.[date] <= @date AND (i.fertilizer_id IS NULL OR sf.[date] >= i.[date])
		GROUP BY sf.fertilizer_id
	)

	SELECT
		mf.fertilizer_id, SUM(ISNULL(i.[value], 0) + ISNULL(s.[value], 0)) AS stock, u.unit AS stock_unit, u.unit_id AS stock_unit_id
	FROM myFertilizers mf
		LEFT JOIN initialStock i ON mf.fertilizer_id = i.fertilizer_id
		LEFT JOIN sumStock s ON mf.fertilizer_id = s.fertilizer_id
	    LEFT JOIN FieldBook.v_fertilizer_unit u ON u.farm_id = @farmId AND u.fertilizer_id = mf.fertilizer_id
	GROUP BY mf.fertilizer_id, u.unit, u.unit_id
)
GO
/****** Object:  Table [FieldBook].[stock_pesticide]    Script Date: 09/09/2024 11:20:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [FieldBook].[stock_pesticide](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[uuid] [uniqueidentifier] NULL,
	[farm_id] [int] NULL,
	[task_id] [int] NULL,
	[date] [datetime] NULL,
	[pesticide_id] [int] NULL,
	[stock_type_id] [int] NULL,
	[stock_type] [smallint] NULL,
	[stock_name] [varchar](250) NULL,
	[value] [decimal](18, 2) NULL,
	[value_unit_id] [int] NULL,
	[value_unit] [varchar](50) NULL,
	[note] [varchar](max) NULL,
	[created] [datetime] NULL,
	[created_by] [int] NULL,
	[modified] [datetime] NULL,
	[modified_by] [int] NULL,
	[is_deleted] [bit] NULL,
	[is_active] [bit] NULL,
 CONSTRAINT [PK_stock_pesticide] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  View [FieldBook].[v_pesticide_unit]    Script Date: 09/09/2024 11:20:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO







CREATE VIEW [FieldBook].[v_pesticide_unit] AS
WITH tmp AS(
	SELECT DISTINCT p.farm_id, p.pesticide_id, p.value_unit_id, ISNULL(p.value_unit, u.name) AS value_unit, DENSE_RANK() OVER(PARTITION BY farm_id, pesticide_id ORDER BY [date] ASC) AS rnk
	FROM FieldBook.stock_pesticide p
		LEFT JOIN FieldBook.unit u ON u.id = p.value_unit_id
	WHERE 
		(ISNULL(p.is_deleted, 0) = 0 AND ISNULL(p.is_active, 1) = 1)
		AND p.value_unit_id IS NOT NULL
)
SELECT
	farm_id,
	pesticide_id,
	value_unit_id AS unit_id,
	value_unit AS unit
FROM tmp
WHERE rnk = 1
GO
/****** Object:  UserDefinedFunction [FieldBook].[fn_pesticide_stock]    Script Date: 09/09/2024 11:20:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE FUNCTION [FieldBook].[fn_pesticide_stock]
(	
	-- Add the parameters for the function here
	@farmId int,
	@pesticideId int,
	@date date,
	@excludeTaskId int
)
RETURNS TABLE 
AS
RETURN 
(
	WITH myStock AS (
		SELECT id, date, pesticide_id, value, stock_type, stock_type_id
		FROM FieldBook.stock_pesticide
		WHERE		
			(ISNULL(is_deleted, 0) = 0 AND ISNULL(is_active, 1) = 1)
			AND farm_id = @farmId
			AND (@pesticideId IS NULL OR pesticide_id = @pesticideId)
			AND (@excludeTaskId IS NULL OR task_id IS NULL OR task_id <> @excludeTaskId)
			AND [date] <= @date
	),
	myPesticides AS (
		SELECT DISTINCT pesticide_id FROM myStock
	),
	initialStockAll AS (
		SELECT 
			sf.date, sf.pesticide_id, sf.[value], ROW_NUMBER() OVER (PARTITION BY sf.pesticide_id ORDER BY sf.[date] DESC, sf.id DESC) AS rn
		FROM myStock sf
		WHERE sf.stock_type = 0 AND sf.[date] <= @date
	),
	initialStock AS (
		SELECT date, pesticide_id, [value]
		FROM initialStockAll
		WHERE rn = 1
	),
	sumStock AS (
		SELECT 
			sf.pesticide_id, SUM(CASE WHEN sf.stock_type = -1 THEN (sf.[value] * -1) ELSE sf.[value] END) AS [value]
		FROM myStock sf
				LEFT JOIN initialStock i ON i.pesticide_id = sf.pesticide_id
		WHERE sf.stock_type <> 0 AND sf.[date] <= @date AND (i.pesticide_id IS NULL OR sf.[date] >= i.[date])
		GROUP BY sf.pesticide_id
	)

	SELECT
		mf.pesticide_id, SUM(ISNULL(i.[value], 0) + ISNULL(s.[value], 0)) AS stock, u.unit AS stock_unit, u.unit_id AS stock_unit_id
	FROM myPesticides mf
		LEFT JOIN initialStock i ON mf.pesticide_id = i.pesticide_id
		LEFT JOIN sumStock s ON mf.pesticide_id = s.pesticide_id
	    LEFT JOIN FieldBook.v_pesticide_unit u ON u.farm_id = @farmId AND u.pesticide_id = mf.pesticide_id
	GROUP BY mf.pesticide_id, u.unit, u.unit_id
)
--SELECT * FROM sumStock

--select * from FieldBook.stock_pesticide ORDER BY date desc
--SELECT * FROM FieldBook.stock_type
GO
/****** Object:  Table [FieldBook].[crop]    Script Date: 09/09/2024 11:20:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [FieldBook].[crop](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[uuid] [uniqueidentifier] NULL,
	[identifier] [varchar](50) NULL,
	[name] [varchar](250) NULL,
	[name_en] [varchar](250) NULL,
	[agrovoc_code] [varchar](10) NULL,
	[created] [datetime] NULL,
	[created_by] [int] NULL,
	[modified] [datetime] NULL,
	[modified_by] [int] NULL,
	[is_deleted] [bit] NULL,
	[is_active] [bit] NULL,
 CONSTRAINT [PK_plant] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [FieldBook].[field_plot]    Script Date: 09/09/2024 11:20:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [FieldBook].[field_plot](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[uuid] [uniqueidentifier] NULL,
	[field_id] [int] NOT NULL,
	[year] [int] NOT NULL,
	[crop_type] [int] NULL,
	[crop_id] [int] NULL,
	[crop_variety] [varchar](250) NULL,
	[area] [decimal](8, 2) NULL,
	[start_date] [datetime] NULL,
	[start_task_id] [int] NULL,
	[sowing_date] [datetime] NULL,
	[sowing_task_id] [int] NULL,
	[harvest_date] [datetime] NULL,
	[harvest_task_id] [int] NULL,
	[created] [datetime] NULL,
	[created_by] [int] NULL,
	[modified] [datetime] NULL,
	[modified_by] [int] NULL,
	[is_deleted] [bit] NULL,
	[is_active] [bit] NULL,
 CONSTRAINT [PK_field_plot] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [FieldBook].[field]    Script Date: 09/09/2024 11:20:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [FieldBook].[field](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[uuid] [uniqueidentifier] NULL,
	[farm_id] [int] NULL,
	[identifier] [varchar](50) NULL,
	[name] [varchar](250) NULL,
	[description] [varchar](500) NULL,
	[type_id] [int] NULL,
	[area] [decimal](8, 2) NULL,
	[location] [geography] NULL,
	[field_geometry] [geography] NULL,
	[image_file_id] [int] NULL,
	[created] [datetime] NULL,
	[created_by] [int] NULL,
	[modified] [datetime] NULL,
	[modified_by] [int] NULL,
	[is_deleted] [bit] NULL,
	[is_active] [bit] NULL,
 CONSTRAINT [PK_field_1] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  UserDefinedFunction [FieldBook].[fn_active_crops]    Script Date: 09/09/2024 11:20:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE FUNCTION [FieldBook].[fn_active_crops]
(	
	-- Add the parameters for the function here
	@farmId int,
	@fieldId int,
	@dateFrom date,
	@dateTo date
)
RETURNS TABLE 
AS
RETURN 
(
	SELECT
		f.farm_id,
		fp.field_id,
		fp.id AS field_plot_id,
		fp.area,
		fp.crop_id,
		fp.sowing_date,
		fp.sowing_task_id,
		fp.harvest_date,
		fp.harvest_task_id,
		c.identifier as crop_identifier,
		c.name as crop_name,
		c.name_en as crop_name_en
	FROM FieldBook.field_plot AS fp
		JOIN FieldBook.field f ON f.id = fp.field_id
		JOIN FieldBook.crop c ON fp.crop_id = c.id
	WHERE
		(ISNULL(fp.is_deleted, 0) = 0 AND ISNULL(fp.is_active, 1) = 1)
		AND (@farmId IS NULL OR f.farm_id = @farmId)
		AND (@fieldId IS NULL OR f.id = @fieldId)
		AND fp.sowing_date <= ISNULL(@dateTo, @dateFrom)
		AND (fp.harvest_date IS NULL OR fp.harvest_date > @dateFrom)
	--WITH cropActivityRaw AS (
	--	SELECT 
	--		farm_id, field_id, crop_id, activity_type, activity_date,
	--		ROW_NUMBER() OVER (PARTITION BY field_id ORDER BY activity_date DESC) AS rn
	--	FROM (
	--		SELECT f.farm_id, fc.field_id, fc.crop_id, 1 AS activity_type, sowing_date AS activity_date,
	--			ROW_NUMBER() OVER (PARTITION BY field_id ORDER BY sowing_date DESC) AS rn
	--		FROM FieldBook.field_plot AS fc
	--			JOIN FieldBook.field f ON f.id = fc.field_id
	--		WHERE
	--			(ISNULL(fc.is_deleted, 0) = 0 AND ISNULL(fc.is_active, 1) = 1)
	--			AND f.farm_id = @farmId
	--			AND (@fieldId IS NULL OR f.id = @fieldId)
	--			AND (@date IS NULL OR sowing_date <= @date)
	--			AND fc.sowing_date IS NOT NULL

	--		UNION ALL
	
	--		SELECT f.farm_id, fc.field_id, fc.crop_id, -1 AS activity_type, harvest_date AS activity_date,
	--			ROW_NUMBER() OVER (PARTITION BY field_id ORDER BY harvest_date DESC) AS rn
	--		FROM FieldBook.field_plot AS fc
	--			JOIN FieldBook.field f ON f.id = fc.field_id
	--		WHERE
	--			(ISNULL(fc.is_deleted, 0) = 0 AND ISNULL(fc.is_active, 1) = 1)
	--			AND f.farm_id = @farmId
	--			AND (@fieldId IS NULL OR f.id = @fieldId)
	--			AND (@date IS NULL OR harvest_date <= @date )
	--			AND fc.harvest_date IS NOT NULL
	--	) s WHERE rn = 1
	--),
	----cropActivityGrouped
	--cropActivity AS (
	--	SELECT
	--		*
	--	FROM cropActivityRaw
	--	WHERE rn = 1
	--)

	--SELECT
	--	lc.farm_id,
	--	lc.field_id,
	--	c.id,
	--	CASE WHEN lc.activity_type = -1 THEN NULL ELSE c.id END as active_crop_id,
	--	CASE WHEN lc.activity_type = -1 THEN NULL ELSE c.identifier END as active_crop_identifier,
	--	CASE WHEN lc.activity_type = -1 THEN NULL ELSE c.name END as active_crop_name,
	--	CASE WHEN lc.activity_type = -1 THEN NULL ELSE c.name_en END as active_crop_name_en
	--FROM cropActivity lc
	--	JOIN FieldBook.crop c ON lc.crop_id = c.id

	--WITH tmpLastCrops AS (
	--	SELECT f.farm_id, fc.field_id, fc.crop_type, fc.crop_id, fc.sowing_date, fc.sowing_task_id, fc.harvest_date, fc.harvest_task_id,
	--		ROW_NUMBER() OVER (PARTITION BY field_id ORDER BY ISNULL(harvest_date, sowing_date) DESC) AS rn
	--	FROM FieldBook.field_crop AS fc
	--		JOIN FieldBook.field f ON f.id = fc.field_id
	--	WHERE
	--	f.farm_id = @farmId
	--	AND (@fieldId IS NULL OR f.id = @fieldId)
	--	AND (@date IS NULL OR ISNULL(harvest_date, sowing_date) <= @date)
	--	AND (fc.sowing_date IS NOT NULL OR fc.harvest_date IS NOT NULL)
	--)

	--SELECT
	--	lc.farm_id,
	--	lc.field_id,
	--	lc.crop_type,
	--	CASE WHEN lc.harvest_date IS NOT NULL AND lc.harvest_date <= @date THEN NULL ELSE c.id END as last_crop_id,
	--	CASE WHEN lc.harvest_date IS NOT NULL AND lc.harvest_date <= @date THEN NULL ELSE c.identifier END as active_crop_identifier,
	--	CASE WHEN lc.harvest_date IS NOT NULL AND lc.harvest_date <= @date THEN NULL ELSE c.name END as active_crop_name,
	--	CASE WHEN lc.harvest_date IS NOT NULL AND lc.harvest_date <= @date THEN NULL ELSE c.name_en END as active_crop_name_en
	--FROM tmpLastCrops lc
	--	JOIN FieldBook.crop c ON lc.crop_id = c.id
	--WHERE lc.rn = 1
)
GO
/****** Object:  Table [FieldBook].[document]    Script Date: 09/09/2024 11:20:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [FieldBook].[document](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[uuid] [uniqueidentifier] NULL,
	[farm_id] [int] NULL,
	[file_id] [int] NOT NULL,
	[description] [varchar](500) NULL,
	[document_type_id] [int] NULL,
	[task_id] [int] NULL,
	[created] [datetime] NULL,
	[created_by] [int] NULL,
	[modified] [datetime] NULL,
	[modified_by] [int] NULL,
	[is_deleted] [bit] NULL,
	[is_active] [bit] NULL,
 CONSTRAINT [PK_document] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [FieldBook].[document_type]    Script Date: 09/09/2024 11:20:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [FieldBook].[document_type](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[uuid] [uniqueidentifier] NULL,
	[name] [varchar](250) NULL,
	[name_en] [varchar](250) NULL,
	[created] [datetime] NULL,
	[created_by] [int] NULL,
	[modified] [datetime] NULL,
	[modified_by] [int] NULL,
	[is_deleted] [bit] NULL,
	[is_active] [bit] NULL,
 CONSTRAINT [PK_document_type] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [FieldBook].[farm]    Script Date: 09/09/2024 11:20:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [FieldBook].[farm](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[uuid] [uniqueidentifier] NULL,
	[name] [varchar](250) NULL,
	[owner] [varchar](250) NULL,
	[address] [varchar](500) NULL,
	[post_code] [varchar](50) NULL,
	[city] [varchar](500) NULL,
	[identifier] [varchar](50) NULL,
	[eco_farm] [bit] NULL,
	[location] [geography] NULL,
	[farm_geometry] [geography] NULL,
	[thumb_file_id] [int] NULL,
	[created] [datetime] NULL,
	[created_by] [int] NULL,
	[modified] [datetime] NULL,
	[modified_by] [int] NULL,
	[is_deleted] [bit] NULL,
	[is_active] [bit] NULL,
 CONSTRAINT [PK_farm_1] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [FieldBook].[fertilizer]    Script Date: 09/09/2024 11:20:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [FieldBook].[fertilizer](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[uuid] [uniqueidentifier] NULL,
	[name] [varchar](250) NULL,
	[name_en] [varchar](250) NULL,
	[brand_name] [varchar](250) NULL,
	[type] [varchar](50) NULL,
	[ratio_n] [int] NULL,
	[ratio_p] [int] NULL,
	[ratio_k] [int] NULL,
	[data] [varchar](max) NULL,
	[status] [int] NULL,
	[is_global] [bit] NULL,
	[farm_id] [int] NULL,
	[created] [datetime] NULL,
	[created_by] [int] NULL,
	[modified] [datetime] NULL,
	[modified_by] [int] NULL,
	[is_deleted] [bit] NULL,
	[is_active] [bit] NULL,
 CONSTRAINT [PK_fertilizer] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [FieldBook].[field_measure]    Script Date: 09/09/2024 11:20:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [FieldBook].[field_measure](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[uuid] [uniqueidentifier] NULL,
	[field_id] [int] NULL,
	[field_plot_id] [int] NULL,
	[field_crop_id] [int] NULL,
	[measure_id] [int] NULL,
	[year] [int] NULL,
	[created] [datetime] NULL,
	[created_by] [int] NULL,
	[modified] [datetime] NULL,
	[modified_by] [int] NULL,
	[is_deleted] [bit] NULL,
	[is_active] [bit] NULL,
 CONSTRAINT [PK_field_measure] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [FieldBook].[field_type]    Script Date: 09/09/2024 11:20:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [FieldBook].[field_type](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[uuid] [uniqueidentifier] NULL,
	[identifier] [varchar](50) NULL,
	[name] [varchar](250) NULL,
	[name_en] [varchar](250) NULL,
	[environment_type] [varchar](50) NULL,
	[include_in_crop_rotation] [bit] NULL,
	[created] [datetime] NULL,
	[created_by] [int] NULL,
	[modified] [datetime] NULL,
	[modified_by] [int] NULL,
	[is_deleted] [bit] NULL,
	[is_active] [bit] NULL,
 CONSTRAINT [PK_field_type] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [FieldBook].[file]    Script Date: 09/09/2024 11:20:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [FieldBook].[file](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[uuid] [uniqueidentifier] NULL,
	[farm_id] [int] NULL,
	[displayname] [varchar](500) NULL,
	[filename] [varchar](1000) NULL,
	[size] [int] NULL,
	[extension] [varchar](10) NULL,
	[storage_type] [varchar](50) NULL,
	[created] [datetime] NULL,
	[created_by] [int] NULL,
	[modified] [datetime] NULL,
	[modified_by] [int] NULL,
	[is_deleted] [bit] NULL,
	[is_active] [bit] NULL,
 CONSTRAINT [PK_file] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [FieldBook].[machinery]    Script Date: 09/09/2024 11:20:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [FieldBook].[machinery](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[uuid] [uniqueidentifier] NULL,
	[farm_id] [int] NULL,
	[name] [varchar](250) NULL,
	[description] [varchar](500) NULL,
	[performance] [decimal](18, 2) NULL,
	[performance_unit_id] [int] NULL,
	[performance_unit] [varchar](50) NULL,
	[classification_id] [int] NULL,
	[tracking_id] [varchar](50) NULL,
	[tracking_type] [int] NULL,
	[farm_manager_id] [int] NULL,
	[farm_manager_name] [varchar](250) NULL,
	[created] [datetime] NULL,
	[created_by] [int] NULL,
	[modified] [datetime] NULL,
	[modified_by] [int] NULL,
	[is_deleted] [bit] NULL,
	[is_active] [bit] NULL,
 CONSTRAINT [PK_machinery] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [FieldBook].[machinery_classification]    Script Date: 09/09/2024 11:20:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [FieldBook].[machinery_classification](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[uuid] [uniqueidentifier] NULL,
	[code] [varchar](50) NULL,
	[parent_code] [varchar](50) NULL,
	[name] [varchar](500) NULL,
	[data] [varchar](max) NULL,
	[task_type_id] [int] NULL,
	[created] [datetime] NULL,
	[created_by] [int] NULL,
	[modified] [datetime] NULL,
	[modified_by] [int] NULL,
	[is_deleted] [bit] NULL,
	[is_active] [bit] NULL,
 CONSTRAINT [PK_machinery_classification] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [FieldBook].[measure]    Script Date: 09/09/2024 11:20:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [FieldBook].[measure](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[uuid] [uniqueidentifier] NULL,
	[name] [varchar](250) NULL,
	[name_en] [varchar](250) NULL,
	[description] [varchar](500) NULL,
	[description_en] [varchar](500) NULL,
	[category] [varchar](50) NULL,
	[year_from] [int] NULL,
	[year_to] [int] NULL,
	[created] [datetime] NULL,
	[created_by] [int] NULL,
	[modified] [datetime] NULL,
	[modified_by] [int] NULL,
	[is_deleted] [bit] NULL,
	[is_active] [bit] NULL,
 CONSTRAINT [PK_measure] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [FieldBook].[pesticide]    Script Date: 09/09/2024 11:20:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [FieldBook].[pesticide](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[uuid] [uniqueidentifier] NULL,
	[name] [varchar](250) NULL,
	[name_en] [varchar](250) NULL,
	[brand_name] [varchar](250) NULL,
	[data] [varchar](max) NULL,
	[status] [int] NULL,
	[is_global] [bit] NULL,
	[farm_id] [int] NULL,
	[created] [datetime] NULL,
	[created_by] [int] NULL,
	[modified] [datetime] NULL,
	[modified_by] [int] NULL,
	[is_deleted] [bit] NULL,
	[is_active] [bit] NULL,
 CONSTRAINT [PK_pesticide] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [FieldBook].[season]    Script Date: 09/09/2024 11:20:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [FieldBook].[season](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[farm_id] [int] NOT NULL,
	[year] [int] NOT NULL,
	[uuid] [uniqueidentifier] NULL,
	[active] [bit] NULL,
	[created] [datetime] NULL,
	[created_by] [int] NULL,
	[modified] [datetime] NULL,
	[modified_by] [int] NULL,
	[is_deleted] [bit] NULL,
	[is_active] [bit] NULL,
 CONSTRAINT [PK_season] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [FieldBook].[task]    Script Date: 09/09/2024 11:20:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [FieldBook].[task](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[uuid] [uniqueidentifier] NULL,
	[farm_id] [int] NOT NULL,
	[year] [int] NULL,
	[date_from] [datetime] NULL,
	[date_to] [datetime] NULL,
	[task_type_id] [int] NULL,
	[crop_status_after] [varchar](50) NULL,
	[crop_id] [int] NULL,
	[executor] [varchar](50) NULL,
	[description] [varchar](max) NULL,
	[note] [varchar](max) NULL,
	[equipment_id] [int] NULL,
	[attached_equipment_id] [int] NULL,
	[treatment_successful] [bit] NULL,
	[harvest_amount] [int] NULL,
	[tracking_trip_id] [int] NULL,
	[data] [varchar](max) NULL,
	[created] [datetime] NULL,
	[created_by] [int] NULL,
	[modified] [datetime] NULL,
	[modified_by] [int] NULL,
	[is_deleted] [bit] NULL,
	[is_active] [bit] NULL,
 CONSTRAINT [PK_task] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [FieldBook].[task_fertilizer]    Script Date: 09/09/2024 11:20:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [FieldBook].[task_fertilizer](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[uuid] [uniqueidentifier] NULL,
	[task_id] [int] NOT NULL,
	[fertilizer_id] [int] NOT NULL,
	[amount] [decimal](18, 2) NULL,
	[amount_unit_id] [int] NULL,
	[amount_unit] [varchar](50) NULL,
	[amount_used] [decimal](18, 2) NULL,
	[amount_used_unit_id] [int] NULL,
	[amount_used_unit] [varchar](50) NULL,
	[created] [datetime] NULL,
	[created_by] [int] NULL,
	[modified] [datetime] NULL,
	[modified_by] [int] NULL,
	[is_deleted] [bit] NULL,
	[is_active] [bit] NULL,
 CONSTRAINT [PK_task_fertilizer] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [FieldBook].[task_field]    Script Date: 09/09/2024 11:20:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [FieldBook].[task_field](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[uuid] [uniqueidentifier] NULL,
	[task_id] [int] NULL,
	[field_id] [int] NULL,
	[field_plot_id] [int] NULL,
	[field_crop_id] [int] NULL,
	[duration] [int] NULL,
	[created] [datetime] NULL,
	[created_by] [int] NULL,
	[modified] [datetime] NULL,
	[modified_by] [int] NULL,
	[is_deleted] [bit] NULL,
	[is_active] [bit] NULL,
 CONSTRAINT [PK_task_field] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [FieldBook].[task_pesticide]    Script Date: 09/09/2024 11:20:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [FieldBook].[task_pesticide](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[uuid] [uniqueidentifier] NULL,
	[task_id] [int] NOT NULL,
	[pesticide_id] [int] NOT NULL,
	[amount] [decimal](18, 2) NULL,
	[amount_unit_id] [int] NULL,
	[amount_unit] [varchar](50) NULL,
	[amount_used] [decimal](18, 2) NULL,
	[amount_used_unit_id] [int] NULL,
	[amount_used_unit] [varchar](50) NULL,
	[created] [datetime] NULL,
	[created_by] [int] NULL,
	[modified] [datetime] NULL,
	[modified_by] [int] NULL,
	[is_deleted] [bit] NULL,
	[is_active] [bit] NULL,
 CONSTRAINT [PK_task_pesticide] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [FieldBook].[task_type]    Script Date: 09/09/2024 11:20:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [FieldBook].[task_type](
	[id] [int] NOT NULL,
	[uuid] [uniqueidentifier] NULL,
	[name] [varchar](250) NULL,
	[name_en] [varchar](250) NULL,
	[system_type] [varchar](50) NULL,
	[ploutos_type] [varchar](50) NULL,
	[crop_status] [varchar](50) NULL,
	[requires_time] [bit] NULL,
	[created] [datetime] NULL,
	[created_by] [int] NULL,
	[modified] [datetime] NULL,
	[modified_by] [int] NULL,
	[is_deleted] [bit] NULL,
	[is_active] [bit] NULL,
 CONSTRAINT [PK_task_type] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [FieldBook].[unit_group]    Script Date: 09/09/2024 11:20:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [FieldBook].[unit_group](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[unit_id] [int] NOT NULL,
	[group_id] [varchar](50) NOT NULL,
	[created] [datetime] NULL,
	[created_by] [int] NULL,
	[modified] [datetime] NULL,
	[modified_by] [int] NULL,
	[is_deleted] [bit] NULL,
	[is_active] [bit] NULL,
 CONSTRAINT [PK_unit_group] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [FieldBook].[user]    Script Date: 09/09/2024 11:20:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [FieldBook].[user](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[uuid] [uniqueidentifier] NULL,
	[name] [varchar](50) NULL,
	[surname] [varchar](50) NULL,
	[displayname] [varchar](100) NULL,
	[email] [varchar](100) NULL,
	[password] [varchar](256) NULL,
	[password_salt] [varchar](256) NULL,
	[farm_id] [int] NULL,
	[role] [varchar](50) NULL,
	[last_login] [datetime] NULL,
	[status] [int] NULL,
	[created] [datetime] NULL,
	[created_by] [int] NULL,
	[modified] [datetime] NULL,
	[modified_by] [int] NULL,
	[is_deleted] [bit] NULL,
	[is_active] [bit] NULL,
 CONSTRAINT [PK_user_1] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [FieldBook].[user_crop_setting]    Script Date: 09/09/2024 11:20:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [FieldBook].[user_crop_setting](
	[user_id] [int] NOT NULL,
	[crop_id] [int] NOT NULL,
	[is_favorite] [bit] NULL,
	[hex_color] [varchar](9) NULL,
	[created] [datetime] NULL,
	[created_by] [int] NULL,
	[modified] [datetime] NULL,
	[modified_by] [int] NULL,
	[is_deleted] [bit] NULL,
	[is_active] [bit] NULL,
 CONSTRAINT [PK_user_crop_setting] PRIMARY KEY CLUSTERED 
(
	[user_id] ASC,
	[crop_id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [FieldBook].[user_farm]    Script Date: 09/09/2024 11:20:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [FieldBook].[user_farm](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[user_id] [int] NOT NULL,
	[farm_id] [int] NOT NULL,
	[created] [datetime] NULL,
	[created_by] [int] NULL,
	[modified] [datetime] NULL,
	[modified_by] [int] NULL,
	[is_deleted] [bit] NULL,
	[is_active] [bit] NULL,
 CONSTRAINT [PK_user_farm] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [Farmtopia].[provider]    Script Date: 13/09/2024 13:21:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Farmtopia].[provider](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](250) NULL,
	[directory_url] [varchar](1000) NULL,
	[secret_key] [varchar](1000) NULL,
	[active] [bit] NULL,
	[created] [datetime] NULL,
	[modified] [datetime] NULL,
 CONSTRAINT [PK_provider] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [Farmtopia].[provider_user]    Script Date: 13/09/2024 13:22:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Farmtopia].[provider_user](
	[provider_id] [int] NOT NULL,
	[username] [varchar](50) NOT NULL,
	[dataset_id] [int] NOT NULL,
	[active] [bit] NULL,
	[created] [datetime] NULL,
	[modified] [datetime] NULL,
 CONSTRAINT [PK_provider_user] PRIMARY KEY CLUSTERED 
(
	[provider_id] ASC,
	[username] ASC,
	[dataset_id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  UserDefinedFunction [dbo].[GetGeoJSON]    Script Date: 13/09/2024 14:04:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[GetGeoJSON] (@geo geography)
	RETURNS varchar(max)
	WITH SCHEMABINDING
/*
 * Reference: http://stackoverflow.com/questions/6506720/reformat-sqlgeography-polygons-to-json
 */
AS
BEGIN
	DECLARE @Result varchar(max)
	SELECT  @Result = '{' +
    CASE @geo.STGeometryType()
        WHEN 'POINT' THEN
            '"type": "Point","coordinates":' +
            REPLACE(REPLACE(REPLACE(REPLACE(@geo.ToString(),'POINT ',''),'(','['),')',']'),' ',',')
        WHEN 'POLYGON' THEN 
            '"type": "Polygon","coordinates":' +
            '[' + REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@geo.ToString(),'POLYGON ',''),'(','['),')',']'),'], ',']],['),', ','],['),' ',',') + ']'
        WHEN 'MULTIPOLYGON' THEN 
            '"type": "MultiPolygon","coordinates":' +
            '[' + REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@geo.ToString(),'MULTIPOLYGON ',''),'(','['),')',']'),'], ',']],['),', ','],['),' ',',') + ']'
    ELSE NULL
    END
    +'}'

    RETURN @Result

END
GO
ALTER TABLE [FieldBook].[crop] ADD  CONSTRAINT [DF_crop_uuid]  DEFAULT (newid()) FOR [uuid]
GO
ALTER TABLE [FieldBook].[crop] ADD  CONSTRAINT [DF_crop_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [FieldBook].[crop] ADD  CONSTRAINT [DF_crop_is_active]  DEFAULT ((1)) FOR [is_active]
GO
ALTER TABLE [FieldBook].[document] ADD  CONSTRAINT [DF_document_uuid]  DEFAULT (newid()) FOR [uuid]
GO
ALTER TABLE [FieldBook].[document] ADD  CONSTRAINT [DF_document_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [FieldBook].[document] ADD  CONSTRAINT [DF_document_is_active]  DEFAULT ((1)) FOR [is_active]
GO
ALTER TABLE [FieldBook].[document_type] ADD  CONSTRAINT [DF_document_type_uuid]  DEFAULT (newid()) FOR [uuid]
GO
ALTER TABLE [FieldBook].[document_type] ADD  CONSTRAINT [DF_document_type_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [FieldBook].[document_type] ADD  CONSTRAINT [DF_document_type_is_active]  DEFAULT ((1)) FOR [is_active]
GO
ALTER TABLE [FieldBook].[farm] ADD  CONSTRAINT [DF_farm_uuid]  DEFAULT (newid()) FOR [uuid]
GO
ALTER TABLE [FieldBook].[farm] ADD  CONSTRAINT [DF_farm_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [FieldBook].[farm] ADD  CONSTRAINT [DF_farm_is_active]  DEFAULT ((1)) FOR [is_active]
GO
ALTER TABLE [FieldBook].[fertilizer] ADD  CONSTRAINT [DF_fertilizer_uuid]  DEFAULT (newid()) FOR [uuid]
GO
ALTER TABLE [FieldBook].[fertilizer] ADD  CONSTRAINT [DF_fertilizer_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [FieldBook].[fertilizer] ADD  CONSTRAINT [DF_fertilizer_is_active]  DEFAULT ((1)) FOR [is_active]
GO
ALTER TABLE [FieldBook].[field] ADD  CONSTRAINT [DF_field_uuid]  DEFAULT (newid()) FOR [uuid]
GO
ALTER TABLE [FieldBook].[field] ADD  CONSTRAINT [DF_field_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [FieldBook].[field] ADD  CONSTRAINT [DF_field_is_active]  DEFAULT ((1)) FOR [is_active]
GO
ALTER TABLE [FieldBook].[field_measure] ADD  CONSTRAINT [DF_field_measure_uuid]  DEFAULT (newid()) FOR [uuid]
GO
ALTER TABLE [FieldBook].[field_measure] ADD  CONSTRAINT [DF_field_measure_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [FieldBook].[field_measure] ADD  CONSTRAINT [DF_field_measure_is_active]  DEFAULT ((1)) FOR [is_active]
GO
ALTER TABLE [FieldBook].[field_plot] ADD  CONSTRAINT [DF_field_plot_uuid]  DEFAULT (newid()) FOR [uuid]
GO
ALTER TABLE [FieldBook].[field_plot] ADD  CONSTRAINT [DF_field_plot_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [FieldBook].[field_plot] ADD  CONSTRAINT [DF_field_plot_is_active]  DEFAULT ((1)) FOR [is_active]
GO
ALTER TABLE [FieldBook].[field_type] ADD  CONSTRAINT [DF_field_type_uuid]  DEFAULT (newid()) FOR [uuid]
GO
ALTER TABLE [FieldBook].[field_type] ADD  CONSTRAINT [DF_field_type_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [FieldBook].[field_type] ADD  CONSTRAINT [DF_field_type_is_active]  DEFAULT ((1)) FOR [is_active]
GO
ALTER TABLE [FieldBook].[file] ADD  CONSTRAINT [DF_file_uuid]  DEFAULT (newid()) FOR [uuid]
GO
ALTER TABLE [FieldBook].[file] ADD  CONSTRAINT [DF_file_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [FieldBook].[file] ADD  CONSTRAINT [DF_file_is_active]  DEFAULT ((1)) FOR [is_active]
GO
ALTER TABLE [FieldBook].[machinery] ADD  CONSTRAINT [DF_machinery_uuid]  DEFAULT (newid()) FOR [uuid]
GO
ALTER TABLE [FieldBook].[machinery] ADD  CONSTRAINT [DF_machinery_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [FieldBook].[machinery] ADD  CONSTRAINT [DF_machinery_is_active]  DEFAULT ((1)) FOR [is_active]
GO
ALTER TABLE [FieldBook].[machinery_classification] ADD  CONSTRAINT [DF_machinery_classification_uuid]  DEFAULT (newid()) FOR [uuid]
GO
ALTER TABLE [FieldBook].[machinery_classification] ADD  CONSTRAINT [DF_machinery_classification_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [FieldBook].[machinery_classification] ADD  CONSTRAINT [DF_machinery_classification_is_active]  DEFAULT ((1)) FOR [is_active]
GO
ALTER TABLE [FieldBook].[measure] ADD  CONSTRAINT [DF_measure_uuid]  DEFAULT (newid()) FOR [uuid]
GO
ALTER TABLE [FieldBook].[measure] ADD  CONSTRAINT [DF_measure_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [FieldBook].[measure] ADD  CONSTRAINT [DF_measure_is_active]  DEFAULT ((1)) FOR [is_active]
GO
ALTER TABLE [FieldBook].[pesticide] ADD  CONSTRAINT [DF_pesticide_uuid]  DEFAULT (newid()) FOR [uuid]
GO
ALTER TABLE [FieldBook].[pesticide] ADD  CONSTRAINT [DF_pesticide_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [FieldBook].[pesticide] ADD  CONSTRAINT [DF_pesticide_is_active]  DEFAULT ((1)) FOR [is_active]
GO
ALTER TABLE [FieldBook].[season] ADD  CONSTRAINT [DF_season_uuid]  DEFAULT (newid()) FOR [uuid]
GO
ALTER TABLE [FieldBook].[season] ADD  CONSTRAINT [DF_season_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [FieldBook].[season] ADD  CONSTRAINT [DF_season_is_active]  DEFAULT ((1)) FOR [is_active]
GO
ALTER TABLE [FieldBook].[stock_fertilizer] ADD  CONSTRAINT [DF_stock_fertilizer_uuid]  DEFAULT (newid()) FOR [uuid]
GO
ALTER TABLE [FieldBook].[stock_fertilizer] ADD  CONSTRAINT [DF_stock_fertilizer_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [FieldBook].[stock_fertilizer] ADD  CONSTRAINT [DF_stock_fertilizer_is_active]  DEFAULT ((1)) FOR [is_active]
GO
ALTER TABLE [FieldBook].[stock_pesticide] ADD  CONSTRAINT [DF_stock_pesticide_uuid]  DEFAULT (newid()) FOR [uuid]
GO
ALTER TABLE [FieldBook].[stock_pesticide] ADD  CONSTRAINT [DF_stock_pesticide_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [FieldBook].[stock_pesticide] ADD  CONSTRAINT [DF_stock_pesticide_is_active]  DEFAULT ((1)) FOR [is_active]
GO
ALTER TABLE [FieldBook].[task] ADD  CONSTRAINT [DF_task_uuid]  DEFAULT (newid()) FOR [uuid]
GO
ALTER TABLE [FieldBook].[task] ADD  CONSTRAINT [DF_task_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [FieldBook].[task] ADD  CONSTRAINT [DF_task_is_active]  DEFAULT ((1)) FOR [is_active]
GO
ALTER TABLE [FieldBook].[task_fertilizer] ADD  CONSTRAINT [DF_task_fertilizer_uuid]  DEFAULT (newid()) FOR [uuid]
GO
ALTER TABLE [FieldBook].[task_fertilizer] ADD  CONSTRAINT [DF_task_fertilizer_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [FieldBook].[task_fertilizer] ADD  CONSTRAINT [DF_task_fertilizer_is_active]  DEFAULT ((1)) FOR [is_active]
GO
ALTER TABLE [FieldBook].[task_field] ADD  CONSTRAINT [DF_task_field_uuid]  DEFAULT (newid()) FOR [uuid]
GO
ALTER TABLE [FieldBook].[task_field] ADD  CONSTRAINT [DF_task_field_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [FieldBook].[task_field] ADD  CONSTRAINT [DF_task_field_is_active]  DEFAULT ((1)) FOR [is_active]
GO
ALTER TABLE [FieldBook].[task_pesticide] ADD  CONSTRAINT [DF_task_pesticide_uuid]  DEFAULT (newid()) FOR [uuid]
GO
ALTER TABLE [FieldBook].[task_pesticide] ADD  CONSTRAINT [DF_task_pesticide_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [FieldBook].[task_pesticide] ADD  CONSTRAINT [DF_task_pesticide_is_active]  DEFAULT ((1)) FOR [is_active]
GO
ALTER TABLE [FieldBook].[task_type] ADD  CONSTRAINT [DF_task_type_uuid]  DEFAULT (newid()) FOR [uuid]
GO
ALTER TABLE [FieldBook].[task_type] ADD  CONSTRAINT [DF_task_type_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [FieldBook].[task_type] ADD  CONSTRAINT [DF_task_type_is_active]  DEFAULT ((1)) FOR [is_active]
GO
ALTER TABLE [FieldBook].[unit] ADD  CONSTRAINT [DF_unit_uuid]  DEFAULT (newid()) FOR [uuid]
GO
ALTER TABLE [FieldBook].[unit] ADD  CONSTRAINT [DF_unit_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [FieldBook].[unit] ADD  CONSTRAINT [DF_unit_is_active]  DEFAULT ((1)) FOR [is_active]
GO
ALTER TABLE [FieldBook].[unit_group] ADD  CONSTRAINT [DF_unit_group_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [FieldBook].[unit_group] ADD  CONSTRAINT [DF_unit_group_is_active]  DEFAULT ((1)) FOR [is_active]
GO
ALTER TABLE [FieldBook].[user] ADD  CONSTRAINT [DF_user_uuid]  DEFAULT (newid()) FOR [uuid]
GO
ALTER TABLE [FieldBook].[user] ADD  CONSTRAINT [DF_user_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [FieldBook].[user] ADD  CONSTRAINT [DF_user_is_active]  DEFAULT ((1)) FOR [is_active]
GO
ALTER TABLE [FieldBook].[user_crop_setting] ADD  CONSTRAINT [DF_user_crop_setting_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [FieldBook].[user_crop_setting] ADD  CONSTRAINT [DF_user_crop_setting_is_active]  DEFAULT ((1)) FOR [is_active]
GO
ALTER TABLE [FieldBook].[user_farm] ADD  CONSTRAINT [DF_user_farm_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [FieldBook].[user_farm] ADD  CONSTRAINT [DF_user_farm_is_active]  DEFAULT ((1)) FOR [is_active]
GO
PRINT 'DATABASE & TABLES CREATED...'
GO
