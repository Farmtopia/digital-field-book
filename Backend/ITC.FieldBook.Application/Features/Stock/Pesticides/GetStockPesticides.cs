﻿using ITC.FieldBook.Application.Infrastructure.Interfaces;
using ITC.FieldBook.Application.Infrastructure;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata;
using Dapper;
using ITC.FieldBook.Application.Models;
using ITC.FieldBook.Application.Infrastructure.Models;

namespace ITC.FieldBook.Application.Features.Stock.Pesticides
{
    public class GetStockPesticidesController : ApiControllerBase
    {
        [HttpGet("stock/pesticides")]
        public Task<DbData<StockPesticide>> GetStockPesticides()
        {
            return Mediator.Send(new GetStockPesticidesQuery { });
        }
    }

    public class GetStockPesticidesQuery : IRequest<DbData<StockPesticide>>
    {
    }

    internal sealed class GetStockPesticidesQueryHandler : IRequestHandler<GetStockPesticidesQuery, DbData<StockPesticide>>
    {
        private readonly DbContext _ctx;
        private readonly IConfiguration _configuration;
        private readonly ICurrentUserService _currentUserService;

        public GetStockPesticidesQueryHandler(DbContext ctx, IConfiguration configuration, ICurrentUserService currentUserService)
        {
            _ctx = ctx;
            _configuration = configuration;
            _currentUserService = currentUserService;
        }

        public Task<DbData<StockPesticide>> Handle(GetStockPesticidesQuery request, CancellationToken cancellationToken)
        {
            var stockRepository = new StockRepository(_ctx);
            var data = stockRepository.GetStockPesticides(_currentUserService.FarmId!.Value);

            return Task.FromResult(data);
        }
    }
}
