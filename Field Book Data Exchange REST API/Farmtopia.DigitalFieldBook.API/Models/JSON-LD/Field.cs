﻿using System.Text.Json.Serialization;

namespace Farmtopia.DigitalFieldBook.API.Models.JSON_LD
{
    public class Field : JsonLdBase
    {
        [JsonPropertyName("landType")]
        public LandType LandType { get; set; }

        [JsonPropertyName("contains")]
        public List<Crop> Contains { get; set; }
    }
}
