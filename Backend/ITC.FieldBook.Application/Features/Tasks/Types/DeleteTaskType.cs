﻿using ITC.FieldBook.Application.Infrastructure.Models;
using ITC.FieldBook.Application.Infrastructure;
using MediatR;
using Microsoft.AspNetCore.DataProtection.KeyManagement;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using ITC.FieldBook.Application.Models;
using ITC.FieldBook.Application.Infrastructure.Services;
using ITC.FieldBook.Application.Infrastructure.Interfaces;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Authorization;
using ITC.FieldBook.Application.Infrastructure.Enums;

namespace ITC.FieldBook.Application.Features.Tasks.Types
{
    [Authorize(Roles = EnumRole.Admin)]
    public class DeleteTaskTypeController : ApiControllerBase
    {
        [HttpDelete("tasks/task-types/{taskTypeId}")]
        public Task DeleteTaskType([FromRoute] int? taskTypeId)
        {
            return Mediator.Send(new DeleteTaskTypeCommand { TaskTypeId = taskTypeId });
        }
    }

    public class DeleteTaskTypeCommand : IRequest
    {
        [Required]
        public int? TaskTypeId { get; set; }
    }

    internal sealed class DeleteTaskTypeCommandHandler : IRequestHandler<DeleteTaskTypeCommand>
    {
        private readonly DbContext _ctx;
        private readonly ICurrentUserService _currentUserService;

        public DeleteTaskTypeCommandHandler(DbContext ctx, ICurrentUserService currentUserService)
        {
            _ctx = ctx;
            _currentUserService = currentUserService;
        }

        public Task Handle(DeleteTaskTypeCommand request, CancellationToken cancellationToken)
        {
            var taskTypesRepository = new TasksRepository(_ctx);
            var taskType = taskTypesRepository.GetTaskType(request.TaskTypeId!.Value).Item;
            if (taskType != null)
            {
                var user = _currentUserService.GetUser()!;
                _ctx.DeleteItem(taskType, user);
            }

            return Task.CompletedTask;
        }
    }
}

