﻿using ITC.FieldBook.Application.Infrastructure.Interfaces;
using ITC.FieldBook.Application.Infrastructure;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata;
using Dapper;
using ITC.FieldBook.Application.Models;
using ITC.FieldBook.Application.Infrastructure.Models;
using ITC.FieldBook.Application.Infrastructure.Enums;
using ITC.FieldBook.Application.Infrastructure.Tools;

namespace ITC.FieldBook.Application.Features.Farms
{
    public class GetFarmFromRkgController : ApiControllerBase
    {
        [HttpGet("rkg/farm")]
        public Task<RkgKmg?> GetFarmFromRkg(int kmgMid)
        {
            return Mediator.Send(new GetFarmFromRkgQuery { KmgMid = kmgMid });
        }
    }

    public class GetFarmFromRkgQuery : IRequest<RkgKmg?>
    {
        public int? KmgMid { get; set; }
    }

    internal sealed class GetFarmFromRkgQueryHandler : IRequestHandler<GetFarmFromRkgQuery, RkgKmg?>
    {
        private readonly DbContext _ctx;
        private readonly IConfiguration _configuration;
        private readonly ICurrentUserService _currentUserService;

        public GetFarmFromRkgQueryHandler(DbContext ctx, IConfiguration configuration, ICurrentUserService currentUserService)
        {
            _ctx = ctx;
            _configuration = configuration;
            _currentUserService = currentUserService;
        }

        public async Task<RkgKmg?> Handle(GetFarmFromRkgQuery request, CancellationToken cancellationToken)
        {
            var rkgManager = new RkgManager(_configuration["Rkg:ApiUrl"], _configuration["Rkg:Username"], _configuration["Rkg:Password"]);
            var rkgData = await rkgManager.GetFarm(request.KmgMid!.Value);

            return rkgData;
        }
    }
}
