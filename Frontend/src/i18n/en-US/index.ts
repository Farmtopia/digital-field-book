// This is just an example,
// so you can safely delete all default props below
export default {
  title: 'Digital Fieldbook',
  login: 'Login',
  logout: 'Logout',
  error404: 'Ups, this page does not exist...',
  sessionExpired: 'Your session has expired. Please login again.',
  initialLoading: 'Loading, please wait...',

  common: {
    home: 'Home',
    warning: 'Warning',
    validationError: 'Check validation errors',
    saveSuccess: 'Saved successfully',
    deleteSuccess: 'Deleted successfully',
    yes: 'Yes',
    no: 'No',
    add: 'Add',
    edit: 'Edit',
    delete: 'Delete',
    isActive: 'Active',
    showMore: 'Show more',
    showLess: 'Show less',

    units: {
      ha: 'ha',
      are: 'are',
      m2: 'm²',
    },
  },

  components: {
    file: {
      downloadFile: 'Download file',
    },
  },

  validationMessages: {
    requiredField: 'This field is required',
    minLength: 'Minimum required length: {minLength}',
    valuesMustMatch: 'Values must match',
    greatherThan: 'Value must be greather than {value}',
    greatherThanOrEqual: 'Value must be greather than or equal to {value}',
  },

  dialog: {
    ok: 'OK',
    confirm: 'Confirm',
    save: 'Save',
    delete: 'Delete',
    cancel: 'Cancel',
    close: 'Close',

    messages: {
      confirmDelete: 'Are you sure you want to delete selected item?',
    },
  },

  table: {
    search: 'Search',
    buttons: {
      view: 'View',
      add: 'Add',
      edit: 'Edit',
      delete: 'Delete',
    },
  },

  select: {
    noResults: 'No results',
  },

  adminLayout: {
    drawer: {
      administration: 'Administration',
      fertilizers: 'Fertilizers',
      pesticides: 'Pesticides',
      crops: 'Crops',
      units: 'Unit of measurement',
      measureTypes: 'Measures',
      documentTypes: 'Document types',
      fieldTypes: 'Field types',
      taskTypes: 'Task types',
      users: 'Users',
      settings: 'Settings',
      fieldbook: 'Fieldbook',
    },
  },

  mainLayout: {
    drawer: {
      farm: 'Farm',
      fields: 'Fields',
      cropRotation: 'Crop rotation',
      cropRotationSum: 'Sum by crops',
      log: 'Farm Diary',
      tasks: 'Tasks',
      fertilizers: 'Fertilizers',
      fertilizersList: 'List',
      pesticides: 'Pesticides',
      pesticidesList: 'List',
      stockControl: 'Stock control',
      stockStatus: 'Stock status',
      machinery: 'Machinery',
      documents: 'Documents',
      reports: 'Reports',
      settings: 'Settings',
      administration: 'Administration',
      useMyLocation: 'Use my location',
      showMyLocationTooltip: 'Where am i?',
    },
  },

  modules: {
    login: {
      title: 'Login',
    },

    farm: {
      title: 'Farm',
      farmInfo: 'Farm',
      farmIdentifier: 'Identifier',
      farmName: 'Name',
      farmOwner: 'Owner',
      farmAddress: 'Address',
      farmPostZipCode: 'Post/Zip code',
      farmCity: 'City',
      ecoFarm: 'Eco farm',

      fields: 'Fields',
      fieldIdentifier: 'Identifier',
      fieldName: 'Name',
      fieldArea: 'Area',
      fieldType: 'Field type',

      editDialog: {
        title: 'Farm',
        basicInfo: 'Basic info',
        basicInfoDescription: 'Provide basic info for the farm.',
        location: 'Location',
        selectLocationDescription:
          'Select location of your farm by placing a pin on the map.',
        onlyOnePinAllowed:
          'Only one location pin is allowed. Drag or remove existing pin if you want to change location',

        inputs: {
          farmIdentifier: 'Identifier',
          farmName: 'Name',
          farmOwner: 'Owner',
          farmAddress: 'Address',
          farmPostZipCode: 'Post/Zip code',
          farmCity: 'City',
          ecoFarm: 'Eco farm',
        },
      },
    },

    fields: {
      title: 'Fields',
      buttons: {
        addTask: 'Task',
        showTracking: 'Tracking',
        addField: 'Add field',
        editField: 'Edit field',
        showFieldDetails: 'Show field details',
      },
      filter: {
        date: 'Date',
      },
      table: {
        columns: {
          fieldIdentifier: 'Identifier',
          fieldName: 'Name',
          fieldArea: 'Area',
          fieldType: 'Field type',
          currentCrop: 'Crop on selected date',
          lastTask: 'Last task',
          location: 'Location',
          lastTasks: 'Last tasks',
        },
        noTasks: 'No tasks',
      },
      card: {
        details: 'Details',
      },
      editDialog: {
        title: 'Field',
        location: 'Location',

        basicInfo: 'Basic info',
        basicInfoDescription: 'Provide basic info for the field.',
        selectLocationDescription:
          'Select location of your field and draw polygon on the map.',

        inputs: {
          fieldIdentifier: 'Identifier',
          fieldName: 'Name',
          fieldArea: 'Area (are)',
          fieldType: 'Field type',
          fieldDescription: 'Description / note',
        },
      },
    },

    field: {
      title: 'Field',
      fieldInfo: 'Field',
      crops: 'Crops',
      tasks: 'Tasks',

      fieldColumns: {
        identifier: 'Identifier',
        name: 'Name',
        area: 'Area',
        type: 'Field type',
      },

      cropColumns: {
        name: 'Name',
        type: 'Crop type',
        sowingDate: 'Sowing date',
        harvestDate: 'Harvest date',
      },

      taskColumns: {
        from: 'From',
        to: 'To',
        name: 'Task',
        crop: 'Crop',
        executor: 'Executor',
        machine: 'Machine',
        attachment: 'Attachment',
        note: 'Note',
      },
    },

    cropRotation: {
      title: 'Crop rotation',
      noCropRotationFields: 'No fields defined for crop rotation.',
      selectOnlyOneYearForMeasures:
        'You can only edit measures for  multiple crops in same year.',

      filter: {
        cropType: 'Crop type',
      },

      columns: {
        field: 'Field',
        area: 'Area',
      },

      cropContextMenu: {
        editMeasures: 'Edit measures',
        setColor: 'Set color',
        setCropColor: 'Set color for selected crop',
        clear: 'Clear',
        selectMore: 'Select more',
        withSelected: 'With selected',
        setMeasures: 'Set measures',
        clearSelected: 'Clear selected',
      },

      fieldContextMenu: {
        editCrops: 'Edit crops',
        selectMore: 'Select more',
        withSelected: 'With selected',
        setCrops: 'Set crops',
        clearSelected: 'Clear selected',
      },
    },

    cropRotationSum: {
      title: 'Sum by crops',
      noCropRotationFields: 'No fields defined for crop rotation.',

      filter: {
        cropType: 'Crop type',
      },

      columns: {
        crop: 'Crop',
      },

      sum: 'Sum',
    },

    tasks: {
      title: 'Tasks',
      expandMoreFields: '(+ {count} more...)',
      expandAll: 'Expand all',
      confirmDelete: 'Are you sure you want to delete selected task?',

      confirmDeleteMultiple:
        'Deleting selected task will delete task on multiple ({count}) fields. If you want to remove only specific field, edit task.<br /><br />Are you sure you want to delete selected task?',

      report: 'Report',
      downloadReport: 'Download report',

      filter: {
        field: 'Field',
        dateFrom: 'Date from',
        dateTo: 'Date to',
      },

      columns: {
        dateFrom: 'Date',
        fieldName: 'Field / Plot',
        taskTypeName: 'Task type',
        currentCrops: 'Current crops',
        cropName: 'Crop',
        executor: 'Executor',
        machineName: 'Machine',
        attachmentName: 'Attachment',
        note: 'Note',
      },
    },

    taskEdit: {
      basic: {
        title: 'Task',
        basicInfo: 'Basic info',
        basicInfoDescription: 'Provide basic info for the task.',
      },
      plots: {
        title: 'Plots',
        description: 'Select plots you have worked on.',
        validationError:
          'Please fix validation errors or confirm if you want to continue anyway.',
        editCropRotation: 'Edit crop rotation',
        calculateDuration: 'Calculate duration',
        taskDuration: 'Task duration',
        taskDurationDescription:
          'This task requires task duration for each plot. Distribute the duration of the task among the selected plots. By default, the time is calculated based on the plot area.',

        columns: {
          area: 'Area',
          crop: 'Crop',
          sowingDate: 'Sowing date',
          harvestDate: 'Harvest date',
          durationMinutes: 'Duration (min)',
        },

        validationMessages: {
          noPlots:
            'No plots defined for {year}. Please define plots in crop rotation.',
          alreadySown: 'You have selected plots that have already been sown',
          alreadyHarvested:
            'You have selected plots that have already been harvested',
          selectAtLeastOnePlot: 'Select at least one plot',
          selectedDifferentCropTypes:
            'You can only select plots with the same crop type.',
          previousPlotsNotCompleted: "You haven't finished previous plot.",
          distributeEntireDuration:
            'The entire duration must be distributed among the selected plots ({distributedDuration}/{taskDuration}, difference: {difference}).',
        },
      },
      sowing: {
        title: 'Sowing',
        description: 'Select crops you have sown.',
      },
      harvesting: {
        title: 'Harvesting',
        description: 'Provide additional details about the completed harvest.',
      },
      fertilizing: {
        title: 'Ferilizing',
        description: 'Select fertilizers you used for this task.',
        stockDescription:
          'Stock status is shown for selected date. Arrange the stock if fertilizer is not on list.',
        addRow: 'Add row',
      },
      spraying: {
        title: 'Spraying',
        description: 'Select pesticides, you used for this task.',
        stockDescription:
          'Stock status is shown for selected date. Arrange the stock if pesticide is not on list.',
        addRow: 'Add row',
      },
      machinery: {
        title: 'Machinery',
        description: 'Select machinery, you used for this task.',
      },

      misc: {
        title: 'Misc',
        attachments: 'Attachments',
        addAttachments:
          'Add attachments (optional). Attachments will also be available in "Documents" module.',
      },

      inputs: {
        fields: 'Fields',
        from: 'From',
        to: 'To',
        taskType: 'Task type',
        cropStatusAfter: 'Crop status (if exists) after task execution',
        executor: 'Executor',
        crop: 'Crop',
        fertilizer: 'Fertilizer',
        pesticide: 'Pesticide',
        unit: 'Unit',
        quantity: 'Quantity',
        treatmentSuccess: 'Treatment successful',
        harvestAmount: 'Harvest amount (kg)',
        machine: 'Machine',
        machineAttachment: 'Attachment',
        note: 'Note',
        file: 'File',
        documentType: 'Document type',
        description: 'Description',
      },

      stockCalculation: {
        area: 'Area',
        stock: 'Stock',
        quantity: 'Quantity',
        usage: 'Usage',
        remainingStock: 'Remaining stock',
        noStock: 'Not enough stock',
        editStock: 'Edit stock',
      },

      selectField: 'Select at least 1 field',
      provideTime: 'This task requires start and end time of execution',
      continueNoStockTitle: 'Not enough stock',
      continueNoStock:
        'Please arrange the stock. Do you want to continue anyway?',
      next: 'Next',
      previous: 'Previous',
      save: 'Save',
      saveAndContinue: 'Save and continue',
      saveAndNew: 'Save and new',
    },

    fertilizers: {
      title: 'Fertilizers',
      list: 'List',
      stockControlTitle: 'Stock control',
      taskUsage: 'Task usage',

      filter: {
        myFertilizers: 'Added by me',
      },

      columns: {
        date: 'Date',
        fertilizerName: 'Fertilizer',
        stockName: 'Stock type',
        value: 'Quantity',
        note: 'Note',
      },

      editDialog: {
        title: 'Fertilizers - stock edit',
        selectStockType: 'Select stock type',

        inputs: {
          date: 'Date',
          fertilizer: 'Fertilizer',
          stockType: 'Stock type',
          value: 'Quantity',
          unit: 'Unit',
          note: 'Note',
        },
      },
    },

    fertilizerStockStatus: {
      title: 'Stock status',

      filter: {
        date: 'Date',
      },

      columns: {
        fertilizerName: 'Fertilizer',
        stock: 'Stock',
      },
    },

    pesticides: {
      title: 'Pesticides',
      list: 'List',
      stockControlTitle: 'Stock control',
      taskUsage: 'Task usage',

      columns: {
        date: 'Date',
        pesticideName: 'Pesticide',
        stockName: 'Stock type',
        value: 'Quantity',
        note: 'Note',
      },

      editDialog: {
        title: 'Pesticide - stock edit',
        selectStockType: 'Select stock type',

        inputs: {
          date: 'Date',
          pesticide: 'Pesticide',
          stockType: 'Stock type',
          value: 'Quantity',
          unit: 'Unit',
          note: 'Note',
        },
      },
    },

    pesticideStockStatus: {
      title: 'Stock status',

      filter: {
        date: 'Date',
      },

      columns: {
        pesticideName: 'Pesticide',
        stock: 'Stock',
      },
    },

    machinery: {
      title: 'Machinery',

      columns: {
        name: 'Name',
        description: 'Description',
        performance: 'Performance',
        classification: 'Classification',
        trackingId: 'Tracking ID',
        trackingDeviceType: 'Device type',
        farmManagerMachinery: 'Machinery (Farm Manager)',
      },

      editDialog: {
        title: 'Machine - edit',
        farmManager: 'Farm Manager',

        inputs: {
          name: 'Name',
          description: 'Description',
          performance: 'Performance',
          performanceUnit: 'Unit',
          classification: 'Classification',
          trackingId: 'Tracking ID',
          trackingDeviceType: 'Device type',
          farmManagerMachinery: 'Machinery',
        },
      },
    },

    documents: {
      title: 'Documents',
      download: 'Download',

      columns: {
        fileDisplayname: 'Filename',
        documentTypeName: 'Document type',
        description: 'Description',
        task: 'Task',
        created: 'Created',
      },

      editDialog: {
        title: 'Document - edit',

        inputs: {
          file: 'File',
          documentType: 'Document type',
          description: 'Description',
        },
      },
    },

    reports: {
      title: 'Reports',
      prepare: 'Prepare',

      basicReport: {
        title: 'Basic report',
        description:
          'General report based on data from the fieldbook (tasks, crops, ...).',
        yearHint: 'Enter the year for which you want to prepare the report.',
        fieldsHint: 'Leave empty if you want to prepare report for all fields.',
        exportCrops: 'Crops',
        exportMeasures: 'Measures',
        exportTasks: 'Tasks',
      },

      kopopReport: {
        title: 'Enotne evidence o delovnih opravilih, verzija 2.5',
        description:
          'Enotne evidence o delovnih opravilih za posamezne intervencije Strateškega načrta skupne kmetijske politike 2023-2027 - intervencija kmetijsko-okoljsko-podnebna plačila (KOPOP), intervencija lokalne pasme in sorte (LOPS), intervencija biotično varstvo rastlin (BVR), intervencija plačila Natura 2000 (N2000) ter za namen ekološkega kmetovanja (EK). Enotne evidence vodijo tisti nosilci kmetijskih gospodarstev, ki so vključeni v eno ali več intervencij - KOPOP, LOPS, N2000, BVR, SOPO ali EK.',
      },
    },

    settings: {
      title: 'Settings',
    },

    administration: {
      title: 'Administration',
    },

    admin: {
      farms: {
        title: 'Farms',

        buttons: {
          import: 'Import',
          fieldbook: 'Go to Fieldbook...',
        },

        columns: {
          identifier: 'Identifier',
          name: 'Name',
          owner: 'Owner',
          address: 'Address',
          city: 'City',
          ecoFarm: 'Eco farm',
        },
      },

      fertilizers: {
        title: 'Fertilizers',

        columns: {
          name: 'Name',
          type: 'Type',
          nameEn: 'Name (English)',
          ratio: 'Ratio',
          isGlobal: 'Global',
          addedByFarm: 'Added by farm',
        },

        editDialog: {
          title: 'Fertilizer - edit',
          basicInfo: 'Basic info',
          ratio: 'Ratio',
          visiblity: 'Visiblity',

          inputs: {
            name: 'Name',
            nameEn: 'Name (English)',
            type: 'Fertilizer type',
            ratioN: 'N - nitrogen',
            ratioP: 'P - phosphorus',
            ratioK: 'K - potassium',
            isGlobal: 'Global - available to all',
            isGlobalHint:
              'This item was added by a farm, so it is only available to that farm, unless an administrator sets it as global. Global pesticides are available to all users.',
            addedByFarm: 'Added by farm',
          },
        },
      },

      pesticides: {
        title: 'Pesticides',

        columns: {
          name: 'Name',
          nameEn: 'Name (English)',
          isGlobal: 'Global',
          addedByFarm: 'Added by farm',
        },

        editDialog: {
          title: 'Pesticide - edit',
          visiblity: 'Visiblity',

          inputs: {
            name: 'Name',
            nameEn: 'Name (English)',
            isGlobal: 'Global - available to all',
            isGlobalHint:
              'This item was added by a farm, so it is only available to that farm, unless an administrator sets it as global. Global pesticides are available to all users.',
            addedByFarm: 'Added by farm',
          },
        },
      },

      crops: {
        title: 'Crops',

        columns: {
          identifier: 'Identifier',
          name: 'Name',
          nameEn: 'Name (English)',
        },

        editDialog: {
          title: 'Crop - edit',

          inputs: {
            identifier: 'Identifier',
            name: 'Name',
            nameEn: 'Name (English)',
          },
        },
      },

      units: {
        title: 'Unit of measurement',

        columns: {
          name: 'Name',
          description: 'Description',
          descriptionEn: 'Description (English)',
          groups: 'Modules',
        },

        editDialog: {
          title: 'Unit - edit',
          groupsTitle: 'Modules',
          groupsDescription: 'Select modules where unit should be available.',

          groups: {
            task: 'Task',
            stock: 'Stock control',
            machinery: 'Machinery',
          },

          inputs: {
            name: 'Name',
            description: 'Description',
            descriptionEn: 'Description (English)',
            fertilizerUsage: 'Fertilizer usage',
            fertilizerStockControl: 'Fertilizer stock',
            pesticideUsage: 'Pesticide usage',
            pesticideStockControl: 'Pesticide stock',
            performance: 'Performance',
          },
        },
      },

      measureTypes: {
        title: 'Measures',

        columns: {
          name: 'Name',
          nameEn: 'Name (English)',
          description: 'Description',
          descriptionEn: 'Description (English)',
          category: 'Category',
          yearFrom: 'Active from (year)',
          yearTo: 'Active to (year)',
        },

        editDialog: {
          title: 'Measure - edit',

          inputs: {
            name: 'Name',
            nameEn: 'Name (English)',
            description: 'Description',
            descriptionEn: 'Description (English)',
            category: 'Category',
            yearFrom: 'Active from (year)',
            yearTo: 'Active to (year)',
          },
        },
      },

      documentTypes: {
        title: 'Document types',

        columns: {
          name: 'Name',
          nameEn: 'Name (English)',
        },

        editDialog: {
          title: 'Document type - edit',

          inputs: {
            name: 'Name',
            nameEn: 'Name (English)',
          },
        },
      },

      fieldTypes: {
        title: 'Field types',

        columns: {
          identifier: 'Identifier',
          name: 'Name',
          nameEn: 'Name (English)',
          environmentType: 'Environment type',
          includeInCropRotation: 'Include in crop rotation',
        },

        editDialog: {
          title: 'Field type - edit',

          inputs: {
            identifier: 'Identifier',
            name: 'Name',
            nameEn: 'Name (English)',
            environmentType: 'Environment type',
            includeInCropRotation: 'Include in crop rotation',
          },
        },
      },

      taskTypes: {
        title: 'Task types',

        columns: {
          identifier: 'Identifier',
          name: 'Name',
          nameEn: 'Name (English)',
          systemTypeName: 'System type',
          cropStatusName: 'Crop status after task',
          cropStatusEditable: 'User can set crop status',
          requiresTime: 'Requires time input',
        },

        editDialog: {
          title: 'Task type - edit',

          inputs: {
            identifier: 'Identifier',
            name: 'Name',
            nameEn: 'Name (English)',
            systemTypeName: 'System type',
            cropStatusName: 'Crop status after completing task',
            cropStatusEditable: 'User can set crop status',
            requiresTime: 'Requires time input',
          },
        },
      },

      users: {
        title: 'Users',

        role: {
          admin: 'Admin',
          user: 'User',
        },

        status: {
          inactive: 'Inactive',
          active: 'Active',
        },

        columns: {
          id: 'ID',
          name: 'Name',
          surname: 'Surname',
          email: 'Email',
          role: 'Role',
          farm: 'Farm',
        },

        editDialog: {
          user: 'User',
          password: 'Password',
          newPassword: 'New password',

          inputs: {
            name: 'Name',
            surname: 'Surname',
            email: 'Email',
            password: 'Password',
            passwordConfirmation: 'Password confirmation',
            role: 'Role',
            farm: 'Farm',
          },
        },
      },

      settings: {
        title: 'Settings',
      },
    },
  },

  dialogs: {
    batchMeasureSetDialog: {
      title: 'Set measures for crops',
      description: 'Existing measures for selected crops will be overriden.',
    },

    measureEditDialog: {
      title: 'Edit measures',
      fieldInfo: 'Field',
      fieldColumns: {
        field: 'Field',
        year: 'Year',
        area: 'Area',
        crop: 'Crop',
      },
    },

    cropRotationEditDialog: {
      title: 'Edit crops',
      fieldInfo: 'Field',
      crops: 'Crops',
      cropsDescription:
        'Select crops for field. If field has multiple crops at once, you can add multiple crops for each crop type (primary, secondary, winter crop), by splitting field to multiple areas.',
      sumMustMatch: 'Sum of crops area must match field area',
      fieldColumns: {
        field: 'Field',
        year: 'Year',
        area: 'Area',
      },

      inputs: {
        crop: 'Crop',
        variety: 'Variety',
        area: 'Area (are)',
      },

      cropAreaCalculation: {
        sum: 'Sum',
        difference: 'Difference',
      },
    },
    batchCropSetDialog: {
      title: 'Set crops for fields',
      description:
        'Existing crops for selected fields will be overriden (except crops that are already in use).',
    },

    trackingTripsDialog: {
      title: 'Tracking',
      downloadingDataFromTrackers:
        'Downloading data from tracking devices, please wait...',

      filter: {
        date: 'Date',
        minTaskTimeMinutes: 'Min. task time (minutes)',
      },

      buttons: {
        refresh: 'Refresh',
        refreshFromTrackingDevices: 'Refresh from tracking devices',
      },

      columns: {
        startTime: 'From',
        endTime: 'To',
        field: 'Field',
        machine: 'Machine',
        attachment: 'Attachment',
        duration: 'Duration',
      },
    },

    rkgDialog: {
      title: 'Import farm',
      description: 'Provide farm identifier to import existing farm.',
      farm: 'Farm',
      fields: 'Fields',
      importSuccess: 'Import successful',
      noResults: 'No results',

      inputs: {
        identifier: 'Identifier',
      },

      farmColumns: {
        identifier: 'Identifier',
        name: 'Name',
        address: 'Address',
        owner: 'Owner',
      },

      fieldColumns: {
        identifier: 'Identifier',
        name: 'Name',
        area: 'Area',
        type: 'Field type',
      },

      buttons: {
        import: 'Import',
      },
    },
  },

  loginPage: {
    existingUser: 'Existing user?',
    login: 'Login',
    noAccess:
      "You don't have access to this application. Please visit 'MyApplications' and request access.",
    tryAgain: 'Login failed. Please try again.',
    loginFailed: 'Login failed. Check username and password.',
  },

  admin: {
    lastLogin: 'Last login',
    welcome: 'Welcome, {user}',

    administration: 'Administration',
    menu: {
      home: 'Home',
      fieldbook: 'Fieldbook',
    },
  },

  crop: {
    types: {
      type1: 'Primary crop',
      typeShort1: 'PC',
      type2: 'Secondary crop',
      typeShort2: 'SC',
      type3: 'Winter crop',
      typeShort3: 'WC',
    },
  },

  fertilizerType: {
    mineral: 'Mineral fertilizer',
    organic: 'Organic fertilizer',
  },

  fieldEnvironmentType: {
    outdoorCultivation: 'Outdoor cultivation',
    protectedEnvironment: 'Protected environment',
  },

  stockType: {
    initialStock: 'Initial stock',
    purchase: 'Purchase',
    usage: 'Usage',
    receipt: 'Receipt',
    sale: 'Issuance/Sale',
  },

  trackingTypes: {
    trackingDevice: 'Tracking device',
    beacon: 'Beacon',
  },

  systemTaskTypes: {
    sowing: 'Sowing',
    harvesting: 'Harvesting',
    fertilizing: 'Fertilizing',
    pestControl: 'Pest control',
    other: 'Other',
  },

  cropStatusAfterTask: {
    cropStay: 'Crop stays',
    cropRemove: 'Crop removed',
    userDefined: 'Defined by user when adding task',
  },

  unitModuleLocation: {
    taskFertilizerUsage: 'Task - Fertilizer usage',
    taskPesticideUsage: 'Task - Pesticide usage',
    stockFertilizer: 'Stock control - Fertilizer stock',
    stockPesticide: 'Stock control - Pesticide stock',
    machineryPerformance: 'Machinery - Performance',
  },

  userRoles: {
    admin: 'Admin',
    user: 'User',
  },
};
