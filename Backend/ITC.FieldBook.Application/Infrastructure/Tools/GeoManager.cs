﻿using NetTopologySuite.Geometries;
using NetTopologySuite.IO;
using ProjNet.CoordinateSystems;
using ProjNet.CoordinateSystems.Transformations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITC.FieldBook.Application.Infrastructure.Tools
{
    public class GeoManager
    {
        public Geometry TransformWktSLOToWGS84(string wkt)
        {
            var reader = new WKTReader();
            var geometry = reader.Read(wkt);
            var geometryTransformed = ProjectSLOGeometryToWGS84(geometry);

            geometryTransformed.SRID = 4326;
            return geometryTransformed;
        }


        public Point TransformSLOCoordinatesToLongLat(double x, double y)
        {
            var point = new Point(x, y);
            var geometryTransformed = (Point)ProjectSLOGeometryToWGS84(point);

            geometryTransformed.SRID = 4326;
            return geometryTransformed;//(geometryTransformed.X, geometryTransformed.Y);
        }

        private Geometry ProjectSLOGeometryToWGS84(Geometry geom/*, string FromWKT, string ToWKT*/)
        {
            var SourceCoordSystem = new CoordinateSystemFactory().CreateFromWkt("PROJCS[\"Slovenia 1996 / Slovene National Grid\",GEOGCS[\"Slovenia 1996\",DATUM[\"Slovenia_Geodetic_Datum_1996\",SPHEROID[\"GRS 1980\",6378137,298.257222101,AUTHORITY[\"EPSG\",\"7019\"]],TOWGS84[0,0,0,0,0,0,0],AUTHORITY[\"EPSG\",\"6765\"]],PRIMEM[\"Greenwich\",0,AUTHORITY[\"EPSG\",\"8901\"]],UNIT[\"degree\",0.0174532925199433,AUTHORITY[\"EPSG\",\"9122\"]],AUTHORITY[\"EPSG\",\"4765\"]],PROJECTION[\"Transverse_Mercator\"],PARAMETER[\"latitude_of_origin\",0],PARAMETER[\"central_meridian\",15],PARAMETER[\"scale_factor\",0.9999],PARAMETER[\"false_easting\",500000],PARAMETER[\"false_northing\",-5000000],UNIT[\"metre\",1,AUTHORITY[\"EPSG\",\"9001\"]],AXIS[\"Easting\",EAST],AXIS[\"Northing\",NORTH],AUTHORITY[\"EPSG\",\"3794\"]]");
            //var TargetCoordSystem = new CoordinateSystemFactory().CreateFromWkt(ToWKT); ;

            var trans = new CoordinateTransformationFactory().CreateFromCoordinateSystems(SourceCoordSystem, GeographicCoordinateSystem.WGS84);

            var projGeom = Transform(geom, trans.MathTransform);
            
            return projGeom;
        }

        private Geometry Transform(Geometry geom, MathTransform transform)
        {
            geom = geom.Copy();
            geom.Apply(new MTF(transform));
            return geom;
        }

        private sealed class MTF : ICoordinateSequenceFilter
        {
            private readonly MathTransform _mathTransform;

            public MTF(MathTransform mathTransform) => _mathTransform = mathTransform;

            public bool Done => false;
            public bool GeometryChanged => true;
            public void Filter(CoordinateSequence seq, int i)
            {
                double x = seq.GetX(i);
                double y = seq.GetY(i);
                double z = seq.GetZ(i);
                _mathTransform.Transform(ref x, ref y, ref z);
                seq.SetX(i, x);
                seq.SetY(i, y);
                seq.SetZ(i, z);
            }
        }
    }
}
