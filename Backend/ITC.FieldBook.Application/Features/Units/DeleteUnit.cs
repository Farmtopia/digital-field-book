﻿using ITC.FieldBook.Application.Infrastructure.Models;
using ITC.FieldBook.Application.Infrastructure;
using MediatR;
using Microsoft.AspNetCore.DataProtection.KeyManagement;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using ITC.FieldBook.Application.Models;
using ITC.FieldBook.Application.Infrastructure.Services;
using ITC.FieldBook.Application.Infrastructure.Interfaces;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Authorization;
using ITC.FieldBook.Application.Infrastructure.Enums;

namespace ITC.FieldBook.Application.Features.Units
{
    [Authorize(Roles = EnumRole.Admin)]
    public class DeleteUnitController : ApiControllerBase
    {
        [HttpDelete("units/{unitId}")]
        public Task DeleteUnit([FromRoute] int? unitId)
        {
            return Mediator.Send(new DeleteUnitCommand { UnitId = unitId });
        }
    }

    public class DeleteUnitCommand : IRequest
    {
        [Required]
        public int? UnitId { get; set; }
    }

    internal sealed class DeleteUnitCommandHandler : IRequestHandler<DeleteUnitCommand>
    {
        private readonly DbContext _ctx;
        private readonly ICurrentUserService _currentUserService;

        public DeleteUnitCommandHandler(DbContext ctx, ICurrentUserService currentUserService)
        {
            _ctx = ctx;
            _currentUserService = currentUserService;
        }

        public Task Handle(DeleteUnitCommand request, CancellationToken cancellationToken)
        {
            var unitsRepository = new UnitsRepository(_ctx);
            var unit = unitsRepository.GetUnit(request.UnitId!.Value).Item;
            if (unit != null)
            {
                var user = _currentUserService.GetUser()!;
                _ctx.DeleteItem(unit, user);
            }

            return Task.CompletedTask;
        }
    }
}

