﻿using Dapper;
using ITC.FieldBook.Application.Features.Fertilizers;
using ITC.FieldBook.Application.Features.Pesticides;
using ITC.FieldBook.Application.Features.Stock.Fertilizers;
using ITC.FieldBook.Application.Features.Units;
using ITC.FieldBook.Application.Infrastructure;
using ITC.FieldBook.Application.Infrastructure.Models;
using ITC.FieldBook.Application.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITC.FieldBook.Application.Features.Stock
{
    public class StockRepository
    {
        private DbContext _ctx;

        public StockRepository(DbContext ctx) {
            _ctx = ctx;
        }

        public DbData<StockFertilizer> GetStockFertilizers(int farmId)
        {
            var data = _ctx.Get<StockFertilizer>($@"
                SELECT 
                    s.id,
                    s.farm_id,
                    s.task_id,
                    s.date,
                    s.fertilizer_id,
                    s.stock_type_id,
                    s.stock_type,
                    s.stock_name,
                    s.[value],
                    s.value_unit_id,
                    s.note,
                    s.task_id,
                    f.name AS fertilizer_name,
                    f.name_en AS fertilizer_name_en,
                    u.name AS value_unit
                FROM FieldBook.stock_fertilizer s
		            LEFT JOIN FieldBook.fertilizer f ON f.id = s.fertilizer_id
		            LEFT JOIN FieldBook.unit u ON u.id = s.value_unit_id
                WHERE
                    {DbContext.GetIsDeletedIsActiveCondition("s")}
                    AND s.farm_id = @farmId
                ORDER BY s.date DESC, s.id DESC", new { farmId });

            return data;
        }

        public DbItem<StockFertilizer> GetStockFertilizer(int stockFertilizerId, int farmId)
        {
            var data = _ctx.GetItem<StockFertilizer>($@"
                SELECT TOP(1)
                    s.id,
                    s.farm_id,
                    s.task_id,
                    s.date,
                    s.fertilizer_id,
                    s.stock_type_id,
                    s.stock_type,
                    s.stock_name,
                    s.[value],
                    s.value_unit_id,
                    s.note,
                    s.task_id,
                    f.name AS fertilizer_name,
                    f.name_en AS fertilizer_name_en,
                    u.name AS value_unit
                FROM FieldBook.stock_fertilizer s
		            LEFT JOIN FieldBook.fertilizer f ON f.id = s.fertilizer_id
		            LEFT JOIN FieldBook.unit u ON u.id = s.value_unit_id
                WHERE
                    {DbContext.GetIsDeletedIsActiveCondition("s")}
                    AND s.farm_id = @farmId
                    AND s.id = @stockFertilizerId
                ORDER BY s.date DESC, s.id DESC", new { stockFertilizerId, farmId });

            return data;
        }

        public void SaveStockFertilizer(StockFertilizer data, User user)
        {
            _ctx.Save(data, user: user);
        }

        public void DeleteFertilizerStockForTask(int farmId, int taskId, int userId)
        {
            using (var conn = _ctx.CreateConnection())
            {
                var now = DateTime.Now;
                conn.Execute($@"
                    UPDATE s
                    SET is_deleted = 1, is_active = 0, modified = @now, modified_by = @userId
                    FROM FieldBook.stock_fertilizer s
                    WHERE
                        {DbContext.GetIsDeletedIsActiveCondition("s")}
                        AND s.farm_id = @farmId
                        AND s.task_id = @taskId
                    ", new { farmId, taskId, userId, now });
            }
        }


        public DbData<StockPesticide> GetStockPesticides(int farmId)
        {
            var data = _ctx.Get<StockPesticide>($@"
                SELECT 
                    s.id,
                    s.farm_id,
                    s.task_id,
                    s.date,
                    s.pesticide_id,
                    s.stock_type_id,
                    s.stock_type,
                    s.stock_name,
                    s.[value],
                    s.value_unit_id,
                    s.note,
                    s.task_id,
                    p.name AS pesticide_name,
                    p.name_en AS pesticide_name_en,
                    u.name AS value_unit
                FROM FieldBook.stock_pesticide s
		            LEFT JOIN FieldBook.pesticide p ON p.id = s.pesticide_id
		            LEFT JOIN FieldBook.unit u ON u.id = s.value_unit_id
                WHERE
                    {DbContext.GetIsDeletedIsActiveCondition("s")}
                    AND s.farm_id = @farmId
                ORDER BY s.date DESC, s.id DESC", new { farmId });

            return data;
        }

        public DbItem<StockPesticide> GetStockPesticide(int stockPesticideId, int farmId)
        {
            var data = _ctx.GetItem<StockPesticide>($@"
                SELECT TOP(1)
                    s.id,
                    s.farm_id,
                    s.task_id,
                    s.date,
                    s.pesticide_id,
                    s.pesticide_name,
                    s.pesticide_brand_name,
                    s.stock_type_id,
                    s.stock_type,
                    s.stock_name,
                    s.[value],
                    s.value_unit_id,
                    s.note,
                    s.task_id,
                    p.name AS pesticide_name,
                    p.name_en AS pesticide_name_en,
                    u.name AS value_unit
                FROM FieldBook.stock_pesticide s
		            LEFT JOIN FieldBook.pesticide p ON p.id = s.pesticide_id
		            LEFT JOIN FieldBook.unit u ON u.id = s.value_unit_id
                WHERE
                    {DbContext.GetIsDeletedIsActiveCondition("s")}
                    AND s.farm_id = @farmId
                    AND s.id = @stockPesticideId
                ORDER BY s.date DESC, s.id DESC", new { stockPesticideId, farmId });

            return data;
        }

        public void SaveStockPesticide(StockPesticide data, User user) {
            _ctx.Save(data, user: user);
        }

        public void DeletePesticideStockForTask(int farmId, int taskId, int userId)
        {
            using (var conn = _ctx.CreateConnection())
            {
                var now = DateTime.Now;
                conn.Execute($@"
                    UPDATE s
                    SET is_deleted = 1, is_active = 0, modified = @now, modified_by = @userId
                    FROM FieldBook.stock_pesticide s
                    WHERE
                        {DbContext.GetIsDeletedIsActiveCondition("s")}
                        AND s.farm_id = @farmId
                        AND s.task_id = @taskId
                    ", new { farmId, taskId, userId, now });
            }
        }
    }
}
