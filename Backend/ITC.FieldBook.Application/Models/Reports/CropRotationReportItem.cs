﻿using ITC.FieldBook.Application.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITC.FieldBook.Application.Models.Reports
{
    public class CropRotationReportItem : DbEntity
    {
        public string? FieldIdentifier { get; set; }

        public string? FieldName { get; set; }

        public decimal? Area { get; set; }

        public Dictionary<int, string> YearCrops { get; set; } = new Dictionary<int, string>();
    }
}
