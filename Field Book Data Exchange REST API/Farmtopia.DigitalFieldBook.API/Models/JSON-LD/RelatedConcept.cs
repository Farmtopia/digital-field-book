﻿using System.Text.Json.Serialization;

namespace Farmtopia.DigitalFieldBook.API.Models.JSON_LD
{
    public class RelatedConcept
    {
        [JsonPropertyName("@id")]
        public string Id { get; set; }
    }
}
