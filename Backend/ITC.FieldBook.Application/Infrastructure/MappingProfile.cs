﻿using AutoMapper;
using ITC.FieldBook.Application.Infrastructure.Models;
using ITC.FieldBook.Application.Infrastructure.Tools;
using ITC.FieldBook.Application.Models;
using ITC.FieldBook.Application.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITC.FieldBook.Application.Infrastructure
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap(typeof(DbData<>), typeof(DbData<>));
            CreateMap<Field, FieldDTO>();
            CreateMap<DeviceTrip, DeviceTripDto>();
        }
    }
}
