﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITC.FieldBook.Application.Models
{
    public class Response
    {
        public Response() { }

        public Response(bool success, string? message = null) { Success = success; Message = message; }

        public bool Success { get; set; }

        public string? Message { get; set; }
    }

    public class Response<T> : Response
    {
        public T? Data { get; set; }
    }
}
