﻿using ITC.FieldBook.Application.Infrastructure.Models;
using ITC.FieldBook.Application.Infrastructure;
using MediatR;
using Microsoft.AspNetCore.DataProtection.KeyManagement;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using ITC.FieldBook.Application.Models;
using ITC.FieldBook.Application.Infrastructure.Services;
using ITC.FieldBook.Application.Infrastructure.Interfaces;
using System.ComponentModel.DataAnnotations;

namespace ITC.FieldBook.Application.Features.Documents.Types
{
    public class DeleteDocumentTypesController : ApiControllerBase
    {
        [HttpDelete("documents/document-types/{documentTypeId}")]
        public Task DeleteDocumentTypes([FromRoute] int? documentTypeId)
        {
            return Mediator.Send(new DeleteDocumentTypesCommand { DocumentTypeId = documentTypeId });
        }
    }

    public class DeleteDocumentTypesCommand : IRequest
    {
        [Required]
        public int? DocumentTypeId { get; set; }
    }

    internal sealed class DeleteDocumentTypesCommandHandler : IRequestHandler<DeleteDocumentTypesCommand>
    {
        private readonly DbContext _ctx;
        private readonly ICurrentUserService _currentUserService;

        public DeleteDocumentTypesCommandHandler(DbContext ctx, ICurrentUserService currentUserService)
        {
            _ctx = ctx;
            _currentUserService = currentUserService;
        }

        public Task Handle(DeleteDocumentTypesCommand request, CancellationToken cancellationToken)
        {
            var documentsRepository = new DocumentsRepository(_ctx);
            var documentType = documentsRepository.GetDocumentType(request.DocumentTypeId!.Value).Item;
            if (documentType != null)
            {
                var user = _currentUserService.GetUser()!;
                _ctx.DeleteItem(documentType, user);
            }

            return Task.CompletedTask;
        }
    }
}

