﻿using ITC.FieldBook.Application.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITC.FieldBook.Application.Models.Reports
{
    public class PesticidesStockReportItem : DbEntity
    {
        public int? Id { get; set; }

        public string? Name { get; set; }

        public string? NameEn { get; set; }

        public string? BrandName { get; set; }

        public decimal? Stock { get; set; }

        public string? StockUnit { get; set; }
    }
}
