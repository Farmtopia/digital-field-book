﻿namespace Farmtopia.DigitalFieldBook.API.Models
{
    public class CropEntity
    {
        public Guid? Uuid { get; set; }

        public string? Name { get; set; }

        public string? NameEn { get; set; }

        public string? AgrovocCode { get; set; }
    }

    public class Crop
    {
        public Guid? Uuid { get; set; }

        public string? Name { get; set; }

        public string? NameEn { get; set; }

        public string? AgrovocCode { get; set; }
    }
}
