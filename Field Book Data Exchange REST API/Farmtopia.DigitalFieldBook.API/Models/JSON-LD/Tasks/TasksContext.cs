﻿using Swashbuckle.AspNetCore.Annotations;
using System.Text.Json.Serialization;

namespace Farmtopia.DigitalFieldBook.API.Models.JSON_LD.Tasks
{
    public class TasksContext
    {
        [SwaggerSchema(Nullable = false)]
        [JsonPropertyName("fsm")]
        public Uri Fsm { get; set; }

        [SwaggerSchema(Nullable = false)]
        [JsonPropertyName("pcsm")]
        public Uri Pcsm { get; set; }

        [SwaggerSchema(Nullable = false)]
        [JsonPropertyName("operatedOn")]
        public TermDefinition OperatedOn { get; set; }

        [SwaggerSchema(Nullable = false)]
        [JsonPropertyName("label")]
        public string Label { get; set; }

        [SwaggerSchema(Nullable = false)]
        [JsonPropertyName("startDatetime")]
        public string StartDatetime { get; set; }

        [SwaggerSchema(Nullable = false)]
        [JsonPropertyName("endDatetime")]
        public string EndDatetime { get; set; }

        [SwaggerSchema(Nullable = false)]
        [JsonPropertyName("operationNotes")]
        public string OperationNotes { get; set; }

        [SwaggerSchema(Nullable = false)]
        [JsonPropertyName("sliceContents")]
        public TermDefinition SliceContents { get; set; }

        [SwaggerSchema(Nullable = false)]
        [JsonPropertyName("datasetSlices")]
        public TermDefinition DatasetSlices { get; set; }

        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
        [JsonPropertyName("fertilizer")]
        public TermDefinition Fertilizer { get; set; }

        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
        [JsonPropertyName("pesticide")]
        public TermDefinition Pesticide { get; set; }

        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
        [JsonPropertyName("commercialName")]
        public string CommercialName { get; set; }
    }
}
