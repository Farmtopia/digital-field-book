﻿using ITC.FieldBook.Application.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITC.FieldBook.Application.Models.DTO
{
    public class FieldDTO
    {
        public int? Id { get; set; }

        public int? FarmId { get; set; }

        public string? Name { get; set; }

        public string? Identifier { get; set; }

        public string? Displayname => Identifier == null ? null : $"{Identifier} {Name}";

        public string? Description { get; set; }

        public int? TypeId { get; set; }

        public decimal? Area { get; set; }

        public int? ImageFileId { get; set; }

        public string? TypeIdentifier { get; set; }

        public string? TypeName { get; set; }

        public string? TypeNameEn { get; set; }

        public string? TypeDisplayname => TypeIdentifier == null ? TypeName : $"{TypeIdentifier} {TypeName}";

        public string? ActiveCropIdentifier { get; set; }

        public string? ActiveCropName { get; set; }

        public string? ActiveCropDisplayname => ActiveCropIdentifier == null ? ActiveCropName : $"{ActiveCropIdentifier} {ActiveCropName}";

        public TaskEntity? LastTask { get; set; }

        public double? Latitude { get; set; }

        public double? Longitude { get; set; }

        public decimal? DistanceFromTarget { get; set; }

        public string? GeoJSON { get; set; }

        public List<TaskEntity> LastTasks { get; set; } = new List<TaskEntity>();

        public List<FieldPlot> ActivePlots { get; set; } = new List<FieldPlot>();
    }
}
