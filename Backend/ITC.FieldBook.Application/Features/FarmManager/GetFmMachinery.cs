﻿using ITC.FieldBook.Application.Infrastructure.Interfaces;
using ITC.FieldBook.Application.Infrastructure;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using ITC.FieldBook.Application.Infrastructure.Models;
using ITC.FieldBook.Application.Models;
using Microsoft.AspNetCore.Authorization;
using ITC.FieldBook.Application.Models.DTO;
using AutoMapper;
using ITC.FieldBook.Application.Features.Machinery;
using ITC.FieldBook.Application.Features.Fields;
using ITC.FieldBook.Application.Infrastructure.Tools;
using ITC.FieldBook.Application.Enums;

namespace ITC.FieldBook.Application.Features.FarmManager
{
    public class GetFmMachineryController : ApiControllerBase
    {
        [HttpGet("farm-manager/machinery")]
        public Task<DbData<FmMachinery>> GetFmMachinery([FromQuery] GetFmMachineryQuery query)
        {
            return Mediator.Send(query);
        }
    }

    public class GetFmMachineryQuery : IRequest<DbData<FmMachinery>>
    {
    }

    internal sealed class GetFmMachineryQueryHandler : IRequestHandler<GetFmMachineryQuery, DbData<FmMachinery>>
    {
        private readonly IFarmManagerService _farmManagerService;

        public GetFmMachineryQueryHandler(IFarmManagerService farmManagerService)
        {
            _farmManagerService = farmManagerService;
        }

        public async Task<DbData<FmMachinery>> Handle(GetFmMachineryQuery request, CancellationToken cancellationToken)
        {
            if(Globals.AppEnvironment != EnumAppEnvironment.DEFAULT)
                return new DbData<FmMachinery>();

            var fmMachinery = await _farmManagerService.Instance.GetMachinery();
            return new DbData<FmMachinery> { Data = fmMachinery };
        }
    }
}
