﻿using ITC.FieldBook.Application.Infrastructure.Models;
using Microsoft.AspNetCore.DataProtection.KeyManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Dapper.SqlMapper;

namespace ITC.FieldBook.Application.Models
{
    public class Measure : DbEntityWithAudit
    {
        public Measure() : base("FieldBook.measure") { }

        private int? _id;
        [DbColumn("id", IsKey = true, Identity = true)]
        public int? Id { get => _id; set { _id = value; RaisePropertyChanged(nameof(Id)); } }

        private string? _name;
        [DbColumn("name")]
        public string? Name { get => _name; set { _name = value; RaisePropertyChanged(nameof(Name)); } }

        private string? _nameEn;
        [DbColumn("name_en")]
        public string? NameEn { get => _nameEn; set { _nameEn = value; RaisePropertyChanged(nameof(NameEn)); } }

        private string? _description;
        [DbColumn("description")]
        public string? Description { get => _description; set { _description = value; RaisePropertyChanged(nameof(Description)); } }

        private string? _descriptionEn;
        [DbColumn("description_en")]
        public string? DescriptionEn { get => _descriptionEn; set { _descriptionEn = value; RaisePropertyChanged(nameof(DescriptionEn)); } }

        private string? _category;
        [DbColumn("category")]
        public string? Category { get => _category; set { _category = value; RaisePropertyChanged(nameof(Category)); } }

        private int? _activeFrom;
        [DbColumn("year_from")]
        public int? YearFrom { get => _activeFrom; set { _activeFrom = value; RaisePropertyChanged(nameof(YearFrom)); } }

        private int? _activeTo;
        [DbColumn("year_to")]
        public int? YearTo { get => _activeTo; set { _activeTo = value; RaisePropertyChanged(nameof(YearTo)); } }
    }
}
