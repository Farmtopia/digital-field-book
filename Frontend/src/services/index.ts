import axios, { InternalAxiosRequestConfig } from 'axios';
import { useAuthStore } from 'src/stores/auth.store';
import { showDeleteConfirm, showError, showSuccess } from 'src/helpers';
import {
  DbData,
  DbEntity,
  DbItem,
  ErrorDetails,
  ExportRequest,
} from 'src/models/core.models';
import { useFieldbookStore } from 'src/stores/fieldbook.store';
import { t } from 'src/boot/i18n';
import { router } from 'src/router';
import { i18n } from 'boot/i18n';

export const baseURL = '/api';

/*export const loginApi = axios.create({
  baseURL,
  withCredentials: true,
});*/

export const axiosApi = axios.create({
  baseURL,
  withCredentials: true,
  paramsSerializer: {
    indexes: true,
  },
});

const requestInterceptor = function (config: InternalAxiosRequestConfig<any>) {
  const fieldbookStore = useFieldbookStore();
  //const authStore = useAuthStore();

  config.headers['FarmId'] = fieldbookStore.farm?.id;
  //config.headers['Year'] = fieldbookStore.season?.year;
  config.headers['Accept-Language'] = i18n.global.locale.value;
  config.params = { ...config.params, lang: i18n.global.locale.value };

  /* if (authStore.jwtToken) {
    config.headers['Authorization'] = `Bearer ${authStore.jwtToken}`;
  }*/

  return config;
};

// Add a request interceptor
axiosApi.interceptors.request.use(requestInterceptor);
//loginApi.interceptors.request.use(requestInterceptor);

axiosApi.interceptors.response.use(
  function (response) {
    return response;
  },
  async function (error) {
    if (error.response.status == 400) {
      const data = <ErrorDetails>error.response.data;
      showError(
        `${
          data.errors?.length ?? 0 > 0
            ? data.errors?.join('<br />')
            : data.message
        }`
      );
    } else if (error.response.status === 401) {
      showError(t('sessionExpired'));

      const authStore = useAuthStore();
      await authStore.logout();

      router.push({ name: 'login' });
    } else if (error.response.status == 422) {
      showError(`${error.response.data?.message ?? error.message}`);
    } else {
      showError(`${error.response.data?.message ?? error.message}`);
    }
    return Promise.reject(error);
  }
);

export const getData = async function <T extends DbEntity>(
  route: string,
  params?: any
): Promise<DbData<T>> {
  const p = prepareRoute(route, params);
  if (!p.success) {
    return <DbData<T>>{ data: [] };
  }
  return (await axiosApi.get<DbData<T>>(p.route, { params: p.params })).data;
};

export const getItem = async function <T>(
  route: string,
  params?: any
): Promise<DbItem<T>> {
  const p = prepareRoute(route, params);

  if (!p.success) {
    return <DbItem<T>>{};
  }

  return (await axiosApi.get<DbItem<T>>(p.route, { params: p.params })).data;
};

export const saveItem = async function <T>(
  route: string,
  data: any,
  urlParams?: any,
  successMessage: string | boolean = t('common.saveSuccess')
) {
  const p = prepareRoute(route, urlParams);

  if (!p.success) {
    showError('Invalid route');
    return null;
  }

  const response = await axiosApi.post<T>(p.route, data);
  if (successMessage) {
    showSuccess(t('common.saveSuccess'));
  }
  return response;
};

export const deleteItem = async function (
  route: string,
  params: any,
  confirmMessage: string | boolean | undefined = t(
    'dialog.messages.confirmDelete'
  ),
  successMessage: string | boolean | undefined = t('common.saveSuccess')
) {
  const p = prepareRoute(route, params);

  if (!p.success) {
    showError('Invalid route');
    return false;
  }

  if (
    confirmMessage === false ||
    (await showDeleteConfirm(<string>confirmMessage))
  ) {
    await axiosApi.delete(p.route, { params });
    if (successMessage) {
      showSuccess(t('common.deleteSuccess'));
    }
    return true;
  }

  return false;
};

export const exportData = async function (data: ExportRequest) {
  const id = (await axiosApi.post<string | null>('export-data', data)).data;
  if (id != null) {
    const response = await axiosApi.get(`export-data/${id}`, {
      responseType: 'blob',
    });

    const headerLine = response.headers['content-disposition'];
    const startFileNameIndex = headerLine.indexOf('"') + 1;
    const endFileNameIndex = headerLine.lastIndexOf('"');
    const filename = headerLine.substring(startFileNameIndex, endFileNameIndex);
    const url = window.URL.createObjectURL(new Blob([response.data]));
    const link = document.createElement('a');

    link.href = url;
    link.setAttribute('download', filename);
    document.body.appendChild(link);
    link.click();
  }
};

export const getFile = async function (route: string, params?: any) {
  const response = await axiosApi.get(route, {
    responseType: 'blob',
    params,
  });

  const disposition = response.headers['content-disposition'];
  let filename = 'file';

  if (disposition && disposition.indexOf('attachment') !== -1) {
    const filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
    const matches = filenameRegex.exec(disposition);
    if (matches != null && matches[1]) {
      filename = matches[1].replace(/['"]/g, '');
    }
  }

  const url = window.URL.createObjectURL(new Blob([response.data]));
  const link = document.createElement('a');

  link.href = url;
  link.setAttribute('download', filename);
  document.body.appendChild(link);
  link.click();
};

export const prepareRoute = function (route: string, params?: any) {
  params = { ...params }; // clone
  const regex = /{([^}]+)}/g;
  const paramMatches = [];
  let success = true;
  let match;
  while ((match = regex.exec(route))) {
    paramMatches.push(match[1]);
  }

  if (paramMatches.length > 0 && params) {
    paramMatches.forEach((x) => {
      const value = params[x];
      if (value) {
        route = route.replaceAll(`{${x}}`, value);
        delete params[x];
      } else {
        success = false;
      }
    });
  }

  return { route, params, success };
};

export default {
  //loginApi,
  axiosApi,
  getData,
  getItem,
  getFile,
  saveItem,
  deleteItem,
  exportData,
};
