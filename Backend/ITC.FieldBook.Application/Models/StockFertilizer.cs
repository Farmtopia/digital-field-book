﻿using ITC.FieldBook.Application.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITC.FieldBook.Application.Models
{
    public class StockFertilizer : DbEntityWithAudit
    {
        public StockFertilizer() : base("FieldBook.stock_fertilizer") { }
                
        private int? _id;
        [DbColumn("id", IsKey = true, Identity = true)]
        public int? Id { get => _id; set { _id = value; RaisePropertyChanged(nameof(Id)); } }

        private int? _farmId;
        [DbColumn("farm_id")]
        public int? FarmId { get => _farmId; set { _farmId = value; RaisePropertyChanged(nameof(FarmId)); } }

        private int? _taskId;
        [DbColumn("task_id")]
        public int? TaskId { get => _taskId; set { _taskId = value; RaisePropertyChanged(nameof(TaskId)); } }

        private DateTime? _date;
        [DbColumn("date")]
        public DateTime? Date { get => _date; set { _date = value; RaisePropertyChanged(nameof(Date)); } }

        private int? _fertilizerId;
        [DbColumn("fertilizer_id")]
        public int? FertilizerId { get => _fertilizerId; set { _fertilizerId = value; RaisePropertyChanged(nameof(FertilizerId)); } }

        private int? _stockTypeId;
        [DbColumn("stock_type_id")]
        public int? StockTypeId { get => _stockTypeId; set { _stockTypeId = value; RaisePropertyChanged(nameof(StockTypeId)); } }

        private int? _stockType;
        [DbColumn("stock_type")]
        public int? StockType { get => _stockType; set { _stockType = value; RaisePropertyChanged(nameof(StockType)); } }

        private string? _stockName;
        [DbColumn("stock_name")]
        public string? StockName { get => _stockName; set { _stockName = value; RaisePropertyChanged(nameof(StockName)); } }

        private decimal? _value;
        [DbColumn("value")]
        public decimal? Value { get => _value; set { _value = value; RaisePropertyChanged(nameof(Value)); } }

        private int? _valueUnitId;
        [DbColumn("value_unit_id")]
        public int? ValueUnitId { get => _valueUnitId; set { _valueUnitId = value; RaisePropertyChanged(nameof(ValueUnitId)); } }

        private string? _valueUnit;
        [DbColumn("value_unit")]
        public string? ValueUnit { get => _valueUnit; set { _valueUnit = value; RaisePropertyChanged(nameof(ValueUnit)); } }

        private string? _note;
        [DbColumn("note")]
        public string? Note { get => _note; set { _note = value; RaisePropertyChanged(nameof(Note)); } }
        
        public string? FertilizerName { get; set; }

        public string? FertilizerNameEn { get; set; }
    }
}
