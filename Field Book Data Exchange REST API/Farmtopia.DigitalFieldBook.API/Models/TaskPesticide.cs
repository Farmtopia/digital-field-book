﻿namespace Farmtopia.DigitalFieldBook.API.Models
{
    public class TaskPesticideEntity
    {
        public Guid? PesticideUuid { get; set; }

        public string? PesticideName { get; set; }

        public string? PesticideNameEn { get; set; }

        public decimal? AmountUsed { get; set; }

        public string? AmountUsedUnit { get; set; }

        public string? AmountUsedUnitDescription { get; set; }

        public string? AmountUsedUnitDescriptionEn { get; set; }
    }

    public class TaskPesticide
    {
        public Guid? PesticideUuid { get; set; }

        public string? PesticideName { get; set; }

        public string? PesticideNameEn { get; set; }

        public decimal? AmountUsed { get; set; }

        public string? AmountUsedUnit { get; set; }

        public string? AmountUsedUnitDescription { get; set; }

        public string? AmountUsedUnitDescriptionEn { get; set; }
    }
}
