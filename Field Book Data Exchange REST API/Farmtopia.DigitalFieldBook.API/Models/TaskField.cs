﻿using System.Text.Json.Serialization;

namespace Farmtopia.DigitalFieldBook.API.Models
{
    public class TaskFieldEntity
    {
        public Guid? FieldUuid { get; set; }

        public string? FieldIdentifier { get; set; }

        public string? FieldName { get; set; }

        public decimal? FieldAreaM2 { get; set; }

        public Guid? CropUuid { get; set; }
    }

    public class TaskField
    {
        public Guid? FieldUuid { get; set; }

        public string? FieldIdentifier { get; set; }

        public string? FieldName { get; set; }

        public decimal? FieldAreaM2 { get; set; }

        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
        public Crop? Crop { get; set; }
    }
}
