﻿using ITC.FieldBook.Application.Infrastructure.Models;
using ITC.FieldBook.Application.Infrastructure;
using MediatR;
using Microsoft.AspNetCore.DataProtection.KeyManagement;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using ITC.FieldBook.Application.Models;
using ITC.FieldBook.Application.Infrastructure.Services;
using ITC.FieldBook.Application.Infrastructure.Interfaces;
using System.ComponentModel.DataAnnotations;

namespace ITC.FieldBook.Application.Features.Fields
{
    public class DeleteFieldController : ApiControllerBase
    {
        [HttpDelete("fields/{fieldId}")]
        public Task DeleteField([FromRoute] int? fieldId)
        {
            return Mediator.Send(new DeleteFieldCommand { FieldId = fieldId });
        }
    }

    public class DeleteFieldCommand : IRequest
    {
        [Required]
        public int? FieldId { get; set; }
    }

    internal sealed class DeleteFieldCommandHandler : IRequestHandler<DeleteFieldCommand>
    {
        private readonly DbContext _ctx;
        private readonly ICurrentUserService _currentUserService;

        public DeleteFieldCommandHandler(DbContext ctx, ICurrentUserService currentUserService)
        {
            _ctx = ctx;
            _currentUserService = currentUserService;
        }

        public Task Handle(DeleteFieldCommand request, CancellationToken cancellationToken)
        {
            var fieldsRepository = new FieldsRepository(_ctx);
            var field = fieldsRepository.GetField(_currentUserService.FarmId!.Value, request.FieldId!.Value).Item;
            if (field != null)
            {
                var user = _currentUserService.GetUser()!;
                _ctx.DeleteItem(field, user);
            }

            return Task.CompletedTask;
        }
    }
}

