﻿using Swashbuckle.AspNetCore.Annotations;
using System.Text.Json.Serialization;

namespace Farmtopia.DigitalFieldBook.API.Models.JSON_LD.Fields
{
    public class FieldsSliceContent : SliceContentBase
    {
        [JsonPropertyName("name")]
        public string Name { get; set; }

        [JsonPropertyName("nationalIdentifier")]
        public string NationalIdentifier { get; set; }

        [JsonPropertyName("hasArea")]
        public HasArea HasArea { get; set; }

        [JsonPropertyName("geometry")]
        public FieldGeometry Geometry { get; set; }

        [JsonPropertyName("contains")]
        public List<Field> Contains { get; set; }
    }
}
