﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITC.FieldBook.Application.Models.DTO
{
    public class CropRotationSumDto
    {
        public bool HasCropRotation { get; set; }

        public List<CropRotationSumCropDto> Crops { get; set; } = new List<CropRotationSumCropDto>();

        public Dictionary<int, CropRotationYearSumDto> YearSum { get; set; } = new Dictionary<int, CropRotationYearSumDto>();

        public List<int> Years { get; set; } = new List<int>();
    }

    public class CropRotationYearSumDto
    {
        public decimal SumAll { get; set; }

        public decimal SumAssigned { get; set; }
    }

    public class CropRotationSumCropDto
    {
        public int? CropId { get; set; }

        public string? CropIdentifier { get; set; }

        public string? CropName { get; set; }

        public string? CropNameEn { get; set; }

        public string? HexColor { get; set; }

        public Dictionary<int, CropRotationCropYearSumDto> Years { get; set; } = new Dictionary<int, CropRotationCropYearSumDto>();
    }

    public class CropRotationCropYearSumDto
    {
        public int Year { get; set; }

        public decimal Sum { get; set; } = 0;

        public decimal Percentage { get; set; } = 0;

    }
}
