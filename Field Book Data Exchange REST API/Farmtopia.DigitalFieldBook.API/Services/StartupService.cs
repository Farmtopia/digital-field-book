﻿using Farmtopia.DigitalFieldBook.API.Models.Providers;
using Farmtopia.DigitalFieldBook.API.Repository;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using static Dapper.SqlMapper;

namespace Farmtopia.DigitalFieldBook.API.Services
{
    public class StartupService : IHostedService
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly IConfiguration _configuration;
        private readonly DbContext _ctx;

        public StartupService(IServiceProvider serviceProvider, IConfiguration configuration, DbContext ctx)
        {
            _serviceProvider = serviceProvider;
            _configuration = configuration;
            _ctx = ctx;
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            var providerRepository = new ProviderRepository(_ctx);

            // Initialize settings
            var providerId = _configuration.GetValue<int?>("ProviderId");
            if (providerId != null)
            {
                Globals.ProviderId = providerId;

                // Check if provider has secret key, if not, request from directory service
                // TODO: for now manually added. Should this even be part of API? Because then we need to store user credentials in db for this API (to get auth token).
                //if (
                //    settings != null &&
                //    !string.IsNullOrEmpty(settings.DirectoryUrl) &&
                //    string.IsNullOrEmpty(settings.SecretKey))
                //{
                //    // Get shared secret key from directory service
                //    var dirService = _serviceProvider.GetService<DirectoryService>()!;
                //    var secretKey = await dirService.GetSecretKey();

                //    if (!string.IsNullOrEmpty(secretKey))
                //    {
                //        settings.SecretKey = secretKey;
                //        providerRepository.UpdateSecretKey(providerId!.Value, secretKey);
                //    }

                //}
            }
        }


        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }
    }
}
