﻿using AutoMapper;
using Azure;
using Farmtopia.DigitalFieldBook.API.Filters;
using Farmtopia.DigitalFieldBook.API.Models;
using Farmtopia.DigitalFieldBook.API.Models.JSON_LD;
using Farmtopia.DigitalFieldBook.API.Models.JSON_LD.Farms;
using Farmtopia.DigitalFieldBook.API.Models.JSON_LD.Fields;
using Farmtopia.DigitalFieldBook.API.Models.JSON_LD.Tasks;
using Farmtopia.DigitalFieldBook.API.Models.Providers;
using Farmtopia.DigitalFieldBook.API.Models.Requests;
using Farmtopia.DigitalFieldBook.API.Repository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualBasic.FileIO;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Data;
using System.Runtime.CompilerServices;
using System.Security.Claims;
using System.Text.Json;
using System.Text.Json.Nodes;
using System.Text.Json.Serialization;
using System.Xml.Linq;

namespace Farmtopia.DigitalFieldBook.API.Controllers
{
#if !DEBUG
    [DirectoryAuthorization]
#endif
    [Tags("Farmtopia Digital FieldBook API")]
    [ApiController]
    public class FieldbookController : ControllerBase
    {
        private readonly string Token;

        private readonly string FsmUrl;
        private readonly string PcsmUrl;
        private readonly string GeosparqlUrl;
        private readonly string QudtUrl;
        private readonly string OboUrl;

        private DbContext _ctx;
        private IMapper _mapper;

        public FieldbookController(DbContext ctx, IConfiguration configuration, IMapper mapper)
        {
            _ctx = ctx;
            _mapper = mapper;

            Token = configuration["Token"];

            FsmUrl = configuration["Context:Fsm"];
            PcsmUrl = configuration["Context:Pcsm"];
            GeosparqlUrl = configuration["Context:Geosparql"];
            QudtUrl = configuration["Context:Qudt"];
            OboUrl = configuration["Context:Obo"];
        }

        [AllowAnonymous]
        [HttpPost("administration/shared_secret_key")]
        [SwaggerOperation(Description = "Set shared secret key", Tags = new[] { "Administration" })]
        public IActionResult SetSharedSecretKey([FromBody] SetSharedSecretKeyDto request)
        {
            if(!(request.Token?.Equals(Token) ?? false))
                return Unauthorized(new { status = "invalid token" });

            var providerId = Globals.ProviderId!.Value;
            var providerRepository = new ProviderRepository(_ctx);
            providerRepository.UpdateSecretKey(providerId, request.Key ?? "");

            return Ok(new { status = "success" });
        }

        [HttpPut("directory/subscription/dataset_id/{dataset_id}")]
        [SwaggerOperation(Description = "Subscribe a user to a Dataset", Tags = new[] { "Directory" })]
        public IActionResult SubscribeToDataset(int dataset_id)
        {
            var user = HttpContext.User;
            var providerId = Globals.ProviderId!.Value;

            // Claims containing subscribing user info, received from Directory API
            var datasetId = int.Parse(user.FindFirstValue("dataset"));
            var username = user.FindFirstValue("consumer");

            var providerRepository = new ProviderRepository(_ctx);
            var providerUser = providerRepository.GetUser(providerId, datasetId, username);

            if (providerUser == null)
                providerRepository.AddUser(providerId, datasetId, username);
            else if (providerUser.Active)
                providerRepository.UpdateUser(providerId, datasetId, username);
            else
                return Unauthorized();

            return Ok(new { status = "success" });
        }

        /// <summary>
        /// Get farms
        /// </summary>
        /// <returns>List of farms in JSON-LD format</returns>
        [HttpGet("farms")]
        [SwaggerOperation(Description = "Get farms")]
        public FarmsRoot GetFarmsJsonLd()
        {
            var repo = new FieldbookRepository(_ctx);
            var farms = repo.GetFarms();

            var context = new FarmsContext
            {
                Fsm = new Uri(FsmUrl),
                Pcsm = new Uri(PcsmUrl),
                Geosparql = new Uri(GeosparqlUrl),
                Centroid = new TermDefinition { Id = "fsm:centroid", Type = "@id" },
                GeoJson = new TermDefinition { Id = "geosparql:asGeoJSON", Type = "@json" },
                IsEcoFarm = "fsm:isEcoFarm",
                SliceContents = new TermDefinition { Id = "fsm:sliceContents", Type = "@json", Container = "@set" },
                DatasetSlices = new TermDefinition { Id = "fsm:datasetSlices", Type = "@json", Container = "@set" }
            };

            var sliceContents = new List<FarmsSliceContent>();

            foreach (var f in farms)
            {
                // Create the farm complex
                var farmComplex = new FarmsSliceContent
                {
                    Id = f.Uuid!.Value.ToString(),
                    Type = "fsm:FarmComplex",
                    Centroid = new Centroid
                    {
                        Id = $"{f.Uuid}_GeometryCentroid",
                        Type = "geosparql:Geometry",
                        GeoJson = f.Latitude != null && f.Longitude != null ? new CentroidGeoJson
                        {
                            Type = "Point",
                            Coordinates = new List<decimal> { f.Latitude ?? 0, f.Longitude ?? 0 }
                        } : null
                    },
                    IsEcoFarm = f.EcoFarm
                };

                // Create the graph
                sliceContents.Add(farmComplex);
            }

            // Create the final JSON-LD document
            var jsonLdDocument = new FarmsRoot
            {
                Context = context,
                Graph = new List<FarmsGraph>
                {
                   new FarmsGraph {
                       Id = "ITC_farms_serviceResource_representation_instance_43728",
                       Type = "fsm:Dataset",
                       DatasetSlices = new List<FarmsDatasetSlice>
                       {
                           new FarmsDatasetSlice
                           {
                               Id = "FarmComplexesSlice_1",
                               Type = "fsm:Slice",
                               SliceContents = sliceContents
                           }
                       }
                   }
                }
            };

            return jsonLdDocument;
        }

        /// <summary>
        /// Get fields
        /// </summary>
        /// <param name="farmUuid">Farm UUID</param>
        /// <returns>List of fields for farm in JSON-LD</returns>
        [HttpGet("fields")]
        [SwaggerOperation(Description = "Get fields")]
        public FieldsRoot GetFieldsJsonLd(Guid farmUuid)
        {
            var repo = new FieldbookRepository(_ctx);
            var fields = repo.GetFields(farmUuid);
            var allCrops = repo.GetCrops();

            var context = new FieldsContext
            {
                Fsm = new Uri(FsmUrl),
                Pcsm = new Uri(PcsmUrl),
                Geosparql = new Uri(GeosparqlUrl),
                Qudt = new Uri(QudtUrl),
                Obo = new Uri(OboUrl),
                Contains = new TermDefinition { Id = "fsm:contains", Type = "@id", Container = "@set" },
                Geometry = new TermDefinition { Id = "geosparql:hasGeometry", Type = "@id" },
                GeoJson = new TermDefinition { Id = "geosparql:asGeoJSON", Type = "@json" },
                HasArea = new TermDefinition { Id = "fsm:hasArea", Type = "@id" },
                UnitOfMeasure = new TermDefinition { Id = "fsm:unitOfMeasure", Type = "@id" },
                LandType = "fsm:landType",
                Name = "saref4agri:hasName",
                NationalIdentifier = "fsm:nationalIdentifier",
                NumericValue = "qudt:numericValue",
                Label = "fsm:label",
                CropVariety = "pcsm:hasCropVariety",
                AgrovocCode = "fsm:agrovocCode",
                HasPlantDate = "pcsm:hasPlantDate",
                HasHarvestDate = "pcsm:hasHarvestDate",
                SliceContents = new TermDefinition { Id = "fsm:sliceContents", Type = "@json", Container = "@set" },
                DatasetSlices = new TermDefinition { Id = "fsm:datasetSlices", Type = "@json", Container = "@set" }
            };

            var sliceContents = new List<FieldsSliceContent>();

            foreach (var f in fields)
            {
                //// Create the geometry
                //var geometry = new JsonObject
                //{
                //    { "@id", $"{f.Uuid}_FieldGeometry" },
                //    { "@type", "geosparql:Geometry" },
                //    { "geoJson", !string.IsNullOrEmpty(f.GeoJSON) ? JsonNode.Parse(f.GeoJSON) : null
                //    }
                //};

                //// Create the area
                //var area = new JsonObject
                //{
                //    { "@id", $"{f.Uuid}_FieldArea" },
                //    { "@type", "qudt:QuantityValue" },
                //    { "numericValue", f.AreaM2 },
                //    { "unitOfMeasure", "unit:M2" }
                //};

                // Create the farm site
                var farmSite = new FieldsSliceContent
                {
                    Id = f.Uuid!.Value.ToString(),
                    Type = "fsm:FarmSite",
                    Name = f.Name,
                    NationalIdentifier = f.Identifier,
                    HasArea = new HasArea
                    {
                        Id = $"{f.Uuid}_FieldArea",
                        Type = "qudt:QuantityValue",
                        NumericValue = f.AreaM2,
                        UnitOfMeasure = "unit:M2"
                    },
                    Geometry = new FieldGeometry
                    {
                        Id = $"{f.Uuid}_FieldGeometry",
                        Type = "geosparql:Geometry",
                        GeoJson = !string.IsNullOrEmpty(f.GeoJSON) ? JsonNode.Parse(f.GeoJSON) : null
                    }
                };

                if (f.FieldTypeId != null)
                {
                    var fieldTypes = repo.GetFieldTypes();
                    var fieldType = fieldTypes.FirstOrDefault(x => x.Id?.Equals(f.FieldTypeId.Value) ?? false)!;
                    if (fieldType != null)
                    {
                        var plots = new List<Models.JSON_LD.Field>();

                        var fieldCrops = repo.GetFieldCrops(f.Uuid!.Value);
                        if (fieldCrops != null && fieldCrops.Any())
                        {
                            foreach (var fc in fieldCrops)
                            {
                                var crop = allCrops.FirstOrDefault(x => x.Uuid.Equals(fc.CropUuid!.Value))!;

                                // Create the plot
                                var plotObject = new Models.JSON_LD.Field
                                {
                                    Id = fc.Uuid!.Value.ToString(),
                                    Type = "fsm:Plot",
                                    LandType = new LandType
                                    {
                                        Id = fieldType.Uuid!.Value.ToString(),
                                        Type = "obo:ENVO_01001177",
                                        Label = fieldType.NameEn
                                    },
                                    Contains = new List<Models.JSON_LD.Crop>
                                    {
                                        new Models.JSON_LD.Crop
                                        {
                                            Id = crop.Uuid!.Value.ToString(),
                                            Type = "pcsm:Crop",
                                            CropVariety = crop.NameEn,
                                            AgrovocCode = crop.AgrovocCode,
                                            HasPlantDate = fc.SowingDate,
                                            HasHarvestDate = fc.HarvestDate
                                        }
                                    }
                                };

                                plots.Add(plotObject);
                            }
                        }

                        farmSite.Contains = plots;
                    }
                }
                sliceContents.Add(farmSite);
            }

            // Create the final JSON-LD document
            var jsonLdDocument = new FieldsRoot
            {
                Context = context,
                Graph = new List<FieldsGraph>
                {
                   new FieldsGraph {
                       Id = "ITC_fields_serviceResource_representation_instance_23328",
                       Type = "fsm:Dataset",
                       DatasetSlices = new List<FieldsDatasetSlice>
                       {
                           new FieldsDatasetSlice
                           {
                               Id = "FarmSitesSlice_1",
                               Type = "fsm:Slice",
                               SliceContents = sliceContents
                           }
                       }
                   }
                }
            };

            return jsonLdDocument;
        }

        /// <summary>
        /// Get tasks
        /// </summary>
        /// <param name="farmUuid">Farm UUID</param>
        /// <param name="dateFrom">Date from</param>
        /// <param name="dateTo">Date to</param>
        /// <returns></returns>
        [HttpGet("calendar")]
        [SwaggerOperation(Description = "Get tasks")]
        public TasksRoot GetTasksJsonLd(Guid farmUuid, DateTime? dateFrom, DateTime? dateTo)
        {
            var repo = new FieldbookRepository(_ctx);
            var tasks = repo.GetTasks(farmUuid, dateFrom, dateTo);
            var taskTypes = repo.GetTaskTypes();
            var crops = repo.GetCrops();

            var context = new TasksContext
            {
                Fsm = new Uri(FsmUrl),
                Pcsm = new Uri(PcsmUrl),
                OperatedOn = new TermDefinition { Id = "pcsm:isOperatedOn", Type = "@id" },
                Label = "fsm:label",
                StartDatetime = "pcsm:hasStartDatetime",
                EndDatetime = "pcsm:hasEndDatetime",
                OperationNotes = "fsm:operationNotes",
                SliceContents = new TermDefinition { Id = "fsm:sliceContents", Type = "@json", Container = "@set" },
                DatasetSlices = new TermDefinition { Id = "fsm:datasetSlices", Type = "@json", Container = "@set" }
            };

            var sliceContents = new List<TasksSliceContent>();
            var usesFertilizer = false;
            var usesPesticide = false;

            foreach (var task in tasks)
            {
                var taskType = taskTypes.FirstOrDefault(x => x.Id?.Equals(task.TaskTypeId) ?? false);
                var fields = repo.GetTaskFields(task.Uuid!.Value).ToList();

                foreach (var field in fields)
                {
                    // Create the operation
                    var operation = new TasksSliceContent
                    {
                        Id = task.Uuid!.Value.ToString(),
                        Type = !string.IsNullOrEmpty(taskType?.PloutosType) ? $"pcsm:{taskType.PloutosType}" : "",
                        Label = taskType?.NameEn ?? taskType?.Name ?? "",
                        StartDatetime = task.DateFrom,
                        EndDatetime = task.DateTo,
                        OperationNotes = task.Note,
                        OperatedOn = new OperatedOn
                        {
                            Id = field.FieldUuid!.Value.ToString()
                        }
                    };

                    // Get fertilizer for task
                    var fertilizer = repo.GetTaskFertilizers(task.Uuid!.Value).FirstOrDefault();
                    if (fertilizer != null)
                    {
                        usesFertilizer = true;
                        operation.Fertilizer = new Fertilizer
                        {
                            Id = fertilizer.FertilizerUuid!.Value.ToString(),
                            Type = "pcsm:Fertilizer",
                            CommercialName = fertilizer.FertilizerNameEn ?? fertilizer.FertilizerName ?? "",
                        };

                        if (fertilizer.RatioN != null || fertilizer.RatioP != null || fertilizer.RatioK != null)
                        {
                            operation.Fertilizer.ProductRatio = new List<ProductRatio>();
                            if (fertilizer.RatioN != null)
                            {
                                operation.Fertilizer.ProductRatio.Add(new ProductRatio
                                {
                                    Id = $"{fertilizer.FertilizerUuid!.Value.ToString()}_NitrogenConcentration",
                                    Type = "qudt:QuantityValue",
                                    RelatedConcept = new RelatedConcept { Id = "obo:CHEBI_25555" },
                                    NumericValue = fertilizer.RatioN?.ToString(),
                                    UnitOfMeasure = new UnitOfMeasure { Id = "unit:PERCENT" }
                                });
                            }
                            if (fertilizer.RatioP != null)
                            {
                                operation.Fertilizer.ProductRatio.Add(new ProductRatio
                                {
                                    Id = $"{fertilizer.FertilizerUuid!.Value.ToString()}_PhosphorusConcentration",
                                    Type = "qudt:QuantityValue",
                                    RelatedConcept = new RelatedConcept { Id = "obo:CHEBI_28659" },
                                    NumericValue = fertilizer.RatioP?.ToString(),
                                    UnitOfMeasure = new UnitOfMeasure { Id = "unit:PERCENT" }
                                });
                            }
                            if (fertilizer.RatioK != null)
                            {
                                operation.Fertilizer.ProductRatio.Add(new ProductRatio
                                {
                                    Id = $"{fertilizer.FertilizerUuid!.Value.ToString()}_PotassiumConcentration",
                                    Type = "qudt:QuantityValue",
                                    RelatedConcept = new RelatedConcept { Id = "obo:CHEBI_26216" },
                                    NumericValue = fertilizer.RatioK?.ToString(),
                                    UnitOfMeasure = new UnitOfMeasure { Id = "unit:PERCENT" }
                                });
                            }
                        }

                        if (fertilizer.AmountUsed != null)
                        {
                            var qudtAmount = ConvertToQudtUnit(fertilizer.AmountUsed, fertilizer.AmountUsedUnit);
                            if (qudtAmount.QudtUnit != null && qudtAmount.Value != null)
                            {
                                operation.ResourceQuantityUsed = new ResourceQuantityUsed
                                {
                                    Id = $"{fertilizer.FertilizerUuid!.Value.ToString()}_Quantity",
                                    Type = "qudt:QuantityValue",
                                    NumericValue = qudtAmount.Value?.ToString("0.00", System.Globalization.CultureInfo.InvariantCulture),
                                    UnitOfMeasure = new UnitOfMeasure
                                    {
                                        Id = qudtAmount.QudtUnit
                                    }
                                };
                            }
                        }
                    }

                    // Get pesticide for task
                    var pesticide = repo.GetTaskPesticides(task.Uuid!.Value).FirstOrDefault();
                    if (pesticide != null)
                    {
                        usesPesticide = true;
                        operation.Pesticide = new Pesticide
                        {
                            Id = pesticide.PesticideUuid!.Value.ToString(),
                            Type = "pcsm:Pesticide",
                            CommercialName = pesticide.PesticideNameEn ?? pesticide.PesticideName ?? ""
                        };
                    }

                    // Add the operation to the graph
                    sliceContents.Add(operation);
                }
            }

            // Specific contexts
            if (usesFertilizer || usesFertilizer)
            {
                if (usesFertilizer)
                    context.Fertilizer = new TermDefinition { Id = "pcsm:usesFertilizer", Type = "@id" };
                if (usesPesticide)
                    context.Pesticide = new TermDefinition { Id = "pcsm:usesPesticide", Type = "@id" };
                context.CommercialName = "pcsm:hasCommercialName";
            }

            // Create the final JSON-LD document
            var jsonLdDocument = new TasksRoot
            {
                Context = context,
                Graph = new List<TasksGraph>
                {
                   new TasksGraph {
                       Id = "ITC_fields_serviceResource_representation_instance_23328",
                       Type = "fsm:Dataset",
                       DatasetSlices = new List<TasksDatasetSlice>
                       {
                           new TasksDatasetSlice
                           {
                               Id = "TasksSlice_1",
                               Type = "fsm:Slice",
                               SliceContents = sliceContents
                           }
                       }
                   }
                }
            };

            return jsonLdDocument;
        }

        private UsageCalculationResult ConvertToQudtUnit(decimal? value, string? unit)
        {
            if(value == null)
            {
                return new UsageCalculationResult
                {
                    Value = null,
                    QudtUnit = null
                };
            }
            if(unit == "kg")
            {
                return new UsageCalculationResult
                {
                    Value = value,
                    QudtUnit = "unit:KiloGM"
                };
            }
            else if (unit == "l")
            {
                return new UsageCalculationResult
                {
                    Value = value,
                    QudtUnit = "unit:L"
                };
            }
            if (unit == "m3")
            {
                return new UsageCalculationResult
                {
                    Value = value,
                    QudtUnit = "unit:M3"
                };
            }
            else if (unit == "kg/ha")
            {
                return new UsageCalculationResult
                {
                    Value = value,
                    QudtUnit = "unit:KiloGM-PER-HA"
                };
            }
            else if (unit == "l/ha")
            {
                return new UsageCalculationResult
                {
                    Value = value,
                    QudtUnit = "unit:L-PER-HA"
                };
            }
            else
            {
                return new UsageCalculationResult
                {
                    Value = value,
                    QudtUnit = unit
                };
            }
        }

        #region OLD - Regular (non JSON-LD) format

        /////// <summary>
        /////// Get farms
        /////// </summary>
        /////// <returns>List of farms</returns>
        ////[HttpGet("farms")]
        ////[SwaggerOperation(Description = "Get farms")]
        ////public IEnumerable<Farm> GetFarms()
        ////{
        ////    var repo = new FieldbookRepository(_ctx);
        ////    var data = _mapper.Map<IEnumerable<Farm>>(repo.GetFarms());

        ////    return data;
        ////}

        ///// <summary>
        ///// Get fields
        ///// </summary>
        ///// <param name="farmUuid">Farm UUID</param>
        ///// <returns>List of fields for farm</returns>
        //[HttpGet("calendar")]
        //[SwaggerOperation(Description = "Get calendar (OBSOLETE)")]
        //public List<Models.Field> GetCalendar([FromRoute] Guid farmUuid)
        //{
        //    var repo = new FieldbookRepository(_ctx);
        //    var fields = repo.GetFields(farmUuid);
        //    var crops = repo.GetCrops();
        //    var data = _mapper.Map<List<Models.Field>>(fields);

        //    if (data.Count > 0)
        //    {
        //        var fieldTypes = repo.GetFieldTypes();
        //        if (fieldTypes.Count() > 0)
        //        {
        //            foreach (var d in data)
        //            {
        //                // Map field types
        //                if (d.FieldType != null && d.FieldType.Uuid != null)
        //                {
        //                    d.FieldType = _mapper.Map<Models.FieldType>(fieldTypes.FirstOrDefault(x => x.Uuid?.Equals(d.FieldType.Uuid.Value) ?? false));
        //                }

        //                // Map field crops
        //                var fieldCrops = repo.GetFieldCrops(d.Uuid!.Value);
        //                if (fieldCrops != null && fieldCrops.Any())
        //                {
        //                    d.Crops = _mapper.Map<List<FieldCrop>>(fieldCrops).OrderBy(x => x.Year).ThenBy(x => x.CropType).ToList();

        //                    foreach (var c in d.Crops)
        //                    {
        //                        if (c.Crop != null && c.Crop.Uuid != null)
        //                        {
        //                            c.Crop = _mapper.Map<Models.Crop>(crops.FirstOrDefault(x => x.Uuid.Equals(c.Crop.Uuid.Value)));
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //    }

        //    return data;
        //}

        ///// <summary>
        ///// Get tasks
        ///// </summary>
        ///// <param name="farmUuid">Farm UUID</param>
        ///// <param name="dateFrom">Date from</param>
        ///// <param name="dateTo">Date to</param>
        ///// <returns></returns>
        //[HttpGet("farms/{farmUuid}/tasks")]
        //[SwaggerOperation(Description = "Get tasks")]
        //public IEnumerable<Models.Task> GetTasks([FromRoute] Guid farmUuid, DateTime? dateFrom, DateTime? dateTo)
        //{
        //    var repo = new FieldbookRepository(_ctx);
        //    var tasks = repo.GetTasks(farmUuid, dateFrom, dateTo);
        //    var crops = repo.GetCrops();
        //    var data = _mapper.Map<List<Models.Task>>(tasks);

        //    if (data.Count > 0)
        //    {
        //        var taskTypes = repo.GetTaskTypes();
        //        if (taskTypes.Count() > 0)
        //        {
        //            foreach (var d in data)
        //            {
        //                // Map field types
        //                if (d.TaskType != null && d.TaskType.Id != null)
        //                    d.TaskType = _mapper.Map<TaskType>(taskTypes.FirstOrDefault(x => x.Id?.Equals(d.TaskType.Id.Value) ?? false));

        //                var fields = repo.GetTaskFields(d.Uuid!.Value).ToList();
        //                d.Fields = fields != null ? _mapper.Map<List<TaskField>>(fields) : new List<TaskField>();

        //                // Map crop
        //                foreach (var f in d.Fields)
        //                {
        //                    if (f.Crop != null && f.Crop.Uuid != null)
        //                        f.Crop = _mapper.Map<Crop>(crops.FirstOrDefault(x => x.Uuid.Equals(f.Crop.Uuid.Value)));
        //                }

        //                // Get fertilizers for task
        //                var fertilizers = repo.GetTaskFertilizers(d.Uuid!.Value).ToList();
        //                if (fertilizers.Any())
        //                    d.Fertilizers = _mapper.Map<List<TaskFertilizer>>(fertilizers);

        //                // Get pesticides for task
        //                var pesticides = repo.GetTaskPesticides(d.Uuid!.Value).ToList();
        //                if (pesticides.Any())
        //                    d.Pesticides = _mapper.Map<List<TaskPesticide>>(pesticides);
        //            }
        //        }
        //    }

        //    return data;
        //}

        #endregion
    }
}
