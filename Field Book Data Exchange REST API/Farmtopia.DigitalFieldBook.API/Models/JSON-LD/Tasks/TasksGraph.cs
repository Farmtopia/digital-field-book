﻿using System.Text.Json.Serialization;

namespace Farmtopia.DigitalFieldBook.API.Models.JSON_LD.Tasks
{
    public class TasksGraph : GraphBase
    {
        [JsonPropertyName("datasetSlices")]
        public List<TasksDatasetSlice> DatasetSlices { get; set; }
    }
}
