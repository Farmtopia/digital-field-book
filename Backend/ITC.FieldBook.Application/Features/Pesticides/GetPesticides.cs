﻿using ITC.FieldBook.Application.Infrastructure.Interfaces;
using ITC.FieldBook.Application.Infrastructure;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata;
using Dapper;
using ITC.FieldBook.Application.Models;
using ITC.FieldBook.Application.Infrastructure.Models;
using DocumentFormat.OpenXml.Office2010.Excel;
using DocumentFormat.OpenXml.Spreadsheet;
using ITC.FieldBook.Application.Infrastructure.Enums;

namespace ITC.FieldBook.Application.Features.Pesticides
{
    public class GetPesticidesController : ApiControllerBase
    {
        [HttpGet("pesticides")]
        public Task<DbData<Pesticide>> GetPesticides([FromQuery]GetPesticidesQuery query)
        {
            return Mediator.Send(query);
        }
    }

    public class GetPesticidesQuery : BaseDataQuery<DbData<Pesticide>>
    {
        public bool All { get; set; }

        public bool IncludeStock { get; set; }

        public bool StockOnly { get; set; }

        public DateTime? StockDate { get; set; }

        public int? TaskId { get; set; }
    }

    internal sealed class GetPesticidesQueryHandler : IRequestHandler<GetPesticidesQuery, DbData<Pesticide>>
    {
        private readonly DbContext _ctx;
        private readonly IConfiguration _configuration;
        private readonly ICurrentUserService _currentUserService;

        public GetPesticidesQueryHandler(DbContext ctx, IConfiguration configuration, ICurrentUserService currentUserService)
        {
            _ctx = ctx;
            _configuration = configuration;
            _currentUserService = currentUserService;
        }

        public Task<DbData<Pesticide>> Handle(GetPesticidesQuery request, CancellationToken cancellationToken)
        {
            var user = _currentUserService.GetUser()!;
            var pesticidesRepository = new PesticidesRepository(_ctx);

            var data = pesticidesRepository.GetPesticides(
                request.IncludeStock,
                request.StockOnly,
                _currentUserService.FarmId,
                request.StockDate ?? DateTime.Now,
                request.TaskId);

            if (request.All && user.HasRole(EnumRole.Admin))
                return Task.FromResult(data);
            else
                data.Data = data.Data.Where(x=>x.IsGlobal || (_currentUserService.FarmId != null && x.FarmId == _currentUserService.FarmId));

            return Task.FromResult(data);
        }
    }
}
