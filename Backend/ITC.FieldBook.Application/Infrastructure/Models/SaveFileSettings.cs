﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITC.FieldBook.Application.Infrastructure.Models
{
    public class SaveFileSettings
    {
        public string? Folder { get; set; }
    }
}
