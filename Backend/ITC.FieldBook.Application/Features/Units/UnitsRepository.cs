﻿using Azure.Core;
using Dapper;
using ITC.FieldBook.Application.Features.Fertilizers;
using ITC.FieldBook.Application.Features.Pesticides;
using ITC.FieldBook.Application.Infrastructure;
using ITC.FieldBook.Application.Infrastructure.Models;
using ITC.FieldBook.Application.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITC.FieldBook.Application.Features.Units
{
    public class UnitsRepository
    {
        private DbContext _ctx;

        public UnitsRepository(DbContext ctx) {
            _ctx = ctx;
        }

        public DbData<Unit> GetUnits(string? group = null)
        {
            var data = _ctx.Get<Unit>($@"
                SELECT 
                    u.id,
                    u.name,
                    u.description,
                    u.description_en,
                    (SELECT TOP(1) STRING_AGG(group_id, ',') WITHIN GROUP (ORDER BY group_id) FROM FieldBook.unit_group WHERE {DbContext.GetIsDeletedIsActiveCondition()} AND unit_id = u.id) AS groups_displayvalue
                FROM FieldBook.unit u
                WHERE
                    {DbContext.GetIsDeletedIsActiveCondition("u")}
                    {(string.IsNullOrWhiteSpace(group) ? "" : $"AND u.id IN (SELECT unit_id FROM FieldBook.unit_group WHERE {DbContext.GetIsDeletedIsActiveCondition()} AND group_id = @group)")}", new { group });

            return data;
        }

        public DbItem<Unit> GetUnit(int id)
        {
            var data = _ctx.GetItem<Unit>($@"
                SELECT 
                    u.id,
                    u.name,
                    u.description,
                    u.description_en
                FROM FieldBook.unit u
                WHERE
                    {DbContext.GetIsDeletedIsActiveCondition("u")}
                    AND u.id = @id", new { id });

            return data;
        }

        public DbItem<Unit> GetUnitByName(string name)
        {
            var data = _ctx.GetItem<Unit>($@"
                SELECT 
                    u.id,
                    u.name,
                    u.description,
                    u.description_en
                FROM FieldBook.unit u
                WHERE
                    {DbContext.GetIsDeletedIsActiveCondition("u")}
                    AND u.name = @name", new { name });

            return data;
        }

        public DbItem<Unit> GetUnitForPesticide(int farmId, int pesticideId)
        {
            var data = _ctx.GetItem<Unit>($@"
                SELECT TOP(1)
                    pu.unit_id AS id,
                    ISNULL(pu.unit, u.name) AS name
                FROM FieldBook.v_pesticide_unit pu
		            LEFT JOIN FieldBook.unit u ON u.id = pu.unit_id
                WHERE pu.farm_id = @farmId AND pu.pesticide_id = @pesticideId", new { farmId, pesticideId });

            return data;
        }

        public DbItem<Unit> GetUnitForFertilizer(int farmId, int fertilizerId)
        {
            var data = _ctx.GetItem<Unit>($@"
                SELECT TOP(1)
                    pu.unit_id AS id,
                    ISNULL(pu.unit, u.name) AS name
                FROM FieldBook.v_fertilizer_unit pu
		            LEFT JOIN FieldBook.unit u ON u.id = pu.unit_id
                WHERE pu.farm_id = @farmId AND pu.fertilizer_id = @fertilizerId", new { farmId, fertilizerId });

            return data;
        }

        public DbData<UnitGroup> GetUnitGroups(int unitId)
        {
            var data = _ctx.Get<UnitGroup>($@"
                SELECT
                    ug.id,
                    ug.unit_id,
                    ug.group_id
                FROM FieldBook.unit_group ug
                WHERE
                    {DbContext.GetIsDeletedIsActiveCondition("ug")}
                    AND ug.unit_id = @unitId", new { unitId });

            return data;
        }
    }
}
