﻿using Dapper;
using ITC.FieldBook.Application.Infrastructure;
using ITC.FieldBook.Application.Infrastructure.Models;
using ITC.FieldBook.Application.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITC.FieldBook.Application.Features.Crops
{
    public class CropsRepository
    {
        private DbContext _ctx;

        public CropsRepository(DbContext ctx)
        {
            _ctx = ctx;
        }

        public DbData<Crop> GetCrops()
        {
            var data = _ctx.Get<Crop>($@"
                SELECT
                    c.id,
                    c.name,
                    c.name_en,
                    c.identifier
                FROM FieldBook.crop c
                WHERE
                    {DbContext.GetIsDeletedIsActiveCondition("c")}");

            return data;
        }

        public DbItem<Crop> GetCrop(int id)
        {
            var data = _ctx.GetItem<Crop>($@"
                SELECT TOP(1)
                    c.id,
                    c.name,
                    c.name_en,
                    c.identifier
                FROM FieldBook.crop c
                WHERE
                    {DbContext.GetIsDeletedIsActiveCondition("c")}
                    AND c.id = @id", new { id });

            return data;
        }
    }
}
