﻿using System.Text.Json;

namespace ITC.FieldBook.API.Models
{
    public class ErrorDetails
    {
        public int StatusCode { get; set; }

        public string? Message { get; set; }

        public List<string> Errors { get; set; } = new List<string>();

        public override string ToString()
        {
            return JsonSerializer.Serialize(this, new JsonSerializerOptions { PropertyNamingPolicy = JsonNamingPolicy.CamelCase });
        }
    }
}
