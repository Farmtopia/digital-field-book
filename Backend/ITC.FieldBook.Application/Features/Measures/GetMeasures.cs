﻿using ITC.FieldBook.Application.Infrastructure.Interfaces;
using ITC.FieldBook.Application.Infrastructure;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using ITC.FieldBook.Application.Infrastructure.Models;
using ITC.FieldBook.Application.Models;

namespace ITC.FieldBook.Application.Features.Measures
{
    public class GetMeasuresController : ApiControllerBase
    {
        [HttpGet("measures")]
        public Task<DbData<Measure>> GetMeasures([FromQuery] GetMeasuresQuery query)
        {
            return Mediator.Send(query);
        }
    }

    public class GetMeasuresQuery : IRequest<DbData<Measure>>
    {
        public int? Year { get; set; }
    }

    internal sealed class GetMeasuresQueryHandler : IRequestHandler<GetMeasuresQuery, DbData<Measure>>
    {
        private readonly DbContext _ctx;
        private readonly IConfiguration _configuration;
        private readonly ICurrentUserService _currentUserService;

        public GetMeasuresQueryHandler(DbContext ctx, IConfiguration configuration, ICurrentUserService currentUserService)
        {
            _ctx = ctx;
            _configuration = configuration;
            _currentUserService = currentUserService;
        }

        public Task<DbData<Measure>> Handle(GetMeasuresQuery request, CancellationToken cancellationToken)
        {
            var measuresRepository = new MeasuresRepository(_ctx);
            var data = measuresRepository.GetMeasures(request.Year);

            return Task.FromResult(data);
        }
    }
}
