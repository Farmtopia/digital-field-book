﻿using ITC.FieldBook.Application.Infrastructure.Models;
using ITC.FieldBook.Application.Infrastructure;
using MediatR;
using Microsoft.AspNetCore.DataProtection.KeyManagement;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using ITC.FieldBook.Application.Models;
using System.ComponentModel.DataAnnotations;
using Dapper;
using DocumentFormat.OpenXml.Bibliography;
using System.Transactions;
using ITC.FieldBook.Application.Infrastructure.Interfaces;
using ITC.FieldBook.Application.Features.Measures;
using ITC.FieldBook.Application.Features.CropRotation;
using ITC.FieldBook.Application.Features.Crops;

namespace ITC.FieldBook.Application.Features.Fields.Measures
{
    public class SaveFieldMeasuresController : ApiControllerBase
    {
        [HttpPost("fields/measures-batch")]
        public Task SaveFieldMeasures([FromBody] SaveFieldMeasuresCommand command)
        {
            return Mediator.Send(command);
        }
    }

    public class SaveFieldMeasuresCommand : IRequest
    {
        [Required]
        public List<FieldMeasure> FieldMeasures { get; set; } = new List<FieldMeasure>();

        public int? FieldId { get; set; }
        
        public int? FieldPlotId { get; set; }

        public int? Year { get; set; }
    }

    internal sealed class SaveFieldMeasuresCommandHandler : IRequestHandler<SaveFieldMeasuresCommand>
    {
        private readonly DbContext _ctx;
        private readonly ICurrentUserService _currentUserService;

        public SaveFieldMeasuresCommandHandler(DbContext ctx, ICurrentUserService currentUserService)
        {
            _ctx = ctx;
            _currentUserService = currentUserService;
        }

        public Task Handle(SaveFieldMeasuresCommand request, CancellationToken cancellationToken)
        {
            var user = _currentUserService.GetUser()!;
            var userId = user.Id!.Value;
            var fieldMeasures = request.FieldMeasures ?? new List<FieldMeasure>();
            var farmId = _currentUserService.FarmId!.Value;
            var measuresRepository = new MeasuresRepository(_ctx);
            var fieldsRepository = new FieldsRepository(_ctx);

            if(fieldMeasures.Count == 0)
            {
                var fieldId = request.FieldId!.Value;
                var fieldPlotId = request.FieldPlotId!.Value;
                var year = request.Year!.Value;

                using (var scope = new TransactionScope())
                {
                    // Delete existing measures
                    DeleteFieldMeasures(fieldId, fieldPlotId, year, userId);

                    scope.Complete();
                }
                return Task.CompletedTask;
            }

            var fieldMeasuresGrouped = fieldMeasures.GroupBy(x => new { x.FieldId, x.FieldPlotId, x.Year  }).ToList();

            using (var scope = new TransactionScope())
            {
                foreach (var fy in fieldMeasuresGrouped)
                {
                    var fieldId = fy.Key.FieldId!.Value;
                    var fieldPlotId = fy.Key.FieldPlotId!.Value;
                    var year = fy.Key.Year!.Value;
                    var fieldMeasuresGroup = fy.ToList();

                    // Field exists
                    var field = fieldsRepository.GetField(farmId, fieldId).Item;
                    if (field == null)
                        continue;

                    // Field crop exsists
                    var fieldPlots = fieldsRepository.GetFieldPlots(farmId, year, fieldIds: new List<int> { fieldId }).Data;
                    var fieldPlot = fieldPlots.FirstOrDefault(x => x.Id == fieldPlotId);
                    if (fieldPlot == null)
                        continue;

                    // Exclude existing measures
                    var excludeFieldMeasureIds = fy.Where(x => x.Id != null).Select(x => x.Id!.Value).ToList();

                    // Delete existing measures
                    DeleteFieldMeasures(fieldId, fieldPlotId, year, userId, excludeFieldMeasureIds);

                    // Measures
                    var measures = measuresRepository.GetMeasures(year).Data;

                    foreach(var m in fieldMeasuresGroup)
                    {
                        if (m.MeasureId == null)
                            continue;

                        // Measure exists?
                        var measure = measures.FirstOrDefault(x => x.Id == m.MeasureId);
                        if (measure == null)
                            continue;

                        var keys = _ctx.Save(m, user: user);
                        m.Id = Convert.ToInt32(keys["id"]);
                    }
                }

                scope.Complete();
            }

            return Task.CompletedTask;
        }

        private void DeleteFieldMeasures(int fieldId, int fieldPlotId, int year, int userId, List<int>? excludeFieldMeasureIds = null)
        {
            using (var conn = _ctx.CreateConnection())
            {
                var now = DateTime.Now;
                conn.Execute($@"
                    UPDATE FieldBook.field_measure
                    SET is_deleted = 1, is_active = 0, modified = @now, modified_by = @userId
                    WHERE
                        {DbContext.GetIsDeletedIsActiveCondition()}
                        AND field_id = @fieldId
                        AND year = @year
                        AND field_plot_id = @fieldPlotId
                        {((excludeFieldMeasureIds?.Any() ?? false) ? "AND id NOT IN @excludeFieldMeasureIds" : "")}
                ", new { fieldId, fieldPlotId, year, userId, now, excludeFieldMeasureIds });
            }
        }
    }
}

