﻿using System.Text.Json.Serialization;

namespace Farmtopia.DigitalFieldBook.API.Models.JSON_LD
{
    public class Fertilizer : JsonLdBase
    {
        [JsonPropertyName("commercialName")]
        public string CommercialName { get; set; }

        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
        [JsonPropertyName("productRatio")]
        public List<ProductRatio> ProductRatio { get; set; }
    }
}
