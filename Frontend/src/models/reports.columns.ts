/*
import { QTableColumn } from 'quasar';
import { CropRotationReportItem } from './reports.models';
import { formatArea } from 'src/helpers';

export const taskReportColumns: QTableColumn<TasksReportItem>[] = [
  {
    name: 'fieldName',
    label: 'GERK',
    field: (row) => (row.fieldIdentifier ?? '') + ' ' + (row.fieldName ?? ''),
    sortable: true,
    align: 'left',
  },
  {
    name: 'cropName',
    label: 'Naziv posevka',
    field: (row) => (row.cropIdentifier ?? '') + ' ' + (row.cropName ?? ''),
    sortable: true,
    align: 'left',
  },
  {
    name: 'year',
    label: 'Leto',
    field: 'year',
    sortable: true,
    align: 'left',
  },
  {
    name: 'taskTypeName',
    label: 'Vrsta opravila',
    field: 'taskTypeName',
    sortable: true,
    align: 'left',
  },
  {
    name: 'dateFrom',
    label: 'Od',
    field: 'dateFrom',
    format: (val) => formatDate(val) ?? '',
    sortable: true,
    style: 'width:150px;',
  },
  {
    name: 'dateTo',
    label: 'Do',
    field: 'dateTo',
    format: (val) => formatDate(val) ?? '',
    sortable: true,
    style: 'width:150px;',
  },
  {
    name: 'executor',
    label: 'Oseba',
    field: 'executor',
    sortable: true,
    align: 'left',
  },
  {
    name: 'equipmentName',
    label: 'Stroj',
    field: 'equipmentName',
    sortable: true,
    align: 'left',
  },
  {
    name: 'attachmentName',
    label: 'Priključek',
    field: 'attachmentName',
    sortable: true,
    align: 'left',
  },
  {
    name: 'note',
    label: 'Opomba',
    field: 'note',
    sortable: true,
    align: 'left',
  },
];*/
