﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Http.Json;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using System.Web;

namespace ITC.FieldBook.Application.Infrastructure.Tools
{
    public class FmManager
    {
        string _url;
        HttpClient _client;

        public FmManager(string apiUrl)
        {
            _url = apiUrl;
            _client = new HttpClient() { BaseAddress = new Uri(_url) };
        }

        public async Task<List<FmMachinery>> GetMachinery()
        {
            var response = await _client.GetAsync(new Uri(new Uri(_url), $"machinery/getMachinery"));

            if (response.IsSuccessStatusCode)
            {
                var data = await response.Content.ReadFromJsonAsync<List<FmMachinery>>(new System.Text.Json.JsonSerializerOptions { PropertyNameCaseInsensitive = true }) ?? new List<FmMachinery>();
                return data;
            }
            else
            {
                var message = await response.Content.ReadAsStringAsync();
                throw new Exception($"{response.StatusCode} : {message}");
            }
        }

        public async Task<FmMachinery?> GetMachineryByCode(string code)
        {
            var response = await _client.GetAsync(new Uri(new Uri(_url), $"machinery/getMachinery/{code}"));

            if (response.IsSuccessStatusCode)
            {
                var data = await response.Content.ReadFromJsonAsync<FmMachinery>(new System.Text.Json.JsonSerializerOptions { PropertyNameCaseInsensitive = true });
                return data;
            }
            else
            {
                return null;
            }

        }
    }

    public class FmMachinery
    {
        [JsonNumberHandling(JsonNumberHandling.AllowReadingFromString)]
        public int? Id { get; set; }

        public string? Ime { get; set; }

        public string? DisplayName { get => $"{Id} {Ime}"; }
    }
}
