﻿using ITC.FieldBook.Application.Infrastructure.Interfaces;
using ITC.FieldBook.Application.Infrastructure.Tools;
using ITC.FieldBook.Application.Infrastructure;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using ITC.FieldBook.Application.Models;
using ITC.FieldBook.Application.Models.Reports;
using ITC.FieldBook.Application.Features.Fields;
using ITC.FieldBook.Application.Features.Farms;
using ITC.FieldBook.Application.Features.Crops;
using ITC.FieldBook.Application.Features.Fields.Crops;
using ITC.FieldBook.Application.Infrastructure.Models;

namespace ITC.FieldBook.Application.Features.Reports
{
    public class GetFertilizerStockReportController : ApiControllerBase
    {
        [HttpGet("reports/stock/fertilizers")]
        public Task<DbData<FertilizersStockReportItem>> GetFertilizerStockReport(DateTime? date)
        {
            return Mediator.Send(new GetFertilizerStockReportQuery { Date = date });
        }
    }

    public class GetFertilizerStockReportQuery : IRequest<DbData<FertilizersStockReportItem>>
    {

        public DateTime? Date { get; set; }
    }

    internal sealed class GetFertilizerStockReportQueryHandler : IRequestHandler<GetFertilizerStockReportQuery, DbData<FertilizersStockReportItem>>
    {
        private readonly DbContext _ctx;
        private readonly IConfiguration _configuration;
        private readonly ICurrentUserService _currentUserService;

        public GetFertilizerStockReportQueryHandler(DbContext ctx, IConfiguration configuration, ICurrentUserService currentUserService)
        {
            _ctx = ctx;
            _configuration = configuration;
            _currentUserService = currentUserService;
        }

        public Task<DbData<FertilizersStockReportItem>> Handle(GetFertilizerStockReportQuery request, CancellationToken cancellationToken)
        {
            var date = request.Date ?? DateTime.Now;
            var data = _ctx.Get<FertilizersStockReportItem>(@"
                SELECT 
                    f.id,
	                f.name,
	                f.name_en,
	                f.brand_name,
	                s.stock,
	                s.stock_unit
                FROM FieldBook.fn_fertilizer_stock(@farmId, NULL, @date, null) s
	                JOIN FieldBook.fertilizer f ON f.id = s.fertilizer_id
                ", new { farmId = _currentUserService.FarmId, date });

            return Task.FromResult(data);
        }
    }
}
