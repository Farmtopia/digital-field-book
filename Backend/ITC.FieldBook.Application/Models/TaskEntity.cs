﻿using ITC.FieldBook.Application.Infrastructure.Models;
using ITC.FieldBook.Application.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITC.FieldBook.Application.Models
{
    public class TaskEntity : DbEntityWithAudit
    {
        public TaskEntity() : base("FieldBook.task") { }

        private int? _id;
        [DbColumn("id", IsKey = true, Identity = true)]
        public int? Id { get => _id; set { _id = value; RaisePropertyChanged(nameof(Id)); } }

        private int? _farmId;
        [DbColumn("farm_id")]
        public int? FarmId { get => _farmId; set { _farmId = value; RaisePropertyChanged(nameof(FarmId)); } }

        private int? _year;
        [DbColumn("year")]
        public int? Year { get => _year; set { _year = value; RaisePropertyChanged(nameof(Year)); } }

        private DateTime? _dateFrom;
        [DbColumn("date_from")]
        public DateTime? DateFrom { get => _dateFrom; set { _dateFrom = value; RaisePropertyChanged(nameof(DateFrom)); } }

        private DateTime? _dateTo;
        [DbColumn("date_to")]
        public DateTime? DateTo { get => _dateTo; set { _dateTo = value; RaisePropertyChanged(nameof(DateTo)); } }

        private int? _taskTypeId;
        [DbColumn("task_type_id")]
        public int? TaskTypeId { get => _taskTypeId; set { _taskTypeId = value; RaisePropertyChanged(nameof(TaskTypeId)); } }

        //private int? _cropId;
        //[DbColumn("crop_id")]
        //public int? CropId { get => _cropId; set { _cropId = value; RaisePropertyChanged(nameof(CropId)); } }

        private string? _executor;
        [DbColumn("executor")]
        public string? Executor { get => _executor; set { _executor = value; RaisePropertyChanged(nameof(Executor)); } }

        private string? _note;
        [DbColumn("note")]
        public string? Note { get => _note; set { _note = value; RaisePropertyChanged(nameof(Note)); } }

        private int? _equipmentId;
        [DbColumn("equipment_id")]
        public int? EquipmentId { get => _equipmentId; set { _equipmentId = value; RaisePropertyChanged(nameof(EquipmentId)); } }

        private int? _attachedEquipmentId;
        [DbColumn("attached_equipment_id")]
        public int? AttachedEquipmentId { get => _attachedEquipmentId; set { _attachedEquipmentId = value; RaisePropertyChanged(nameof(AttachedEquipmentId)); } }

        private bool? _treatmentSuccessful;
        [DbColumn("treatment_successful")]
        public bool? TreatmentSuccessful { get => _treatmentSuccessful; set { _treatmentSuccessful = value; RaisePropertyChanged(nameof(TreatmentSuccessful)); } }

        private int? _harvestAmount;
        [DbColumn("harvest_amount")]
        public int? HarvestAmount { get => _harvestAmount; set { _harvestAmount = value; RaisePropertyChanged(nameof(HarvestAmount)); } }

        private int? _trackingTripId;
        [DbColumn("tracking_trip_id")]
        public int? TrackingTripId { get => _trackingTripId; set { _trackingTripId = value; RaisePropertyChanged(nameof(TrackingTripId)); } }

        private string? _data;
        [DbColumn("data")]
        public string? Data { get => _data; set { _data = value; RaisePropertyChanged(nameof(Data)); } }

        /// <summary>
        /// Crop status when EnumCropStatus.USER_DEFINED
        /// </summary>
        private string? _cropStatusAfter;
        [DbColumn("crop_status_after")]
        public string? CropStatusAfter { get => _cropStatusAfter; set { _cropStatusAfter = value; RaisePropertyChanged(nameof(CropStatusAfter)); } }

        // --- Joined columns ---

        /// <summary>
        ///  Used when grouped tasks by fields
        /// </summary>
        public int? FieldId { get; set; }

        public string? TaskTypeName { get; set; }

        public string? TaskTypeNameEn { get; set; }

        public string? MachineName { get; set; }

        public string? AttachmentName { get; set; }

        public List<TaskField>? TaskFields { get; set; }

        public List<TaskPesticide>? Pesticides { get; set; }

        public List<TaskFertilizer>? Fertilizers { get; set; }

        public List<Document>? Documents { get; set; }

        /// <summary>
        /// Active plots for selected fields, on task date (DateFrom)
        /// TODO: To new DTO? This is just virtual field, not in db.
        /// </summary>
        public List<FieldDTO>? Fields { get; set; }
    }
}
