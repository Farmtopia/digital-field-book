﻿using ITC.FieldBook.Application.Infrastructure.Models;
using ITC.FieldBook.Application.Infrastructure;
using MediatR;
using Microsoft.AspNetCore.DataProtection.KeyManagement;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using ITC.FieldBook.Application.Models;
using DocumentFormat.OpenXml.Bibliography;
using ITC.FieldBook.Application.Features.CropRotation;
using ITC.FieldBook.Application.Infrastructure.Interfaces;
using AutoMapper.Features;
using ITC.FieldBook.Application.Features.Measures;
using ITC.FieldBook.Application.Features.Files;

namespace ITC.FieldBook.Application.Features.Fields.Measures
{
    public class SaveFieldMeasureController : ApiControllerBase
    {
        [HttpPost("fields/{fieldId}/measures")]
        public Task SaveFieldMeasure([FromBody] FieldMeasure data)
        {
            return Mediator.Send(new SaveFieldMeasureCommand { Data = data });
        }
    }

    public class SaveFieldMeasureCommand : IRequest
    {
        public FieldMeasure? Data { get; set; }
    }

    internal sealed class SaveFieldMeasureCommandHandler : IRequestHandler<SaveFieldMeasureCommand>
    {
        private readonly DbContext _ctx;
        private readonly ICurrentUserService _currentUserService;

        public SaveFieldMeasureCommandHandler(DbContext ctx, ICurrentUserService currentUserService)
        {
            _ctx = ctx;
            _currentUserService = currentUserService;
        }

        public Task Handle(SaveFieldMeasureCommand request, CancellationToken cancellationToken)
        {
            var data = request.Data;
            var farmId = _currentUserService.FarmId!.Value;

            if (data != null)
            {
                var measuresRepository = new MeasuresRepository(_ctx);
                var fieldsRepository = new FieldsRepository(_ctx);

                // Field exists
                var field = fieldsRepository.GetField(farmId, data.FieldId!.Value).Item;
                if (field == null)
                    return Task.FromResult(Enumerable.Empty<FieldPlot>());

                // Field crop exsists
                var fieldPlots = fieldsRepository.GetFieldPlots(farmId, data.Year!.Value, fieldIds: new List<int> { data.FieldId!.Value }).Data;
                var fieldPlot = fieldPlots.FirstOrDefault(x => x.Id == data.FieldPlotId);
                if (fieldPlot == null)
                    return Task.CompletedTask;

                // Measure exists?
                var measures = measuresRepository.GetMeasures(data.Year!.Value).Data;
                var measure = measures.FirstOrDefault(x => x.Id == data.MeasureId);
                if (measure == null)
                    return Task.CompletedTask;

                _ctx.Save(data);
            }

            return Task.CompletedTask;
        }
    }
}

