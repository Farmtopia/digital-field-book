﻿using ITC.FieldBook.Application.Infrastructure.Interfaces;
using ITC.FieldBook.Application.Infrastructure;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using ITC.FieldBook.Application.Infrastructure.Models;
using ITC.FieldBook.Application.Models;

namespace ITC.FieldBook.Application.Features.Units
{
    public class GetUnitController : ApiControllerBase
    {
        [HttpGet("units/{unitId}")]
        public Task<DbItem<Models.Unit>> GetUnit(int unitId)
        {
            return Mediator.Send(new GetUnitQuery { UnitId = unitId });
        }
    }

    public class GetUnitQuery : IRequest<DbItem<Models.Unit>>
    {
        public int UnitId { get; set; }
    }

    internal sealed class GetUnitQueryHandler : IRequestHandler<GetUnitQuery, DbItem<Models.Unit>>
    {
        private readonly DbContext _ctx;
        private readonly IConfiguration _configuration;
        private readonly ICurrentUserService _currentUserService;

        public GetUnitQueryHandler(DbContext ctx, IConfiguration configuration, ICurrentUserService currentUserService)
        {
            _ctx = ctx;
            _configuration = configuration;
            _currentUserService = currentUserService;
        }

        public Task<DbItem<Models.Unit>> Handle(GetUnitQuery request, CancellationToken cancellationToken)
        {
            var unitsRepo = new UnitsRepository(_ctx);
            var dbItm = unitsRepo.GetUnit(request.UnitId);
            var itm = dbItm.Item;

            if(itm != null)
                itm.Groups = unitsRepo.GetUnitGroups(itm.Id!.Value).Data;

            return Task.FromResult(dbItm);
        }
    }
}
