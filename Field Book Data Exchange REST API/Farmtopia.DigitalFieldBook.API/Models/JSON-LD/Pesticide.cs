﻿using System.Text.Json.Serialization;

namespace Farmtopia.DigitalFieldBook.API.Models.JSON_LD
{
    public class Pesticide : JsonLdBase
    {
        [JsonPropertyName("commercialName")]
        public string CommercialName { get; set; }
    }
}
