export enum EnumRole {
  Admin = '1',
  User = '2',
}
export enum EnumLoginType {
  LOCAL_LOGIN = 'LOCAL_LOGIN',
  ITC_SSO_LOGIN = 'ITC_SSO_LOGIN',
}

export enum EnumAppEnvironment {
  DEFAULT = 'DEFAULT',
  FARMTOPIA = 'FARMTOPIA',
}
