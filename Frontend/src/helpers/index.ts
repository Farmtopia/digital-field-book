import { Dialog, Notify, date } from 'quasar';
import { Component } from 'vue';
import { t, i18n } from 'src/boot/i18n';

/*export function confirmDelete() {
  return new Promise(function (resolve) {
    Dialog.create({
      title: 'Izbris',
      message: t('dialog.messages.confirmDelete'),
      ok: {
        push: true,
        label: t('dialog.delete'),
        style: 'min-width:100px',
      },
      cancel: {
        push: true,
        color: 'negative',
        label: t('dialog.cancel'),
        style: 'min-width:100px',
      },
      persistent: true,
    }).onOk(async () => {
      resolve(true);
    });
  });
}*/

export function getLocalizedValue(
  value: string | null | undefined,
  valueEn: string | null | undefined
) {
  const locale = i18n.global.locale.value;
  if (locale == 'en-US' && valueEn) {
    return valueEn;
  }
  return value;
}

export function showCustomDialog(component: Component, componentProps?: any) {
  return new Promise<{ confirm: boolean; data?: any }>(function (resolve) {
    Dialog.create({
      component,
      componentProps,
      persistent: true,
    })
      .onOk(async (payload) => {
        resolve({ confirm: true, data: payload });
      })
      .onCancel(async () => {
        resolve({ confirm: false });
      });
  });
}

export function showDialog(title: string, message: string) {
  return new Promise(function (resolve) {
    Dialog.create({
      title: title,
      message: message,
      ok: {
        push: true,
        label: t('dialog.close'),
        style: 'min-width:100px',
      },
    }).onOk(async () => {
      resolve(true);
    });
  });
}

export function showConfirm(
  title: string,
  message: string,
  opts?: { html?: boolean }
) {
  return new Promise<boolean>(function (resolve) {
    Dialog.create({
      title: title,
      message: message,
      html: opts?.html,
      style: { width: '600px', maxWidth: '600px' },
      ok: {
        flat: true,
        color: 'positive',
        label: t('dialog.ok'),
        style: 'min-width:100px',
      },
      cancel: {
        flat: true,
        color: 'negative',
        label: t('dialog.cancel'),
        style: 'min-width:100px',
      },
      persistent: true,
    })
      .onOk(async () => {
        resolve(true);
      })
      .onCancel(async () => {
        resolve(false);
      });
  });
}

export function showDeleteConfirm(message: string) {
  return new Promise<boolean>(function (resolve) {
    Dialog.create({
      title: t('dialog.delete'),
      message,
      ok: {
        flat: true,
        icon: 'delete',
        label: t('dialog.delete'),
        style: 'min-width:100px',
      },
      cancel: {
        autofocus: true,
        flat: true,
        color: 'negative',
        label: t('dialog.cancel'),
        style: 'min-width:100px',
      },
      persistent: true,
      html: true,
    })
      .onOk(async () => {
        resolve(true);
      })
      .onCancel(async () => {
        resolve(false);
      });
  });
}

export function showError(errorMessage: string) {
  Notify.create({
    type: 'negative',
    html: true,
    message: errorMessage,
  });
}

export function showSuccess(successMessage: string) {
  Notify.create({
    icon: 'done',
    type: 'positive',
    message: successMessage,
  });
}

export function formatDecimal(value?: number) {
  return value?.toLocaleString('sl-SI', {
    maximumFractionDigits: 2,
    minimumFractionDigits: 2,
  });
}

export function formatInt(value?: number) {
  console.log('in val', value);
  return value
    ? Math.floor(value).toLocaleString('sl-SI', {
        maximumFractionDigits: 0,
        minimumFractionDigits: 0,
      })
    : null;
}

export function formatArea(valueInAre: number | undefined) {
  if (valueInAre == null) {
    return null;
  }

  const totalSquareMeters = valueInAre * 100; // 1 ar = 100 m²
  const hectares = Math.floor(totalSquareMeters / 10000); // 1 ha = 10000 m²
  const squareMeters = totalSquareMeters % 10000;
  const ares = Math.floor(squareMeters / 100);
  const remainingSquareMeters = Math.round(squareMeters % 100);

  let result = '';

  if (hectares > 0) {
    result += hectares + ` ${t('common.units.ha')}`;
  }
  if (ares > 0) {
    if (result.length > 0) {
      result += ' ';
    }
    result += ares + ` ${t('common.units.are')}`;
  }
  if (remainingSquareMeters > 0) {
    if (result.length > 0) {
      result += ' ';
    }
    result += remainingSquareMeters + ` ${t('common.units.m2')}`;
  }

  return result;
}

export function formatDate(d: Date | string | undefined, time = false) {
  if (!d) {
    return null;
  }
  return date.formatDate(d, 'DD.MM.YYYY' + (time ? ' HH:mm' : ''), undefined);
}

export function formatTime(d: Date | string | undefined) {
  if (!d) {
    return null;
  }
  return date.formatDate(d, 'HH:mm', undefined);
}

export function formatBoolean(val: boolean | undefined) {
  return val ? t('common.yes') : t('common.no');
}
