// This is just an example,
// so you can safely delete all default props below
export default {
  title: 'Dnevnik opravil',
  login: 'Prijava',
  logout: 'Odjava',
  error404: 'Ups, ta stran ne obstaja...',
  sessionExpired: 'Seja je potekla. Prosimo prijavite se ponovno.',
  initialLoading: 'Nalaganje, prosimo počakajte...',

  common: {
    home: 'Domov',
    warning: 'Opozorilo',
    validationError: 'Odpravite validacijske napake',
    saveSuccess: 'Shranjevanje uspešno',
    deleteSuccess: 'Brisanje uspešno',
    yes: 'Da',
    no: 'Ne',
    add: 'Dodaj',
    edit: 'Uredi',
    delete: 'Izbriši',
    isActive: 'Aktiven',
    showMore: 'Prikaži več',
    showLess: 'Prikaži manj',
    created: 'Ustvarjeno',
    createdBy: 'Ustvaril',
    modified: 'Spremenjeno',
    modifiedBy: 'Spremenil',

    units: {
      ha: 'ha',
      are: 'ar',
      m2: 'm²',
    },
  },

  components: {
    file: {
      downloadFile: 'Prenesi datoteko',
    },
  },

  validationMessages: {
    requiredField: 'To polje je obvezno',
    minLength: 'Minimalna dolžina: {minLength}',
    valuesMustMatch: 'Vrednosti se morata ujemati',
    greatherThan: 'Vrednost mora biti večja od {value}',
    greatherThanOrEqual: 'Vrednost mora biti večja ali enaka {value}',
  },

  dialog: {
    ok: 'V redu',
    confirm: 'Potrdi',
    save: 'Shrani',
    delete: 'Izbriši',
    cancel: 'Prekliči',
    close: 'Zapri',

    messages: {
      confirmDelete: 'Ste prepričani, da želite izbrisati izbrani zapis?',
    },
  },

  table: {
    search: 'Iskanje',
    buttons: {
      view: 'Ogled',
      add: 'Dodaj',
      edit: 'Uredi',
      delete: 'Izbriši',
    },
  },

  select: {
    noResults: 'Ni rezultatov',
  },

  adminLayout: {
    drawer: {
      administration: 'Administracija',
      fertilizers: 'Gnojila',
      pesticides: 'FFS',
      crops: 'Posevki',
      units: 'Merske enote',
      measureTypes: 'Ukrepi',
      documentTypes: 'Vrste dokumentov',
      fieldTypes: 'Vrste zemljišč',
      taskTypes: 'Vrste opravil',
      users: 'Uporabniki',
      settings: 'Nastavitve',
      fieldbook: 'Dnevnik opravil',
    },
  },

  mainLayout: {
    drawer: {
      farm: 'Kmetija',
      fields: 'Zemljišča',
      cropRotation: 'Kolobar',
      cropRotationSum: 'Seštevek po kulturah',
      log: 'Dnevnik',
      tasks: 'Opravila',
      fertilizers: 'Gnojila',
      fertilizersList: 'Seznam',
      pesticides: 'FFS',
      pesticidesList: 'Seznam',
      stockControl: 'Urejanje zaloge',
      stockStatus: 'Stanje zaloge',
      machinery: 'Mehanizacija',
      documents: 'Datoteke',
      reports: 'Poročila',
      settings: 'Nastavitve',
      administration: 'Administracija',
      useMyLocation: 'Uporabi mojo lokacijo',
      showMyLocationTooltip: 'Kje sem?',
    },
  },

  modules: {
    login: {
      title: 'Prijava',
    },

    farm: {
      title: 'Kmetija',
      farmInfo: 'Kmetija',
      farmIdentifier: 'KMG-MID',
      farmName: 'Ime',
      farmOwner: 'Nosilec',
      farmAddress: 'Naslov',
      farmPostZipCode: 'Poštna številka',
      farmCity: 'Kraj',
      ecoFarm: 'Ekološka kmetija',

      fields: 'Zemljišča',
      fieldIdentifier: 'GERK-PID',
      fieldName: 'Ime',
      fieldArea: 'Površina',
      fieldType: 'Vrsta zemljišča',

      editDialog: {
        title: 'Kmetija',
        basicInfo: 'Osnovne informacije',
        basicInfoDescription: 'Vnesite osnovne informacije o kmetiji.',
        location: 'Lokacija',
        selectLocationDescription:
          'Izberite lokacijo na kmetiji in označite kmetijo z točko na zemljevidu.',
        onlyOnePinAllowed:
          'Dovoljena je samo ena oznaka lokacije na zemljevidu. Premaknite ali odstranite trenutno oznako če želite spremeniti lokacijo.',

        inputs: {
          farmIdentifier: 'KMG-MID',
          farmName: 'Ime',
          farmOwner: 'Nosilec',
          farmAddress: 'Naslov',
          farmPostZipCode: 'Poštna številka',
          farmCity: 'Kraj',
          ecoFarm: 'Ekološka kmetija',
        },
      },
    },

    fields: {
      title: 'Zemljišča',
      buttons: {
        addTask: 'Opravilo',
        showTracking: 'Sledenje',
        addField: 'Dodaj zemljišče',
        editField: 'Uredi zemljišče',
        showFieldDetails: 'Prikaži podrobnosti',
      },
      filter: {
        date: 'Datum',
      },
      table: {
        columns: {
          fieldIdentifier: 'GERK-PID',
          fieldName: 'Ime',
          fieldArea: 'Površina',
          fieldType: 'Vrsta zemljišča',
          currentCrop: 'Posevek na izbrani datum',
          lastTask: 'Zadnje opravilo',
          location: 'Lokacija',
          lastTasks: 'Zadnja opravila',
        },
        noTasks: 'Ni opravil',
      },
      card: {
        details: 'Podrobnosti',
      },
      editDialog: {
        title: 'Zemljišče',
        location: 'Lokacija',

        basicInfo: 'Osnovne informacije',
        basicInfoDescription: 'Vnesite osnovne informacije o zemljišču.',
        selectLocationDescription:
          'Izberite lokacijo in narišite poligon okrog zemljišča.',

        inputs: {
          fieldIdentifier: 'GERK-PID',
          fieldName: 'Ime',
          fieldArea: 'Površina (ar)',
          fieldType: 'Vrsta zemljišča',
          fieldDescription: 'Opis / opomba',
        },
      },
    },

    field: {
      title: 'Zemljišče',
      fieldInfo: 'Zemljišče',
      crops: 'Posevki',
      tasks: 'Opravila',

      fieldColumns: {
        identifier: 'GERK-PID',
        name: 'Ime',
        area: 'Površina',
        type: 'Vrsta zemljišča',
      },

      cropColumns: {
        name: 'Ime',
        type: 'Vrsta posevka',
        sowingDate: 'Datum setve',
        harvestDate: 'Datum žetve',
      },

      taskColumns: {
        from: 'Od',
        to: 'Do',
        name: 'Opravilo',
        crop: 'Posevek',
        executor: 'Izvajalec',
        machine: 'Mehanizacija',
        attachment: 'Priključek',
        note: 'Opomba',
      },
    },

    cropRotation: {
      title: 'Kolobar',
      noCropRotationFields:
        'Nimate zemljišč za katere je možen vnos kolobarja.',
      selectOnlyOneYearForMeasures:
        'Za urejanje ukrepov za več posevkov naenkrat, lahko izberete samo posevke v istem letu.',

      filter: {
        cropType: 'Vrsta posevka',
      },

      columns: {
        field: 'Zemljišče',
        area: 'Površina',
      },

      cropContextMenu: {
        editMeasures: 'Uredi ukrepe',
        setColor: 'Nastavi barvo',
        setCropColor: 'Nastavi barvo izbranega posevka',
        clear: 'Počisti',
        selectMore: 'Izberi več',
        withSelected: 'Z izbranimi',
        setMeasures: 'Nastavi ukrepe',
        clearSelected: 'Počisti izbrane',
      },

      fieldContextMenu: {
        editCrops: 'Uredi posevke',
        selectMore: 'Izberi več',
        withSelected: 'Z izbranimi',
        setCrops: 'Nastavi posevke',
        clearSelected: 'Počisti izbrane',
      },
    },

    cropRotationSum: {
      title: 'Seštevek po kulturah',
      noCropRotationFields:
        'Nimate zemljišč za katere je možen vnos kolobarja.',

      filter: {
        cropType: 'Vrsta posevka',
      },

      columns: {
        crop: 'Posevek',
      },

      sum: 'Seštevek',
    },

    tasks: {
      title: 'Opravila',
      expandMoreFields: '(+ {count} več...)',
      expandAll: 'Razširi vse',
      confirmDelete: 'Ste prepričani da želite izbrisati izbrano opravilo?',

      confirmDeleteMultiple:
        'Izbrisali boste opravilo na večih ({count}) zemljiščih. Če želite odstraniti samo določeno zemljišče, uredite opravilo.<br /><br />Ste prepričani da želite izbrisati izbrano opravilo?',

      report: 'Poročilo',
      downloadReport: 'Prenesi poročilo',

      filter: {
        field: 'Zemljišče',
        dateFrom: 'Datum od',
        dateTo: 'Datum do',
      },

      columns: {
        dateFrom: 'Datum',
        fieldName: 'Zemljišče / Poljina',
        taskTypeName: 'Vrsta opravila',
        currentCrops: 'Trenutni posevki',
        cropName: 'Posevek',
        executor: 'Izvajalec',
        machineName: 'Mehanizacija',
        attachmentName: 'Priključek',
        note: 'Opomba',
      },
    },

    taskEdit: {
      basic: {
        title: 'Opravilo',
        basicInfo: 'Osnovne informacije',
        basicInfoDescription: 'Vnesite osnovne informacije o opravilu.',
      },
      plots: {
        title: 'Poljine',
        description: 'Izberite poljine, na katerih ste izvajali opravilo.',
        validationError:
          'Odpravite validacijske napake ali potrdite, če želite kljub temu nadaljevati.',
        editCropRotation: 'Uredi kolobar',
        calculateDuration: 'Preračunaj trajanje',
        taskDuration: 'Task duration',
        taskDurationDescription:
          'To opravilo zahteva vnos trajanja opravila na posamezni poljini. Razporedite trajanje opravila med izbrane poljine. Privzeto se izračuna čas glede na površino poljine.',

        columns: {
          area: 'Površina',
          crop: 'Posevek',
          sowingDate: 'Datum setve',
          harvestDate: 'Datum žetve',
          durationMinutes: 'Trajanje (min)',
        },

        validationMessages: {
          noPlots:
            'Ni določenih poljin za {year}. Prosimo uredite kolobar pred nadaljevanjem.',
          alreadySown: 'Setev na izbrani poljini je bila že opravljena.',
          alreadyHarvested: 'Žetev na izbrani poljini je bila že opravljena.',
          selectAtLeastOnePlot: 'Izberite vsaj eno poljino.',
          selectedDifferentCropTypes:
            'Izberete lahko poljine z enako vrsto posevkov.',
          previousPlotsNotCompleted: 'Niste še uredili prejšnje poljine.',
          distributeEntireDuration:
            'Celotni čas mora biti razporejen med izbrane poljine ({distributedDuration}/{taskDuration}, razlika: {difference}).',
        },
      },
      sowing: {
        title: 'Setev',
        description: 'Izberite posevke za katere ste opravili setev.',
      },
      harvesting: {
        title: 'Žetev',
        description: 'Vnesite dodatne podatke o opravljeni žetvi.',
      },
      fertilizing: {
        title: 'Gnojenje',
        description:
          'Izberite gnojila ki ste jih uporabili pri izvedbi opravila.',
        stockDescription:
          'Prikazano je stanje zalog na izbrani datum. V kolikor gnojila ni v šifrantu, morate najprej urediti zaloge.',
        addRow: 'Dodaj vrstico',
      },
      spraying: {
        title: 'Uporaba FFS',
        description:
          'Izberite fitofarmacevtska sredstva, ki ste jih uporabili pri izvedbi opravila.',
        stockDescription:
          'Prikazano je stanje zalog na izbrani datum. V kolikor FFS ni v šifrantu, morate najprej urediti zaloge.',
        addRow: 'Dodaj vrstico',
      },
      machinery: {
        title: 'Mehanizacija',
        description:
          'Vnesite mehanizacijo ki ste jo uporabili za izvedbo opravila.',
      },

      misc: {
        title: 'Dodatno',
        attachments: 'Priloge',
        addAttachments:
          'Po potrebi priložite datoteke/slike. Priloge bodo dostopne tudi v modulu "Datoteke".',
      },

      inputs: {
        fields: 'Zemljišča',
        from: 'Od',
        to: 'Do',
        taskType: 'Vrsta opravila',
        cropStatusAfter: 'Stanje posevka (če obstaja) po izvedbi opravila',
        executor: 'Izvajalec',
        crop: 'Posevek',
        fertilizer: 'Gnojilo',
        pesticide: 'FFS',
        unit: 'EM',
        quantity: 'Količina',
        treatmentSuccess: 'Uspešno tretiranje',
        harvestAmount: 'Količina pridelka (kg)',
        machine: 'Mehanizacija',
        machineAttachment: 'Priključek',
        note: 'Opomba',
        file: 'Priloga',
        documentType: 'Vrsta dokumenta',
        description: 'Opis',
      },

      stockCalculation: {
        area: 'Površina',
        stock: 'Zaloga',
        quantity: 'Količina',
        usage: 'Poraba',
        remainingStock: 'Preostanek',
        noStock: 'Ni dovolj zaloge',
        editStock: 'Uredi zalogo',
      },

      selectField: 'Izberite vsaj 1 zemljišče',
      provideTime: 'Opravilo zahteva vnos ure začetka in konca izvedbe.',
      continueNoStockTitle: 'Ni dovolj zaloge',
      continueNoStock: 'Prosimo uredite zaloge. Želite kljub temu nadaljevati?',
      next: 'Naprej',
      previous: 'Nazaj',
      save: 'Shrani',
      saveAndContinue: 'Shrani in nadaljuj',
      saveAndNew: 'Shrani in novo',
    },

    fertilizers: {
      title: 'Gnojila',
      list: 'Seznam',
      stockControlTitle: 'Urejanje zaloge',
      taskUsage: 'Uporaba pri opravilu',

      filter: {
        myFertilizers: 'Moja dodana',
      },

      columns: {
        date: 'Datum',
        fertilizerName: 'Gnojilo',
        stockName: 'Vrsta zaloge',
        value: 'Količina',
        note: 'Opomba',
      },

      editDialog: {
        title: 'Gnojila - urejanje zaloge',
        selectStockType: 'Izberite vrsto zaloge',

        inputs: {
          date: 'Datum',
          fertilizer: 'Gnojilo',
          stockType: 'Vrsta zaloge',
          value: 'Količina',
          unit: 'EM',
          note: 'Opomba',
        },
      },
    },

    fertilizerStockStatus: {
      title: 'Stanje zaloge',

      filter: {
        date: 'Datum',
      },

      columns: {
        fertilizerName: 'Gnojilo',
        stock: 'Zaloga',
      },
    },

    pesticides: {
      title: 'FFS',
      list: 'Seznam',
      stockControlTitle: 'Urejanje zaloge',
      taskUsage: 'Uporaba pri opravilu',

      columns: {
        date: 'Datum',
        pesticideName: 'FFS',
        stockName: 'Vrsta zaloge',
        value: 'Količina',
        note: 'Opomba',
      },

      editDialog: {
        title: 'FFS - urejanje zaloge',
        selectStockType: 'Izberite vrsto zaloge',

        inputs: {
          date: 'Datum',
          pesticide: 'FFS',
          stockType: 'Vrsta zaloge',
          value: 'Količina',
          unit: 'EM',
          note: 'Opomba',
        },
      },
    },

    pesticideStockStatus: {
      title: 'Stanje zaloge',

      filter: {
        date: 'Datum',
      },

      columns: {
        pesticideName: 'FFS',
        stock: 'Zaloga',
      },
    },

    machinery: {
      title: 'Mehanizacija',

      columns: {
        name: 'Ime',
        description: 'Opis',
        performance: 'Zmogljivost',
        classification: 'Klasifikacija',
        trackingId: 'ID Sledilne naprave',
        trackingDeviceType: 'Vrsta sledilne naprave',
        farmManagerMachinery: 'Šifrant iz kalkulacij - Stroji in oprema',
      },

      editDialog: {
        title: 'Mehanizacija - urejanje',
        farmManager: 'Farm Manager',

        inputs: {
          name: 'Ime',
          description: 'Opis',
          performance: 'Zmogljivost',
          performanceUnit: 'EM',
          classification: 'Klasifikacija',
          trackingId: 'ID Sledilne naprave',
          trackingDeviceType: 'Vrsta sledilne naprave',
          farmManagerMachinery: 'Šifrant iz kalkulacij - Stroji in oprema',
        },
      },
    },

    documents: {
      title: 'Dokumenti',
      download: 'Prenesi',

      columns: {
        fileDisplayname: 'Ime datoteke',
        documentTypeName: 'Vrsta dokumenta',
        description: 'Opis',
        task: 'Opravilo',
        created: 'Ustvarjeno',
      },

      editDialog: {
        title: 'Dokument - urejanje',

        inputs: {
          file: 'Datoteka',
          documentType: 'Vrsta dokumenta',
          description: 'Opis',
        },
      },
    },

    reports: {
      title: 'Poročila',
      prepare: 'Pripravi',

      basicReport: {
        title: 'Splošno poročilo',
        description:
          'Splošno poročilo na podlagi podatkov iz dnevnika opravil (opravila, posevki, ...).',
        yearHint: 'Vnesite leto za katerega želite pripraviti poročilo.',
        fieldsHint:
          'Pustite prazno, če želite pripraviti poročilo za vsa zemljišča.',
        exportCrops: 'Posevki',
        exportMeasures: 'Ukrepi',
        exportTasks: 'Opravila',
      },

      kopopReport: {
        title: 'Enotne evidence o delovnih opravilih, verzija 2.5',
        description:
          'Enotne evidence o delovnih opravilih za posamezne intervencije Strateškega načrta skupne kmetijske politike 2023-2027 - intervencija kmetijsko-okoljsko-podnebna plačila (KOPOP), intervencija lokalne pasme in sorte (LOPS), intervencija biotično varstvo rastlin (BVR), intervencija plačila Natura 2000 (N2000) ter za namen ekološkega kmetovanja (EK). Enotne evidence vodijo tisti nosilci kmetijskih gospodarstev, ki so vključeni v eno ali več intervencij - KOPOP, LOPS, N2000, BVR, SOPO ali EK.',
      },
    },

    settings: {
      title: 'Nastavitve',
    },

    administration: {
      title: 'Administracija',
    },

    admin: {
      farms: {
        title: 'Kmetije',

        buttons: {
          import: 'Uvoz',
          fieldbook: 'Odpri dnevnik opravil...',
        },

        columns: {
          identifier: 'KMG-MID',
          name: 'Ime',
          owner: 'Nosilec',
          address: 'Naslov',
          city: 'Kraj',
          ecoFarm: 'Ekološka kmetija',
        },
      },

      fertilizers: {
        title: 'Gnojila',

        columns: {
          name: 'Ime',
          nameEn: 'Ime (Angleško)',
          type: 'Vrsta',
          ratio: 'Sestava',
          isGlobal: 'Globalno',
          addedByFarm: 'Dodala kmetija',
        },

        editDialog: {
          title: 'Gnojilo - urejanje',
          basicInfo: 'Osnovni podatki',
          ratio: 'Sestava',
          visiblity: 'Vidnost',

          inputs: {
            name: 'Ime',
            nameEn: 'Ime (Angleško)',
            type: 'Vrsta gnojila',
            ratioN: 'N - dušik',
            ratioP: 'P - fosfor',
            ratioK: 'K - kalij',
            isGlobal: 'Globalno - na voljo vsem',
            isGlobalHint:
              'Ta zapis je dodala kmetija, zato je na voljo le tej kmetiji, razen če jo administrator nastavi kot globalno. Globalna gnojila so na voljo vsem uporabnikom.',
            addedByFarm: 'Dodala kmetija',
          },
        },
      },

      pesticides: {
        title: 'FFS',

        columns: {
          name: 'Ime',
          nameEn: 'Ime (Angleško)',
          isGlobal: 'Globalno',
          addedByFarm: 'Dodala kmetija',
        },

        editDialog: {
          title: 'FFS - urejanje',
          visiblity: 'Vidnost',

          inputs: {
            name: 'Ime',
            nameEn: 'Ime (Angleško)',
            isGlobal: 'Globalno - na voljo vsem',
            isGlobalHint:
              'Ta zapis je dodala kmetija, zato je na voljo le tej kmetiji, razen če jo administrator nastavi kot globalno. Globalna gnojila so na voljo vsem uporabnikom.',
            addedByFarm: 'Dodala kmetija',
          },
        },
      },

      crops: {
        title: 'Posevki',

        columns: {
          identifier: 'Šifra',
          name: 'Ime',
          nameEn: 'Ime (Angleško)',
        },

        editDialog: {
          title: 'Posevek - urejanje',

          inputs: {
            identifier: 'Šifra',
            name: 'Ime',
            nameEn: 'Ime (Angleško)',
          },
        },
      },

      units: {
        title: 'Merske enote',

        columns: {
          name: 'Ime',
          description: 'Opis',
          descriptionEn: 'Opis (Angleško)',
          groups: 'Moduli',
        },

        editDialog: {
          title: 'Merska enota - urejanje',
          groupsTitle: 'Moduli',
          groupsDescription:
            'Izberite v katerih modulih naj bo merska enota na voljo.',

          groups: {
            task: 'Opravilo',
            stock: 'Urejanje zaloge',
            machinery: 'Mehanizacija',
          },

          inputs: {
            name: 'Ime',
            description: 'Opis',
            descriptionEn: 'Opis (Angleško)',
            fertilizerUsage: 'Poraba gnojila',
            fertilizerStockControl: 'Vnos gnojil',
            pesticideUsage: 'Poraba FFS',
            pesticideStockControl: 'Vnos FFS',
            performance: 'Zmogljivost',
          },
        },
      },

      measureTypes: {
        title: 'Vrste ukrepov',

        columns: {
          name: 'Ime',
          nameEn: 'Ime (Angleško)',
          description: 'Opis',
          descriptionEn: 'Opis (Angleško)',
          category: 'Kategorija',
          yearFrom: 'Aktiven od (leto)',
          yearTo: 'Aktiven do (leto)',
        },

        editDialog: {
          title: 'Ukrep - Urejanje',

          inputs: {
            name: 'Ime',
            nameEn: 'Ime (Angleško)',
            description: 'Opis',
            descriptionEn: 'Opis (Angleško)',
            category: 'Kategorija',
            yearFrom: 'Aktiven od (leto)',
            yearTo: 'Aktiven do (leto)',
          },
        },
      },

      documentTypes: {
        title: 'Vrste dokumentov',

        columns: {
          name: 'Ime',
          nameEn: 'Ime (Angleško)',
        },

        editDialog: {
          title: 'Vrsta dokumenta - urejanje',

          inputs: {
            name: 'Ime',
            nameEn: 'Ime (Angleško)',
          },
        },
      },

      fieldTypes: {
        title: 'Vrste zemljišč',

        columns: {
          identifier: 'Šifra',
          name: 'Ime',
          nameEn: 'Ime (Angleško)',
          environmentType: 'Vrsta okolja',
          includeInCropRotation: 'Upoštevaj v kolobarju',
        },

        editDialog: {
          title: 'Vrsta zemljišča - urejanje',

          inputs: {
            identifier: 'Šifra',
            name: 'Ime',
            nameEn: 'Ime (Angleško)',
            environmentType: 'Vrsta okolja',
            includeInCropRotation: 'Upoštevaj v kolobarju',
          },
        },
      },

      taskTypes: {
        title: 'Vrste opravil',

        columns: {
          identifier: 'Šifra',
          name: 'Ime',
          nameEn: 'Ime (Angleško)',
          systemTypeName: 'Sistemski tip',
          cropStatusName: 'Stanje posevka po opravilu',
          cropStatusEditable: 'Uporabnik lahko določi stanje posevka',
          requiresTime: 'Zahteva vnos ure',
        },

        editDialog: {
          title: 'Vrsta opravila - urejanje',

          inputs: {
            identifier: 'Šifra',
            name: 'Ime',
            nameEn: 'Ime (Angleško)',
            systemTypeName: 'Sistemski tip',
            cropStatusName: 'Stanje posevka po opravilu',
            cropStatusEditable: 'Uporabnik lahko določi stanje posevka',
            requiresTime: 'Zahteva vnos ure',
          },
        },
      },

      users: {
        title: 'Uporabniki',

        role: {
          admin: 'Administrator',
          user: 'Uporabnik',
        },

        status: {
          inactive: 'Neaktiven',
          active: 'Aktiven',
        },

        columns: {
          id: 'ID',
          name: 'Ime',
          surname: 'Priimek',
          email: 'Email',
          role: 'Vloga',
          farm: 'Kmetija',
        },

        editDialog: {
          user: 'Uporabnik',
          password: 'Geslo',
          newPassword: 'Novo geslo',

          inputs: {
            name: 'Ime',
            surname: 'Priimek',
            email: 'Email',
            password: 'Geslo',
            passwordConfirmation: 'Potrditev gesla',
            role: 'Vloga',
            farm: 'Kmetija',
          },
        },
      },

      settings: {
        title: 'Nastavitve',
      },
    },
  },

  dialogs: {
    batchMeasureSetDialog: {
      title: 'Nastavitev ukrepov',
      description:
        'Obstoječi ukrepi za izbrane posevke bodo nastavljeni za izbrane.',
    },

    measureEditDialog: {
      title: 'Ukrepi - urejanje',
      fieldInfo: 'Zemljišče',
      fieldColumns: {
        field: 'Zemljišče',
        year: 'Leto',
        area: 'Površina',
        crop: 'Posevek',
      },
    },

    cropRotationEditDialog: {
      title: 'Posevki - urejanje',
      fieldInfo: 'Zemljišče',
      crops: 'Posevki',
      cropsDescription:
        'Določite posevke za zemljišče. Če imate na enem zemljišču več posevkov, lahko posamezno vrsto posevka (glavni, neprezimni, prezimni) razdelite na več različnih posevkov.',
      sumMustMatch:
        'Seštevek površine posevkov se mora ujemati z površino zemljišča',

      fieldColumns: {
        field: 'Zemljišče',
        year: 'Leto',
        area: 'Površina',
      },

      inputs: {
        crop: 'Posevek',
        variety: 'Sorta',
        area: 'Površina (ar)',
      },

      cropAreaCalculation: {
        sum: 'Seštevek',
        difference: 'Razlika',
      },
    },
    batchCropSetDialog: {
      title: 'Nastavitev posevkov',
      description:
        'Obstoječi posevki za izbrana zemljišča bodo nastavljeni na izbrane (razen posevki ki so že v uporabi).',
    },

    trackingTripsDialog: {
      title: 'Sledenje',
      downloadingDataFromTrackers:
        'Prenos podatkov iz sledilnih naprav, prosimo počakajte...',

      filter: {
        date: 'Datum',
        minTaskTimeMinutes: 'Min. trajanje (nminute)',
      },

      buttons: {
        refresh: 'Osveži',
        refreshFromTrackingDevices: 'Osveži iz sledilnih naprav',
      },

      columns: {
        startTime: 'Od',
        endTime: 'Do',
        field: 'Zemljišče',
        machine: 'Mehanizacija',
        attachment: 'Priključek',
        duration: 'Trajanje',
      },
    },

    rkgDialog: {
      title: 'Uvoz kmetije',
      description: 'Vnesite GERK-PID za uvoz kmetije iz obstoječega sistema.',
      farm: 'Kmetija',
      fields: 'Zemljišča',
      importSuccess: 'Uvoz uspešen',
      noResults: 'Ni rezutatov',

      inputs: {
        identifier: 'KMG-MID',
      },

      farmColumns: {
        identifier: 'KMG-MID',
        name: 'Ime',
        address: 'Naslov',
        owner: 'Nosilec',
      },

      fieldColumns: {
        identifier: 'GERK-PID',
        name: 'Ime',
        area: 'Površina',
        type: 'Vrsta zemljišča',
      },

      buttons: {
        import: 'Uvozi',
      },
    },
  },

  loginPage: {
    existingUser: 'Obstoječi uporabnik?',
    login: 'Prijava',
    noAccess:
      "Nimate dostopa do te aplikacije. Pojdite na portal 'Moje Aplikacije' in zahtevajte dostop.",
    tryAgain: 'Prijava neuspešna. Poskusite ponovno.',
    loginFailed: 'Prijava ni uspela. Preverite uporabniško ime in geslo.',
  },

  admin: {
    lastLogin: 'Zadnja prijava',
    welcome: 'Dobrodošel, {user}',

    administration: 'Administracija',
  },

  crop: {
    types: {
      type1: 'Glavni posevek',
      typeShort1: 'GP',
      type2: 'Neprezimni posevek',
      typeShort2: 'NP',
      type3: 'Prezimni posevek',
      typeShort3: 'PP',
    },
  },

  fertilizerType: {
    mineral: 'Mineralno gnojilo',
    organic: 'Organsko gnojilo',
  },

  fieldEnvironmentType: {
    outdoorCultivation: 'Pridelava na prostem',
    protectedEnvironment: 'Zaščiten prostor',
  },

  stockType: {
    initialStock: 'Izhodiščno stanje',
    purchase: 'Nabava',
    usage: 'Poraba',
    receipt: 'Prejem',
    sale: 'Oddaja',
  },

  trackingTypes: {
    trackingDevice: 'Sledilna naprava',
    beacon: 'Beacon',
  },

  systemTaskTypes: {
    sowing: 'Setev',
    harvesting: 'Žetev',
    fertilizing: 'Gnojenje',
    pestControl: 'Uporaba FFS',
    other: 'Ostalo',
  },

  cropStatusAfterTask: {
    cropStay: 'Posevek ostane',
    cropRemove: 'Posevek se odstrani',
    userDefined: 'Določi uporabnik pri vnosu opravila',
  },

  unitModuleLocation: {
    taskFertilizerUsage: 'Opravilo - Poraba gnojila',
    taskPesticideUsage: 'Opravilo - Poraba FFS',
    stockFertilizer: 'Urejanje zaloge - Vnos gnojil',
    stockPesticide: 'Urejanje zaloge - Vnos FFS',
    machineryPerformance: 'Mehanizacija - Zmogljivost',
  },

  userRoles: {
    admin: 'Administrator',
    user: 'Uporabnik',
  },
};
