﻿using ITC.FieldBook.Application.Infrastructure.Models;
using ITC.FieldBook.Application.Infrastructure;
using MediatR;
using Microsoft.AspNetCore.DataProtection.KeyManagement;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using ITC.FieldBook.Application.Models;
using ITC.FieldBook.Application.Infrastructure.Services;
using ITC.FieldBook.Application.Infrastructure.Interfaces;
using System.ComponentModel.DataAnnotations;

namespace ITC.FieldBook.Application.Features.Stock.Fertilizers
{
    public class DeleteStockFertilizerController : ApiControllerBase
    {
        [HttpDelete("stock/fertilizers/{stockFertilizerId}")]
        public Task DeleteStockFertilizer([FromRoute] int? stockFertilizerId)
        {
            return Mediator.Send(new DeleteStockFertilizerCommand { StockFertilizerId = stockFertilizerId });
        }
    }

    public class DeleteStockFertilizerCommand : IRequest
    {
        [Required]
        public int? StockFertilizerId { get; set; }
    }

    internal sealed class DeleteStockFertilizerCommandHandler : IRequestHandler<DeleteStockFertilizerCommand>
    {
        private readonly DbContext _ctx;
        private readonly ICurrentUserService _currentUserService;

        public DeleteStockFertilizerCommandHandler(DbContext ctx, ICurrentUserService currentUserService)
        {
            _ctx = ctx;
            _currentUserService = currentUserService;
        }

        public Task Handle(DeleteStockFertilizerCommand request, CancellationToken cancellationToken)
        {
            var stockRepository = new StockRepository(_ctx);
            var stockFertilizer = stockRepository.GetStockFertilizer(request.StockFertilizerId!.Value, _currentUserService.FarmId!.Value).Item;
            if (stockFertilizer != null)
            {
                var user = _currentUserService.GetUser()!;
                _ctx.DeleteItem(stockFertilizer, user);
            }

            return Task.CompletedTask;
        }
    }
}

