﻿using Dapper;
using ITC.FieldBook.Application.Infrastructure;
using ITC.FieldBook.Application.Infrastructure.Interfaces;
using ITC.FieldBook.Application.Infrastructure.Models;
using ITC.FieldBook.Application.Infrastructure.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Localization;
using Microsoft.Extensions.DependencyInjection;
using System.Globalization;
using System.Reflection;

namespace ITC.FieldBook.Application
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddApplication(this IServiceCollection services)
        {
            services.AddMediatR(cfg => cfg.RegisterServicesFromAssembly(Assembly.GetExecutingAssembly()));

            return services;
        }

        public static IServiceCollection AddInfrastructure(this IServiceCollection services)
        {
            // Dapper settings
            //Dapper.EntityFramework.Handlers.Register();
            DefaultTypeMap.MatchNamesWithUnderscores = true;
            SqlMapper.AddTypeHandler(new GeometryHandler(true));
            SqlMapper.AddTypeHandler(new GeometryCollectionHandler(true));
            //SqlMapper.AddTypeHandler(new SqlGeographyHandler());

            // Services
            services.AddAutoMapper(Assembly.GetExecutingAssembly());
            services.AddScoped<DbContext>();
            services.AddScoped<ICurrentUserService, CurrentUserService>();
            services.AddScoped<IFileService, FileService>();
            services.AddSingleton<ITempFileService, TempFileService>();
            services.AddSingleton<ITrackingService, TrackingService>();
            services.AddSingleton<IFarmManagerService, FarmManagerService>();

            return services;
        }
    }
}