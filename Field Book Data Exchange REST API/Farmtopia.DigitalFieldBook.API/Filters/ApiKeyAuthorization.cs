﻿//using Microsoft.AspNetCore.DataProtection.KeyManagement;
//using Microsoft.AspNetCore.Mvc;
//using Microsoft.AspNetCore.Mvc.Filters;

//namespace Farmtopia.DigitalFieldBook.API.Filters
//{
//    public sealed class ApiKeyAuthorizationAttribute : Attribute, IAuthorizationFilter
//    {
//        public void OnAuthorization(AuthorizationFilterContext context)
//        {
//            if (context != null)
//            {
//                // Validate user
//                var authHeader = context.HttpContext.Request.Headers["Authorization"].FirstOrDefault();

//                if (string.IsNullOrEmpty(authHeader))
//                {
//                    // No authorization header present, return unauthorized
//                    context.Result = new UnauthorizedObjectResult("Required authorization header");
//                    return;
//                }

//                if (!authHeader.StartsWith("Bearer ", StringComparison.OrdinalIgnoreCase))
//                {
//                    // Invalid authentication scheme, return unauthorized
//                    context.Result = new UnauthorizedObjectResult("Invalid authorization scheme. Provide API key in format: Bearer <apiKey>");
//                    return;
//                }

//                var headerApiKey = authHeader.Substring("Bearer ".Length).Trim();
//                if (string.IsNullOrWhiteSpace(headerApiKey))
//                {
//                    // Invalid authentication scheme, return unauthorized
//                    context.Result = new UnauthorizedObjectResult("Required ApiKey. Provide API key in format: Bearer <apiKey>");
//                    return;
//                }

//                var configuration = (IConfiguration)context.HttpContext.RequestServices.GetService(typeof(IConfiguration))!;
//                var apiKeys = configuration.GetSection("ApiKeys").Get<IList<string>>();
//                if(!apiKeys.Contains(headerApiKey))
//                {
//                    // Invalid authentication scheme, return unauthorized
//                    context.Result = new UnauthorizedObjectResult("No permission.");
//                    return;
//                }
//            }
//        }
//    }
//}
