﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITC.FieldBook.Application.Models
{
    public class FieldBookInfo
    {
        public List<Farm> Farms { get; set; } = new List<Farm>();

        public Farm? DefaultFarm { get; set; }
    }
}
