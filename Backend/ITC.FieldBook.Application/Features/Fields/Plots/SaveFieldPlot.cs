﻿using ITC.FieldBook.Application.Infrastructure.Models;
using ITC.FieldBook.Application.Infrastructure;
using MediatR;
using Microsoft.AspNetCore.DataProtection.KeyManagement;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using ITC.FieldBook.Application.Models;
using ITC.FieldBook.Application.Features.Crops;
using ITC.FieldBook.Application.Infrastructure.Interfaces;
using DocumentFormat.OpenXml.Bibliography;
using ITC.FieldBook.Application.Features.CropRotation;

namespace ITC.FieldBook.Application.Features.Fields.Crops
{
    public class SaveFieldPlotController : ApiControllerBase
    {
        [HttpPost("fields/plots")]
        public Task SaveFieldPlot([FromBody] FieldPlot data)
        {
            return Mediator.Send(new SaveFieldPlotCommand { Data = data });
        }
    }

    public class SaveFieldPlotCommand : IRequest
    {
        public FieldPlot? Data { get; set; }
    }

    internal sealed class SaveFieldPlotCommandHandler : IRequestHandler<SaveFieldPlotCommand>
    {
        private readonly DbContext _ctx;
        private readonly ICurrentUserService _currentUserService;

        public SaveFieldPlotCommandHandler(DbContext ctx, ICurrentUserService currentUserService)
        {
            _ctx = ctx;
            _currentUserService = currentUserService;
        }

        public Task Handle(SaveFieldPlotCommand request, CancellationToken cancellationToken)
        {
            var data = request.Data;

            if (data != null)
            {
                var user = _currentUserService.GetUser()!;
                var farmId = _currentUserService.FarmId!.Value;
                var fieldsRepository = new FieldsRepository(_ctx);
                var cropsRepository = new CropsRepository(_ctx);
                var cropRotationRepository = new CropRotationRepository(_ctx);

                // In use
                var fieldPlotsInUse = cropRotationRepository.GetCropsInUse(farmId, data.FieldId!.Value, data.Year!.Value);
                if (data.Id != null && fieldPlotsInUse.Any(x => x.FieldPlotId == data.Id))
                    return Task.CompletedTask;

                // Field exists
                var field = fieldsRepository.GetField(farmId, data.FieldId!.Value).Item;
                if (field == null)
                    return Task.CompletedTask;

                // Invalid crop
                var crops = cropsRepository.GetCrops().Data;
                var crop = crops.FirstOrDefault(x => x.Id == data.CropId);
                if (crop == null)
                    return Task.CompletedTask;

                if (data.Area == null)
                    data.Area = field.Area;

                data.CropIdentifier = crop.Identifier;
                data.CropName = crop.Name;

                _ctx.Save(data, user: user);
            }

            return Task.CompletedTask;
        }
    }
}

