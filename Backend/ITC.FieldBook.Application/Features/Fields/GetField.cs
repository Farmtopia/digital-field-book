﻿using ITC.FieldBook.Application.Infrastructure.Interfaces;
using ITC.FieldBook.Application.Infrastructure;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata;
using Dapper;
using ITC.FieldBook.Application.Models;
using ITC.FieldBook.Application.Infrastructure.Models;
using ITC.FieldBook.Application.Infrastructure.Services;
using ITC.FieldBook.Application.Infrastructure.Enums;
using ITC.FieldBook.Application.Models.DTO;
using AutoMapper;
using NetTopologySuite.IO;
using NetTopologySuite.Geometries;
using System.Xml.Schema;
using NetTopologySuite.Features;

namespace ITC.FieldBook.Application.Features.Fields
{
    public class GetFieldController : ApiControllerBase
    {
        [HttpGet("fields/{fieldId}")]
        public Task<DbItem<FieldDTO>> GetField([FromRoute] int fieldId)
        {
            return Mediator.Send(new GetFieldQuery {  FieldId = fieldId });
        }
    }

    public class GetFieldQuery : IRequest<DbItem<FieldDTO>>
    {
        [FromRoute]
        public int FieldId { get; set; }
    }


    internal sealed class GetFieldQueryHandler : IRequestHandler<GetFieldQuery, DbItem<FieldDTO>>
    {
        private readonly DbContext _ctx;
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;
        private readonly ICurrentUserService _currentUserService;

        public GetFieldQueryHandler(DbContext ctx, IConfiguration configuration, ICurrentUserService currentUserService, IMapper mapper)
        {
            _ctx = ctx;
            _configuration = configuration;
            _currentUserService = currentUserService;
            _mapper = mapper;
        }

        public Task<DbItem<FieldDTO>> Handle(GetFieldQuery request, CancellationToken cancellationToken)
        {
            var fieldsRepository = new FieldsRepository(_ctx);
            var field = fieldsRepository.GetField(_currentUserService.FarmId!.Value, request.FieldId).Item;

            if(!string.IsNullOrEmpty(field?.FieldGeometryWkt))
            {
                var wkt = field.FieldGeometryWkt;
                var wkbReader = new WKTReader();
                var geography = wkbReader.Read(wkt);

                var featureCollection = new FeatureCollection();
                if (geography.GeometryType == "GeometryCollection")
                {
                    foreach(var g in (GeometryCollection)geography)
                    {
                        var feature = new Feature();
                        feature.Geometry = g;
                        featureCollection.Add(feature);
                    }
                }
                else
                {
                    var feature = new Feature();
                    feature.Geometry = geography;
                    featureCollection.Add(feature);
                }


                var geoJsonWritter = new GeoJsonWriter();
                field.GeoJSON = geoJsonWritter.Write(featureCollection);
            }

            var fieldDto = _mapper.Map<FieldDTO>(field);

            return Task.FromResult(new DbItem<FieldDTO> {  Item = fieldDto });
        }

    }
}
