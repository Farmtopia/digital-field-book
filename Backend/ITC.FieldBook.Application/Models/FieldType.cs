﻿using ITC.FieldBook.Application.Infrastructure.Interfaces;
using ITC.FieldBook.Application.Infrastructure.Models;
using ITC.FieldBook.Application.Infrastructure.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITC.FieldBook.Application.Models
{
    public class FieldType : DbEntityWithAudit
    {
        public FieldType() : base("FieldBook.field_type") { }

        private int? _id;
        [DbColumn("id", IsKey = true, Identity = true)]
        public int? Id { get => _id; set { _id = value; RaisePropertyChanged(nameof(Id)); } }

        private string? _name;
        [DbColumn("name")]
        public string? Name { get => _name; set { _name = value; RaisePropertyChanged(nameof(Name)); } }

        private string? _nameEn;
        [DbColumn("name_en")]
        public string? NameEn { get => _nameEn; set { _nameEn = value; RaisePropertyChanged(nameof(NameEn)); } }

        private string? _identifier;
        [DbColumn("identifier")]
        public string? Identifier { get => _identifier; set { _identifier = value; RaisePropertyChanged(nameof(Identifier)); } }

        private string? _environmentType;
        [DbColumn("environment_type")]
        public string? EnvironmentType { get => _environmentType; set { _environmentType = value; RaisePropertyChanged(nameof(EnvironmentType)); } }

        private bool _includeInCropRotation;
        [DbColumn("include_in_crop_rotation")]
        public bool IncludeInCropRotation { get => _includeInCropRotation; set { _includeInCropRotation = value; RaisePropertyChanged(nameof(IncludeInCropRotation)); } }
    }
}
