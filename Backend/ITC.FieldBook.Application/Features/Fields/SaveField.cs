﻿using ITC.FieldBook.Application.Infrastructure.Models;
using ITC.FieldBook.Application.Infrastructure;
using MediatR;
using Microsoft.AspNetCore.DataProtection.KeyManagement;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using ITC.FieldBook.Application.Models;
using ITC.FieldBook.Application.Infrastructure.Interfaces;
using Dapper;
using NetTopologySuite.Geometries;
using NetTopologySuite.IO;
using System.Transactions;
using NetTopologySuite.Geometries.Utilities;
using NetTopologySuite.Algorithm;
using Microsoft.Extensions.Configuration;

namespace ITC.FieldBook.Application.Features.Fields
{
    public class SaveFieldController : ApiControllerBase
    {
        [HttpPost("fields")]
        public Task SaveField([FromBody] Field data)
        {
            return Mediator.Send(new SaveFieldCommand { Data = data });
        }
    }

    public class SaveFieldCommand : IRequest
    {
        public Field? Data { get; set; }
    }

    internal sealed class SaveFieldCommandHandler : IRequestHandler<SaveFieldCommand>
    {
        private readonly DbContext _ctx;
        private readonly ICurrentUserService _currentUserService;
        private readonly IConfiguration _configuration;
        private readonly IFileService _fileService;

        public SaveFieldCommandHandler(DbContext ctx, IConfiguration configuration, ICurrentUserService currentUserService, IFileService fileService)
        {
            _ctx = ctx;
            _configuration = configuration;
            _currentUserService = currentUserService;
            _fileService = fileService;
        }

        public Task Handle(SaveFieldCommand request, CancellationToken cancellationToken)
        {
            var user = _currentUserService.GetUser()!;
            var data = request.Data;
            if (data != null)
            {
                var fieldsBll = new FieldsBll(_ctx);
                var farmId = _currentUserService.FarmId!.Value;
                if (data.Id != null)
                {
                    // Validate existing field
                    var fieldsRepo = new FieldsRepository(_ctx);
                    var existingField = fieldsRepo.GetField(farmId, data.Id.Value);
                    if (existingField == null)
                        return Task.CompletedTask;
                }

                data.FarmId = farmId;


                using (var scope = new TransactionScope())
                {
                    var ids = _ctx.Save(data, user: user);
                    var fieldId = Convert.ToInt32(ids["id"]);

                    GeometryCollection? geometryCollection = null;
                    if (!string.IsNullOrEmpty(data.GeoJSON))
                    {
                        var reader = new GeoJsonReader();

                        var fc = reader.Read<NetTopologySuite.Features.FeatureCollection>(data.GeoJSON!);
                        if (fc != null && fc.Count > 0)
                        {
                            var geometries = new List<Geometry>();
                            foreach (var feature in fc)
                            {
                                var geometry = feature.Geometry;
                                if (!Orientation.IsCCW(geometry.Coordinates))
                                    geometry = geometry.Reverse();

                                geometry.SRID = 4326;
                                geometries.Add(geometry);
                            }

                            geometryCollection = new GeometryCollection(geometries.ToArray());
                            geometryCollection.SRID = 4326;
                        }

                        var location = geometryCollection?.Centroid;

                        // If only one, save as single item, else as collection
                        SaveFieldGeometry(fieldId, geometryCollection?.Count == 1 ? geometryCollection[0] : geometryCollection, location);

                        if(location != null)
                            fieldsBll.UpdateImage(_configuration, _fileService, fieldId, location.Y, location.X);
                    }

                    scope.Complete();
                }
            }

            return Task.CompletedTask;
        }

        private void SaveFieldGeometry(int fieldId, Geometry? fieldGeometry, Geometry? location)
        {
            using (var conn = _ctx.CreateConnection())
            {
                conn.Execute(@"
                    UPDATE
                        FieldBook.field
                    SET
                        field_geometry = @fieldGeometry,
                        location = @location
                    WHERE
                        id = @fieldId"
                , new { fieldId, fieldGeometry, location });
            }
        }
    }
}

