﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net.Http.Headers;
using System.Net.Http.Json;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace ITC.FieldBook.Application.Infrastructure.Tools
{
    public class ItcSsoManager
    {
        public Uri ApiUrl { get; set; }

        public ItcSsoManager(string apiUrl)
        {
            ApiUrl = new Uri(apiUrl);
        }

        public JwtData ValidateToken(string token, string key, string issuer)
        {
            var data = new JwtData();
            var validationParameters = new TokenValidationParameters
            {
                ValidateIssuer = true,
                ValidateAudience = true,
                ValidateLifetime = true,
                ValidateIssuerSigningKey = true,
                ValidIssuer = issuer,
                ValidAudience = issuer,
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(key)),
            };

            try
            {
                var tokenHandler = new JwtSecurityTokenHandler();
                tokenHandler.ValidateToken(token, validationParameters, out SecurityToken validatedToken);
                var jwt = (JwtSecurityToken)validatedToken;
                data.Token = jwt;

                var nameIdentifier = jwt.Claims.FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier)?.Value;
                if (string.IsNullOrWhiteSpace(nameIdentifier))
                    throw new Exception("Missing user ID in claims.");
                var user = new JwtUserData();
                user.Uuid = Guid.Parse(nameIdentifier);
                user.Name = jwt.Claims.FirstOrDefault(x => x.Type == ClaimTypes.GivenName)?.Value;
                user.Surname = jwt.Claims.FirstOrDefault(x => x.Type == ClaimTypes.Surname)?.Value;
                user.Displayname = jwt.Claims.FirstOrDefault(x => x.Type == ClaimTypes.Name)?.Value;
                user.Email = jwt.Claims.FirstOrDefault(x => x.Type == ClaimTypes.Email)?.Value;

                data.User = user;
                data.Success = true;
            }
            catch (SecurityTokenValidationException ex)
            {
                data.Success = false;
                data.Message = ex.Message;
            }

            return data;
        }

        public async Task<ItcSsoUser?> GetUser(string token)
        {
            var url = new Uri(ApiUrl, "me");
            using var client = new HttpClient();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            var response = await client.GetAsync(url);

            if (response.IsSuccessStatusCode)
                return await response.Content.ReadFromJsonAsync<ItcSsoUser>();
            return null;
        }
    }

    public class JwtData
    {
        public JwtSecurityToken? Token { get; set; }

        public JwtUserData? User { get; set; }

        public bool Success { get; set; }

        public string? Message { get; set; }
    }

    public class JwtUserData
    {
        public Guid? Uuid { get; set; }

        public string? Name { get; set; }

        public string? Surname { get; set; }

        public string? Displayname { get; set; }

        public string? Email { get; set; }
    }

    public class ItcSsoUser
    {
        public int? Id { get; set; }

        public Guid? Uuid { get; set; }

        public string? Name { get; set; }

        public string? Surname { get; set; }

        public string? Displayname { get; set; }

        public string? Email { get; set; }

        public int? Role { get; set; }

        public DateTime? LastLogin { get; set; }

        public IEnumerable<Application> Applications { get; set; } = Enumerable.Empty<Application>();

        public IEnumerable<UserSetting> Settings { get; set; } = Enumerable.Empty<UserSetting>();
    }

    public class Application
    {
        public int? Id { get; set; }
        public Guid? Uuid { get; set; }
        public string? Name { get; set; }
    }

    public class UserSetting
    {
        public int? UserId { get; set; }

        public string? Name { get; set; }

        public string? Value { get; set; }
    }
}