﻿namespace Farmtopia.DigitalFieldBook.API.Models.Providers
{
    public class Provider
    {
        public int? Id { get; set; }

        public string? Name { get; set; }

        public string? DirectoryUrl { get; set; }

        public string? SecretKey { get; set; }
    }
}
