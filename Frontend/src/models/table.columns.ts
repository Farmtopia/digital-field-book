import { QTableColumn } from 'quasar';
import {
  formatBoolean,
  formatDate,
  formatDecimal,
  formatArea,
  getLocalizedValue,
} from 'src/helpers';
import {
  Crop,
  Document,
  DocumentType,
  Farm,
  Fertilizer,
  Field,
  FieldType,
  Measure,
  Pesticide,
  TaskByField,
} from './fieldbook.models';

import { t } from 'src/boot/i18n';
import { computed } from 'vue';
import {
  FertilizersStockReportItem,
  PesticidesStockReportItem,
} from './fieldbook.models';

export const farmsColumns = computed<QTableColumn<Farm>[]>(() => [
  {
    name: 'identifier',
    label: t('modules.admin.farms.columns.identifier'),
    field: 'identifier',
    sortable: true,
    align: 'left',
    style: 'width: 150px;',
  },
  {
    name: 'name',
    label: t('modules.admin.farms.columns.name'),
    field: 'name',
    sortable: true,
    align: 'left',
  },
  {
    name: 'owner',
    label: t('modules.admin.farms.columns.owner'),
    field: 'owner',
    sortable: true,
    align: 'left',
  },
  {
    name: 'address',
    label: t('modules.admin.farms.columns.address'),
    field: 'address',
    sortable: true,
    align: 'left',
  },
  {
    name: 'city',
    label: t('modules.admin.farms.columns.city'),
    field: (row: Farm) =>
      row.postCode ? `${row.postCode} ${row.city}` : row.city,
    sortable: true,
    align: 'left',
  },
  {
    name: 'ecoFarm',
    label: t('modules.admin.farms.columns.ecoFarm'),
    field: 'ecoFarm',
    format: (data) => formatBoolean(data) ?? '',
    sortable: true,
    align: 'left',
    style: 'width: 50px;',
  },
]);

export const fieldColumns = computed<QTableColumn<Field>[]>(() => [
  {
    name: 'identifier',
    label: t('modules.fields.table.columns.fieldIdentifier'),
    field: 'identifier',
    sortable: true,
    align: 'left',
    style: 'width: 100px;',
  },
  {
    name: 'name',
    label: t('modules.fields.table.columns.fieldName'),
    field: 'name',
    sortable: true,
    align: 'left',
    style: 'width: 250px;',
  },
  {
    name: 'area',
    label: t('modules.fields.table.columns.fieldArea'),
    field: 'area',
    format: (val) => formatArea(val) ?? '',
    sortable: true,
    align: 'right',
    style: 'width: 100px;',
  },
  {
    name: 'typeDisplayname',
    label: t('modules.fields.table.columns.fieldType'),
    field: (row: Field) =>
      `${row.typeIdentifier} ${getLocalizedValue(
        row.typeName,
        row.typeNameEn
      )}`,
    sortable: true,
    align: 'left',
    style: 'width: 100px;',
  },
  {
    name: 'activeCropDisplayname',
    label: t('modules.fields.table.columns.currentCrop'),
    field: (row: Field) => {
      return (
        row.activePlots
          ?.map(
            (x) =>
              `${x.cropIdentifier} ${getLocalizedValue(
                x.cropName,
                x.cropNameEn
              )}`
          )
          .join(', ') ?? ''
      );
    },
    sortable: true,
    align: 'left',
    style: 'width:150px;',
  },
  {
    name: 'lastTask',
    label: t('modules.fields.table.columns.lastTask'),
    field: (row: Field) =>
      row.lastTask
        ? `${formatDate(row.lastTask?.dateFrom)} ${getLocalizedValue(
            row.lastTask?.taskTypeName,
            row.lastTask?.taskTypeNameEn
          )}`
        : '',
    sortable: true,
    align: 'left',
  },
  {
    name: 'distanceFromTarget',
    label: t('modules.fields.table.columns.location'),
    field: 'distanceFromTarget',
    format: (val) => (val != null ? formatDecimal(val) + ' km' : ''),
    sortable: true,
    align: 'center',
    style: 'width: 50px;',
  },
]);

export const cropsColumns = computed<QTableColumn<Crop>[]>(() => [
  {
    name: 'identifier',
    label: t('modules.admin.crops.columns.identifier'),
    field: 'identifier',
    sortable: true,
    align: 'left',
    style: 'width: 150px;',
  },
  {
    name: 'name',
    label: t('modules.admin.crops.columns.name'),
    field: 'name',
    sortable: true,
    align: 'left',
  },
  {
    name: 'nameEn',
    label: t('modules.admin.crops.columns.nameEn'),
    field: 'nameEn',
    sortable: true,
    align: 'left',
  },
]);

export const documentsColumns = computed<QTableColumn<Document>[]>(() => [
  {
    name: 'fileDisplayname',
    label: t('modules.documents.columns.fileDisplayname'),
    field: 'fileDisplayname',
    sortable: true,
    align: 'left',
    style: 'width: 200px;',
  },
  {
    name: 'documentTypeName',
    label: t('modules.documents.columns.documentTypeName'),
    field: (row) =>
      getLocalizedValue(row.documentTypeName, row.documentTypeNameEn),
    sortable: true,
    align: 'left',
    style: 'width: 200px;',
  },
  {
    name: 'description',
    label: t('modules.documents.columns.description'),
    field: 'description',
    sortable: true,
    align: 'left',
  },
  {
    name: 'taskId',
    label: t('modules.documents.columns.task'),
    field: 'taskId',
    sortable: true,
    align: 'center',
    style: 'width: 50px;',
  },
  {
    name: 'created',
    label: t('modules.documents.columns.created'),
    field: 'created',
    format: (val) => formatDate(val, true) ?? '',
    sortable: true,
    align: 'center',
    style: 'width: 50px;',
  },
]);

export const measuresColumns = computed<QTableColumn<Measure>[]>(() => [
  {
    name: 'name',
    label: t('modules.admin.measureTypes.columns.name'),
    field: 'name',
    sortable: true,
    align: 'left',
    style: 'width: 200px;',
  },
  {
    name: 'description',
    label: t('modules.admin.measureTypes.columns.description'),
    field: 'description',
    sortable: true,
    align: 'left',
    style: 'width: 250px;',
  },
  {
    name: 'name',
    label: t('modules.admin.measureTypes.columns.nameEn'),
    field: 'nameEn',
    sortable: true,
    align: 'left',
    style: 'width: 200px;',
  },
  {
    name: 'description',
    label: t('modules.admin.measureTypes.columns.descriptionEn'),
    field: 'descriptionEn',
    sortable: true,
    align: 'left',
    style: 'width: 250px;',
  },
  {
    name: 'category',
    label: t('modules.admin.measureTypes.columns.category'),
    field: 'category',
    sortable: true,
    align: 'left',
    style: 'width: 150px;',
  },
  {
    name: 'yearFrom',
    label: t('modules.admin.measureTypes.columns.yearFrom'),
    field: 'yearFrom',
    sortable: true,
    align: 'center',
    style: 'width:50px;',
  },
  {
    name: 'yearTo',
    label: t('modules.admin.measureTypes.columns.yearTo'),
    field: 'yearTo',
    sortable: true,
    align: 'center',
    style: 'width:50px;',
  },
]);

export const fertilizersStockColumns = computed<
  QTableColumn<FertilizersStockReportItem>[]
>(() => [
  {
    name: 'name',
    label: t('modules.fertilizerStockStatus.columns.fertilizerName'),
    field: (row: Fertilizer) => getLocalizedValue(row.name, row.nameEn),
    sortable: true,
    align: 'left',
  },
  {
    name: 'stock',
    label: t('modules.fertilizerStockStatus.columns.stock'),
    field: 'stock',
    format: (val, row) =>
      (formatDecimal(val) ?? '') + (row.stockUnit ? ` ${row.stockUnit}` : ''),
    sortable: true,
    align: 'right',
  },
]);

export const pesticidesStockColumns = computed<
  QTableColumn<PesticidesStockReportItem>[]
>(() => [
  {
    name: 'name',
    label: t('modules.pesticideStockStatus.columns.pesticideName'),
    field: (row: Pesticide) => getLocalizedValue(row.name, row.nameEn),
    sortable: true,
    align: 'left',
  },
  {
    name: 'stock',
    label: t('modules.pesticideStockStatus.columns.stock'),
    field: 'stock',
    format: (val, row) =>
      (formatDecimal(val) ?? '') + (row.stockUnit ? ` ${row.stockUnit}` : ''),
    sortable: true,
    align: 'right',
  },
]);

export const documentTypesColumns = computed<QTableColumn<DocumentType>[]>(
  () => [
    {
      name: 'name',
      label: t('modules.admin.documentTypes.columns.name'),
      field: 'name',
      sortable: true,
      align: 'left',
    },
    {
      name: 'nameEn',
      label: t('modules.admin.documentTypes.columns.nameEn'),
      field: 'nameEn',
      sortable: true,
      align: 'left',
    },
  ]
);
