﻿using ITC.FieldBook.Application.Infrastructure.Models;
using ITC.FieldBook.Application.Infrastructure;
using MediatR;
using Microsoft.AspNetCore.DataProtection.KeyManagement;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using ITC.FieldBook.Application.Models;
using ITC.FieldBook.Application.Infrastructure.Services;
using ITC.FieldBook.Application.Infrastructure.Interfaces;

namespace ITC.FieldBook.Application.Features.Documents.Types
{
    public class SaveDocumentTypeController : ApiControllerBase
    {
        [HttpPost("documents/document-types")]
        public Task SaveDocumentType([FromBody] DocumentType data)
        {
            return Mediator.Send(new SaveDocumentTypeCommand { Data = data });
        }
    }

    public class SaveDocumentTypeCommand : IRequest
    {
        public DocumentType? Data { get; set; }
    }

    internal sealed class SaveDocumentTypeCommandHandler : IRequestHandler<SaveDocumentTypeCommand>
    {
        private readonly DbContext _ctx;
        private readonly ICurrentUserService _currentUserService;

        public SaveDocumentTypeCommandHandler(DbContext ctx, ICurrentUserService currentUserService)
        {
            _ctx = ctx;
            _currentUserService = currentUserService;
        }

        public Task Handle(SaveDocumentTypeCommand request, CancellationToken cancellationToken)
        {
            if (request.Data != null)
            {
                var user = _currentUserService.GetUser()!;
                _ctx.Save(request.Data, user: user);

            }

            return Task.CompletedTask;
        }
    }
}

