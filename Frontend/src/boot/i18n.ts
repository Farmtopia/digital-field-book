import { boot } from 'quasar/wrappers';
import { createI18n } from 'vue-i18n';
import { Quasar, LocalStorage } from 'quasar';
import { localeOptions } from 'src/i18n/languageSwitcher';

import messages from 'src/i18n';
import { ref, watch } from 'vue';

const qLangList = import.meta.glob(
  '../../node_modules/quasar/lang/(en-US|sl).mjs'
);

console.log(qLangList);
//const langList = import.meta.glob('../../node_modules/quasar/lang/*.js');

export type MessageLanguages = keyof typeof messages;
// Type-define 'en-US' as the master schema for the resource
export type MessageSchema = (typeof messages)['en-US'];

// See https://vue-i18n.intlify.dev/guide/advanced/typescript.html#global-resource-schema-type-definition
/* eslint-disable @typescript-eslint/no-empty-interface */
declare module 'vue-i18n' {
  // define the locale messages schema
  export interface DefineLocaleMessage extends MessageSchema {}

  // define the datetime format schema
  export interface DefineDateTimeFormat {}

  // define the number format schema
  export interface DefineNumberFormat {}
}
/* eslint-enable @typescript-eslint/no-empty-interface */

let locale =
  LocalStorage.getItem('locale')?.toString() ?? Quasar.lang.getLocale(); // returns a string

if (
  !localeOptions.some(function (x) {
    return x.value == locale;
  })
) {
  locale = 'en-US';
}

const i18n = createI18n({
  locale: locale,
  legacy: false,
  messages,
});

const setLocale = ref(i18n.global.locale);

const setQuasarLang = async function (isoName: string) {
  try {
    const lang = await qLangList[
      `../../node_modules/quasar/lang/${isoName}.mjs`
    ]();
    Quasar.lang.set(lang.default);

    document.title = t('title');
  } catch (err) {
    console.error(err);
    // Requested Quasar Language Pack does not exist,
    // let's not break the app, so catching error
  }
};

export default boot(({ app }) => {
  watch(
    () => setLocale.value,
    (newVal) => {
      setQuasarLang(newVal);
      LocalStorage.set('locale', newVal);
    }
  );

  setQuasarLang(setLocale.value);

  // Set i18n instance on app
  app.use(i18n);
});

const t = i18n?.global.t;

export { t, i18n };
