﻿using ITC.FieldBook.Application.Infrastructure.Models;
using ITC.FieldBook.Application.Infrastructure;
using MediatR;
using Microsoft.AspNetCore.DataProtection.KeyManagement;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using ITC.FieldBook.Application.Models;
using ITC.FieldBook.Application.Infrastructure.Interfaces;
using Microsoft.AspNetCore.Authorization;
using ITC.FieldBook.Application.Infrastructure.Enums;

namespace ITC.FieldBook.Application.Features.Fields.Types
{
    [Authorize(Roles = EnumRole.Admin)]
    public class SaveFieldTypeController : ApiControllerBase
    {
        [HttpPost("fields/field-types")]
        public Task SaveFieldType([FromBody] FieldType data)
        {
            return Mediator.Send(new SaveFieldTypeCommand { Data = data });
        }
    }

    public class SaveFieldTypeCommand : IRequest
    {
        public FieldType? Data { get; set; }
    }

    internal sealed class SaveFieldTypeCommandHandler : IRequestHandler<SaveFieldTypeCommand>
    {
        private readonly DbContext _ctx;
        private readonly ICurrentUserService _currentUserService;

        public SaveFieldTypeCommandHandler(DbContext ctx, ICurrentUserService currentUserService)
        {
            _ctx = ctx;
            _currentUserService = currentUserService;
        }

        public Task Handle(SaveFieldTypeCommand request, CancellationToken cancellationToken)
        {
            if (request.Data != null)
            {
                var user = _currentUserService.GetUser()!;
                _ctx.Save(request.Data, user: user);
            }

            return Task.CompletedTask;
        }
    }
}

