﻿using ITC.FieldBook.Application.Enums;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITC.FieldBook.Application
{
    public sealed class Globals
    {
        private static readonly Lazy<Globals> instance = new Lazy<Globals>(() => new Globals());

        public static Globals Instance
        {
            get
            {
                return instance.Value;
            }
        }

        public static void Initialize(IConfiguration configuration)
        {
            if (configuration.GetValue<string>("AppEnvironment") is string appEnvConf && !string.IsNullOrEmpty(appEnvConf))
                AppEnvironment = appEnvConf;
            else
                AppEnvironment = EnumAppEnvironment.DEFAULT;

            if (configuration.GetValue<string>("LoginType") is string loginTypeConf && !string.IsNullOrEmpty(loginTypeConf))
                LoginType = loginTypeConf;
            else
                LoginType = EnumLoginType.LOCAL_LOGIN;
        }

        public static string? AppEnvironment { get; set; }

        public static string? LoginType { get; set; }

        public bool HasLoginType(string loginType)
        {
            return LoginType?.Contains(loginType) ?? false;
        }
    }
}
