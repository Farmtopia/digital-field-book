﻿using ITC.FieldBook.Application.Infrastructure.Interfaces;
using ITC.FieldBook.Application.Infrastructure.Tools;
using ITC.FieldBook.Application.Infrastructure;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using ClosedXML.Excel;
using DocumentFormat.OpenXml.Spreadsheet;
using Microsoft.AspNetCore.Authorization;
using System.Data;
using ITC.FieldBook.Application.Models;
using Dapper;
using DocumentFormat.OpenXml.Wordprocessing;
using Microsoft.Extensions.Localization;
using Irony;
using ITC.FieldBook.Application.Resources;

namespace ITC.FieldBook.Application.Features.Reports
{
    public class GetKopopReportontroller : ApiControllerBase
    {
        [HttpGet("reports/kopop")]
        public async Task<IActionResult> GetKopopReport([FromQuery] int[]? fieldIds, int? year, bool exportCrops, bool exportMeasures, bool exportTasks)
        {
            var file = await Mediator.Send(new GetKopopReportQuery() { FieldIds = fieldIds?.ToList(), Year = year, ExportCrops = exportCrops, ExportMeasures = exportMeasures, ExportTasks = exportTasks });

            if (file == null || file.Filename == null || file.Bytes == null)
                return NotFound();

            return File(file.Bytes, file.ContentType, file.Filename);
        }
    }

    public class GetKopopReportQuery : IRequest<Infrastructure.Models.File>
    {
        public List<int>? FieldIds { get; set; }

        public int? Year { get; set; }

        public bool ExportCrops { get; set; }

        public bool ExportMeasures { get; set; }

        public bool ExportTasks { get; set; }
    }

    internal sealed class GetKopopReportQueryHandler : IRequestHandler<GetKopopReportQuery, Infrastructure.Models.File>
    {
        private readonly DbContext _ctx;
        private readonly IConfiguration _configuration;
        private readonly ICurrentUserService _currentUserService;
        private readonly IStringLocalizer<Localization> _localizer;


        public GetKopopReportQueryHandler(DbContext ctx, IConfiguration configuration, ICurrentUserService currentUserService, IStringLocalizer<Localization> localizer)
        {
            _ctx = ctx;
            _configuration = configuration;
            _currentUserService = currentUserService;
            _localizer = localizer;
        }

        public Task<Infrastructure.Models.File> Handle(GetKopopReportQuery request, CancellationToken cancellationToken)
        {
            var locale = _currentUserService.Locale;
            var year = request.Year ?? DateTime.Now.Year;
            //var exportManager = new ExcelExportManager();


            var baseReportBytes = Properties.Resources.Evidence_enotne_verzija_2_5;

            using (MemoryStream stream = new MemoryStream(baseReportBytes))
            {
                // Load the workbook from the stream
                using (XLWorkbook wb = new XLWorkbook(stream))
                {
                    // Now you can work with the workbook, e.g., access sheets, cells, etc.
                    var worksheet = wb.Worksheet(1); // Access the first worksheet

                    // Search for the word in the entire worksheet
                    var matchingCells = worksheet.CellsUsed(cell => cell.GetValue<string>().Contains("{Farm.Identifier}", StringComparison.OrdinalIgnoreCase));

                    // Loop through the matching cells and print the address
                    foreach (var cell in matchingCells)
                    {
                        cell.SetValue("MID1234567");
                    }

                    var exportManager = new ExcelExportManager();
                    var bytes = exportManager.Export(wb);

                    return Task.FromResult(new Infrastructure.Models.File { Bytes = bytes, Filename = "Evidence-enotne-verzija-2_5.xlsx" });
                }
            }
        }

        public IEnumerable<TaskReportTaskItem> GetTasks(int farmId, int year, List<int>? fieldIds = null)
        {
            using (var conn = _ctx.CreateConnection())
            {
                var data = conn.Query<TaskReportTaskItem>($@"
                SELECT 
                    t.id,
                    t.farm_id,
                    t.date_from,
                    t.date_to,
                    t.executor,
                    t.note,
                    t.treatment_successful,
                    t.harvest_amount,
                    tt.name AS task_type_name,
                    tt.name_en AS task_type_name_en,
                    tf.duration,
                    f.identifier AS field_identifier,
                    f.name AS field_name,
                    c.identifier AS crop_identifier,
                    c.name AS crop_name,
                    c.name_en AS crop_name_en,
	                ISNULL(fp.area, f.area) AS field_area,
                    m.name AS machine_name,
                    att.name AS attachment_name
                FROM FieldBook.task t
	                JOIN FieldBook.task_field tf ON tf.task_id = t.id AND {DbContext.GetIsDeletedIsActiveCondition("tf")}
	                LEFT JOIN FieldBook.field_plot fp ON tf.field_plot_id = fp.id
                    LEFT JOIN FieldBook.crop c ON c.id = fp.crop_id
	                JOIN FieldBook.field f ON tf.field_id = f.id
                    LEFT JOIN FieldBook.task_type tt ON tt.id = t.task_type_id
                    LEFT JOIN FieldBook.machinery m ON m.id = t.equipment_id
                    LEFT JOIN FieldBook.machinery att ON att.id = t.attached_equipment_id
                WHERE
                    {DbContext.GetIsDeletedIsActiveCondition("t")}
                    AND t.farm_id = @farmId
                    AND YEAR(t.date_from) = @year
                    {((fieldIds?.Any() ?? false) ? "AND f.id IN @fieldIds" : "")}
                ORDER BY t.date_from DESC",
                new { farmId, year, fieldIds });

                return data;
            }
        }

        public IEnumerable<TaskReportMeasureItem> GetMeasures(int farmId, int year, List<int>? fieldIds = null)
        {
            using (var conn = _ctx.CreateConnection())
            {
                var data = conn.Query<TaskReportMeasureItem>($@"
                SELECT
                    fm.year,
	                f.identifier AS field_identifier,
	                f.name AS field_name,
	                c.identifier AS crop_identifier,
	                c.name AS crop_name,
                    c.name_en AS crop_name_en,
	                m.name AS measure_name,
                    m.name_en AS measure_name_en,
	                m.description AS measure_description,
	                m.description_en AS measure_description_en
                FROM FieldBook.field_measure fm
	                JOIN FieldBook.field_plot fp ON fm.field_plot_id = fp.id
	                JOIN FieldBook.field f ON fp.field_id = f.id
	                JOIN FieldBook.crop c ON fp.crop_id = c.id
	                JOIN FieldBook.measure m ON fm.measure_id = m.id
                WHERE
                    {DbContext.GetIsDeletedIsActiveCondition("fm")}
                    AND f.farm_id = @farmId
                    AND fm.year = @year
                    {((fieldIds?.Any() ?? false) ? "AND f.id IN @fieldIds" : "")}
                ORDER BY f.name ASC",
                new { farmId, year, fieldIds });

                return data;
            }
        }


        public IEnumerable<TaskReportCropItem> GetCrops(int farmId, int year, List<int>? fieldIds = null)
        {
            using (var conn = _ctx.CreateConnection())
            {
                var data = conn.Query<TaskReportCropItem>($@"
                 SELECT
	                fp.year,
	                fp.crop_type,
	                fp.area,
	                f.identifier AS field_identifier,
	                f.name AS field_name,
	                c.identifier AS crop_identifier,
	                c.name AS crop_name,
                    c.name_en AS crop_name_en
                 FROM FieldBook.field_plot fp
                     JOIN FieldBook.field f ON fp.field_id = f.id
                     JOIN FieldBook.crop c ON fp.crop_id = c.id
                WHERE
                    {DbContext.GetIsDeletedIsActiveCondition("fp")}
                    AND f.farm_id = @farmId
                    AND fp.year = @year
                    {((fieldIds?.Any() ?? false) ? "AND f.id IN @fieldIds" : "")}
                ORDER BY f.name ASC",
                new { farmId, year, fieldIds });

                return data;
            }
        }
    }
}