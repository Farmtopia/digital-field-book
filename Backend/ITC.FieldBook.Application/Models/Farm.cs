﻿using ITC.FieldBook.Application.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITC.FieldBook.Application.Models
{
    public class Farm : DbEntityWithAudit
    {
        public Farm() : base("FieldBook.farm") { }
                
        private int? _id;
        [DbColumn("id", IsKey = true, Identity = true)]
        public int? Id { get => _id; set { _id = value; RaisePropertyChanged(nameof(Id)); } }

        private string? _name;
        [DbColumn("name")]
        public string? Name { get => _name; set { _name = value; RaisePropertyChanged(nameof(Name)); } }

        private string? _owner;
        [DbColumn("owner")]
        public string? Owner { get => _owner; set { _owner = value; RaisePropertyChanged(nameof(Owner)); } }

        private string? _address;
        [DbColumn("address")]
        public string? Address { get => _address; set { _address = value; RaisePropertyChanged(nameof(Address)); } }

        private string? _postCode;
        [DbColumn("post_code")]
        public string? PostCode { get => _postCode; set { _postCode = value; RaisePropertyChanged(nameof(PostCode)); } }

        private string? _city;
        [DbColumn("city")]
        public string? City { get => _city; set { _city = value; RaisePropertyChanged(nameof(City)); } }

        private string? _identifier;
        [DbColumn("identifier")]
        public string? Identifier { get => _identifier; set { _identifier = value; RaisePropertyChanged(nameof(Identifier)); } }

        private bool _ecoFarm = false;
        [DbColumn("eco_farm")]
        public bool EcoFarm { get => _ecoFarm; set { _ecoFarm = value; RaisePropertyChanged(nameof(EcoFarm)); } }

        private int? _thumbFileId;
        [DbColumn("thumb_file_id")]
        public int? ThumbFileId { get => _thumbFileId; set { _thumbFileId = value; RaisePropertyChanged(nameof(ThumbFileId)); } }

        public double? Latitude { get; set; }

        public double? Longitude { get; set; }
    }
}
