﻿using ITC.FieldBook.Application.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITC.FieldBook.Application.Models
{
    public class UserCropSetting : DbEntityWithAudit
    {
        public UserCropSetting() : base("FieldBook.user_crop_setting") { }

        private int? _userId;
        [DbColumn("user_id", IsKey = true)]
        public int? UserId { get => _userId; set { _userId = value; RaisePropertyChanged(nameof(UserId)); } }

        private int? _cropId;
        [DbColumn("crop_id", IsKey = true)]
        public int? CropId { get => _cropId; set { _cropId = value; RaisePropertyChanged(nameof(CropId)); } }

        private bool _isFavorite;
        [DbColumn("is_favorite")]
        public bool IsFavorite { get => _isFavorite; set { _isFavorite = value; RaisePropertyChanged(nameof(IsFavorite)); } }

        private string? _hexColor;
        [DbColumn("hex_color")]
        public string? HexColor { get => _hexColor; set { _hexColor = value; RaisePropertyChanged(nameof(HexColor)); } }
    }
}
