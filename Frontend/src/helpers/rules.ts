import { t } from 'src/boot/i18n';
import { formatDate } from '.';

export const rules = {
  required: (val: unknown) => {
    return (
      (!Array.isArray(val) && !!val) ||
      (Array.isArray(val) && val.length > 0) ||
      t('validationMessages.requiredField')
    );
  },
  requiredIf: (val: unknown, condition: boolean) => {
    return (
      !condition ||
      (val !== undefined && val !== null && val !== '') ||
      (Array.isArray(val) && val.length > 0) ||
      t('validationMessages.requiredField')
    );
  },
  minLength: (
    val: string | null | undefined,
    minLength: number,
    message = t('validationMessages.minLength', { minLength })
  ) => (val && val.length >= minLength) || message,

  match: (
    val1: unknown,
    val2: unknown,
    message = t('validationMessages.valuesMustMatch')
  ) => {
    if ((val1 && !val2) || val1 != val2) return message;
    return true;
  },
  compareDate: (
    val1: Date | string | null | undefined,
    val2: Date | string | null | undefined,
    condition: 'gt' | 'geq'
  ) => {
    if (val1 == null) {
      return true;
    } else if (val2 == null) {
      return t('validationMessages.requiredField');
    }

    const d1 = typeof val1 == 'string' ? val1.toDate() : val1;
    const d2 = typeof val2 == 'string' ? val2.toDate() : val2;

    if (condition == 'geq' && !(d1.getTime() >= d2.getTime())) {
      return t('validationMessages.greatherThanOrEqual', {
        value: formatDate(d2, true),
      });
    } else {
    }

    if (condition == 'gt' && !(d1.getTime() > d2.getTime())) {
      return t('validationMessages.greatherThan', {
        value: formatDate(d2, true),
      });
    }

    return true;
  },
};

export default rules;
