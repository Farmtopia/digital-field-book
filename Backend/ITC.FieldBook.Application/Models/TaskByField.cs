﻿using ITC.FieldBook.Application.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITC.FieldBook.Application.Models
{
    public class TaskByField : DbEntity
    {
        public int? TaskId { get; set; }

        public int? Year { get; set; }

        public DateTime? DateFrom { get; set; }

        public DateTime? DateTo { get; set; }

        public int? FieldId { get; set; }

        public string? FieldIdentifier { get; set; }

        public string? FieldName { get; set; }

        public string? CropIdentifier { get; set; }

        public string? CropName { get; set; }

        public string? CropNameEn { get; set; }

        public string? TaskTypeName { get; set; }

        public string? TaskTypeNameEn { get; set; }

        public string? MachineName { get; set; }

        public string? AttachmentName { get; set; }

        public string? Executor { get; set; }

        public string? Note { get; set; }
    }
}
