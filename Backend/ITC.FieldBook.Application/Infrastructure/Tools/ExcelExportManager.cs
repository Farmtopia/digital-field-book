﻿using ClosedXML.Excel;
using DocumentFormat.OpenXml.Spreadsheet;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITC.FieldBook.Application.Infrastructure.Tools
{
    public class ExcelExportManager
    {
        public static readonly XLTableTheme DefaultTheme = XLTableTheme.TableStyleLight11;
                
        public void AdjustToContents(IXLWorksheet ws)
        {
            foreach (var c in ws.Columns())
            {
                c.AdjustToContents();
            }
        }

        public byte[] Export(DataTable dataTable, string sheetName = "List 1")
        {
            XLWorkbook wb = new XLWorkbook();
            IXLWorksheet ws = wb.Worksheets.Add(dataTable, sheetName);
            ws.Unprotect();

            ws.Table(0).Theme = DefaultTheme;

            AdjustToContents(ws);

            var workbookBytes = Export(wb);

            return workbookBytes;
        }

        public byte[] Export(XLWorkbook xLWorkbook)
        {
            var workbookBytes = new byte[0];
            using (var ms = new MemoryStream())
            {
                xLWorkbook.SaveAs(ms);
                workbookBytes = ms.ToArray();
            }

            return workbookBytes;
        }

        public string GetExportFilename()
        {
            return $"Export_{DateTime.Now:dd.MM.yyyy HHmmss}.xlsx";
        }
    }
}
