﻿using AutoMapper;

namespace Farmtopia.DigitalFieldBook.API.Models
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<FieldEntity, Field>()
                .AfterMap((src, dest) =>
                {
                    if (dest.FieldType?.Uuid == null)
                        dest.FieldType = null;
                });

            CreateMap<FieldCropEntity, CropGeneric>()
                .ForPath(x => x.Crop!.Uuid, opts =>
                {
                    opts.MapFrom(y => y.CropUuid);
                })
                .AfterMap((src, dest) =>
                {
                    if (dest.Crop?.Uuid == null)
                        dest.Crop = null;
                });

            CreateMap<TaskEntity, Task>()
                .ForPath(x => x.TaskType!.Id, opts =>
                {
                    opts.MapFrom(y => y.TaskTypeId);
                })
                .AfterMap((src, dest) =>
                {
                    if (dest.TaskType?.Id == null)
                        dest.TaskType = null;
                });

            CreateMap<TaskFieldEntity, TaskField>()
                .ForPath(x => x.Crop!.Uuid, opts =>
                {
                    opts.MapFrom(y => y.CropUuid);
                })
                .AfterMap((src, dest) =>
                {
                    if (dest.Crop?.Uuid == null)
                        dest.Crop = null;
                });

            CreateMap<CropEntity, Crop>();
            CreateMap<FarmEntity, Farm>();
            CreateMap<FieldTypeEntity, FieldType>();
            CreateMap<TaskFertilizerEntity, TaskFertilizer>();
            CreateMap<TaskPesticideEntity, TaskPesticide>();
            CreateMap<TaskTypeEntity, TaskType>();
        }
    }
}
