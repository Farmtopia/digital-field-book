﻿using ITC.FieldBook.Application.Infrastructure.Models;
using Microsoft.AspNetCore.DataProtection.KeyManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Dapper.SqlMapper;

namespace ITC.FieldBook.Application.Models
{
    public class Unit : DbEntityWithAudit
    {
        public Unit() : base("FieldBook.unit") { }

        private int? _id;
        [DbColumn("id", IsKey = true, Identity = true)]
        public int? Id { get => _id; set { _id = value; RaisePropertyChanged(nameof(Id)); } }

        private string? _name;
        [DbColumn("name")]
        public string? Name { get => _name; set { _name = value; RaisePropertyChanged(nameof(Name)); } }

        private string? _description;
        [DbColumn("description")]
        public string? Description { get => _description; set { _description = value; RaisePropertyChanged(nameof(Description)); } }

        private string? _descriptionEn;
        [DbColumn("description_en")]
        public string? DescriptionEn { get => _descriptionEn; set { _descriptionEn = value; RaisePropertyChanged(nameof(DescriptionEn)); } }

        public string? GroupsDisplayValue { get; set; }

        public IEnumerable<UnitGroup> Groups { get; set; } = new List<UnitGroup>();

        public string GetUnitPerAreaName()
        {
            return Name + "/ha";
        }

        public bool IsUnitPerArea()
        {
            return Name?.EndsWith("/ha") ?? false;
        }

        public string? GetBaseUnit()
        {
            return Name?.Replace("/ha", "");
        }
    }
}
