PRINT 'INSERTING DATA...'
GO

USE [fieldbook-db]
GO

SET IDENTITY_INSERT [FieldBook].[farm] ON 
GO
INSERT [FieldBook].[farm] ([id], [uuid], [name], [owner], [address], [post_code], [city], [identifier], [eco_farm], [location], [farm_geometry], [thumb_file_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (1, N'6890e576-9648-49d2-b5a9-caf3539e3280', N'Farmtopia Demo Farm', N'ITC Murska Sobota', N'Lendavska 5a', N'9000', N'Murska Sobota', N'000000001', 0, 0xE6100000010CBC6D6DE20B554740B0B7CE0B5D2A3040, NULL, 1, CAST(N'2024-09-09T12:08:56.477' AS DateTime), 1, CAST(N'2024-09-09T12:09:19.717' AS DateTime), 1, 0, 1)
GO
SET IDENTITY_INSERT [FieldBook].[farm] OFF
GO

SET IDENTITY_INSERT [FieldBook].[file] ON 
GO
INSERT [FieldBook].[file] ([id], [uuid], [farm_id], [displayname], [filename], [size], [extension], [storage_type], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (1, N'08c317bf-2ed8-4361-9d4f-8d89a8531b22', 1, N'Image.jpg', N'/app/Files/Farms/Images/1/760ab24a70634c36a70d956a09df1ecc_Image.jpg', 179815, N'jpg', N'FILESYSTEM', CAST(N'2024-09-09T12:09:21.833' AS DateTime), 1, CAST(N'2024-09-09T12:09:21.833' AS DateTime), 1, 0, 1)
GO
INSERT [FieldBook].[file] ([id], [uuid], [farm_id], [displayname], [filename], [size], [extension], [storage_type], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (2, N'9bb228c2-36a0-46cd-b2a0-fe87936bca4f', 1, N'Image.jpg', N'/app/Files/Fields/Images/1/8959dbef3fe843ce8ecdd17a287906b3_Image.jpg', 117710, N'jpg', N'FILESYSTEM', CAST(N'2024-09-09T12:15:08.017' AS DateTime), 1, CAST(N'2024-09-09T12:15:08.017' AS DateTime), 1, 0, 1)
GO
INSERT [FieldBook].[file] ([id], [uuid], [farm_id], [displayname], [filename], [size], [extension], [storage_type], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (3, N'd2370ae2-bc34-4d5b-a419-6b0117a5a102', 1, N'Image.jpg', N'/app/Files/Fields/Images/2/ccc06fa1f37d4187bf3712d11f509936_Image.jpg', 113154, N'jpg', N'FILESYSTEM', CAST(N'2024-09-09T12:15:38.800' AS DateTime), 1, CAST(N'2024-09-09T12:15:38.800' AS DateTime), 1, 0, 1)
GO
INSERT [FieldBook].[file] ([id], [uuid], [farm_id], [displayname], [filename], [size], [extension], [storage_type], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (4, N'87c6a334-641b-4339-8114-5f632b973002', 1, N'Image.jpg', N'/app/Files/Fields/Images/3/c98819e5603b45d78f48df781829d65b_Image.jpg', 104097, N'jpg', N'FILESYSTEM', CAST(N'2024-09-09T12:16:05.663' AS DateTime), 1, CAST(N'2024-09-09T12:16:05.663' AS DateTime), 1, 0, 1)
GO
SET IDENTITY_INSERT [FieldBook].[file] OFF
GO

SET IDENTITY_INSERT [FieldBook].[field] ON 
GO
INSERT [FieldBook].[field] ([id], [uuid], [farm_id], [identifier], [name], [description], [type_id], [area], [location], [field_geometry], [image_file_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (1, N'a2c2f0c4-778a-4012-ab62-4ed0e4dd5185', 1, N'001', N'Field 1', NULL, 1, CAST(153.97 AS Decimal(8, 2)), 0xE6100000010C47988DBDB8554740675CC5B8342B3040, 0xE6100000010405000000F8DCCF1C9E554740B0F61F90F52A3040A412B5A493554740A07A3F06262B3040F0EEEBC3D255474040D12534732B3040E4923977DE55474060BA8289442B3040F8DCCF1C9E554740B0F61F90F52A304001000000020000000001000000FFFFFFFF0000000003, 2, CAST(N'2024-09-09T12:15:05.840' AS DateTime), 1, CAST(N'2024-09-09T12:15:05.840' AS DateTime), 1, 0, 1)
GO
INSERT [FieldBook].[field] ([id], [uuid], [farm_id], [identifier], [name], [description], [type_id], [area], [location], [field_geometry], [image_file_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (2, N'7b395f2c-b17d-4f55-80cc-96c9d4a00d8b', 1, N'002', N'Field 2', NULL, 1, CAST(695.58 AS Decimal(8, 2)), 0xE6100000010C7381B9EE915447407D168F35EA253040, 0xE610000001040600000048C9CFCF78544740604309076125304094A33A785E5447401096D2BC49263040B45569A7B1544740581299835B26304070A837F4C3544740383C0F1AB92530407C96A393BD544740688C30D29C25304048C9CFCF78544740604309076125304001000000020000000001000000FFFFFFFF0000000003, 3, CAST(N'2024-09-09T12:15:37.557' AS DateTime), 1, CAST(N'2024-09-09T12:15:37.557' AS DateTime), 1, 0, 1)
GO
INSERT [FieldBook].[field] ([id], [uuid], [farm_id], [identifier], [name], [description], [type_id], [area], [location], [field_geometry], [image_file_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (3, N'98c10286-db61-4e60-8512-145acc65dd17', 1, N'003', N'Field 3', NULL, 1, CAST(1434.73 AS Decimal(8, 2)), 0xE6100000010C56ED03325C554740F03E5536A8303040, 0xE61000000104050000009CE2622751554740186FE1728E2F3040FCC308D421554740588C122E8D313040744939FB78554740305C41E98F31304058B93C418B554740602BFDE0BC2F30409CE2622751554740186FE1728E2F304001000000020000000001000000FFFFFFFF0000000003, 4, CAST(N'2024-09-09T12:16:03.673' AS DateTime), 1, CAST(N'2024-09-09T12:16:03.673' AS DateTime), 1, 0, 1)
GO
SET IDENTITY_INSERT [FieldBook].[field] OFF
GO

SET IDENTITY_INSERT [FieldBook].[field_plot] ON 
GO
INSERT [FieldBook].[field_plot] ([id], [uuid], [field_id], [year], [crop_type], [crop_id], [crop_variety], [area], [start_date], [start_task_id], [sowing_date], [sowing_task_id], [harvest_date], [harvest_task_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (1, N'962d6a3d-2fc1-4d16-95d9-356a19cc7b6d', 1, 2024, 1, 111, NULL, CAST(153.97 AS Decimal(8, 2)), NULL, NULL, CAST(N'2024-03-06T00:00:00.000' AS DateTime), 4, CAST(N'2024-09-19T00:00:00.000' AS DateTime), 13, CAST(N'2024-09-19T09:59:06.737' AS DateTime), 1, CAST(N'2024-09-19T09:59:06.737' AS DateTime), 1, 0, 1)
GO
INSERT [FieldBook].[field_plot] ([id], [uuid], [field_id], [year], [crop_type], [crop_id], [crop_variety], [area], [start_date], [start_task_id], [sowing_date], [sowing_task_id], [harvest_date], [harvest_task_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (2, N'6e2b8307-e105-4a6b-9741-9505836c8680', 2, 2024, 1, 133, NULL, CAST(695.58 AS Decimal(8, 2)), NULL, NULL, CAST(N'2024-04-10T00:00:00.000' AS DateTime), 5, CAST(N'2024-09-19T00:00:00.000' AS DateTime), 13, CAST(N'2024-09-19T09:59:15.557' AS DateTime), 1, CAST(N'2024-09-19T09:59:15.557' AS DateTime), 1, 0, 1)
GO
INSERT [FieldBook].[field_plot] ([id], [uuid], [field_id], [year], [crop_type], [crop_id], [crop_variety], [area], [start_date], [start_task_id], [sowing_date], [sowing_task_id], [harvest_date], [harvest_task_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (3, N'e118a6ae-640a-4037-ab78-508bc4dd7135', 3, 2024, 1, 116, NULL, CAST(1434.73 AS Decimal(8, 2)), NULL, NULL, CAST(N'2024-03-06T00:00:00.000' AS DateTime), 3, CAST(N'2024-09-19T00:00:00.000' AS DateTime), 13, CAST(N'2024-09-19T09:59:26.960' AS DateTime), 1, CAST(N'2024-09-19T09:59:26.960' AS DateTime), 1, 0, 1)
GO
SET IDENTITY_INSERT [FieldBook].[field_plot] OFF
GO

SET IDENTITY_INSERT [FieldBook].[machinery] ON 
GO
INSERT [FieldBook].[machinery] ([id], [uuid], [farm_id], [name], [description], [performance], [performance_unit_id], [performance_unit], [classification_id], [tracking_id], [tracking_type], [farm_manager_id], [farm_manager_name], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (263, N'65de1be4-6ad6-41a0-ad63-dd4f74357de7', 1, N'Tractor', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, CAST(N'2024-09-09T12:12:43.730' AS DateTime), 1, CAST(N'2024-09-09T12:12:43.730' AS DateTime), 1, 0, 1)
GO
INSERT [FieldBook].[machinery] ([id], [uuid], [farm_id], [name], [description], [performance], [performance_unit_id], [performance_unit], [classification_id], [tracking_id], [tracking_type], [farm_manager_id], [farm_manager_name], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (264, N'a244f398-9667-4e98-b9c6-ee45ce6d9e33', 1, N'Plow', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, CAST(N'2024-09-09T12:12:48.620' AS DateTime), 1, CAST(N'2024-09-09T12:12:48.620' AS DateTime), 1, 0, 1)
GO
INSERT [FieldBook].[machinery] ([id], [uuid], [farm_id], [name], [description], [performance], [performance_unit_id], [performance_unit], [classification_id], [tracking_id], [tracking_type], [farm_manager_id], [farm_manager_name], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (265, N'551f7143-8c53-4f7f-9903-a98145dafa7c', 1, N'Seeder', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, CAST(N'2024-09-09T12:13:38.920' AS DateTime), 1, CAST(N'2024-09-09T12:13:38.920' AS DateTime), 1, 0, 1)
GO
INSERT [FieldBook].[machinery] ([id], [uuid], [farm_id], [name], [description], [performance], [performance_unit_id], [performance_unit], [classification_id], [tracking_id], [tracking_type], [farm_manager_id], [farm_manager_name], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (266, N'25051184-246a-4e03-b05e-3014a317ad7e', 1, N'Sprayer', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, CAST(N'2024-09-09T12:13:51.633' AS DateTime), 1, CAST(N'2024-09-09T12:13:51.633' AS DateTime), 1, 0, 1)
GO
INSERT [FieldBook].[machinery] ([id], [uuid], [farm_id], [name], [description], [performance], [performance_unit_id], [performance_unit], [classification_id], [tracking_id], [tracking_type], [farm_manager_id], [farm_manager_name], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (267, N'9fa0a9c5-c5a7-4944-87b3-0a8fb0cf380d', 1, N'Trailer', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, CAST(N'2024-09-09T12:14:07.807' AS DateTime), 1, CAST(N'2024-09-09T12:14:07.807' AS DateTime), 1, 0, 1)
GO
INSERT [FieldBook].[machinery] ([id], [uuid], [farm_id], [name], [description], [performance], [performance_unit_id], [performance_unit], [classification_id], [tracking_id], [tracking_type], [farm_manager_id], [farm_manager_name], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (268, N'759dc7e7-768f-487a-a5bf-10a4ee444ab1', 1, N'Combine harvester', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, CAST(N'2024-09-09T12:14:11.753' AS DateTime), 1, CAST(N'2024-09-09T12:14:11.753' AS DateTime), 1, 0, 1)
GO
SET IDENTITY_INSERT [FieldBook].[machinery] OFF
GO

SET IDENTITY_INSERT [FieldBook].[task] ON 
GO
INSERT [FieldBook].[task] ([id], [uuid], [farm_id], [year], [date_from], [date_to], [task_type_id], [crop_status_after], [crop_id], [executor], [description], [note], [equipment_id], [attached_equipment_id], [treatment_successful], [harvest_amount], [tracking_trip_id], [data], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (1, N'e7fa8d2f-b35f-4d46-8152-86d1810eda7f', 1, 2024, CAST(N'2024-03-02T00:00:00.000' AS DateTime), CAST(N'2024-03-02T00:00:00.000' AS DateTime), 1, N'CROP_REMOVE', NULL, N'Farmtopia Admin', NULL, NULL, 263, 264, NULL, NULL, NULL, NULL, CAST(N'2024-09-19T10:00:08.610' AS DateTime), 1, CAST(N'2024-09-19T10:05:01.833' AS DateTime), 1, 0, 1)
GO
INSERT [FieldBook].[task] ([id], [uuid], [farm_id], [year], [date_from], [date_to], [task_type_id], [crop_status_after], [crop_id], [executor], [description], [note], [equipment_id], [attached_equipment_id], [treatment_successful], [harvest_amount], [tracking_trip_id], [data], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (2, N'6443e45a-d9ae-4994-9365-25db5dee93bb', 1, 2024, CAST(N'2024-03-05T00:00:00.000' AS DateTime), CAST(N'2024-03-05T00:00:00.000' AS DateTime), 3, N'CROP_REMOVE', NULL, N'Farmtopia Admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, CAST(N'2024-09-19T10:02:09.633' AS DateTime), 1, CAST(N'2024-09-19T10:04:49.787' AS DateTime), 1, 0, 1)
GO
INSERT [FieldBook].[task] ([id], [uuid], [farm_id], [year], [date_from], [date_to], [task_type_id], [crop_status_after], [crop_id], [executor], [description], [note], [equipment_id], [attached_equipment_id], [treatment_successful], [harvest_amount], [tracking_trip_id], [data], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (3, N'b7afd89d-101e-4b77-aa56-b894806d0c67', 1, 2024, CAST(N'2024-03-06T00:00:00.000' AS DateTime), CAST(N'2024-03-06T00:00:00.000' AS DateTime), 2, NULL, NULL, N'Farmtopia Admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, CAST(N'2024-09-19T10:04:16.277' AS DateTime), 1, CAST(N'2024-09-19T10:04:16.277' AS DateTime), 1, 0, 1)
GO
INSERT [FieldBook].[task] ([id], [uuid], [farm_id], [year], [date_from], [date_to], [task_type_id], [crop_status_after], [crop_id], [executor], [description], [note], [equipment_id], [attached_equipment_id], [treatment_successful], [harvest_amount], [tracking_trip_id], [data], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (4, N'5d4967b1-ae03-4b29-8e31-52f8d05d4776', 1, 2024, CAST(N'2024-03-06T00:00:00.000' AS DateTime), CAST(N'2024-03-06T00:00:00.000' AS DateTime), 2, NULL, NULL, N'Farmtopia Admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, CAST(N'2024-09-19T10:05:35.077' AS DateTime), 1, CAST(N'2024-09-19T10:05:35.077' AS DateTime), 1, 0, 1)
GO
INSERT [FieldBook].[task] ([id], [uuid], [farm_id], [year], [date_from], [date_to], [task_type_id], [crop_status_after], [crop_id], [executor], [description], [note], [equipment_id], [attached_equipment_id], [treatment_successful], [harvest_amount], [tracking_trip_id], [data], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (5, N'ac74f7fe-9cc0-44cc-8ba0-450a3362670a', 1, 2024, CAST(N'2024-04-10T00:00:00.000' AS DateTime), CAST(N'2024-04-10T00:00:00.000' AS DateTime), 2, NULL, NULL, N'Farmtopia Admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, CAST(N'2024-09-19T10:06:19.573' AS DateTime), 1, CAST(N'2024-09-19T10:06:19.573' AS DateTime), 1, 0, 1)
GO
INSERT [FieldBook].[task] ([id], [uuid], [farm_id], [year], [date_from], [date_to], [task_type_id], [crop_status_after], [crop_id], [executor], [description], [note], [equipment_id], [attached_equipment_id], [treatment_successful], [harvest_amount], [tracking_trip_id], [data], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (6, N'ddc2b58c-3b02-484a-a4c2-7013bfcf6f38', 1, 2024, CAST(N'2024-05-01T00:00:00.000' AS DateTime), CAST(N'2024-05-01T00:00:00.000' AS DateTime), 13, NULL, NULL, N'Farmtopia Admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, CAST(N'2024-09-19T10:12:05.790' AS DateTime), 1, CAST(N'2024-09-23T06:07:44.497' AS DateTime), 1, 0, 1)
GO
INSERT [FieldBook].[task] ([id], [uuid], [farm_id], [year], [date_from], [date_to], [task_type_id], [crop_status_after], [crop_id], [executor], [description], [note], [equipment_id], [attached_equipment_id], [treatment_successful], [harvest_amount], [tracking_trip_id], [data], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (7, N'8a7de92f-335d-4f62-bccd-25d403067e1b', 1, 2024, CAST(N'2024-02-15T00:00:00.000' AS DateTime), CAST(N'2024-02-15T00:00:00.000' AS DateTime), 13, NULL, NULL, N'Farmtopia Admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, CAST(N'2024-09-19T10:16:39.763' AS DateTime), 1, CAST(N'2024-09-19T10:16:39.763' AS DateTime), 1, 0, 1)
GO
INSERT [FieldBook].[task] ([id], [uuid], [farm_id], [year], [date_from], [date_to], [task_type_id], [crop_status_after], [crop_id], [executor], [description], [note], [equipment_id], [attached_equipment_id], [treatment_successful], [harvest_amount], [tracking_trip_id], [data], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (8, N'dfbd1c58-30cd-4506-a565-df598b54eafd', 1, 2024, CAST(N'2024-06-01T10:00:00.000' AS DateTime), CAST(N'2024-06-01T11:00:00.000' AS DateTime), 14, NULL, NULL, N'Farmtopia Admin', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, CAST(N'2024-09-19T10:20:15.920' AS DateTime), 1, CAST(N'2024-09-19T10:20:15.920' AS DateTime), 1, 0, 1)
GO
INSERT [FieldBook].[task] ([id], [uuid], [farm_id], [year], [date_from], [date_to], [task_type_id], [crop_status_after], [crop_id], [executor], [description], [note], [equipment_id], [attached_equipment_id], [treatment_successful], [harvest_amount], [tracking_trip_id], [data], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (9, N'83c69d45-c4cf-4b83-a44d-63f03586ece7', 1, 2024, CAST(N'2024-07-16T12:00:00.000' AS DateTime), CAST(N'2024-07-16T13:00:00.000' AS DateTime), 14, NULL, NULL, N'Farmtopia Admin', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, CAST(N'2024-09-19T10:50:13.490' AS DateTime), 1, CAST(N'2024-09-19T10:50:13.490' AS DateTime), 1, 0, 1)
GO
INSERT [FieldBook].[task] ([id], [uuid], [farm_id], [year], [date_from], [date_to], [task_type_id], [crop_status_after], [crop_id], [executor], [description], [note], [equipment_id], [attached_equipment_id], [treatment_successful], [harvest_amount], [tracking_trip_id], [data], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (10, N'6154f5df-574f-4f07-98b3-82595d540638', 1, 2024, CAST(N'2024-07-14T00:00:00.000' AS DateTime), CAST(N'2024-07-14T00:00:00.000' AS DateTime), 7, NULL, NULL, N'Farmtopia Admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, CAST(N'2024-09-19T10:51:19.947' AS DateTime), 1, CAST(N'2024-09-19T10:51:19.947' AS DateTime), 1, 0, 1)
GO
INSERT [FieldBook].[task] ([id], [uuid], [farm_id], [year], [date_from], [date_to], [task_type_id], [crop_status_after], [crop_id], [executor], [description], [note], [equipment_id], [attached_equipment_id], [treatment_successful], [harvest_amount], [tracking_trip_id], [data], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (11, N'fe021420-4a6f-4021-8606-caa54a5032eb', 1, 2024, CAST(N'2024-08-28T00:00:00.000' AS DateTime), CAST(N'2024-08-28T00:00:00.000' AS DateTime), 7, NULL, NULL, N'Farmtopia Admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, CAST(N'2024-09-19T10:51:48.217' AS DateTime), 1, CAST(N'2024-09-19T10:51:48.217' AS DateTime), 1, 0, 1)
GO
INSERT [FieldBook].[task] ([id], [uuid], [farm_id], [year], [date_from], [date_to], [task_type_id], [crop_status_after], [crop_id], [executor], [description], [note], [equipment_id], [attached_equipment_id], [treatment_successful], [harvest_amount], [tracking_trip_id], [data], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (12, N'55609c7b-e309-49d1-b4a6-fe9bc5a83580', 1, 2024, CAST(N'2024-09-01T00:00:00.000' AS DateTime), CAST(N'2024-09-01T00:00:00.000' AS DateTime), 7, NULL, NULL, N'Farmtopia Admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, CAST(N'2024-09-19T10:52:14.447' AS DateTime), 1, CAST(N'2024-09-19T10:52:25.510' AS DateTime), 1, 0, 1)
GO
INSERT [FieldBook].[task] ([id], [uuid], [farm_id], [year], [date_from], [date_to], [task_type_id], [crop_status_after], [crop_id], [executor], [description], [note], [equipment_id], [attached_equipment_id], [treatment_successful], [harvest_amount], [tracking_trip_id], [data], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (13, N'74786931-79a5-4df7-8931-063ba1433589', 1, 2024, CAST(N'2024-09-19T00:00:00.000' AS DateTime), CAST(N'2024-09-19T00:00:00.000' AS DateTime), 1, N'CROP_REMOVE', NULL, N'Farmtopia Admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, CAST(N'2024-09-19T10:53:24.757' AS DateTime), 1, CAST(N'2024-09-19T10:53:24.757' AS DateTime), 1, 0, 1)
GO
INSERT [FieldBook].[task] ([id], [uuid], [farm_id], [year], [date_from], [date_to], [task_type_id], [crop_status_after], [crop_id], [executor], [description], [note], [equipment_id], [attached_equipment_id], [treatment_successful], [harvest_amount], [tracking_trip_id], [data], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (14, N'2a08b0be-c1b3-413b-b462-8059b5d8c1f8', 1, 2024, CAST(N'2024-03-23T00:00:00.000' AS DateTime), CAST(N'2024-03-23T00:00:00.000' AS DateTime), 13, NULL, NULL, N'Farmtopia Admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, CAST(N'2024-09-23T06:07:26.137' AS DateTime), 1, CAST(N'2024-09-23T06:21:16.323' AS DateTime), 1, 0, 1)
GO
SET IDENTITY_INSERT [FieldBook].[task] OFF
GO

SET IDENTITY_INSERT [FieldBook].[task_field] ON 
GO
INSERT [FieldBook].[task_field] ([id], [uuid], [task_id], [field_id], [field_plot_id], [field_crop_id], [duration], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (1, N'b2930100-dc2d-4c15-b57b-0225d6d17272', 1, 1, 1, NULL, NULL, CAST(N'2024-09-19T10:00:08.647' AS DateTime), 1, CAST(N'2024-09-19T10:05:01.900' AS DateTime), 1, 0, 1)
GO
INSERT [FieldBook].[task_field] ([id], [uuid], [task_id], [field_id], [field_plot_id], [field_crop_id], [duration], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (2, N'e7bc31d5-09c7-452a-93cc-d17813dae7f2', 1, 2, 2, NULL, NULL, CAST(N'2024-09-19T10:00:08.670' AS DateTime), 1, CAST(N'2024-09-19T10:05:01.913' AS DateTime), 1, 0, 1)
GO
INSERT [FieldBook].[task_field] ([id], [uuid], [task_id], [field_id], [field_plot_id], [field_crop_id], [duration], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (3, N'76bd9ee4-7b0b-4813-8f46-9c95f0d7c3d7', 1, 3, 3, NULL, NULL, CAST(N'2024-09-19T10:00:08.680' AS DateTime), 1, CAST(N'2024-09-19T10:05:01.927' AS DateTime), 1, 0, 1)
GO
INSERT [FieldBook].[task_field] ([id], [uuid], [task_id], [field_id], [field_plot_id], [field_crop_id], [duration], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (4, N'9866388e-7ac7-44e1-ab8e-b380b2970ae7', 2, 1, 1, NULL, NULL, CAST(N'2024-09-19T10:02:09.660' AS DateTime), 1, CAST(N'2024-09-19T10:04:49.800' AS DateTime), 1, 0, 1)
GO
INSERT [FieldBook].[task_field] ([id], [uuid], [task_id], [field_id], [field_plot_id], [field_crop_id], [duration], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (5, N'bd1a1cf9-ec31-4ae2-9e86-1999fc7b8212', 2, 2, 2, NULL, NULL, CAST(N'2024-09-19T10:02:09.667' AS DateTime), 1, CAST(N'2024-09-19T10:04:49.807' AS DateTime), 1, 0, 1)
GO
INSERT [FieldBook].[task_field] ([id], [uuid], [task_id], [field_id], [field_plot_id], [field_crop_id], [duration], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (6, N'25649fff-300f-49b0-aa25-cbdda26963c6', 2, 3, 3, NULL, NULL, CAST(N'2024-09-19T10:02:09.680' AS DateTime), 1, CAST(N'2024-09-19T10:04:49.813' AS DateTime), 1, 0, 1)
GO
INSERT [FieldBook].[task_field] ([id], [uuid], [task_id], [field_id], [field_plot_id], [field_crop_id], [duration], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (7, N'8a1ce310-5f1e-4b2f-9913-7d90cfbab866', 3, 3, 3, NULL, NULL, CAST(N'2024-09-19T10:04:16.300' AS DateTime), 1, CAST(N'2024-09-19T10:04:16.300' AS DateTime), 1, 0, 1)
GO
INSERT [FieldBook].[task_field] ([id], [uuid], [task_id], [field_id], [field_plot_id], [field_crop_id], [duration], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (8, N'e7e51be8-2a8b-46d8-99e6-1228bcc53719', 4, 1, 1, NULL, NULL, CAST(N'2024-09-19T10:05:35.090' AS DateTime), 1, CAST(N'2024-09-19T10:05:35.090' AS DateTime), 1, 0, 1)
GO
INSERT [FieldBook].[task_field] ([id], [uuid], [task_id], [field_id], [field_plot_id], [field_crop_id], [duration], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (9, N'99c43bd3-ad92-4e27-a695-023a415c9859', 5, 2, 2, NULL, NULL, CAST(N'2024-09-19T10:06:19.597' AS DateTime), 1, CAST(N'2024-09-19T10:06:19.597' AS DateTime), 1, 0, 1)
GO
INSERT [FieldBook].[task_field] ([id], [uuid], [task_id], [field_id], [field_plot_id], [field_crop_id], [duration], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (10, N'e7a91019-d6b5-428c-89b0-da441ce6a896', 6, 1, 1, NULL, NULL, CAST(N'2024-09-19T10:12:05.800' AS DateTime), 1, CAST(N'2024-09-23T06:07:44.533' AS DateTime), 1, 0, 1)
GO
INSERT [FieldBook].[task_field] ([id], [uuid], [task_id], [field_id], [field_plot_id], [field_crop_id], [duration], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (11, N'2461ac14-fee2-4f0c-b4da-254017adcba8', 6, 3, 3, NULL, NULL, CAST(N'2024-09-19T10:12:05.810' AS DateTime), 1, CAST(N'2024-09-23T06:07:44.540' AS DateTime), 1, 0, 1)
GO
INSERT [FieldBook].[task_field] ([id], [uuid], [task_id], [field_id], [field_plot_id], [field_crop_id], [duration], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (12, N'88db25b0-7f94-41a9-9b9b-ea9d2d99d80c', 7, 1, 1, NULL, NULL, CAST(N'2024-09-19T10:16:39.773' AS DateTime), 1, CAST(N'2024-09-19T10:16:39.773' AS DateTime), 1, 0, 1)
GO
INSERT [FieldBook].[task_field] ([id], [uuid], [task_id], [field_id], [field_plot_id], [field_crop_id], [duration], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (13, N'542c4920-1f46-4fe9-beef-b113d9991e20', 7, 2, 2, NULL, NULL, CAST(N'2024-09-19T10:16:39.783' AS DateTime), 1, CAST(N'2024-09-19T10:16:39.783' AS DateTime), 1, 0, 1)
GO
INSERT [FieldBook].[task_field] ([id], [uuid], [task_id], [field_id], [field_plot_id], [field_crop_id], [duration], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (14, N'1c065b1e-5fc0-4c9b-8a7d-df063520aa40', 7, 3, 3, NULL, NULL, CAST(N'2024-09-19T10:16:39.787' AS DateTime), 1, CAST(N'2024-09-19T10:16:39.787' AS DateTime), 1, 0, 1)
GO
INSERT [FieldBook].[task_field] ([id], [uuid], [task_id], [field_id], [field_plot_id], [field_crop_id], [duration], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (15, N'51c47648-f3c2-447a-891a-9e522dbac612', 8, 1, 1, NULL, 6, CAST(N'2024-09-19T10:20:15.943' AS DateTime), 1, CAST(N'2024-09-19T10:20:15.943' AS DateTime), 1, 0, 1)
GO
INSERT [FieldBook].[task_field] ([id], [uuid], [task_id], [field_id], [field_plot_id], [field_crop_id], [duration], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (16, N'7dfed427-0bf0-4ec6-8aa2-b0820690ea85', 8, 3, 3, NULL, 54, CAST(N'2024-09-19T10:20:15.967' AS DateTime), 1, CAST(N'2024-09-19T10:20:15.967' AS DateTime), 1, 0, 1)
GO
INSERT [FieldBook].[task_field] ([id], [uuid], [task_id], [field_id], [field_plot_id], [field_crop_id], [duration], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (17, N'348414c4-0a3f-4776-aa4f-a46987973e7a', 9, 2, 2, NULL, 60, CAST(N'2024-09-19T10:50:13.520' AS DateTime), 1, CAST(N'2024-09-19T10:50:13.520' AS DateTime), 1, 0, 1)
GO
INSERT [FieldBook].[task_field] ([id], [uuid], [task_id], [field_id], [field_plot_id], [field_crop_id], [duration], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (18, N'939dc012-6946-49b1-a2f6-a62a73ee47bc', 10, 1, 1, NULL, NULL, CAST(N'2024-09-19T10:51:19.957' AS DateTime), 1, CAST(N'2024-09-19T10:51:19.957' AS DateTime), 1, 0, 1)
GO
INSERT [FieldBook].[task_field] ([id], [uuid], [task_id], [field_id], [field_plot_id], [field_crop_id], [duration], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (19, N'efe35591-01e2-469f-b3cc-e521b4196c49', 11, 2, 2, NULL, NULL, CAST(N'2024-09-19T10:51:48.223' AS DateTime), 1, CAST(N'2024-09-19T10:51:48.223' AS DateTime), 1, 0, 1)
GO
INSERT [FieldBook].[task_field] ([id], [uuid], [task_id], [field_id], [field_plot_id], [field_crop_id], [duration], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (20, N'6b7f5fca-9b21-46c3-a4c0-ddf43d3551a5', 12, 3, 3, NULL, NULL, CAST(N'2024-09-19T10:52:14.453' AS DateTime), 1, CAST(N'2024-09-19T10:52:25.537' AS DateTime), 1, 0, 1)
GO
INSERT [FieldBook].[task_field] ([id], [uuid], [task_id], [field_id], [field_plot_id], [field_crop_id], [duration], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (21, N'e86d1e29-d412-41e0-b1cb-10c26526bbb8', 13, 1, 1, NULL, NULL, CAST(N'2024-09-19T10:53:24.763' AS DateTime), 1, CAST(N'2024-09-19T10:53:24.763' AS DateTime), 1, 0, 1)
GO
INSERT [FieldBook].[task_field] ([id], [uuid], [task_id], [field_id], [field_plot_id], [field_crop_id], [duration], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (22, N'894df5b4-ad2c-4379-8bf4-d1b49f3b081a', 13, 2, 2, NULL, NULL, CAST(N'2024-09-19T10:53:24.773' AS DateTime), 1, CAST(N'2024-09-19T10:53:24.773' AS DateTime), 1, 0, 1)
GO
INSERT [FieldBook].[task_field] ([id], [uuid], [task_id], [field_id], [field_plot_id], [field_crop_id], [duration], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (23, N'0c6dbb24-55f0-46fd-9839-8d2e13910caf', 13, 3, 3, NULL, NULL, CAST(N'2024-09-19T10:53:24.780' AS DateTime), 1, CAST(N'2024-09-19T10:53:24.780' AS DateTime), 1, 0, 1)
GO
INSERT [FieldBook].[task_field] ([id], [uuid], [task_id], [field_id], [field_plot_id], [field_crop_id], [duration], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (24, N'c6704fd7-916c-4100-aed4-f02e6442aba8', 14, 1, 1, NULL, NULL, CAST(N'2024-09-23T06:07:26.157' AS DateTime), 1, CAST(N'2024-09-23T06:21:16.337' AS DateTime), 1, 0, 1)
GO
INSERT [FieldBook].[task_field] ([id], [uuid], [task_id], [field_id], [field_plot_id], [field_crop_id], [duration], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (25, N'ee4df962-a646-45cd-8c88-f0eb4ba8537f', 14, 3, 3, NULL, NULL, CAST(N'2024-09-23T06:07:26.163' AS DateTime), 1, CAST(N'2024-09-23T06:21:16.343' AS DateTime), 1, 0, 1)
GO
SET IDENTITY_INSERT [FieldBook].[task_field] OFF
GO

SET IDENTITY_INSERT [FieldBook].[task_fertilizer] ON 
GO
INSERT [FieldBook].[task_fertilizer] ([id], [uuid], [task_id], [fertilizer_id], [amount], [amount_unit_id], [amount_unit], [amount_used], [amount_used_unit_id], [amount_used_unit], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (1, N'4fa0a634-0787-4c88-8a9a-31863fd80a1f', 6, 8, CAST(150.00 AS Decimal(18, 2)), 4, N'kg/ha', CAST(2383.05 AS Decimal(18, 2)), 1, N'kg', CAST(N'2024-09-19T10:12:05.833' AS DateTime), 1, CAST(N'2024-09-23T06:07:44.550' AS DateTime), 1, 0, 1)
GO
INSERT [FieldBook].[task_fertilizer] ([id], [uuid], [task_id], [fertilizer_id], [amount], [amount_unit_id], [amount_unit], [amount_used], [amount_used_unit_id], [amount_used_unit], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (2, N'45089b57-7a77-4702-b5e5-997e164b92dd', 7, 44, CAST(5.00 AS Decimal(18, 2)), 2, N'm3', CAST(5.00 AS Decimal(18, 2)), 2, N'm3', CAST(N'2024-09-19T10:16:39.793' AS DateTime), 1, CAST(N'2024-09-19T10:16:39.793' AS DateTime), 1, 0, 1)
GO
INSERT [FieldBook].[task_fertilizer] ([id], [uuid], [task_id], [fertilizer_id], [amount], [amount_unit_id], [amount_unit], [amount_used], [amount_used_unit_id], [amount_used_unit], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (3, N'a124f927-8969-4530-be6e-7011d9cdd89e', 14, 8, CAST(80.00 AS Decimal(18, 2)), 4, N'kg/ha', CAST(1270.96 AS Decimal(18, 2)), 1, N'kg', CAST(N'2024-09-23T06:07:26.173' AS DateTime), 1, CAST(N'2024-09-23T06:07:26.173' AS DateTime), 1, 0, 1)
GO
INSERT [FieldBook].[task_fertilizer] ([id], [uuid], [task_id], [fertilizer_id], [amount], [amount_unit_id], [amount_unit], [amount_used], [amount_used_unit_id], [amount_used_unit], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (4, N'7d9925f5-4aca-47b3-9d1c-92e980460a04', 14, 10, CAST(80.00 AS Decimal(18, 2)), 1, N'kg', CAST(80.00 AS Decimal(18, 2)), 1, N'kg', CAST(N'2024-09-23T06:21:16.350' AS DateTime), 1, CAST(N'2024-09-23T06:21:16.350' AS DateTime), 1, 0, 1)
GO
SET IDENTITY_INSERT [FieldBook].[task_fertilizer] OFF
GO

SET IDENTITY_INSERT [FieldBook].[task_pesticide] ON 
GO
INSERT [FieldBook].[task_pesticide] ([id], [uuid], [task_id], [pesticide_id], [amount], [amount_unit_id], [amount_unit], [amount_used], [amount_used_unit_id], [amount_used_unit], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (1, N'52572b24-f5bd-4220-89ca-86bc8584cab8', 8, 2, CAST(0.10 AS Decimal(18, 2)), 3, N'l', CAST(0.10 AS Decimal(18, 2)), 3, N'l', CAST(N'2024-09-19T10:20:15.987' AS DateTime), 1, CAST(N'2024-09-19T10:20:15.987' AS DateTime), 1, 0, 1)
GO
INSERT [FieldBook].[task_pesticide] ([id], [uuid], [task_id], [pesticide_id], [amount], [amount_unit_id], [amount_unit], [amount_used], [amount_used_unit_id], [amount_used_unit], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (2, N'400d5324-3c7a-491d-b594-9ca100662246', 9, 357, CAST(0.20 AS Decimal(18, 2)), 3, N'l', CAST(0.20 AS Decimal(18, 2)), 3, N'l', CAST(N'2024-09-19T10:50:13.530' AS DateTime), 1, CAST(N'2024-09-19T10:50:13.530' AS DateTime), 1, 0, 1)
GO
SET IDENTITY_INSERT [FieldBook].[task_pesticide] OFF
GO

SET IDENTITY_INSERT [FieldBook].[stock_fertilizer] ON 
GO
INSERT [FieldBook].[stock_fertilizer] ([id], [uuid], [farm_id], [task_id], [date], [fertilizer_id], [stock_type_id], [stock_type], [stock_name], [value], [value_unit_id], [value_unit], [note], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (1, N'c9a0fdb4-c637-4cfe-b5aa-b9c07e5a5505', 1, 6, CAST(N'2024-05-01T00:00:00.000' AS DateTime), 8, NULL, -1, N'Uporaba organskih in mineralnih gnojil', CAST(4766.10 AS Decimal(18, 2)), 1, N'kg', NULL, CAST(N'2024-09-19T10:12:05.860' AS DateTime), 1, CAST(N'2024-09-23T06:07:44.527' AS DateTime), 1, 1, 0)
GO
INSERT [FieldBook].[stock_fertilizer] ([id], [uuid], [farm_id], [task_id], [date], [fertilizer_id], [stock_type_id], [stock_type], [stock_name], [value], [value_unit_id], [value_unit], [note], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (2, N'a5d9de94-2fa6-42c9-a6de-fad7df4fb540', 1, 7, CAST(N'2024-02-15T00:00:00.000' AS DateTime), 44, NULL, -1, N'Uporaba organskih in mineralnih gnojil', CAST(5.00 AS Decimal(18, 2)), 2, N'm3', NULL, CAST(N'2024-09-19T10:16:39.813' AS DateTime), 1, CAST(N'2024-09-19T10:16:39.813' AS DateTime), 1, 0, 1)
GO
INSERT [FieldBook].[stock_fertilizer] ([id], [uuid], [farm_id], [task_id], [date], [fertilizer_id], [stock_type_id], [stock_type], [stock_name], [value], [value_unit_id], [value_unit], [note], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (3, N'34200666-2b1b-4508-a6a5-373b087dca4f', 1, 14, CAST(N'2024-03-23T00:00:00.000' AS DateTime), 8, NULL, -1, N'Uporaba organskih in mineralnih gnojil', CAST(1270.96 AS Decimal(18, 2)), 1, N'kg', NULL, CAST(N'2024-09-23T06:07:26.193' AS DateTime), 1, CAST(N'2024-09-23T06:21:16.333' AS DateTime), 1, 1, 0)
GO
INSERT [FieldBook].[stock_fertilizer] ([id], [uuid], [farm_id], [task_id], [date], [fertilizer_id], [stock_type_id], [stock_type], [stock_name], [value], [value_unit_id], [value_unit], [note], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (4, N'a09bb379-a63a-46ce-98cc-72c9c4e9adb8', 1, 6, CAST(N'2024-05-01T00:00:00.000' AS DateTime), 8, NULL, -1, N'Uporaba organskih in mineralnih gnojil', CAST(2383.05 AS Decimal(18, 2)), 1, N'kg', NULL, CAST(N'2024-09-23T06:07:44.563' AS DateTime), 1, CAST(N'2024-09-23T06:07:44.563' AS DateTime), 1, 0, 1)
GO
INSERT [FieldBook].[stock_fertilizer] ([id], [uuid], [farm_id], [task_id], [date], [fertilizer_id], [stock_type_id], [stock_type], [stock_name], [value], [value_unit_id], [value_unit], [note], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (5, N'a4e2c474-87b8-4d8a-bed3-0b7e781cf5df', 1, 14, CAST(N'2024-03-23T00:00:00.000' AS DateTime), 10, NULL, -1, N'Uporaba organskih in mineralnih gnojil', CAST(80.00 AS Decimal(18, 2)), 1, N'kg', NULL, CAST(N'2024-09-23T06:21:16.363' AS DateTime), 1, CAST(N'2024-09-23T06:21:16.363' AS DateTime), 1, 0, 1)
GO
SET IDENTITY_INSERT [FieldBook].[stock_fertilizer] OFF
GO

SET IDENTITY_INSERT [FieldBook].[stock_pesticide] ON 
GO
INSERT [FieldBook].[stock_pesticide] ([id], [uuid], [farm_id], [task_id], [date], [pesticide_id], [stock_type_id], [stock_type], [stock_name], [value], [value_unit_id], [value_unit], [note], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (1, N'16d056a4-c343-4515-a018-920ef1afec98', 1, 8, CAST(N'2024-06-01T10:00:00.000' AS DateTime), 2, NULL, -1, N'Uporaba FFS', CAST(0.10 AS Decimal(18, 2)), 3, N'l', NULL, CAST(N'2024-09-19T10:20:16.010' AS DateTime), 1, CAST(N'2024-09-19T10:20:16.010' AS DateTime), 1, 0, 1)
GO
INSERT [FieldBook].[stock_pesticide] ([id], [uuid], [farm_id], [task_id], [date], [pesticide_id], [stock_type_id], [stock_type], [stock_name], [value], [value_unit_id], [value_unit], [note], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (2, N'3387c8e1-7e7e-483e-8e8b-8eb890a75368', 1, 9, CAST(N'2024-07-16T12:00:00.000' AS DateTime), 357, NULL, -1, N'Uporaba FFS', CAST(0.20 AS Decimal(18, 2)), 3, N'l', NULL, CAST(N'2024-09-19T10:50:13.547' AS DateTime), 1, CAST(N'2024-09-19T10:50:13.547' AS DateTime), 1, 0, 1)
GO
SET IDENTITY_INSERT [FieldBook].[stock_pesticide] OFF
GO

SET IDENTITY_INSERT [FieldBook].[user] ON 
GO
INSERT [FieldBook].[user] ([id], [uuid], [name], [surname], [displayname], [email], [password], [password_salt], [farm_id], [role], [last_login], [status], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (1, N'd40a0fdd-0ba2-436a-8633-b6efb0cf43db', N'Farmtopia', N'Admin', N'Administrator', N'admin@farmtopia.eu', N'BF38E19717393CC5C3C0CAB99A4511D85328DD745E8C7D05AFBEB5CA0B45E68C26AF9F51B9BD461DA27EA4106859BF30B3842F9F977737F2F549AF35268C1837', N'wQLr2g2l8RK4xcGKC+UbQSGEjFAtyx3Apf7+N51LzIjBcLrdt+SICftvtfn8SVVtJMwZLxYkoZqSN60OLPi9xg==', 1, N'1', NULL, 1, GETDATE(), NULL, GETDATE(), NULL, 0, 1)
GO
SET IDENTITY_INSERT [FieldBook].[user] OFF
GO

SET IDENTITY_INSERT [FieldBook].[user_farm] ON 
GO
INSERT [FieldBook].[user_farm] ([id], [user_id], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (1, 1, 1, CAST(N'2024-08-21T12:51:37.293' AS DateTime), 1, CAST(N'2024-08-21T12:51:37.293' AS DateTime), 1, 0, 1)
GO
SET IDENTITY_INSERT [FieldBook].[user_farm] OFF
GO

SET IDENTITY_INSERT [Farmtopia].[provider] ON 
GO
INSERT [Farmtopia].[provider] ([id], [name], [directory_url], [secret_key], [active], [created], [modified]) VALUES (1, N'ITC FieldBook Data Sharing API', NULL, NULL, 1, NULL, GETDATE())
GO
SET IDENTITY_INSERT [Farmtopia].[provider] OFF
GO

SET IDENTITY_INSERT [FieldBook].[crop] ON 
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (1, N'023b0d3b-f84e-4a1a-b971-868e2b3d66ab', N'402', N'brokoli', N'Broccoli', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (2, N'c8397fe7-62f5-4fab-b728-7ba5aa04656e', N'402', N'cvetača', N'Cauliflower', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (3, N'5e43df9f-ab33-4d41-9f26-5fe6a9608b0a', N'402', N'čebula', N'Onion', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (4, N'67c83b21-4d47-49bb-b9da-076a6e811cd6', N'402', N'česen', N'Garlic', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (5, N'068a6eb7-9c09-4f00-8944-181ebff78686', N'402', N'drobnjak', N'Chives', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (6, N'131c500b-cc88-44d8-8549-58a19c5e3ae9', N'402', N'endivija', N'Endive lettuce', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (7, N'6183430f-b076-426f-a5ac-e83fb7a24494', N'402', N'feferoni', N'Chilli or hot peppers', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (8, N'1fcf7a5a-31d7-440f-8b10-5cba35a75714', N'402', N'fižol ', N'Bean', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (9, N'6bcb1f9c-1539-495f-8ec6-e61b2ed76c14', N'402', N'hren', N'Horseradish', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (10, N'74e3ccb6-e4f8-49fb-a7ca-56cdd28054a7', N'402', N'jajčevci oz. melancane', N'Eggplant', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (11, N'08673df6-48e5-4dc3-b9bf-e292ca74bd94', N'402', N'vrtna buča ali bučka ', N'Zuccini', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (12, N'b8077c96-cb7b-4984-8931-cd5b32782503', N'402', N'kitajsko zelje', N'Chinese cabbage', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (13, N'64a22a66-44c4-4a83-9dff-663df5c44558', N'402', N'kolerabica', N'Kohlrabi', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (14, N'8c9fdfcc-c4cb-47d2-bceb-1861038b3905', N'402', N'korenček in korenje', N'Carrot', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (15, N'8fca5779-e9f8-4761-a2b3-3033126f693f', N'402', N'kumare in kumarice', N'Cucumber', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (16, N'da4866e5-7055-4139-a042-da3f80b08a20', N'402', N'ohrovt (brstični)', N'Brussels sprout', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (17, N'3b9b2de7-a9ed-4127-925a-f33776fb886c', N'402', N'ohrovt (glavnati)', N'Savoy cabbage', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (18, N'c2c6a1f5-65f8-4024-b4c8-81bb426ca9ad', N'402', N'ohrovt (listnati)', N'Kale', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (19, N'05b37c66-57a5-4206-bbc1-37431ec5213a', N'402', N'paprika', N'Bell pepper', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (20, N'4a232562-da1c-49af-ac60-ac79d3db2bfe', N'402', N'paradižnik', N'Tomato', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (21, N'27ceb6b7-0f2d-4b7b-8cce-fcfc82a8bcd9', N'402', N'pastinak', N'Parsnip', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (22, N'7be6433d-4d5e-4ae4-a018-04d60631cab2', N'402', N'peteršilj', N'Parsley', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (23, N'3460a853-84fa-4168-ad85-cfa4adececb1', N'402', N'por', N'Leek', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (24, N'ce7dd420-13a6-4b0f-bddc-4f76aa40537b', N'402', N'rdeča pesa', N'Beetroot', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (25, N'ee6e5d8e-acdf-4b11-addf-40875638cb2e', N'402', N'redkvica ', N'Radish', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (26, N'c51df8d8-3915-41e0-909f-1ace78b5dd7e', N'402', N'regrat', N'Dandelion', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (27, N'dc70aca4-5633-4f9c-ac28-4dfc38a5f85e', N'402', N'sladki krompir', N'Sweet potato', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (28, N'04739fb7-0fbd-4e9f-a142-8d277ba0f513', N'402', N'solata', N'Lettuce', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (29, N'505f67e5-9b4c-407c-891e-76bc99d17cda', N'402', N'šalotka', N'Shallot', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (30, N'137007a8-c238-4fa0-8757-e09e43c51295', N'402', N'špinača', N'Spinach', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (31, N'cb60f659-bb35-451c-8c0a-e63fab0c79a5', N'402', N'topinambur', N'Jerusalem artichoke', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (32, N'24f75117-4d8a-4606-a224-c9a83d52f312', N'402', N'vrtna kreša', N'Garden cress', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (33, N'a9dabc56-9fec-4a19-88d2-4fd71dc40309', N'402', N'zelena', N'Celery', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (34, N'1d64174a-31db-4d1b-ab2d-c965e3f32dfc', N'402', N'zelje', N'Cabbage', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (35, N'10a7a7f0-e382-46c0-9dde-d1ad5e6a7778', N'402', N'zimski luk', N'Winter Onion', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (36, N'3fe6cf99-b89d-49ef-bd84-6a209d46b8eb', N'402', N'čičerika', N'Chickpea', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (37, N'e4ba2d20-9ed8-4a94-881e-934f21229d5b', N'402', N'leča', N'Lentil', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (38, N'bb0291c2-1668-4e77-9ee1-1507efd641b1', N'402', N'blitva', N'Mangold', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (39, N'e094b92a-4b36-4d25-b260-962bd395bb81', N'402', N'sladki komarček', N'Sweet fennel', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (40, N'a5b995e2-cfb0-4200-8a1e-07d5a5366127', N'402', N'kardij', N'Artichoke', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (41, N'f18e384d-bda7-4d3b-9abc-b858c08800a3', N'402', N'redkev', N'Radish', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (42, N'2ba29c1e-213a-4458-846f-478ceeaa7f76', N'402', N'lubenice', N'Watermelon', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (43, N'6c27ab81-86b9-4b68-8448-a639df9fc1c6', N'402', N'melone oziroma dinje', N'Melon', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (44, N'bb8f01c6-f695-49ca-bcfc-b25fe5b8cc61', N'402', N'črni koren', N'Black salsify', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (45, N'44c96b58-4949-4388-82b0-73b392e9bdc0', N'402', N'grah', N'Pea', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (46, N'f05c3220-4835-4ce6-b4d6-050de746bfaf', N'402', N'bob', N'Broad bean', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (47, N'ffac24a1-20b4-4def-bdc3-1d3a05c7bac2', N'402', N'navadna buča (jedilna)', N'Pumpkin', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (48, N'e228f28d-8b6c-4065-9999-a64495d09616', N'402', N'motovilec', N'Field salad', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (49, N'73ccf9ef-6659-499d-a78f-177546d74753', N'402', N'radič', N'Radicchio', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (50, N'9557f806-40e7-427d-b448-49f324acdbd4', N'402', N'rukola', N'Rocket', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (51, N'1f5ca82a-39da-4655-a7d2-5ef30679c960', N'038', N'repa', N'Turnip', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (52, N'705323ee-5975-477b-868c-6d1f397e04b9', N'039', N'meliorativna redkev', N'Ameliorative Radish', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (53, N'bddb7b74-8848-4786-8189-c7b6bd43d6a4', N'044', N'grah', N'Pea', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (54, N'3e4f47e3-e096-461e-847b-a27c24c93f6c', N'045', N'bob', N'Broad bean', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (55, N'e1a58d11-5e34-4a29-acc0-58feb4e38d23', N'046', N'motovilec', N'Rocket', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (56, N'4e68a41e-50f2-4b50-b078-0d712f60df6f', N'047', N'radič', N'Radicchio', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (57, N'37226336-5258-4da8-b352-78fa8e2d4126', N'048', N'rukola', N'Rocket', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (58, N'e82d3a87-40ef-412c-a0bc-cc4cd87772ea', N'115', N'abesinska gizotija', N'Niger', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (59, N'9e5d43df-f25c-486a-aafe-02bdf1fa3034', N'004', N'ajda', N'Buckwheat', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (60, N'f3ac8b73-aa57-427d-bc0f-4dff000c4f49', N'220', N'aleksandrijska detelja', N'Egyptian clover', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (61, N'391fe6bb-4a5e-4713-94f9-d98b5c7286d1', N'221', N'perzijska detelja', N'Persian clover', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (62, N'f42f1e4c-fb0e-4c1c-a961-05444681893b', N'037', N'amarant', N'Amarant', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (63, N'04eeb585-d145-4432-8f80-bb0fe4cd1c50', N'652', N'ameriška borovnica', N'Blueberry', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (64, N'b210857f-e734-4401-a5cf-72af04917de1', N'681', N'ameriška brusnica', N'Cranberry', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (65, N'607c5ea7-d72f-41a8-9e34-e5ccfe1846d4', N'738', N'ameriški slamnik', N'Echinacea', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (66, N'f0c94083-f650-4a84-8609-21d65704a0d8', N'657', N'aronija', N'Aronia (Chokeberry)', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (67, N'dc9bf139-4faa-4f1c-a070-22858e296c58', N'733', N'artičoka', N'Artichoke', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (68, N'924b832c-1496-4056-bb44-131741b888b7', N'648', N'asimina', N'Asimina', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (69, N'c7cc6499-6261-4947-aa62-bbdf74602d9b', N'032', N'bar', N'Foxtail millet', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (70, N'fd552bc5-5fab-4c9a-a383-d02eebafe427', N'111', N'bela gorjušica', N'White mustard', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (71, N'f853a108-2f46-4b45-8ba8-155dcb70aa16', N'646', N'bezeg', N'Elderberry', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (73, N'eceef211-762f-4ee3-acfe-f167923fcd0f', N'621', N'breskev', N'Peach', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (75, N'3fa852b5-a3d4-43ee-bbf5-abb81a5baa62', N'555', N'brez zahtevka', N'without claim', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (76, N'103ba255-9fbe-4d25-be07-e6c443ed3249', N'679', N'brusnica (evropska ali gozdna)', N'Lingonberry or Cranberry (European or Forest)', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (77, N'cc582bb9-136c-4568-a687-0f09ae514163', N'625', N'češnja', N'Cherry', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (79, N'430820d8-80c8-441d-8880-82b3e8fdb7a8', N'656', N'črni ribez', N'Black currant', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (80, N'f02aed91-203a-493d-addd-d1dec29b0cb3', N'660', N'črni ribez x kosmulja', N'Black currant * gooseberry', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (81, N'13ccfa33-3704-48d9-b9ed-8ac72fd42305', N'207', N'detelja', N'Clover', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (82, N'f1470d4a-c09c-44cc-bb17-f99fa869c3b9', N'206', N'deteljnotravne mešanice', N'Clover grass mixture', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (83, N'dc2f9014-26bd-44fc-a1f3-c368dde780f8', N'675', N'dren', N'Cornelian cherries', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (85, N'41e4105a-66a5-4927-a28a-3e90015ed703', N'702', N'drevesnice', N'Nurseries', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (86, N'34aa0326-8d08-434d-94ca-ab7a45aeaa7d', N'114', N'druge rastline za krmo na njivah', N'Other fodder crops in fields', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (87, N'be8a0da2-0d15-44eb-b76d-eb152dec91e1', N'721', N'drugi hitro rastoči panjevci', N'Other fast growing trees ', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (88, N'144bdf07-8d7d-4888-98d1-6764be3c8e6b', N'219', N'facelija', N'Phacelia', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (89, N'5766e8b4-db73-4578-b6e2-41354e82b5c5', N'619', N'feioja', N'feioja', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (90, N'aad56a39-30a9-4a33-9f65-a3a201507ffd', N'659', N'goji jagoda', N'goji berry', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (91, N'90d389ae-5a39-4543-9d3f-694cdb7bb41d', N'211', N'grahor', N'Vetch', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (92, N'1d750373-51e1-46ae-84fc-c5c4470fba7f', N'615', N'granatno jabolko', N'Pomegranate', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (94, N'e201b88c-e9df-4f33-94f3-3620d0bea000', N'110', N'grašica (jara)', N' Hairy Vetch (spring)', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (95, N'7657a868-bf86-42aa-868e-2e03dc20dc96', N'810', N'grašica (ozimna)', N'Hairy Vetch (winter)', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (96, N'1e988f50-364c-4166-89fd-f5c6abd95511', N'672', N'grenivka', N'Grapefruit', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (98, N'f6759919-e4b6-4aab-a6ce-e35e1876926e', N'720', N'hitro rastoči panjevec (vrba, topol)', N'Fast growing trees (willow, poplar)', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (99, N'628de824-6e70-41ee-842c-7cb2c5fa0ba5', N'900', N'hmelj', N'Hop', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (100, N'a8ddbf46-0463-4dbf-a364-0adf7f5fee39', N'612', N'hruška', N'Pear', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (102, N'7fbd47c5-2f53-4be3-9df9-33da1590c090', N'222', N'inkarnatka', N'Crimson clover', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (103, N'715554f4-77a7-4640-94ba-62a2ebb3fde6', N'611', N'jablana', N'Apple', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (105, N'598fcf37-0df6-4be4-b3c7-78ad89450d61', N'651', N'jagoda', N'Strawberry', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (106, N'a961c269-3ffa-417f-bc64-f9b580087ede', N'627', N'japonska nešplja', N'Japanese medlar (plum)', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (107, N'79291f01-c96b-4ead-b837-75b46d7476af', N'009', N'ječmen (jari)', N'Barley (spring)', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (108, N'e477771f-a4f8-435a-a091-2d6afd3404ff', N'809', N'ječmen (ozimni)', N'Barley (winter)', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (109, N'272a04a1-0027-4c8e-8df7-701d6b0c897b', N'643', N'kaki', N'Persimmon', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (111, N'50ce2814-f955-4da3-924e-71b2c708c5e2', N'035', N'pšenica horasan (jara)', N'Khorasan wheat (spring)', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (112, N'c540ca3a-8cec-4764-82fd-805bf3e14f75', N'835', N'pšenica horasan (ozimna)', N'Khorasan wheat (winter)', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (113, N'd0f2737a-8838-4273-ad86-6e59511b98d0', N'642', N'kivi', N'Kiwi', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (114, N'd6caa636-88ca-4c44-a7ee-c54fd19582cb', N'027', N'konoplja', N'Hemp', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (115, N'05aad5e0-6948-4920-ba27-58eac1f317cd', N'006', N'koruza za silažo', N'Corn Silage', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (116, N'50d56815-2ef8-4e33-9402-366292f862cb', N'005', N'koruza za zrnje', N'corn for grain', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (117, N'f6a102aa-2deb-4236-ab5a-809130006beb', N'676', N'kosmulja', N'gooseberry', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (118, N'81124fc1-1a76-4e31-8cc1-16723995232e', N'644', N'kostanj', N'Chestnut', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (120, N'6e9c188b-0c67-4369-9df9-0a61d53b55ca', N'112', N'krmna ogrščica (jara)', N'Fodder Rapeseed (spring)', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (121, N'2e0c29ef-e1b0-41b2-8a4f-075051ddf5b0', N'812', N'krmna ogrščica (ozimna)', N'Fodder Rapeseed (winter)', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (122, N'a1ad2314-7dde-4584-881c-043875c4d460', N'101', N'krmna pesa', N'Fodder beet', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (123, N'5791e0a9-9c84-45ae-9cca-4d84fde319d7', N'102', N'krmna repa', N'Fodder beet', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (124, N'05cefaf1-6f1a-4113-94c8-028ae7976e59', N'104', N'krmna repica (jara)', N'Fodder Rapeseed (spring)', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (125, N'48f2f6e2-8d95-45b4-9738-b1bfd0f41590', N'804', N'krmna repica (ozimna)', N'Fodder Rapeseed (winter)', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (126, N'e3f39d93-1803-43e1-8dcb-379667a7cbb0', N'017', N'krmni bob', N'Fodder Fava bean', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (127, N'efd3b6e6-b715-4b6b-b103-9f89ddec2f78', N'033', N'krmni grah (jari)', N'Fodder pea (spring)', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (128, N'ee71b53a-bab3-4b97-b07e-e55b5861e709', N'833', N'krmni grah (ozimni)', N'Fodder pea (winter)', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (129, N'355fcea4-5064-4e07-afba-233c2e248fb8', N'105', N'krmni ohrovt', N'Forage kale', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (130, N'a0b3d9b6-621f-489d-9b15-e876f35b8ca9', N'106', N'krmni radič', N'Forage chicory', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (131, N'983de5c9-28e6-4092-8a3d-9a0e3fe72c84', N'109', N'krmni sirek', N'Fodder sorghum', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (132, N'b7bfd992-5a40-4c75-8228-1ec08e5f1abd', N'107', N'krmno korenje', N'Forage carrot', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (133, N'c1d759d9-bc8a-4435-8554-4810f58cb4a6', N'020', N'krompir-pozni', N'Potatoe (late season)', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (134, N'be2631af-36a5-4d3d-b80c-928d897f4fb2', N'022', N'krompir-zgodnji', N'Potatoe (early season)', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (135, N'd8ec9704-2275-45b0-b850-f8ae2580ddf8', N'613', N'kutina', N'Quince', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (137, N'481d83f5-548c-4751-9eba-4b9f808c7775', N'028', N'lan', N'Flaxseed', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (138, N'db93f97f-25f4-4e0c-b196-1c9a45c1be75', N'632', N'leska ', N'Hazelnut', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (140, N'0c8aad4c-9d9e-4ebf-8e11-cf359eb2b64a', N'671', N'limonovec', N'Lemon tree', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (142, N'7e32f48f-37ba-4930-b1e4-f2277e832497', N'208', N'lucerna', N'Alfalfa', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (143, N'265c80e2-a5d1-4d22-853b-792d185579b9', N'653', N'malina', N'Raspberry', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (144, N'7c964ea7-c6ba-4e8c-a0f7-c88d94b77ece', N'674', N'mandarinovec', N'Mandarin tree', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (146, N'd8733803-f899-429b-9c35-401af667b6ae', N'633', N'mandelj', N'Almond', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (148, N'4eaf10d8-3e56-41fe-ac38-8059754d7949', N'624', N'marelica', N'Apricot', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (150, N'65ca58a0-dcbc-4aaf-87c7-5cff576de6d2', N'707', N'matičnjak', N'', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (151, N'b3ee1802-a30f-4ab0-ae8c-69f07fcb6fdd', N'405', N'mešana raba (zelenjadnice, poljščine, dišavnice in zdravilna zelišča)', N'Mixed use (vegetable, crops, spices, herbs)', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (152, N'c9bf56a9-f1c6-4b78-858f-44c54a437067', N'710', N'mešane rastline za rejo polžev', N'Mixed plants for breeding snails', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (153, N'06e8ce47-32df-461f-92b4-3c4f7363c682', N'699', N'mešane sadne vrste', N'Mixed fruit varieties', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (155, N'72c7eaf0-617c-443a-8f3d-c65327b81ae5', N'409', N'mešane zelenjadnice pod 0,1 ha', N'Mixed vegetables under 0,1 ha', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (156, N'e025aeae-6cc5-489f-ab13-958b026604cd', N'506', N'mešanica rastlin - naknadni posevek', N'Plant mixture - subsequent sowing', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (157, N'd316b2dd-ab0e-40e2-a692-867693eadb16', N'011', N'mešanice žit (jara)', N'Cereal mix (spring)', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (158, N'7d2a889f-6947-49ef-9b56-1fe81a2456a9', N'811', N'mešanice žit (ozimna)', N'Cereal mix (winter)', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (159, N'86d2de8b-2507-492a-8495-47418d69a058', N'722', N'miskant', N'Miscanthus', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (160, N'73fa0cd0-6376-47dd-847f-79651353fd0b', N'658', N'murva', N'Mullbery', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (162, N'b9e35c8d-17f2-4f84-a8d0-1a5e939d6323', N'719', N'murva za gojenje sviloprejk', N'Mullbery for silkworm breeding', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (163, N'805f8544-dac9-4704-86b8-83bfe71f41d0', N'661', N'namizno grozdje', N'Table grapes', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (164, N'9fcdde8e-a519-4c54-a88e-14159e26e72f', N'614', N'nashi', N'Asian pears', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (166, N'65194783-ae52-4a9c-91af-b755eff6b5fc', N'223', N'navadna nokota ', N'Common bird''s-foot trefoil', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (167, N'582f8bc5-2016-4fec-8a7e-24a1ed7aa27a', N'622', N'nektarina', N'Nectarine', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (169, N'5b2b9ef1-dd22-44f8-8666-70377923c70f', N'616', N'nešplja', N'Medlar', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (171, N'2cc29a91-6960-457c-a623-7f2e70f3c3eb', N'000', N'ni v uporabi', N'not in use', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (172, N'5bf1d958-f761-4bec-929b-62e8f0dd7f39', N'404', N'enoletna in dovoletna njivska zelišča', N'Annual and biennial field herbs', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (173, N'ba140d56-1248-4f9a-a2d4-df0aecb3e13b', N'735', N'okrasne rastline', N'Ornamnetal plants', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (174, N'906e731b-b441-4580-80f6-aa6b08dafc49', N'800', N'oljka', N'Olive tree', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (175, N'0d5d1033-f041-4af7-8a87-4ebf22926e2c', N'013', N'oljna buča', N'Oilseed pumpkin', N'c_5328', NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (176, N'9e5a840f-dfd5-4e64-a6a7-1676973249fb', N'014', N'oljna ogrščica (jara)', N'Oilseed rape (spring)', N'c_5328', NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (177, N'df35f640-844b-44b5-b0f1-33dfbdd27f10', N'814', N'oljna ogrščica (ozimna)', N'Oilseed rape (winter)', N'c_5328', NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (178, N'94041799-6654-43fe-85a4-b6053b5cc199', N'113', N'oljna redkev', N'Oil radish', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (179, N'7573944b-d229-4d0e-9237-ef9e4181e718', N'103', N'oljna repica', N'Oilseed rape', N'c_5328', NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (180, N'efa642d4-b3a2-4257-94eb-d058c18b5b45', N'631', N'oreh', N'Walnut', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (182, N'08d1b78c-4b6e-45d0-b065-e9a8f6bdac58', N'698', N'oreh in kostanj', N'Walnut and Chestnut', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (184, N'ebf74250-65ed-4a95-bfc5-92a4fce3325b', N'008', N'oves (jari)', N'Oat (spring)', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (185, N'd63164cd-ead5-4def-b923-16e43f1f1dac', N'808', N'oves (ozimni)', N'Oat (winter)', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (186, N'efc20c29-fbe0-4a1e-a368-0f03e11b0957', N'634', N'pekan oreh op.1', N'Pecan-nut op.1', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (187, N'50b614c6-cebf-4d4f-b08e-4b9086150e33', N'003', N'pira (jara)', N'Spelt (spring)', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (188, N'6cf4453e-894f-4783-b0c0-d6012d9b36ec', N'803', N'pira (ozimna)', N'Spelt (winter)', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (189, N'7b11052d-5683-4e27-a356-c7b40ff3d34e', N'108', N'podzemna koleraba', N'Swedish turnip', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (190, N'a5315d2d-735b-457f-a554-d12e5242b01c', N'673', N'pomarančevec', N'Orange tree', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (192, N'cfa50427-6268-4b78-9944-a10418676fef', N'777', N'površina v odstopu', N'Area in concession', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (193, N'5c5e225c-8ffe-4b9b-aa54-78a8a3f24577', N'026', N'praha', N'Fallow land', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (194, N'ed866c8d-f6ca-4af6-93ac-ac4454a38977', N'010', N'proso', N'Millet', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (195, N'7df12034-72fd-40f3-b12f-f59a02d6aae2', N'001', N'pšenica (jara)', N'Wheat (spring)', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (196, N'eb2f0739-1e9f-4d86-921e-71c6e5a75211', N'801', N'pšenica (ozimna)', N'Wheat (winter)', N'c_8412', NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (197, N'e8af6b03-3d96-4b2d-951f-4bc1e72fd993', N'734', N'rabarbara', N'Rhubarb', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (198, N'232a44bb-5fb6-4dd1-a461-98b96d3bf300', N'649', N'rakitovec', N'Sea buckthorn', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (200, N'2123eefa-2340-42d9-b5fb-774c022ddff4', N'655', N'rdeči ribez', N'Red currant', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (201, N'ee0052a0-e769-43fa-a121-57e52e2baec0', N'036', N'riček', N'Camelina', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (202, N'179a3b70-5891-4e46-a092-9d2ff58c7e7e', N'034', N'rjava indijska gorčica', N'Brown Indian mustard', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (203, N'7e296e61-5847-4e75-98ff-c395a6841d91', N'654', N'robida', N'Blackberry', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (204, N'f0bfc748-3204-40c7-aae2-9d41b1600aab', N'662', N'robida x malina', N'Blackberry * raspberry', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (205, N'320134b3-8f11-4cda-9d61-27cb44d086cd', N'002', N'rž (jara)', N'Rye (spring)', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (206, N'6100adf6-3b6e-407b-9058-30049f0db7c5', N'802', N'rž (ozimna)', N'Rye (winter)', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (207, N'bfb14193-8033-4d2c-bbdb-71b0dd3ce554', N'678', N'užitno modro kosteničje', N'Honeyberry', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (208, N'383e80fd-91f3-4717-ba62-466fea688850', N'024', N'sirek', N'Sorghum', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (209, N'6ef57b82-279a-491f-bd1f-e465fa824b60', N'737', N'sivka', N'Lavender', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (210, N'36a703a6-e832-4ff5-b904-f9f63416b4fe', N'677', N'skorš', N'Service tree', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (212, N'113b57e0-b93c-47fd-bdbc-c9240a21b8ca', N'049', N'sladka koruza', N'Sweet corn', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (213, N'1c5d1d2d-d8e8-4c2b-9e66-37d69d861f61', N'019', N'sladkorna pesa', N'Sugar beet', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (214, N'c49ffd9d-5ea0-4bc2-be96-3f306fa138ae', N'623', N'sliva/češplja', N'Plum', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (216, N'363a12f9-1914-495b-b596-7776affc2af3', N'647', N'smokva (figa)', N'Fig', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (218, N'9d647958-7072-4b93-a4ab-274c9f15d75f', N'030', N'soja', N'Soybean', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (219, N'e3826032-1660-4825-9f1e-f9c397c6eab7', N'012', N'sončnice', N'Sunflower', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (220, N'92b9742e-3c3c-480f-8e25-11febdab518e', N'021', N'soržica (jara) ', N'wheat and rye mix (spring)', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (221, N'531bfe8d-4db6-420d-a764-821c1bbe7b59', N'821', N'soržica (ozimna) ', N'Wheat and rye mix (winter)', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (222, N'9c1aa791-3bbb-4853-8455-49dc9c09415f', N'116', N'sudanska trava', N'Sudan grass', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (223, N'078dbd8b-ecdc-424f-8e7d-b4b81ec03cd9', N'680', N'šipek', N'Rosehip', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (225, N'6235cafd-6878-4337-b6c2-82c26e2c6667', N'703', N'šparglji', N'Asparagus', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (226, N'4ce8b0d3-2894-4719-821b-92e21b388db1', N'333', N'tehnično ali drugo sredstvo', N'technical or other means', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (227, N'5243bd3e-0956-440c-826f-71f07b2fc8ce', N'723', N'tobakovec', N'Tobacco', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (228, N'4bfa3dbb-6f3b-46e2-bb55-ddca13ea8666', N'204', N'trajno travinje', N'Perennial grassland', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (230, N'4f31e245-3dd2-4ea6-a36d-5de94bc047ec', N'505', N'trava - podsevek', N'grass - undersowing', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (231, N'8f53a9b5-083f-41db-8405-e7f89644bfed', N'500', N'podsevek navadne nokote', N'undersowing of coomon bird''s-foot trefoil', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (232, N'064a613f-6bce-4092-a84c-599e65d34f7c', N'501', N'podsevek krmnega graha ', N'undersowing of fodder peas', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (233, N'5be80432-5f73-419e-b937-627d880f82ac', N'502', N'podsevek deteljnotravnih mešanic', N'undersowing of clovergrass mixes', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (234, N'51d1d53b-dbba-4b60-8e27-023bee399986', N'503', N'podsevek detelje', N'undersowing of clover', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (235, N'9583ca05-0e8a-4027-b46a-198e3ea07207', N'504', N'podsevek inkarnatke', N'undersowing of crimson clover', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (236, N'aae5deb8-0c10-49da-838b-33a244e43f77', N'201', N'trave', N'grasses', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (237, N'a64890ea-bd91-4b01-a221-e877f6d7c16e', N'202', N'travna ruša (travni tepih) ', N'grass carpet', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (238, N'4a37717d-1188-43a5-b4a0-c8c8733d785d', N'203', N'travnodeteljne mešanice', N'Grass clover mixtures', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (239, N'e8030314-e0e5-475c-89f6-f7da9e9848eb', N'025', N'trda pšenica (jara)', N'Hard (Durum) wheat (spring)', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (240, N'416b6535-0bce-4b34-839e-fe6100a1f2f7', N'825', N'trda pšenica (ozimna)', N'Hard (Durum) wheat (winter)', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (241, N'65faa49e-2981-4743-bdfa-9fa6c5b6a4ac', N'007', N'tritikala (jara)', N'Triticale (spring)', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (242, N'614e4325-1386-4fd3-9f33-8aeb85add045', N'807', N'tritikala (ozimna)', N'Triticale (winter)', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (243, N'd071dbcb-5a21-4e1c-9ca6-e92005b2e864', N'704', N'trsnice', N'Vine grafts', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (244, N'd2ac3ef7-e3e6-4acb-89b7-d9fb1bdb4661', N'706', N'trta za drugo rabo, ki ni vino ali namizno grozdje', N'Grapevine for other uses, not wine or table grapes', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (245, N'a719c822-1bae-4dbf-b83c-cff7dbebf15b', N'029', N'ukorenišče hmeljnih sadik', N'Hops cutting rootstocks', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (246, N'5c8d82de-2b74-4000-8ca9-e07c31ef18ee', N'100', N'vinska trta', N'Grapevine', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (247, N'798adf7c-2722-475c-b21c-865173eb935f', N'626', N'višnja', N'Sour cherry', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (249, N'2b03cb54-b60c-4501-8789-899972aead27', N'210', N'volčji bob', N'Lupine', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (250, N'5bc6bd3f-67f4-4d0b-a2d0-703b354953bc', N'031', N'vrtni mak (jari)', N'Garden poppy (spring)', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (251, N'7d0e7b1e-ca9a-468a-b0cd-96e9bf09ab30', N'831', N'vrtni mak (ozimni)', N'Garden poppy (winter)', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (252, N'b9884ca1-5d76-406b-8a68-9f630e607b2c', N'736', N'vrtnice', N'Roses', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (253, N'1d9a96b1-7ec5-4ff3-8b3c-d8b896380af8', N'618', N'žižula', N'Jujube', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (255, N'838772fd-6949-4995-857f-fd5dcad8f06a', N'200', N'trave za pridelavo semena', N'Grass for seed production', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (256, N'd129d1ff-6e94-4bfb-a68d-66f77a3c9fc2', N'403', N'različna trajna zelišča', N'various perennial herbs', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (257, N'cf557b34-a5b8-40b4-bfdd-082ef9e458d3', N'705', N'mešane trajne rastline pod 0,1 ha', N'mixed perrenial plants under 0,1 ha', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (258, N'b7ff0c78-3397-48ce-b3c7-5065ac5c67a2', N'129', N'druge mešanice  z rastlinami, ki vežejo dušik', N'other mixtures with nitrogen-fixing plants', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (259, N'4e2efb39-f4c1-45e4-a757-cb8c30c74109', N'057', N'ajda - medonosna praha', N'buckwheat - honeydew fallow land', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (260, N'0b605d3a-33d8-4115-a5c7-6bf5408a14a8', N'056', N'facelija - medonosna praha', N'Phacelia - honeydew fallow land', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (261, N'cb397671-4713-45ca-9318-cd4524d08498', N'059', N'lan - medonosna praha', N'Flaxseed - honeydew fallow land', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (262, N'754ce4c6-c7b9-4a88-b341-3fbf3c43d0aa', N'058', N'sončnice - medonosna praha', N'Sunflower- honeydew fallow land', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (263, N'702a9107-bc90-40e7-87fd-37eb26068683', N'682', N'šmarna hrušica', N'Juneberry', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (264, N'60d7c22b-1270-43b3-8ffd-75a0476ff65d', N'055', N'oljna redkev - medonosna praha', N'Oil radish - honeydew fallow land', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (265, N'6a5fcdc0-4ba3-476b-8a53-8bf111a634a2', N'054', N'bela gorjušica - medonosna praha', N'White mustard - honeydew fallow land', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (266, N'7c19e0bc-2c19-42a6-99a8-069a835f9c59', N'052', N'mešanica medonosnih rastlin', N'Mixture of honeydew plants', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (267, N'3efc352c-de2d-4e0b-b654-c3378ec3683c', N'053', N'mešanica medonosnih rastlin z drugimi kmetijskimi rastlinami', N'Mixture of honeydew plants with other crops', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (268, N'd15b6e0e-abd4-4081-ab7e-415d040681db', N'444', N'pridelava ni v tleh', N'production not in soil ', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[crop] ([id], [uuid], [identifier], [name], [name_en], [agrovoc_code], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (270, N'6a29c9be-06e5-48f3-8a40-8e4eb07c0b58', N'0000', N'avokado', N'avocado', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
SET IDENTITY_INSERT [FieldBook].[crop] OFF
GO
SET IDENTITY_INSERT [FieldBook].[document_type] ON 
GO
INSERT [FieldBook].[document_type] ([id], [uuid], [name], [name_en], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (1, N'c35466e1-a72f-4b19-a327-ac3571efefe3', N'Deklaracija škropiv', N'Pesticide declaration', NULL, NULL, CAST(N'2024-07-08T13:07:53.943' AS DateTime), 3, 0, 1)
GO
INSERT [FieldBook].[document_type] ([id], [uuid], [name], [name_en], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (2, N'23964229-032f-4e44-821c-60c5321de2d6', N'Deklaracija gnojil', N'Fertilizer declaration', NULL, NULL, CAST(N'2024-07-08T13:08:06.770' AS DateTime), 3, 0, 1)
GO
INSERT [FieldBook].[document_type] ([id], [uuid], [name], [name_en], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (3, N'02279ee2-8cf6-4a95-8d32-f3db35812698', N'Deklaracija semen', N'Seet declaration', NULL, NULL, CAST(N'2024-07-08T13:08:15.617' AS DateTime), 3, 0, 1)
GO
INSERT [FieldBook].[document_type] ([id], [uuid], [name], [name_en], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (4, N'30f3d1e2-d1c8-4928-9c4b-41dce4e59715', N'Ostalo', N'Other', NULL, NULL, CAST(N'2024-07-08T13:08:22.460' AS DateTime), 3, 0, 1)
GO
INSERT [FieldBook].[document_type] ([id], [uuid], [name], [name_en], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (5, N'6b953bdd-948e-4f65-9cfd-526d8e1772f5', N'Račun', N'Invoice', NULL, NULL, CAST(N'2024-07-08T13:06:49.903' AS DateTime), 3, 0, 1)
GO
SET IDENTITY_INSERT [FieldBook].[document_type] OFF
GO
SET IDENTITY_INSERT [FieldBook].[fertilizer] ON 
GO
INSERT [FieldBook].[fertilizer] ([id], [uuid], [name], [name_en], [brand_name], [type], [ratio_n], [ratio_p], [ratio_k], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (1, N'0f0557b6-d928-4d10-9f98-3f563ddf7a2c', N'KAN 27%', N'Ammonium Nitrate 27 %', NULL, N'MINERAL', 27, 0, 0, N'{}', 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[fertilizer] ([id], [uuid], [name], [name_en], [brand_name], [type], [ratio_n], [ratio_p], [ratio_k], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (2, N'8615f207-12c8-451e-96b4-8491cee62bd0', N'AN amonijev-nitrat', N'Ammonium Nitrate', NULL, N'MINERAL', 33, 0, 0, N'{}', 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[fertilizer] ([id], [uuid], [name], [name_en], [brand_name], [type], [ratio_n], [ratio_p], [ratio_k], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (3, N'4c3ad1b1-9833-4b7a-9a77-dd8f89467cb9', N'UAN urea + amonnitrat', N'Urea + Ammonium Nitrate', NULL, N'MINERAL', 32, 0, 0, N'{}', 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[fertilizer] ([id], [uuid], [name], [name_en], [brand_name], [type], [ratio_n], [ratio_p], [ratio_k], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (4, N'cb93d1c1-2b16-4018-bab0-bd9e9a8f2c64', N'UREA 46%', N'Urea 46 %', NULL, N'MINERAL', 46, 0, 0, N'{}', 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[fertilizer] ([id], [uuid], [name], [name_en], [brand_name], [type], [ratio_n], [ratio_p], [ratio_k], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (5, N'eaf44a60-f5b4-43cd-b4c0-934fb7ef360b', N'Kalcijev soliter', N'Calcium Nitrate', NULL, N'MINERAL', 20, 0, 0, N'{}', 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[fertilizer] ([id], [uuid], [name], [name_en], [brand_name], [type], [ratio_n], [ratio_p], [ratio_k], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (6, N'5e6677ee-8ea1-4ace-94a0-7afe1a4244ad', N'Apneni dušik', N'Lime Nitrogen', NULL, N'MINERAL', 20, 0, 0, N'{}', 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[fertilizer] ([id], [uuid], [name], [name_en], [brand_name], [type], [ratio_n], [ratio_p], [ratio_k], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (7, N'0677c15a-0c4a-4b11-b716-1e722a0e12ff', N'Amosulfan 20N+24S', N'Ammonium Sulphate', NULL, N'MINERAL', 20, 0, 0, N'{}', 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2024-09-03T15:06:00.430' AS DateTime), 1, 0, 1)
GO
INSERT [FieldBook].[fertilizer] ([id], [uuid], [name], [name_en], [brand_name], [type], [ratio_n], [ratio_p], [ratio_k], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (8, N'00d0e418-b1e2-4fd6-aec2-16401e1a8582', N'NPK 15-15-15', NULL, NULL, N'MINERAL', 15, 15, 15, N'{}', 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[fertilizer] ([id], [uuid], [name], [name_en], [brand_name], [type], [ratio_n], [ratio_p], [ratio_k], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (9, N'c18202dd-11f7-45f0-83cf-a8ea7e9f24eb', N'NPK 15-15-15 + 3S', NULL, NULL, N'MINERAL', 15, 15, 15, N'{}', 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[fertilizer] ([id], [uuid], [name], [name_en], [brand_name], [type], [ratio_n], [ratio_p], [ratio_k], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (10, N'864d8605-64d7-4bba-819c-7ec14c1556ea', N'NPK 7-20-30', NULL, NULL, N'MINERAL', 7, 20, 30, N'{"formulacija_n":7,"formulacija_p":20,"formulacija_k":30}', 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[fertilizer] ([id], [uuid], [name], [name_en], [brand_name], [type], [ratio_n], [ratio_p], [ratio_k], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (11, N'f7ee4395-bfce-4f98-841e-17d5d2f3a29d', N'NPK 7-20-30 +3S', NULL, NULL, N'MINERAL', 7, 20, 30, N'{}', 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[fertilizer] ([id], [uuid], [name], [name_en], [brand_name], [type], [ratio_n], [ratio_p], [ratio_k], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (12, N'8b04a37d-ae78-4e1f-8f8e-0d1ef55419bd', N'NPK 20-8-8', NULL, NULL, N'MINERAL', 20, 8, 8, N'{}', 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[fertilizer] ([id], [uuid], [name], [name_en], [brand_name], [type], [ratio_n], [ratio_p], [ratio_k], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (13, N'8c1448cf-0877-4611-b666-ddfd1dfe151b', N'NPK 11-7-7', NULL, NULL, N'MINERAL', 11, 7, 7, N'{}', 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[fertilizer] ([id], [uuid], [name], [name_en], [brand_name], [type], [ratio_n], [ratio_p], [ratio_k], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (14, N'2bae226e-46fb-42b0-81ea-261e10825ebb', N'NPK 25-20-15', NULL, NULL, N'MINERAL', 25, 20, 15, N'{}', 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[fertilizer] ([id], [uuid], [name], [name_en], [brand_name], [type], [ratio_n], [ratio_p], [ratio_k], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (15, N'883ecb77-77b5-40f6-88ce-4e972e73ce3d', N'NPK 8-12-36 + MgO + ME', NULL, NULL, N'MINERAL', 8, 12, 36, N'{}', 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[fertilizer] ([id], [uuid], [name], [name_en], [brand_name], [type], [ratio_n], [ratio_p], [ratio_k], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (16, N'2b7892a2-cefc-46ed-8ec3-dcfd7085e767', N'NPK 5-17-27 + B', NULL, NULL, N'MINERAL', 5, 17, 27, N'{}', 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[fertilizer] ([id], [uuid], [name], [name_en], [brand_name], [type], [ratio_n], [ratio_p], [ratio_k], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (17, N'037a16bb-ea16-4454-a003-b156ddf24708', N'NPK 8-20-30+2S', NULL, NULL, N'MINERAL', 8, 20, 30, N'{}', 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[fertilizer] ([id], [uuid], [name], [name_en], [brand_name], [type], [ratio_n], [ratio_p], [ratio_k], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (18, N'091f6327-c5c7-47fb-acf1-53ccd7484355', N'NPK 7-21-21+4S+0,05Zn', NULL, NULL, N'MINERAL', 7, 21, 21, N'{}', 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[fertilizer] ([id], [uuid], [name], [name_en], [brand_name], [type], [ratio_n], [ratio_p], [ratio_k], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (19, N'6df42b92-c088-4f5a-b57e-16f36d85880d', N'NPK 8-20-30+2S', NULL, NULL, N'MINERAL', 8, 20, 30, N'{}', 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[fertilizer] ([id], [uuid], [name], [name_en], [brand_name], [type], [ratio_n], [ratio_p], [ratio_k], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (20, N'ef67901c-ee37-4442-beff-ac92726c2fa1', N'NPK 15-15-15+11S', NULL, NULL, N'MINERAL', 15, 15, 15, N'{}', 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[fertilizer] ([id], [uuid], [name], [name_en], [brand_name], [type], [ratio_n], [ratio_p], [ratio_k], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (21, N'218e2f09-9bd6-4787-bd60-0ac14db53530', N'NPK 6-12-24+6S+3Ca', NULL, NULL, N'MINERAL', 6, 12, 24, N'{}', 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[fertilizer] ([id], [uuid], [name], [name_en], [brand_name], [type], [ratio_n], [ratio_p], [ratio_k], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (22, N'f4f3700f-44fd-45fc-8573-598b79b7ce65', N'NPK 6-24-12+5S+2Ca', NULL, NULL, N'MINERAL', 6, 24, 12, N'{}', 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[fertilizer] ([id], [uuid], [name], [name_en], [brand_name], [type], [ratio_n], [ratio_p], [ratio_k], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (23, N'6208d325-0c31-47aa-b2ff-7675ed9ce3ab', N'NP 12-25-0', NULL, NULL, N'MINERAL', 12, 25, 0, N'{}', 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[fertilizer] ([id], [uuid], [name], [name_en], [brand_name], [type], [ratio_n], [ratio_p], [ratio_k], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (24, N'07b2e898-455c-4105-b572-d876dae74ae6', N'NP16-48-0', NULL, NULL, N'MINERAL', 16, 48, 0, N'{}', 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[fertilizer] ([id], [uuid], [name], [name_en], [brand_name], [type], [ratio_n], [ratio_p], [ratio_k], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (25, N'430ed520-e0e5-49bd-83e2-7d722c0d3873', N'NP 18-28-0', NULL, NULL, N'MINERAL', 18, 28, 0, N'{}', 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[fertilizer] ([id], [uuid], [name], [name_en], [brand_name], [type], [ratio_n], [ratio_p], [ratio_k], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (26, N'8af2eed2-16e7-4290-825a-dedcbb500f8c', N'NP 13-23-0', NULL, NULL, N'MINERAL', 13, 23, 0, N'{}', 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[fertilizer] ([id], [uuid], [name], [name_en], [brand_name], [type], [ratio_n], [ratio_p], [ratio_k], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (27, N'f43ffc23-27e0-4c56-a33c-346314670d56', N'NP 12-26-0', NULL, NULL, N'MINERAL', 12, 26, 0, N'{}', 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[fertilizer] ([id], [uuid], [name], [name_en], [brand_name], [type], [ratio_n], [ratio_p], [ratio_k], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (28, N'e8c9a0df-641f-4998-94b3-8d5323a0d9c2', N'NP 6-36-0', NULL, NULL, N'MINERAL', 6, 36, 0, N'{}', 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[fertilizer] ([id], [uuid], [name], [name_en], [brand_name], [type], [ratio_n], [ratio_p], [ratio_k], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (29, N'dff9f0dc-5072-44a1-a148-8912aa66dabd', N'NK 10-0-38', NULL, NULL, N'MINERAL', 10, 0, 38, N'{}', 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2024-09-17T10:22:51.793' AS DateTime), 1, 0, 1)
GO
INSERT [FieldBook].[fertilizer] ([id], [uuid], [name], [name_en], [brand_name], [type], [ratio_n], [ratio_p], [ratio_k], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (30, N'fec62766-f72d-4696-a24f-70f0205a8c4f', N'NK 14-0-30', NULL, NULL, N'MINERAL', 14, 0, 30, N'{}', 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[fertilizer] ([id], [uuid], [name], [name_en], [brand_name], [type], [ratio_n], [ratio_p], [ratio_k], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (31, N'b4df8bc7-be3b-4aa0-98e9-504630033122', N'NK 18-0-16', NULL, NULL, N'MINERAL', 18, 0, 16, N'{}', 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[fertilizer] ([id], [uuid], [name], [name_en], [brand_name], [type], [ratio_n], [ratio_p], [ratio_k], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (32, N'4891fd35-acec-4954-a2ab-11815cf83b62', N'NK 16-0-20 S', NULL, NULL, N'MINERAL', 16, 0, 20, N'{}', 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[fertilizer] ([id], [uuid], [name], [name_en], [brand_name], [type], [ratio_n], [ratio_p], [ratio_k], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (33, N'06f322e0-0d79-4970-806f-33237ab0aa9b', N'NK 18-0-17 S', NULL, NULL, N'MINERAL', 18, 0, 17, N'{}', 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[fertilizer] ([id], [uuid], [name], [name_en], [brand_name], [type], [ratio_n], [ratio_p], [ratio_k], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (34, N'20870749-a8dd-4e78-aa7e-5c9393b96172', N'NK 14-0-24 S', NULL, NULL, N'MINERAL', 14, 0, 24, N'{}', 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[fertilizer] ([id], [uuid], [name], [name_en], [brand_name], [type], [ratio_n], [ratio_p], [ratio_k], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (35, N'ea30b093-768e-4331-8569-806ad5af6914', N'PK 0-18-18', NULL, NULL, N'MINERAL', 0, 18, 18, N'{}', 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[fertilizer] ([id], [uuid], [name], [name_en], [brand_name], [type], [ratio_n], [ratio_p], [ratio_k], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (36, N'f00d8758-4318-478f-8ded-982916a25221', N'PK 0-15-30', NULL, NULL, N'MINERAL', 0, 15, 30, N'{}', 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[fertilizer] ([id], [uuid], [name], [name_en], [brand_name], [type], [ratio_n], [ratio_p], [ratio_k], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (37, N'd7028543-05fb-4162-b697-378f3b80da5b', N'PK 0-24-24 S', NULL, NULL, N'MINERAL', 0, 24, 24, N'{}', 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[fertilizer] ([id], [uuid], [name], [name_en], [brand_name], [type], [ratio_n], [ratio_p], [ratio_k], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (38, N'65a844d1-a792-457b-ac18-a200ff02202e', N'PK 0-29-18 S', NULL, NULL, N'MINERAL', 0, 29, 18, N'{}', 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[fertilizer] ([id], [uuid], [name], [name_en], [brand_name], [type], [ratio_n], [ratio_p], [ratio_k], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (39, N'bebd4cfe-89fc-434a-a2da-02b396417f0a', N'PK 0-20-30', NULL, NULL, N'MINERAL', 0, 20, 30, N'{}', 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[fertilizer] ([id], [uuid], [name], [name_en], [brand_name], [type], [ratio_n], [ratio_p], [ratio_k], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (40, N'b112edfa-e1dc-4764-8701-b92299a831b1', N'PK 0-22-33', NULL, NULL, N'MINERAL', 0, 22, 33, N'{}', 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[fertilizer] ([id], [uuid], [name], [name_en], [brand_name], [type], [ratio_n], [ratio_p], [ratio_k], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (41, N'764e73af-60d9-4a32-bee5-7e5997424364', N'Superfosfat 16-20%', N'Supephosphate 16-20%', NULL, N'MINERAL', 0, 16, 0, N'{}', 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[fertilizer] ([id], [uuid], [name], [name_en], [brand_name], [type], [ratio_n], [ratio_p], [ratio_k], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (42, N'342b6610-88b5-40d3-a916-cd9b4b06dde1', N'Hyperkorn 26 %', N'Hypercorn 26 %', NULL, N'MINERAL', 0, 26, 0, N'{}', 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[fertilizer] ([id], [uuid], [name], [name_en], [brand_name], [type], [ratio_n], [ratio_p], [ratio_k], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (43, N'79ebb39d-e0b5-4c49-9002-21d5dc65da04', N'Kalijeva sol - 46-65%', N'Potassium Salt - 46-65%', NULL, N'MINERAL', 0, 0, 46, N'{}', 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[fertilizer] ([id], [uuid], [name], [name_en], [brand_name], [type], [ratio_n], [ratio_p], [ratio_k], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (44, N'ca49290b-189e-4595-932f-a2ad4579ef74', N'Hlevski gnoj', N'Manure', NULL, N'ORGANIC', NULL, NULL, NULL, N'{}', 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2024-09-18T08:12:17.397' AS DateTime), 1, 0, 1)
GO
INSERT [FieldBook].[fertilizer] ([id], [uuid], [name], [name_en], [brand_name], [type], [ratio_n], [ratio_p], [ratio_k], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (45, N'903a4bc8-c4aa-40d8-86b4-ea29aee40db9', N'Gnojevka', N'Slurry', NULL, N'ORGANIC', NULL, NULL, NULL, N'{}', 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2024-09-17T09:29:35.473' AS DateTime), 1, 0, 1)
GO
INSERT [FieldBook].[fertilizer] ([id], [uuid], [name], [name_en], [brand_name], [type], [ratio_n], [ratio_p], [ratio_k], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (46, N'5ae67f1c-faf8-4523-8cb7-4bf8b9f9b014', N'Kompost', N'Compost', NULL, N'ORGANIC', NULL, NULL, NULL, N'{}', 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2024-09-18T08:12:26.090' AS DateTime), 1, 0, 1)
GO
INSERT [FieldBook].[fertilizer] ([id], [uuid], [name], [name_en], [brand_name], [type], [ratio_n], [ratio_p], [ratio_k], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (47, N'47c095c2-0e81-481f-a7ad-dff3d8a92fe8', N'Bioplinski digestat', N'Biogas digestate', NULL, N'ORGANIC', NULL, NULL, NULL, N'{}', 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2024-09-18T08:12:13.517' AS DateTime), 1, 0, 1)
GO
INSERT [FieldBook].[fertilizer] ([id], [uuid], [name], [name_en], [brand_name], [type], [ratio_n], [ratio_p], [ratio_k], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (48, N'e47c6342-4818-4c68-a61a-08ad83e5b93b', N'N GOOO 26N +44SO3', NULL, NULL, N'MINERAL', 26, 0, 0, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[fertilizer] ([id], [uuid], [name], [name_en], [brand_name], [type], [ratio_n], [ratio_p], [ratio_k], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (49, N'd337a043-226d-4476-9629-756bf55181cc', N'N GOOO 32N +30SO3', NULL, NULL, N'MINERAL', 32, 0, 0, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[fertilizer] ([id], [uuid], [name], [name_en], [brand_name], [type], [ratio_n], [ratio_p], [ratio_k], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (50, N'374004c0-b2d6-4292-8e39-04b25777a0eb', N'N GOOO 40N +2MgO+0,1Zn+5SO3', NULL, NULL, N'MINERAL', 40, 0, 0, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[fertilizer] ([id], [uuid], [name], [name_en], [brand_name], [type], [ratio_n], [ratio_p], [ratio_k], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (51, N'12fe9209-e28d-4c1b-83c7-8668a07ba6ce', N'NPK N-GOOO 15-5-25 +9SO3', NULL, NULL, N'MINERAL', 15, 5, 25, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[fertilizer] ([id], [uuid], [name], [name_en], [brand_name], [type], [ratio_n], [ratio_p], [ratio_k], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (52, N'a46f6c26-f41a-4daf-a526-9c0020d946b7', N'NPK N-GOOO 14-6-16 +3%MgO+ 30%SO3+ 01%B', NULL, NULL, N'MINERAL', 14, 6, 16, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[fertilizer] ([id], [uuid], [name], [name_en], [brand_name], [type], [ratio_n], [ratio_p], [ratio_k], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (53, N'57225bc5-375f-4309-a274-2311fa66ca4a', N'N GOO NP 12-28-0', NULL, NULL, N'MINERAL', 12, 28, 0, NULL, NULL, 1, NULL, NULL, NULL, CAST(N'2024-09-17T10:06:58.123' AS DateTime), 1, 0, 1)
GO
INSERT [FieldBook].[fertilizer] ([id], [uuid], [name], [name_en], [brand_name], [type], [ratio_n], [ratio_p], [ratio_k], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (54, N'd4fb6810-435d-4cd9-be77-aa421816c6e1', N'N GOO NK 18-0-24 +20SO3', NULL, NULL, N'MINERAL', 18, 0, 24, NULL, NULL, 1, NULL, NULL, NULL, CAST(N'2024-09-17T13:41:53.307' AS DateTime), 1, 0, 1)
GO
INSERT [FieldBook].[fertilizer] ([id], [uuid], [name], [name_en], [brand_name], [type], [ratio_n], [ratio_p], [ratio_k], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (55, N'fde52550-bdbf-4c64-866d-e0d781a354dd', N'N GOOO NP 20-10 +32SO3', NULL, NULL, N'MINERAL', 20, 10, 0, NULL, NULL, 1, NULL, NULL, NULL, CAST(N'2024-09-18T08:32:52.173' AS DateTime), 1, 0, 1)
GO
SET IDENTITY_INSERT [FieldBook].[fertilizer] OFF
GO
SET IDENTITY_INSERT [FieldBook].[field_type] ON 
GO
INSERT [FieldBook].[field_type] ([id], [uuid], [identifier], [name], [name_en], [environment_type], [include_in_crop_rotation], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (1, N'61e3a73b-fa33-405d-a76e-ebe6950fa492', N'1100', N'Njiva', N'Arable land', N'OUTDOOR_CULTIVATION', 1, NULL, NULL, CAST(N'2024-07-16T13:38:00.377' AS DateTime), 1, 0, 1)
GO
INSERT [FieldBook].[field_type] ([id], [uuid], [identifier], [name], [name_en], [environment_type], [include_in_crop_rotation], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (2, N'5e43fa29-e4bc-4e54-936c-74f95be45219', N'1131', N'Začasno travinje', N'Temporary meadows or grassland', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[field_type] ([id], [uuid], [identifier], [name], [name_en], [environment_type], [include_in_crop_rotation], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (3, N'16451fdb-8539-44d9-84f0-5a1a05562a7c', N'1211', N'Vinograd', N'Vineyard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[field_type] ([id], [uuid], [identifier], [name], [name_en], [environment_type], [include_in_crop_rotation], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (4, N'9ead6e4a-6874-4f23-9f94-63d9e8ef94f0', N'1221', N'Intenzivni sadovnjak', N'Intensive orchard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[field_type] ([id], [uuid], [identifier], [name], [name_en], [environment_type], [include_in_crop_rotation], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (5, N'bf866b4d-d94f-42c9-87b7-05ee868e9e6e', N'1222', N'Ekstenzivni oz. travniški sadovnjak', N'Extensive orchard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[field_type] ([id], [uuid], [identifier], [name], [name_en], [environment_type], [include_in_crop_rotation], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (6, N'5a8a98ad-94f9-4c58-a54e-b32cef84086b', N'1240', N'Ostali trajni nasadi', N'Other permanent crops', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[field_type] ([id], [uuid], [identifier], [name], [name_en], [environment_type], [include_in_crop_rotation], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (7, N'b888f841-4bf1-4958-84c6-e60ce6d24d28', N'1300', N'Trajni travnik', N'Meadows and pastures', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[field_type] ([id], [uuid], [identifier], [name], [name_en], [environment_type], [include_in_crop_rotation], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (8, N'15e3911b-25d1-4e60-b012-1a4db70a86be', N'1320', N'Travinje z razpršenimi neupravicenimi značilnostmi', N'Grassland rough terrain', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[field_type] ([id], [uuid], [identifier], [name], [name_en], [environment_type], [include_in_crop_rotation], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (9, N'1915cd2f-9423-4d37-939e-3de557f369f4', N'1190', N'Rastlinjak', N'Greenhouse', N'PROTECTED_ENVIRONMENT', 1, NULL, NULL, CAST(N'2024-07-22T08:55:05.383' AS DateTime), 1, 0, 1)
GO
INSERT [FieldBook].[field_type] ([id], [uuid], [identifier], [name], [name_en], [environment_type], [include_in_crop_rotation], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (10, N'cf3aa245-35aa-4d5f-8a02-4b95d94ae2b5', N'1520', N'Mejica', N'Hedgerow', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[field_type] ([id], [uuid], [identifier], [name], [name_en], [environment_type], [include_in_crop_rotation], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (11, N'210c13fe-a0de-4a76-a045-98955bf88014', N'1530', N'Obvodna vegetacija', N'Riparian vegetation', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[field_type] ([id], [uuid], [identifier], [name], [name_en], [environment_type], [include_in_crop_rotation], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (12, N'416f0f40-4b6b-476b-b0ad-7befe38a74a1', N'1192', N'Rastlinjak s sadnimi rastlinami', N'Greenhouse with fruit plants', N'PROTECTED_ENVIRONMENT', 1, NULL, NULL, CAST(N'2024-07-16T14:02:36.340' AS DateTime), 1, 0, 1)
GO
INSERT [FieldBook].[field_type] ([id], [uuid], [identifier], [name], [name_en], [environment_type], [include_in_crop_rotation], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (13, N'516bba24-83ba-4ad0-a002-e0ebcb159b88', N'1191', N'Rastlinjak, kjer pridelava ni v tleh', N'Vertical farming', N'PROTECTED_ENVIRONMENT', 1, NULL, NULL, CAST(N'2024-07-16T14:02:27.273' AS DateTime), 1, 0, 1)
GO
SET IDENTITY_INSERT [FieldBook].[field_type] OFF
GO
SET IDENTITY_INSERT [FieldBook].[pesticide] ON 
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (1, N'1d2e86e7-71a3-4ced-8187-bed07386a15c', N'ACTELLIC 50 EC', N'ACTELLIC 50 EC', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (2, N'c31046ef-5a51-4550-bda7-1260a95a7d97', N'ADENGO', N'ADENGO (en test)', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2024-09-03T13:19:34.123' AS DateTime), 1, 0, 1)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (3, N'bdac67b4-0d48-4acb-9000-5a1ce76855b9', N'ADENTIS', N'ADENTIS', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (4, N'8a64af4f-cf55-4643-919b-167a4059a483', N'AFFIRM', N'AFFIRM', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (5, N'191e42f6-e9ea-47a6-b527-2f21dc57bf99', N'AFFIRM OPTI ', N'AFFIRM OPTI ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (6, N'8c736f7a-bd4d-44a0-b2a0-9e71bd497c2d', N'AFINTO ', N'AFINTO ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (7, N'33aa0ac0-0a69-4a95-8a1e-8a1fdcf3ec86', N'AGIL 100 EC ', N'AGIL 100 EC ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (8, N'0b4cc007-48cb-456d-aac8-63f605c57487', N'AGREE WG ', N'AGREE WG ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (9, N'bb433105-9767-4453-b3f5-01e18400f09e', N'AKRIS ', N'AKRIS ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (10, N'cb69cb86-3581-4052-a204-b2c93dc8581d', N'ALCOBAN ', N'ALCOBAN ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (11, N'270abce2-5e75-48be-8bd6-ca361498772c', N'ALIETTE FLASH ', N'ALIETTE FLASH ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (12, N'efd0856e-3fa3-4d39-bd4e-e5b6fde95acd', N'ALISEO ', N'ALISEO ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (13, N'13c2314e-d719-4cae-9de9-cbb27b2dd7ff', N'ALISEO PLUS ', N'ALISEO PLUS ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (14, N'bd28ca38-1f79-45f9-9117-39dbb440f2d0', N'ALISTER NEW ', N'ALISTER NEW ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (15, N'679ed423-e620-4727-8092-0dcee7ff993d', N'ALLIANCE ', N'ALLIANCE ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (16, N'3fd8f935-94d2-421e-901d-0cef9bb818c2', N'ALLY SX ', N'ALLY SX ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (17, N'bd4050fd-bae7-4b15-a1f6-37eedd236435', N'ALVERDE ', N'ALVERDE ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (18, N'97ec1b04-c971-4e4c-9e7c-faa7c11b517c', N'AMIDIR ', N'AMIDIR ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (19, N'f5ea3f05-3e3d-4ffb-b829-2177856717b9', N'AMISTAR ', N'AMISTAR ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (20, N'5e84a71d-ab88-43f1-974f-1c4394821b6e', N'AMPEXIO ', N'AMPEXIO ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (21, N'd73787c8-6820-48ab-a734-5951c5a659ab', N'AMYLO - X ', N'AMYLO - X ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (22, N'8d4d02d3-fa1b-43eb-825b-629b2642aa64', N'ANGELUS ', N'ANGELUS ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (23, N'8b372c42-a2aa-41c0-a439-13112d4857b8', N'APOLLO 50 SC ', N'APOLLO 50 SC ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (24, N'33a13f17-58fc-4a0e-a340-72476c8a64a2', N'APPLAUD 25 SC ', N'APPLAUD 25 SC ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (25, N'a3c7bf62-c617-4992-b3a5-c44b1d76c4b0', N'AQ-10 ', N'AQ-10 ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (26, N'1ea6a3c2-ad55-446f-975d-609abcf71de2', N'ARGOS ', N'ARGOS ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (27, N'dcc2a04e-7f56-4530-8a10-dbad866f9ec0', N'ARIGO ', N'ARIGO ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (28, N'c242c431-f588-4b4a-ac50-e076d1f96f40', N'ARNOLD ', N'ARNOLD ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (29, N'f239a2cb-6bff-4df6-85e3-480616edc101', N'ARRAT ', N'ARRAT ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (30, N'0731828e-c44d-4813-8462-bda49d3d9556', N'ARVALIN ', N'ARVALIN ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (31, N'69e2667c-a520-4b07-9132-3682cb71bb4d', N'ASCRA XPRO ', N'ASCRA XPRO ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (32, N'404b2e2d-1e16-4274-be49-8a8c2d642354', N'ASEF 3V1 ', N'ASEF 3V1 ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (33, N'94005659-c328-4686-847a-ce1c31248945', N'ASSET FIVE (5) ', N'ASSET FIVE (5) ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (34, N'01343c0e-bb31-4523-b791-8657df40df64', N'ASSET FIVE (6) ', N'ASSET FIVE (6) ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (35, N'1f61bd4c-929a-4162-af4e-8d9abd72d640', N'ATLANTIS STAR ', N'ATLANTIS STAR ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (36, N'9651a08e-4ae5-4493-bd70-aa7fca412a16', N'AVALON ', N'AVALON ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (37, N'7bcabdce-6781-42c0-b667-a96d4a258809', N'AXIAL ', N'AXIAL ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (38, N'b9fc054b-3141-4863-946f-a627c3f32a70', N'AXIAL ONE ', N'AXIAL ONE ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (39, N'43a3399d-dcba-4fed-9a20-7423372ffa7f', N'AZATIN EC ', N'AZATIN EC ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (40, N'687cea89-6852-4133-bb54-972c322332ef', N'AZUMO WG ', N'AZUMO WG ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (41, N'f9c647f1-e1a2-463d-bc83-822fceb077b2', N'BADGE WG ', N'BADGE WG ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (42, N'918dc783-5f82-43e7-a3c4-ed2911c71e44', N'BAIA ', N'BAIA ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (43, N'61342eb7-1464-4877-9acd-6cfa847acad7', N'BANARG ', N'BANARG ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (44, N'b090bc5b-90d1-46ae-87af-cdd0efc2bfe9', N'BANDERA ', N'BANDERA ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (45, N'b73cff33-fc59-4ddd-801c-c6417d9e346f', N'BANJO ', N'BANJO ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (46, N'11ad0eb5-d7e9-4451-a32a-e96625e0f14f', N'BANJO FORTE ', N'BANJO FORTE ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (47, N'bb446070-5270-42c4-a7cd-daf00caee51e', N'BANVEL 480 S ', N'BANVEL 480 S ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (48, N'ddcc7811-646c-4797-9854-6a6ea74a0e51', N'BASAGRAN 480 ', N'BASAGRAN 480 ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (49, N'e11f7398-53f4-4093-978c-e49a4e512ba7', N'BASAMID GRANULAT ', N'BASAMID GRANULAT ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (50, N'1816955f-2714-4299-9edb-a3a98d95ed2d', N'BATALION 450 SC ', N'BATALION 450 SC ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (51, N'0a342e91-e1cf-40f3-b9d2-e971df5e748f', N'BATTERY ', N'BATTERY ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (52, N'63f03f9a-95d5-4b07-ae97-fc7cee5724c8', N'BELKAR ', N'BELKAR ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (53, N'b35c45b4-cefa-4058-806f-a94ebd7b050d', N'BELLIS ', N'BELLIS ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (54, N'8f426f85-c3fb-460c-8c38-09f75b37931d', N'BELOUKHA ', N'BELOUKHA ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (55, N'd376d3b9-db53-4d45-a240-6621e7643831', N'BELTANOL ', N'BELTANOL ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (56, N'a6decbd2-2660-4184-8bfb-ffdcfe1ada74', N'BENEVIA ', N'BENEVIA ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (57, N'cadf599d-39de-4b9f-956e-72a64b5fd216', N'BENI ', N'BENI ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (58, N'c58a3704-69d8-41b2-9944-8249205dd0a3', N'BETTIX FLO ', N'BETTIX FLO ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (59, N'88a9e682-bc8f-400c-9a48-e3c2dda372d5', N'BIATHLON 4D ', N'BIATHLON 4D ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (60, N'fbef7333-58af-457b-9612-9902f009ea7b', N'BIO PLANTELLA ARION PROTI POLŽEM ', N'BIO PLANTELLA ARION PROTI POLŽEM ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (61, N'f47210ec-aa20-4c41-a95a-f15f18d5e5bd', N'BIO PLANTELLA KENYATOX ', N'BIO PLANTELLA KENYATOX ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (62, N'61527997-02d5-4118-a863-4bba08c4dc99', N'BIOTIP APHICID PLUS ', N'BIOTIP APHICID PLUS ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (63, N'd5fa17bd-36df-4dc7-bc8f-2d76039a7544', N'BIOTIP FLORAL ', N'BIOTIP FLORAL ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (64, N'efe7e2ae-b937-42c0-ae1b-fe6bcf925446', N'BIOTIP SULFO 800 SC ', N'BIOTIP SULFO 800 SC ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (65, N'c2407ee0-d761-4bd1-a12c-0786a0649b51', N'BIOTIP UBIJ ME NEŽNO PLUS ', N'BIOTIP UBIJ ME NEŽNO PLUS ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (66, N'22115245-0d11-4711-b280-48cfaaec0fce', N'BIOTIP VABA ZA POLŽE ', N'BIOTIP VABA ZA POLŽE ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (67, N'aa4d86f3-07c1-4605-85f0-d92fcc6dc4db', N'BIZON ', N'BIZON ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (68, N'2f197e6c-d35a-4dcf-943c-f4014d3c83c5', N'BONACA ', N'BONACA ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (69, N'871d5797-b963-4081-85e5-9dba41ab94a7', N'BOOM EFEKT ', N'BOOM EFEKT ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (70, N'f2207362-b5a5-4ddb-a71f-f2da31a533b7', N'BOTANIGARD OD ', N'BOTANIGARD OD ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (71, N'd002b5ac-e811-4be7-a7d4-d17734d7f52e', N'BOTANIGARD WP ', N'BOTANIGARD WP ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (72, N'1219b0f9-0c73-489b-bbe6-47bb12ca6ecd', N'BOTECTOR ', N'BOTECTOR ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (73, N'4ced6846-e01e-40bd-adbf-8c70df10c23d', N'BOTIGA ', N'BOTIGA ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (74, N'e399ef1e-40d0-4300-8bf9-a6783db0bf06', N'BOUDHA ', N'BOUDHA ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (75, N'd3b5123f-8661-4355-9ab7-f23116ef3d7a', N'BOUNTY ', N'BOUNTY ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (76, N'c54d76a2-4d9e-41d2-b7e6-5ddc10041615', N'BOXER ', N'BOXER ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (77, N'5edbc0f9-b9ee-4206-9df4-80a6034c007c', N'BQM SUPER ', N'BQM SUPER ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (78, N'cabca0d7-8e6b-4838-bc22-d81d6cbf9796', N'BREVIS ', N'BREVIS ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (79, N'3e3efd2f-2586-4d4a-9b84-76223d788a6a', N'BRIVELA ', N'BRIVELA ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (80, N'4a4ab52d-2823-4e7d-a55e-31e68fa875d6', N'BUTISAN 400 SC ', N'BUTISAN 400 SC ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (81, N'da23e434-fa0a-45f1-a954-8cf759a9ff12', N'BUTISAN S ', N'BUTISAN S ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (82, N'0859efe7-4e4e-439b-bc1b-8195966512b7', N'BUZZ ULTRA DF ', N'BUZZ ULTRA DF ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (83, N'fb307e4e-c06a-4d41-bdf6-0259615b61dd', N'BUZZIN ', N'BUZZIN ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (84, N'96656175-2599-48fe-8ccd-c459c98690bb', N'CABRIO TOP ', N'CABRIO TOP ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (85, N'b503563d-b11e-4171-93b0-b92a7b6a2e2a', N'CACTAI ', N'CACTAI ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (86, N'6a328916-4eda-45af-9f6a-7c97e5e241c2', N'CALARIS PRO ', N'CALARIS PRO ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (87, N'ef4b01da-288f-4b9e-ac43-1f3287b925ba', N'CALLAM ', N'CALLAM ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (88, N'83b790e5-796a-4717-9267-ca3084704f29', N'CALLISTO 480 SC ', N'CALLISTO 480 SC ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (89, N'92394a96-01d6-4819-b5b7-2ccfd41f1d7f', N'CANTUS ', N'CANTUS ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (90, N'f41083a5-910e-45f4-ac66-a52af754e6fe', N'CAPRENO ', N'CAPRENO ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (91, N'ce8f3d54-8e01-4517-947b-2c254bb2fdde', N'CAPTAN 80 WG ', N'CAPTAN 80 WG ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (92, N'341c5a9f-ba47-4b03-ae40-f8d509f9849b', N'CARAMBA ', N'CARAMBA ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (93, N'b3a15209-a78e-4a60-b5bb-321059b166b1', N'CARIAL FLEX ', N'CARIAL FLEX ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (94, N'19680976-fa6d-40a0-ae07-075b0c2d88bd', N'CARPOVIRUSINE ', N'CARPOVIRUSINE ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (95, N'51c7512f-251b-42d2-a888-062789b195e2', N'CAYUNIS ', N'CAYUNIS ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (96, N'7683ef03-ab8f-44a9-adf9-a1f37fcff876', N'CELAFLOR CAREO GRANULAT ', N'CELAFLOR CAREO GRANULAT ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (97, N'e124188d-77c1-4f57-9791-d91eb11fda89', N'CELAFLOR CAREO KOMBINIRANE PALCKE ', N'CELAFLOR CAREO KOMBINIRANE PALCKE ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (98, N'5603b7a8-738b-4e4f-a1e3-53e6fb95f94b', N'CELAFLOR CAREO KONCENTRAT ', N'CELAFLOR CAREO KONCENTRAT ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (99, N'a032aaf9-4da7-4aa4-aa90-cec34fc81a41', N'CELAFLOR CAREO RAZPRŠILO PROTI ŠKODLJIVCEM ', N'CELAFLOR CAREO RAZPRŠILO PROTI ŠKODLJIVCEM ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (100, N'df5a4369-71fc-4d9a-8dd9-2f521c48243b', N'CELAFLOR CAREO SPRAY PROTI ŠKODLJIVCEM ', N'CELAFLOR CAREO SPRAY PROTI ŠKODLJIVCEM ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (101, N'ad1fab74-5f85-4e1d-9d65-33815aae468e', N'CELAFLOR FORTE - RAZPRŠILKA ', N'CELAFLOR FORTE - RAZPRŠILKA ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (102, N'23902e77-9710-4fe3-a263-df7344d7f8d0', N'CELAFLOR LIMEX ', N'CELAFLOR LIMEX ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (103, N'27aa5b47-e45b-4ab6-9afb-06c8dbf4309d', N'Celaflor Naturen naravni insekticid za sadje, vrtnine in okrasne rastline - koncentrat ', N'Celaflor Naturen naravni insekticid za sadje, vrtnine in okrasne rastline - koncentrat ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (104, N'470aeb9d-7002-47b3-9add-c819e98516b4', N'Celaflor Naturen naravni insekticid za sadje, vrtnine in okrasne rastline - razpršilka ', N'Celaflor Naturen naravni insekticid za sadje, vrtnine in okrasne rastline - razpršilka ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (105, N'c82c3626-73d8-49ba-b3fb-21ea6481a871', N'CENTIUM 36 CS ', N'CENTIUM 36 CS ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (106, N'f19c44a2-b3dc-4ec7-9399-cfcc35007bb7', N'CHALLENGE ', N'CHALLENGE ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (107, N'1df9bc34-b039-4fb2-a8de-25bacc0b4f0b', N'CHALLENGE 600 ', N'CHALLENGE 600 ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (108, N'c646eefa-7e19-4e49-9b96-66938965487c', N'CHAMANE ', N'CHAMANE ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (109, N'd1df72c1-d827-4b05-9178-94709c8285a5', N'CHANON ', N'CHANON ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (110, N'4ac64db0-9b47-432f-bafa-ec31a1c63d98', N'CHECKMATE PUFFER CM-PRO ', N'CHECKMATE PUFFER CM-PRO ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (111, N'736eb4a5-931f-44f4-94a1-53af0c4ca0e4', N'CHORUS 50 WG ', N'CHORUS 50 WG ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (112, N'4114c0f0-1ab5-4a46-81e2-a117e3a5cac3', N'CHORUS FORTE ', N'CHORUS FORTE ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (113, N'5174e7e1-47ed-4217-8403-d986db3e82bf', N'CLAP ', N'CLAP ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (114, N'0d04b345-4cbd-40b8-9ddf-9bc86805bd1d', N'CLAP FORTE ', N'CLAP FORTE ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (115, N'7dfad224-b58a-4d12-b081-e34128acc588', N'CLERANDA ', N'CLERANDA ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (116, N'7ae24335-41af-4470-939b-a4a31df58492', N'CLERAVO ', N'CLERAVO ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (117, N'5245f5ce-bd33-4bcd-8b12-c3ef128aa694', N'CLINIC TF ', N'CLINIC TF ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (118, N'e3e583a4-f28d-4f43-ba1a-fb2833a6ecdc', N'CLINIC XTREME (staro ime CREDIT XTREME) ', N'CLINIC XTREME (staro ime CREDIT XTREME) ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (119, N'5c9c2575-4694-4be0-8c33-2aa24fda3f62', N'CLOMATE ', N'CLOMATE ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (120, N'89513e79-56a7-4862-a61f-4cee0fbd7d44', N'CLOSER ', N'CLOSER ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (121, N'30cbb9d2-a240-4c4b-ab63-bab95b7a7117', N'CLYDE FX ', N'CLYDE FX ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (122, N'f6e83b16-877a-452b-b1e6-c86c0fb0875d', N'COLLIS ', N'COLLIS ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (123, N'729ba220-eafc-4a27-a6ae-ec364ca15e76', N'COLUMBO 0,8 MG ', N'COLUMBO 0,8 MG ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (124, N'1250f907-6acc-4cde-8732-bc039f05bb39', N'COLZAMID ', N'COLZAMID ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (125, N'0335b124-f560-47f9-9705-2e061bc752af', N'COMPO BIO SREDSTVO PROTI POLŽEM ', N'COMPO BIO SREDSTVO PROTI POLŽEM ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (126, N'3a285a25-2e99-46dd-9376-7e3b5af3767d', N'COPFORCE EXTRA ', N'COPFORCE EXTRA ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (127, N'f4d74016-eae5-4dd5-ac6d-8c3c330590c4', N'CORAGEN ', N'CORAGEN ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (128, N'b56f2d57-0160-4b40-b83c-f8be6b7115c0', N'CORELLO DUO ', N'CORELLO DUO ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (129, N'5a3cad76-be41-4748-ad0d-4286a008dbb3', N'CORIDA ', N'CORIDA ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (130, N'30eeae82-815b-4198-a2af-6d956d907e49', N'COSAN ', N'COSAN ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (131, N'bf990726-2b55-4426-b462-ddcf03c0bbce', N'COSAVET DF ', N'COSAVET DF ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (132, N'3e4320ab-88ed-4781-9914-dc26d1605264', N'COSINUS ', N'COSINUS ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (133, N'8909d883-5616-435b-8156-8db7e7268247', N'CROWN MH ', N'CROWN MH ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (134, N'63e2768d-f20a-4a08-b5ab-f5882bcc5a91', N'CUPRABLAU Z 35 WG ', N'CUPRABLAU Z 35 WG ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (135, N'bcd39f61-5fb0-4677-a06b-9e22267d3d44', N'CUPRABLAU Z 35 WP ', N'CUPRABLAU Z 35 WP ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (136, N'78d0c287-d5ad-43b6-8aa6-e3c967551fdc', N'CUPRABLAU Z 50 WP ', N'CUPRABLAU Z 50 WP ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (137, N'e3e43164-21f0-425d-921d-051a4d3bb91e', N'CUPROXAT ', N'CUPROXAT ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (138, N'c3e2da0c-e746-4229-8912-9fb2541e3c61', N'CURATIO žvepleno apnena brozga (4) ', N'CURATIO žvepleno apnena brozga (4) ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (139, N'46478e65-bacf-430e-a892-5660d711697c', N'CURATIO žvepleno apnena brozga (5) ', N'CURATIO žvepleno apnena brozga (5) ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (140, N'2858d190-ced9-4cfa-9d70-feb703ccd8ba', N'CURZATE PARTNER ', N'CURZATE PARTNER ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (141, N'c16434d8-bb81-433d-a84f-becfd7543457', N'CUSTODIA ', N'CUSTODIA ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (142, N'542e9563-20e1-4a25-bf42-ff7d222d5e3f', N'CYMBAL ', N'CYMBAL ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (143, N'ed2bc97a-5387-445c-b8c1-1e126ebab9a5', N'CZAR ', N'CZAR ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (144, N'7dfee941-afaf-43e1-bde4-afb90591e5b4', N'DAIMYO F ', N'DAIMYO F ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (145, N'7b031094-09fb-4200-a774-5c5dedc3fe8a', N'DAZIDE ENHANCE ', N'DAZIDE ENHANCE ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (146, N'26de93d5-c049-43bb-9fcf-2afbc38b4b27', N'DECIS 100 EC ', N'DECIS 100 EC ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (147, N'62694d91-c901-4809-a9c1-a96b7c5e2a81', N'DECIS 2,5 EC ', N'DECIS 2,5 EC ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (148, N'a6e9e6a8-45b1-404d-a8c0-2a07d89a96c1', N'DECIS TRAP CEŠNJEVA MUHA ', N'DECIS TRAP CEŠNJEVA MUHA ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (149, N'5f9eef6d-83e3-4449-8940-2f01bed82cf4', N'DECIS TRAP OREHOVA MUHA ', N'DECIS TRAP OREHOVA MUHA ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (150, N'c45c08e4-4ff1-4425-a2f1-801d1f72b151', N'DEGESCH PLOŠCE IN TRAKOVI ', N'DEGESCH PLOŠCE IN TRAKOVI ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (151, N'c958edc5-afa7-4bb2-b550-4a99d89bee6c', N'DELAN 700 WG ', N'DELAN 700 WG ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (152, N'4bbe5a61-ba39-40f1-9b3d-2d362547de34', N'DELAN PRO ', N'DELAN PRO ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (153, N'5a5e7ab5-550c-4724-8f8f-8c3914a6729c', N'DELARO FORTE ', N'DELARO FORTE ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (154, N'3de49f13-4b0f-4dab-b320-4899e29e46f6', N'DELEGATE 250 WG ', N'DELEGATE 250 WG ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (155, N'7db42a97-5ee5-48a8-adc2-1e826aee2d68', N'DELFIN WG ', N'DELFIN WG ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (156, N'15b1fc96-ade9-459b-96ac-cce122cb21cc', N'DELU REPELENT ZA ODGANJANJE VOLUHARJEV IN KRTOV ', N'DELU REPELENT ZA ODGANJANJE VOLUHARJEV IN KRTOV ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (157, N'87462796-f815-4195-ac9f-6a9a7ad6122c', N'DELUX 050 CS ', N'DELUX 050 CS ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (158, N'69935af5-3e7b-4ea2-b9c9-0b12b144563c', N'DEVRINOL 45 FL ', N'DEVRINOL 45 FL ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (159, N'644cc396-3597-4f9b-82ce-3611fd36bfa3', N'DICASH ', N'DICASH ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (160, N'31c58ab7-3bae-48b6-9692-f92c2d56d237', N'DIFCOR 250 EC ', N'DIFCOR 250 EC ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (161, N'51b12c1f-aacf-4ad7-9ad3-8c92b86590f2', N'DIFEND EXTRA ', N'DIFEND EXTRA ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (162, N'eceb9449-09d5-4ebb-996b-ef9fc583a242', N'DIFENZONE ', N'DIFENZONE ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (163, N'faeababb-39a5-42e1-bd02-92eb59cf4d80', N'DIFOL ', N'DIFOL ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (164, N'30b28e54-1ff0-4514-9e55-8d9b2cee0d94', N'DIGATOR ', N'DIGATOR ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (165, N'17e24ec4-8ef7-4763-9165-bed943a13553', N'DIRAGER ', N'DIRAGER ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (166, N'7b1b6794-7bf3-4d1c-a691-3b69fe7f9b8f', N'DIRAMID ', N'DIRAMID ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (167, N'9bad9785-da0b-4b33-9bb2-75d9a3185c7c', N'DOMARK 100 EC ', N'DOMARK 100 EC ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (168, N'ef7ed688-4dfd-4908-8b10-7bc909846517', N'DUAL GOLD 960 EC ', N'DUAL GOLD 960 EC ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (169, N'116cf962-1db1-4b7c-a9f7-84c34dc1ab0d', N'DUAXO KONCENTRAT ', N'DUAXO KONCENTRAT ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (170, N'54a24c92-5388-49c3-8554-81b3ca95bc33', N'DUAXO SPREJ ', N'DUAXO SPREJ ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (171, N'648892ea-f097-46f7-80d3-5b2c5258eccb', N'DUPLOSAN KV ', N'DUPLOSAN KV ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (172, N'f4d154cd-6e4c-42c2-9129-eeda8a9b5bc6', N'DUPLOSAN KV 600 ', N'DUPLOSAN KV 600 ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (173, N'9c03a0d3-8ffd-4f93-8ec8-5bbced7c937c', N'DYNALI ', N'DYNALI ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (174, N'478abcdf-e918-49fb-b405-aa7f508c91ba', N'ECODIAN CM ', N'ECODIAN CM ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (175, N'bc7d4142-c7fc-4ed2-b015-7987f04cc49c', N'ECOMETAL ', N'ECOMETAL ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (176, N'5b8cd485-2361-43bb-89f8-946d1c4b647e', N'ECO-TRAP ', N'ECO-TRAP ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (177, N'de267c07-6f0d-4adf-a547-7e230c5bcbbe', N'EFICA 960 EC ', N'EFICA 960 EC ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (178, N'2ba167f9-495b-42e6-b849-4dfc207794f2', N'ELATUS ERA ', N'ELATUS ERA ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (179, N'03c48cbf-1fc1-469c-ad67-8a659b3c7501', N'ELATUS PLUS ', N'ELATUS PLUS ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (180, N'0dc49af2-a3d6-4981-93aa-08d036f242d5', N'ELTIVIS ', N'ELTIVIS ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (181, N'84ea1bbe-45a4-4fb1-b0c9-762cc59d0e15', N'ELUMIS ', N'ELUMIS ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (182, N'9910cbc8-eed8-415d-a432-b87ca139ebe2', N'EMCEE ', N'EMCEE ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (183, N'613eaced-9929-496d-8636-6e5bed6838a6', N'EMINENT 125 EW ', N'EMINENT 125 EW ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (184, N'98458db8-23c6-4cd1-86e7-283958e04e2a', N'EMPARTIS ', N'EMPARTIS ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (185, N'3b1dc37b-9ccc-414e-8b2e-3e43579237ab', N'ENERVIN ', N'ENERVIN ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (186, N'7d78f649-c3b4-4f3c-ad62-86dc96d5a810', N'ENTAIL ', N'ENTAIL ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (187, N'24f9beb5-3c1a-4d54-b274-113680a24fdb', N'EQUIP ', N'EQUIP ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (188, N'029e7b3f-e19e-4a34-b8f0-5b1a59c1b49e', N'ERA (staro ime TARTAROS 300 EC) ', N'ERA (staro ime TARTAROS 300 EC) ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (189, N'b7445f7f-0cd9-4925-80b3-a74a1d66850f', N'ERGON ', N'ERGON ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (190, N'a0b364b8-757c-4d67-883e-d868e1687a88', N'ESTERON ', N'ESTERON ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (191, N'a46498a9-1d3e-4a45-b3a6-e4e46f86b2da', N'EVURE ', N'EVURE ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (192, N'51ee326c-b8d1-42e3-b27d-c233baedc4d1', N'EXILIS ', N'EXILIS ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (193, N'78d0f50c-4eb6-4005-8aad-b6ea864d1069', N'EXIREL ', N'EXIREL ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (194, N'37b462b3-f39f-4f3c-9e23-77c1e4b7d655', N'FABAN ', N'FABAN ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (195, N'2330ccba-d139-42ee-8685-5dfd4bd0db00', N'FAZILO SPREJ ', N'FAZILO SPREJ ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (196, N'51249168-7532-4844-a308-307ae590d98f', N'FAZOR ', N'FAZOR ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (197, N'0cf3de0f-2417-4cee-95b9-27c14b24bc11', N'FERRAMOL ', N'FERRAMOL ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (198, N'2c16d9d8-1287-48da-bb37-0c8fa23cf36f', N'FINEX 700 SC ', N'FINEX 700 SC ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (199, N'c3c250e3-fc5b-4b55-9267-70a7f52cc9d6', N'FINY ', N'FINY ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (200, N'87259b3b-c7d3-4a41-8471-fa852712f75d', N'FLAME ', N'FLAME ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (201, N'b2d96c24-c343-48b3-9475-b17e44a6e91d', N'FLASH 500 SC ', N'FLASH 500 SC ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (202, N'ed14e36e-caf3-40aa-b313-14c256d7ccc2', N'FLEXIDOR ', N'FLEXIDOR ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (203, N'ac37516e-2838-4e0b-a348-5e3a6e2130f9', N'FLEXITY ', N'FLEXITY ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (204, N'65994814-7aa5-4641-8f35-239f8012cb39', N'FLORA KENYATOX VERDE PLUS ', N'FLORA KENYATOX VERDE PLUS ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (205, N'f7d4409e-90cd-4dfd-8ae3-ee81efaf28be', N'FLORA VERDE ', N'FLORA VERDE ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (206, N'e2b5a4d6-6b27-4d6b-85eb-a477636d25f2', N'FLORGIB TABLETE ', N'FLORGIB TABLETE ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (207, N'8f066095-c4fb-4675-a7b1-b8693dd8203e', N'FLUENT 500 SC ', N'FLUENT 500 SC ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (208, N'1b32d45b-aa43-46eb-ad4a-a2200fc9eb9c', N'FLUROSTAR 200 ', N'FLUROSTAR 200 ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (209, N'85f381d5-47cf-4a2a-8d91-40f18af2e89d', N'FLUROSTAR SUPER ', N'FLUROSTAR SUPER ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (210, N'ab5cfab2-775e-4a0d-b938-d50bcc342ffd', N'FLYER ', N'FLYER ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (211, N'4d90869d-9ba9-48ac-89e9-22da9eb62cd6', N'FLYPACK DACUS TRAP ', N'FLYPACK DACUS TRAP ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (212, N'dbb45b2f-b7c0-4831-8e07-b9df57b7bed7', N'FOCUS ULTRA ', N'FOCUS ULTRA ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (213, N'316022df-4605-4bb4-a6a0-abfe4dfccbd7', N'FOLICUR EW 250 ', N'FOLICUR EW 250 ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (214, N'617c55fb-e540-4656-a42e-67b8a2acc627', N'FOLLOW 80 WG ', N'FOLLOW 80 WG ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (215, N'b2b97606-e190-4f58-9f80-2327769e2fde', N'FOLPAN 80 WDG ', N'FOLPAN 80 WDG ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (216, N'5bcce066-bf2b-4100-a496-59d25155144d', N'FOLPAN GOLD (staro ime RIDOMIL GOLD COMBI PEPITE) ', N'FOLPAN GOLD (staro ime RIDOMIL GOLD COMBI PEPITE) ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (217, N'9253e0dc-e42a-4afd-a60a-882a7b3f8a59', N'FONGANIL GOLD ', N'FONGANIL GOLD ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (218, N'8eec7cb8-4518-4c43-a60a-f461aef1c839', N'FORCE 1,5 G ', N'FORCE 1,5 G ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (219, N'7d678526-8f57-4621-aa3d-49d6a328bfbb', N'FORCE 20 CS ', N'FORCE 20 CS ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (220, N'5fb949b3-a17b-4d26-b632-50fabaccefb9', N'FORCE 20 CS ', N'FORCE 20 CS ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (221, N'5ed194eb-9791-48ad-95bf-e75fbe93bf1b', N'FORCE EVO ', N'FORCE EVO ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (222, N'f9c15627-00b6-41b4-b8ec-025d75c54b36', N'FORUM STAR ', N'FORUM STAR ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (223, N'e28ff45d-59cd-4ddd-b0ae-88ad6307a4e2', N'FOSHIELD ', N'FOSHIELD ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (224, N'2749c62a-e5a7-4e4d-94da-ee8f61659e84', N'FREQUENT ', N'FREQUENT ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (225, N'7af2964f-3888-46dd-9597-0cf3694cc135', N'FRESCO ', N'FRESCO ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (226, N'4ea2a52c-25cd-4464-b327-da784c5bc47d', N'FRONTIER X2 ', N'FRONTIER X2 ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (227, N'ea94e717-c945-4345-8a4f-0ce36dd615f6', N'FROOTI ', N'FROOTI ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (228, N'c1278077-420a-4477-a512-aa2d3479064b', N'FROWNCIDE ', N'FROWNCIDE ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (229, N'11eb61ac-1dea-460c-b652-d916645a585d', N'FRUIT AUXIN 30 SL ', N'FRUIT AUXIN 30 SL ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (230, N'9939e9dd-26a4-4c69-88e5-bb60863d88b8', N'FRUTAPON ', N'FRUTAPON ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (231, N'2fbbfded-4ee7-4f9c-834f-03045aeae1dc', N'FUEGO ', N'FUEGO ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (232, N'e1c4a1c5-a126-4d03-8a54-b850a6b05ff5', N'FUEGO TOP ', N'FUEGO TOP ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (233, N'a813e9c0-02ca-4454-af4a-7d5a00ecdc0c', N'FUSILADE FORTE ', N'FUSILADE FORTE ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (234, N'3eaa4b36-70ac-4b28-b589-3b53a719619e', N'FUSILADE MAX ', N'FUSILADE MAX ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (235, N'02ba5f5c-35ac-464f-b0b3-6eb35f581d43', N'FYSIUM ', N'FYSIUM ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (236, N'b4c8b1bd-f666-4ab2-be80-13fe1b1f849b', N'FYTOSAVE ', N'FYTOSAVE ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (237, N'6815b5a9-392a-46b9-b764-59a7e9b49bca', N'GEOXE ', N'GEOXE ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (238, N'c467237a-f91c-4064-a550-93e4794f0305', N'GF-120 ', N'GF-120 ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (239, N'a611a500-b05d-44ba-a624-025acef7fd7d', N'GIFT ', N'GIFT ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (240, N'b1671618-7df1-4fe7-8753-20c2fda1d710', N'GOLTIX WG 90 ', N'GOLTIX WG 90 ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (241, N'c39d7e1c-606e-4c84-b0e0-f40a6ae2e1d3', N'GRANPROTEC ', N'GRANPROTEC ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (242, N'37a6ad58-13fd-4b54-9686-e4497b875ed0', N'GRASSROOTER ', N'GRASSROOTER ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (243, N'ddb3ac6c-3893-485a-9e0c-5ff5438b94b6', N'GRETEG ', N'GRETEG ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (244, N'1a7da866-4b88-47dc-8e40-48a8f5642ffd', N'GRODYL  ', N'GRODYL  ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (245, N'73604b5f-9cec-4306-8bac-400f691944e1', N'GUSTO 3 - POLŽOMOR ', N'GUSTO 3 - POLŽOMOR ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (246, N'f253afde-32b7-4d28-93b4-c128c8647b9c', N'HARMONY 50 SX ', N'HARMONY 50 SX ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (247, N'106cbf7b-df72-409e-875a-b878257ce9bb', N'HARPUN ', N'HARPUN ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (248, N'd20c7c20-112f-474b-99a0-4fdfff52d3a4', N'HELOSATE 450 SL ', N'HELOSATE 450 SL ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (249, N'aa58f15f-cd1e-4f1f-8fc0-1f55a9c4eac2', N'HELOSATE 450 TF ', N'HELOSATE 450 TF ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (250, N'267c2f6b-b5f7-4a11-a503-a1ddaa45c90d', N'HERBOCID XL ', N'HERBOCID XL ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (251, N'2ddc37ec-f1bd-4113-ba38-6eaaee487d69', N'HUSSAR OD ', N'HUSSAR OD ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (252, N'f22b04d2-c08e-423c-bf3c-aa077fce5cd9', N'HUSSAR PLUS ', N'HUSSAR PLUS ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (253, N'3f757707-e4fd-4694-93a8-342ab74ba038', N'HUSSAR STAR ', N'HUSSAR STAR ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (254, N'31c95ad5-d751-405a-b597-2873453b2b7f', N'IMTREX XE ', N'IMTREX XE ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (255, N'd061e8d8-00f5-4bfe-9383-af8b9a32db1a', N'INFINITO ', N'INFINITO ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (256, N'77b7cadb-bf31-4a9c-b8d7-ba7de2438f1c', N'INPUT (staro ime PROSARO PLUS) ', N'INPUT (staro ime PROSARO PLUS) ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (257, N'dbc643ad-01b1-43c2-a53a-34d49529cd5c', N'IRONMAX PRO ', N'IRONMAX PRO ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (258, N'fa2b2726-5d59-4ecd-af4e-81ae9eed8d74', N'ISOMATE C TT ', N'ISOMATE C TT ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (259, N'5601d253-3bc8-4e34-be97-f50cda1ce5be', N'ISONET L PLUS ', N'ISONET L PLUS ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (260, N'2cd3e82e-f4b9-4ef7-b22d-d28d3f281ded', N'KALIMBA ', N'KALIMBA ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (261, N'3bb14912-32cb-4d67-b44a-2c19dc93629f', N'KAMBA 480 SL ', N'KAMBA 480 SL ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (262, N'e2b097de-3112-4cfa-8086-3b83e4bb8785', N'KANEMITE SC ', N'KANEMITE SC ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (263, N'61276f04-844e-4dcb-b54a-08bd72e2642e', N'KARATE ZEON 5 CS ', N'KARATE ZEON 5 CS ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (264, N'63f4ec16-11c8-4ba3-ae69-8a6e11b516c1', N'KARATHANE GOLD 350 EC ', N'KARATHANE GOLD 350 EC ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (265, N'aef8b5a0-708d-47ab-a7db-19c912f5a871', N'KARATHANE GOLD EC ', N'KARATHANE GOLD EC ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (266, N'21698632-17e8-40a9-9d94-5b34c7c54bd3', N'KARBICURE ', N'KARBICURE ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (267, N'2e913e5a-aef5-422c-82c0-7141dd50944f', N'KELVIN MAX ', N'KELVIN MAX ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (268, N'114847b3-ddb3-4a4a-ac33-d9f2997f6aad', N'KELVIN OD ', N'KELVIN OD ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (269, N'48b6e333-3301-43c5-a756-d0cd22ef9aa8', N'KEMAKOL EXTRA ', N'KEMAKOL EXTRA ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (270, N'63fa29b8-c995-4cf6-9756-4730693ff7b1', N'KINTO PLUS ', N'KINTO PLUS ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (271, N'8a1eaafa-2e99-42de-9dee-43173bb2cfa2', N'KOBAN TX ', N'KOBAN TX ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (272, N'e735b790-de5e-4375-8467-bea66201c70e', N'K-OBIOL EC 250 ', N'K-OBIOL EC 250 ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (273, N'68659aa2-8c4c-4565-8bbe-7ac44aed2b09', N'KOCIDE 2000 ', N'KOCIDE 2000 ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (274, N'43dae3f3-2564-4811-9450-654a4a2ce616', N'KORIT 420 FS ', N'KORIT 420 FS ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (275, N'2d32e36d-232b-4b44-b8a7-43cb568449f6', N'KUDOS ', N'KUDOS ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (276, N'63a90f69-faae-4e3f-82fc-e4cb95c6a94c', N'KUMULUS DF ', N'KUMULUS DF ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (277, N'e975f961-4e8c-4933-abf2-9526c89f43b2', N'KUSABI 300 SC ', N'KUSABI 300 SC ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (278, N'120b051c-b16e-44e7-968c-ca9e32494900', N'LAMARDOR FS 400 ', N'LAMARDOR FS 400 ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (279, N'1bea7d82-ee5a-43c0-b897-fa737cf88d42', N'LANCELOT SUPER ', N'LANCELOT SUPER ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (280, N'5bfd82ed-376c-42b2-b223-ad7c5bdbf103', N'LANDSCAPER PRO WEED CONTROL ', N'LANDSCAPER PRO WEED CONTROL ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (281, N'd53b4edf-6c72-4e98-8546-d70378270931', N'LASER 240 SC ', N'LASER 240 SC ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (282, N'55a6cf8d-a1e6-4136-81af-e9e334b53c68', N'LASER PLUS ', N'LASER PLUS ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (283, N'2e195741-5ee7-4a5b-b797-0e65200e1308', N'LAUDIS ', N'LAUDIS ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (284, N'123c9f19-af6c-49b4-b576-4f21d01381c7', N'LAUDIS WG 30 ', N'LAUDIS WG 30 ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (285, N'53fd02c7-d114-43fd-bca4-eaf19abc6569', N'LBG-01F34 ', N'LBG-01F34 ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (286, N'aab046b0-336e-49af-badd-90aaa996588b', N'LECTOR DELTA ', N'LECTOR DELTA ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (287, N'dbac1d3e-aff6-4aee-810e-c41ebc292fc2', N'LENTAGRAN WP ', N'LENTAGRAN WP ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (288, N'1bbabf31-0907-496b-b5ee-ac2a9cfc4b9a', N'LEPINOX PLUS ', N'LEPINOX PLUS ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (289, N'3d3482b1-215a-44d3-83a0-96d16945157d', N'LIBRAX ', N'LIBRAX ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (290, N'23376cbb-881a-4db4-b674-e7b90002d608', N'LIBRETO ', N'LIBRETO ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (291, N'b45b5da8-3055-479f-9d11-10d57be50fac', N'LIMA GOLD 3% ', N'LIMA GOLD 3% ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (292, N'6ada8667-c3ac-4325-82b7-c4bf74aef161', N'LONTREL 72SG ', N'LONTREL 72SG ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (293, N'9e63e64e-ef37-4d05-9a22-60669abb8a37', N'LUMAX ', N'LUMAX ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (294, N'4f9c29cf-0161-4668-8ac4-c86c98462d9b', N'LUMAX H 537.5 SE ', N'LUMAX H 537.5 SE ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (295, N'211f350c-54bc-4414-aceb-d6323510bffc', N'LUNA CARE ', N'LUNA CARE ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (296, N'7ae42b92-7b54-4dda-9d39-ef790a72b838', N'LUNA EXPERIENCE ', N'LUNA EXPERIENCE ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (297, N'282ff979-efcf-42f8-95da-c79df634656e', N'LUNA MAX ', N'LUNA MAX ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (298, N'621b1585-7bcb-45af-a2c8-de77f686069f', N'MADEX MAX ', N'MADEX MAX ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (299, N'c295d057-7f32-4cce-93fa-8f25465a7c6c', N'MAGMA TRIPLE WG ', N'MAGMA TRIPLE WG ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (300, N'7b62fc49-943c-4e51-89a2-b9bdbc6681ae', N'MAGNELLO ', N'MAGNELLO ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (301, N'd6b28e02-eef1-4e7b-a488-499220c2801a', N'MAGTOXIN PELETE ', N'MAGTOXIN PELETE ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (302, N'ee21f527-7354-4868-95c9-ee5b5934b817', N'MAJOR 300 SL ', N'MAJOR 300 SL ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (303, N'8ce3234c-c1ca-4a7f-a59c-34a8b72b1209', N'MAVITA 250 EC ', N'MAVITA 250 EC ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (304, N'fdc4e827-9b5d-40e2-a09e-01726d378830', N'MAVRIK 240 EW ', N'MAVRIK 240 EW ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (305, N'cf651099-cf4d-4646-8526-086f9ea25006', N'MAXCEL ', N'MAXCEL ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (306, N'8c2cbc84-06e2-407f-9fc4-69db8be95ced', N'MAXIM EXTRA 050 FS ', N'MAXIM EXTRA 050 FS ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (307, N'5b2861c3-fd7e-4660-b1f1-32b3d84f3dd6', N'MAXIM QUATTRO ', N'MAXIM QUATTRO ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (308, N'e9b1fd5e-1e01-4220-aadf-8c9f0742dafc', N'MAXIM QUATTRO (3) ', N'MAXIM QUATTRO (3) ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (309, N'a0ca42cf-fb85-4065-bd96-1b476d87ffe1', N'MAXIM XL 035 FS ', N'MAXIM XL 035 FS ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (310, N'70ad05de-72dd-4050-ab7e-6b0c176ccfcd', N'MEDAL (staro ime ARION PRO) ', N'MEDAL (staro ime ARION PRO) ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (311, N'385cde2b-deb4-4e15-ad8d-ad4e016182eb', N'MEDAX TOP ', N'MEDAX TOP ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (312, N'9c55081b-b2ba-4186-91f7-d0e499964196', N'MERLIN FLEXX ', N'MERLIN FLEXX ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (313, N'1d7d032f-ca63-42d2-ad2e-3ed41508e41c', N'MERPAN 80 WDG ', N'MERPAN 80 WDG ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (314, N'56265120-7538-4410-ad41-56d4fc838b14', N'MERPLUS ', N'MERPLUS ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (315, N'896bd99e-5f53-48f5-94e2-ad0bc5e5e323', N'METAFOL WG ', N'METAFOL WG ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (316, N'4fa22370-7029-4d17-882e-07f031442d28', N'METAREX INOV ', N'METAREX INOV ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (317, N'17940a8c-d400-412b-9bca-1e4017e9e0f4', N'METRIC ', N'METRIC ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (318, N'a0c13ed7-dd54-4194-8a70-59c306ab4c2d', N'METSO ', N'METSO ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (319, N'e5781065-07ce-4a9d-9810-05bb26588758', N'MEZZO ', N'MEZZO ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (320, N'aa62ce07-4573-41f8-b889-d7b6ee991a11', N'MICROTHIOL SC ', N'MICROTHIOL SC ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (321, N'd9800f1d-3881-4aae-999b-303cc806dc88', N'MICROTHIOL SPECIAL ', N'MICROTHIOL SPECIAL ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (322, N'57386995-50b9-4545-802b-9222280601fc', N'MIKAL FLASH ', N'MIKAL FLASH ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (323, N'aca69b83-2d0d-4183-a07c-1c6af0cbf8d1', N'MIKAL PREMIUM F ', N'MIKAL PREMIUM F ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (324, N'f593cc7b-e262-4f94-b716-04570de8642f', N'MILAGRO 240 SC ', N'MILAGRO 240 SC ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (325, N'3a195133-31dc-40f8-b8fd-2900d6c9edc1', N'MILAGRO EXTRA 6 OD ', N'MILAGRO EXTRA 6 OD ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (326, N'ceca353f-c712-4e97-8902-b47bb540b214', N'MILAGRO PLUS ', N'MILAGRO PLUS ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (327, N'b4820397-e888-4980-813c-4c5af5ff2293', N'MILBEKNOCK ', N'MILBEKNOCK ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (328, N'dc7f5c93-882f-4bcf-b0bd-0a38a7d94590', N'MILDICUT ', N'MILDICUT ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (329, N'28dc755d-cb1e-4ed7-8050-f02804060c00', N'MIMIC ', N'MIMIC ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (330, N'aa8341a8-bf2c-4c5b-b5c9-855ec65270c1', N'MIRADOR 250 SC ', N'MIRADOR 250 SC ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (331, N'b53c7c2a-7895-4101-8ca2-af078b7543cd', N'MIRADOR FORTE ', N'MIRADOR FORTE ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (332, N'4344f875-872a-4e90-8f3f-d03772dd73a5', N'MIZONA ', N'MIZONA ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (333, N'46eb9e77-bbb0-4d73-a60f-1274eac00129', N'MOCLJIVO ŽVEPLO KARSIA DF ', N'MOCLJIVO ŽVEPLO KARSIA DF ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (334, N'28699889-f75c-4ed5-9ad1-5cec90faf8c6', N'MODDUS 250 EC ', N'MODDUS 250 EC ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (335, N'0b8cced1-574e-4f47-b7fd-f9777d9ca79d', N'MODDUS EVO ', N'MODDUS EVO ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (336, N'c30f7720-8d56-44f6-83ad-82340a1a3471', N'MOMENTUM F ', N'MOMENTUM F ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (337, N'9586c4fc-3ef5-4e85-995a-87c9da9bac9e', N'MOMENTUM TRIO ', N'MOMENTUM TRIO ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (338, N'b4310bb9-ccfa-4ccc-a7cf-9d90123e16fe', N'MONCUT ', N'MONCUT ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (339, N'f6d689d5-26e8-4448-8740-cdc140401a0e', N'MONEX ', N'MONEX ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (340, N'218f4762-32b4-47fe-be9b-620a19570374', N'MONSOON ACTIVE ', N'MONSOON ACTIVE ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (341, N'a882c600-383a-454a-b2df-9a6978801b96', N'MOSPILAN 20 SG ', N'MOSPILAN 20 SG ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (342, N'253cac8d-dcc1-489c-9562-60ac8f07df46', N'MOSPILAN 20 SG (3) ', N'MOSPILAN 20 SG (3) ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (343, N'bf55fa42-915c-4a83-9dfc-83c7d9be5054', N'MOTIVELL ', N'MOTIVELL ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (344, N'71ce1f4b-69cd-46fe-b8ba-6d2c03da08f8', N'MOTIVELL EXTRA 6 OD ', N'MOTIVELL EXTRA 6 OD ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (345, N'6ab4c6e8-1eaa-475a-96ba-68fe38944cea', N'MOVENTO SC 100 ', N'MOVENTO SC 100 ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (346, N'eafeeaa9-6d3d-4572-8417-1d3eae87bcef', N'MOXA ', N'MOXA ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (347, N'842e552c-86de-4e11-bda5-940b888086e2', N'MURAL ', N'MURAL ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (348, N'a2d49155-92e2-4abb-80ed-38d6cae67377', N'MUSTANG 306 SE ', N'MUSTANG 306 SE ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (349, N'e91fff79-4093-4468-9345-62773cb03d28', N'MUSTANG FORTE ', N'MUSTANG FORTE ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (350, N'23f072fb-512c-447b-b007-a51acca247be', N'MYSTIC 250 EC ', N'MYSTIC 250 EC ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (351, N'0bb4fb89-31cc-4f02-bcc0-b18de6938b2b', N'MYTHOS ', N'MYTHOS ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (352, N'3c8b886a-28bd-4fb6-9d1f-499951e0151b', N'NATIVO 75 WG ', N'NATIVO 75 WG ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (353, N'79b09819-27b5-4099-a8b7-df17d21dd41f', N'NATURALIS ', N'NATURALIS ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (354, N'f589b7de-ae3f-4df5-81e7-63d8e86d2121', N'NATUREN BIO SREDSTVO PROTI POLŽEM ', N'NATUREN BIO SREDSTVO PROTI POLŽEM ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (355, N'2bcc5421-0aaa-4690-a75b-fb861c25d264', N'NATUREN FORTE - KONCENTRAT ', N'NATUREN FORTE - KONCENTRAT ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (356, N'a8995212-ca28-49f1-8e90-ae9516da38e3', N'NATUREN FORTE - RAZPRŠILKA ', N'NATUREN FORTE - RAZPRŠILKA ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (357, N'e8366cdd-a3f8-41be-a81d-7192c2cfcb87', N'NEEMAZAL - T/S ', N'NEEMAZAL - T/S ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (358, N'8293388b-ace9-42fc-9d1b-b7ecceda1772', N'NEXT ', N'NEXT ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (359, N'fbbfe360-dcb2-4852-9f86-49c8901cbe92', N'NICOSH ', N'NICOSH ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (360, N'ccb08635-1849-415a-ac32-9d1ed770ea2a', N'NISSORUN 10 WP ', N'NISSORUN 10 WP ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (361, N'ddf72fd8-5b0f-4eb8-9269-782d9a3d6d9e', N'NISSORUN 250 SC ', N'NISSORUN 250 SC ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (362, N'7d97e7d8-48b7-486c-a7ee-7d1868a5145d', N'NORDOX 75 WG ', N'NORDOX 75 WG ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (363, N'83d59a5f-62c4-4d31-bdc2-b04046f8b469', N'NORIOS ', N'NORIOS ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (364, N'e0c132d2-4813-4a99-bd4d-8433572f32c3', N'NOVAGIB ', N'NOVAGIB ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (365, N'cc4f49be-371f-4148-8631-3f21b461edc7', N'OBSTHORMON 24A ', N'OBSTHORMON 24A ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (366, N'bdbbd709-4240-41ca-98be-43bafee695bc', N'ONYX ', N'ONYX ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (367, N'52111d80-d091-41c8-b57f-e584df23bcee', N'ORIUS 25 EW ', N'ORIUS 25 EW ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (368, N'794b120c-46c0-4420-b2a0-c76e6723e3f0', N'OROCIDE PLUS ', N'OROCIDE PLUS ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (369, N'b8ad25b8-b85c-4fda-a4d2-49bef01cef13', N'ORONDIS ', N'ORONDIS ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (370, N'0db9a516-3e8f-456b-9ad2-b276f49eebbb', N'ORTHOCIDE 80 WG ', N'ORTHOCIDE 80 WG ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (371, N'ab3ce4f9-0e4c-489a-8877-fe32c0e2ce63', N'ORTIVA ', N'ORTIVA ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (372, N'40f3527f-fba6-4efa-9f2f-6cfae9c8b4db', N'ORTUS 5 SC ', N'ORTUS 5 SC ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (373, N'aee7f750-990f-4836-a510-b61a0b9b8c6f', N'ORVEGO ', N'ORVEGO ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (374, N'e13c1de3-3a2d-430a-b827-d45c2206ca6f', N'OSORNO ', N'OSORNO ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (375, N'7c77d2e2-1e67-41ff-8d61-935136367591', N'OVITEX ', N'OVITEX ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (376, N'ca46ae03-a27e-4d7b-b850-8cfd581bd68f', N'PAKET 250 EC ', N'PAKET 250 EC ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (377, N'8133894c-734f-4200-abc0-01dfec32e9f2', N'PALLAS 75 WG ', N'PALLAS 75 WG ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (378, N'0dd8dcbb-c0f9-447e-9494-a00ae2de69de', N'PEAK 75 WG ', N'PEAK 75 WG ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (379, N'baacd740-5768-4fe6-b06c-bba824194f65', N'PECARI 300 EC ', N'PECARI 300 EC ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (380, N'b932f251-c641-4d36-a2d4-9798998fc054', N'PEPELIN ', N'PEPELIN ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (381, N'202d5a01-7863-487e-b9b9-fa0cad6fa6bb', N'PERGADO C ', N'PERGADO C ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (382, N'095ad4fa-142a-4c31-b1bb-c50f9428d59f', N'PERGADO D ', N'PERGADO D ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (383, N'e6e5f952-e952-4d62-9cdd-a394bc3e1f41', N'PERGADO F ', N'PERGADO F ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (384, N'6b0f48db-9d8b-4c4f-b7f3-ae2dc0ed7a01', N'PERGADO SC ', N'PERGADO SC ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (385, N'78445863-01d7-4ea9-a679-2a0218c26146', N'PHOSTOXIN BAG / BAG BLANKET ', N'PHOSTOXIN BAG / BAG BLANKET ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (386, N'59745374-dcb9-4216-a9a2-99431866430e', N'PHOSTOXIN PELETE ', N'PHOSTOXIN PELETE ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (387, N'e89460d8-14a3-43f0-a06b-61c079374980', N'PIRIMOR 50 WG ', N'PIRIMOR 50 WG ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (388, N'd1852702-e109-4fb6-a561-5da65e7095ee', N'PLANTELLA ARION ', N'PLANTELLA ARION ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (389, N'c64e70d9-10d9-456b-9646-dc276af144d0', N'PLANTELLA RHIZOPON I ', N'PLANTELLA RHIZOPON I ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (390, N'c9bffdfe-f349-49d1-a8f3-afee604977a3', N'PLANTELLA RHIZOPON II ', N'PLANTELLA RHIZOPON II ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (391, N'285dd101-9525-47bc-aa01-45879ab852d1', N'PLANTELLA RHIZOPON III ', N'PLANTELLA RHIZOPON III ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (392, N'062037ce-7386-4cbf-bf48-3506e5bdc63b', N'PLANTLINE E4 ', N'PLANTLINE E4 ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (393, N'c67c1576-1f90-4b24-a8a7-ebc31a16891c', N'PLATEEN WG 41,5 ', N'PLATEEN WG 41,5 ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (394, N'4eb1d0f8-cf7f-4021-9e17-688b169a9795', N'PLEXEO ', N'PLEXEO ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (395, N'7aa16738-1bde-4248-9f15-68f1340b41ce', N'PMV-01 ', N'PMV-01 ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (396, N'dcc8bf39-0bfa-493b-aa10-7ada2c9b9e3c', N'POLECI ', N'POLECI ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (397, N'76d708e6-b9b9-44c4-991c-059450230848', N'POL-SULPHUR 80 WG ', N'POL-SULPHUR 80 WG ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (398, N'f12d6e18-92f1-4bd4-942d-be16fcbf76a2', N'POL-SULPHUR 80 WP ', N'POL-SULPHUR 80 WP ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (399, N'0700b843-95e5-4e6e-8d7d-aa4e48e7d76d', N'POL-SULPHUR 800 SC ', N'POL-SULPHUR 800 SC ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (400, N'67063027-782f-4c2e-8093-d84aa1d8e1ed', N'POLYRAM DF ', N'POLYRAM DF ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (401, N'1158ae24-637a-4023-9891-5a6ed96bb961', N'POLYVERSUM ', N'POLYVERSUM ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (402, N'5802a123-aec0-4ecc-ab1a-783c37c99626', N'POLŽOKILL (staro ime LIMA GOLD 5%) ', N'POLŽOKILL (staro ime LIMA GOLD 5%) ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (403, N'60d8e7d0-ccbb-4b6b-9b31-7fc304bb0372', N'POLŽOMOR BIO VABA ZA ZATIRANJE POLŽEV ', N'POLŽOMOR BIO VABA ZA ZATIRANJE POLŽEV ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (404, N'de54a831-37b5-45d7-ae00-67fb5f5e4ed6', N'POMAX ', N'POMAX ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (405, N'0ab1012b-c080-415e-b8ca-18b0005c4752', N'PRAKTIS ', N'PRAKTIS ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (406, N'c0368df1-a93f-4da5-93f8-8fa3cf106591', N'PRESTOP ', N'PRESTOP ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (407, N'ddc8ff7b-e226-4914-93ba-d0369f3f240e', N'PREV-GOLD ', N'PREV-GOLD ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (408, N'7dcc450f-b171-486d-9caa-b91aac5b7b93', N'PREV-GOLD GARDEN ', N'PREV-GOLD GARDEN ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (409, N'4842f6b4-c0da-4b45-a8dd-e647548159d5', N'PREVICUR ENERGY ', N'PREVICUR ENERGY ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (410, N'b91ece32-4554-46ca-98cc-b4d42f8121be', N'PRIAXOR EC ', N'PRIAXOR EC ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (411, N'7ba761f5-96c1-4f24-b5ac-bbef6184c500', N'PRIMERO ', N'PRIMERO ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (412, N'3bef0e6a-d3f7-495b-a4bc-3a958a8f787f', N'PRIMEXTRA TZ GOLD 500 SC ', N'PRIMEXTRA TZ GOLD 500 SC ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (413, N'227ef424-7920-4cbd-96c2-d662998f3863', N'PROCER 300 EC ', N'PROCER 300 EC ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (414, N'39c5c9e4-ed77-4ad5-9e67-98318d4c9ce4', N'PROFILER ', N'PROFILER ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (415, N'e771e3bc-339d-4ede-a01a-b9ec53b1bcfc', N'PROFUME INSECTICIDE ', N'PROFUME INSECTICIDE ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (416, N'e367815e-4f1b-4e65-b9a3-0d6466dc992f', N'PROLECTUS ', N'PROLECTUS ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (417, N'0537187d-e533-4142-ad46-0fe3ffbd6df0', N'PROMAN ', N'PROMAN ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (418, N'0b08e8e7-a6b2-4e98-b2d7-13473aa6a378', N'PROMANAL AF NEU ', N'PROMANAL AF NEU ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (419, N'b5c64a19-f166-4f4e-8805-f9db06c01751', N'PROMINO 300 EC ', N'PROMINO 300 EC ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (420, N'4e032513-11da-4d6c-bcbd-c6285b3e6cdb', N'PROPLANT ', N'PROPLANT ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (421, N'6685bf9f-3e6d-483c-a9f1-b801d234ac0b', N'PROPULSE ', N'PROPULSE ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (422, N'1ffa3723-bcda-4a58-9002-7fff2df1e0d3', N'PRORADIX ', N'PRORADIX ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (423, N'c904fabb-86f5-4ae8-87f5-65983624b0a6', N'PROSARO ', N'PROSARO ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (424, N'4ec25610-9901-4557-97fe-83b499ae99a6', N'PROSPER CS 300 ', N'PROSPER CS 300 ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (425, N'aa8c6fa2-4a81-4d7d-b361-f83b30757fdb', N'PROTENDO 300 EC ', N'PROTENDO 300 EC ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (426, N'73fb7ec6-8a13-439c-90ee-a446626cc402', N'PROXANIL 450 SC ', N'PROXANIL 450 SC ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (427, N'1739de31-6b08-47d4-b60d-64677db1bc80', N'PYGRAIN NEW ', N'PYGRAIN NEW ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (428, N'f2e80760-84be-4361-a9f5-d830cee9721a', N'PYRAMID ', N'PYRAMID ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (429, N'6d595cf0-42b3-452e-a0f5-73d73eba22a8', N'PYRUS 400 SC ', N'PYRUS 400 SC ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (430, N'd63f9589-1e05-4053-9ba2-11b97491a977', N'QUELEX ', N'QUELEX ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (431, N'7c079b0b-ede3-4502-abd1-f1060b2ff16a', N'QUICK 5 EC ', N'QUICK 5 EC ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (432, N'af788f91-ed11-4feb-b20a-beb35dac6a47', N'QUICKPHOS BAGS ', N'QUICKPHOS BAGS ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (433, N'955246b7-1e66-4785-81a0-717a593eda79', N'QUICKPHOS PELLETS 56 GE ', N'QUICKPHOS PELLETS 56 GE ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (434, N'efdaaa51-85eb-4025-a433-988267e47eea', N'QUICKPHOS TABLETS 56 GE ', N'QUICKPHOS TABLETS 56 GE ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (435, N'2cd914b1-0521-43be-b600-24c8698e91a8', N'RADIANT ', N'RADIANT ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (436, N'daff1b80-13f0-45b4-afee-f650b3adfd8c', N'RAK 3 ', N'RAK 3 ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (437, N'60658c82-288b-40eb-ad18-ef18191c3edb', N'RANMAN TOP ', N'RANMAN TOP ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (438, N'cdec9c2c-02c5-4811-9e2e-8182aaade896', N'RAPSAN 500 SC ', N'RAPSAN 500 SC ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (439, N'0ebd1059-826d-4631-abf9-37062c267e0e', N'RAPTOL KONCENTRAT ', N'RAPTOL KONCENTRAT ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (440, N'c6801fca-ac1f-4a5e-aa69-11b3640939a8', N'RAPTOL SPRAY ', N'RAPTOL SPRAY ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (441, N'cc5d8568-5a1a-4a48-815a-3e1e407bd67a', N'RAPTOL SPRAY AE ', N'RAPTOL SPRAY AE ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (442, N'fc4e0c5d-7b1f-4416-9c6d-9932efd500cb', N'RATRON GW ', N'RATRON GW ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (443, N'18da5f6e-dda8-4f80-9e42-f0c6e3dff697', N'REBOOT ', N'REBOOT ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (444, N'a7538efb-9354-4554-be97-2be5d2a866d0', N'RED FOX ', N'RED FOX ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (445, N'4257ee52-633d-4718-ba16-1886bbbdd54f', N'REDIGO M ', N'REDIGO M ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (446, N'6c24316d-5a5d-48f4-ab37-5d146aa25a19', N'REDIGO PRO ', N'REDIGO PRO ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (447, N'dff03c99-5103-42ed-9f09-5f804f94eb91', N'REGALIS PLUS ', N'REGALIS PLUS ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (448, N'4b9ee063-85d0-417e-8ec4-47159760ea21', N'RETENGO ', N'RETENGO ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (449, N'5e9d7507-8582-42e9-b08f-7c7b49b0c333', N'REVIVE II ', N'REVIVE II ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (450, N'eae14f74-acfe-4594-8dbc-ae8360c11ed2', N'REVUS ', N'REVUS ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (451, N'88861691-5a73-46d3-8980-32e0b8982d99', N'REVUS TOP ', N'REVUS TOP ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (452, N'2e6f1a57-7d28-4cca-8f5b-d113daf7fc6b', N'REVYCARE ', N'REVYCARE ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (453, N'a3028360-d04c-49cb-9bf8-c53e1f0a2ab4', N'REVYONA ', N'REVYONA ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (454, N'1b4b63ec-36ec-48b2-bb93-57cc91b9a7ac', N'REVYSTAR ', N'REVYSTAR ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (455, N'acbce261-4495-4b5c-a9d5-3794418f07db', N'REVYSTAR XL ', N'REVYSTAR XL ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (456, N'9d70064c-0275-434e-a40c-f5f13ba617cb', N'REVYTREX ', N'REVYTREX ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (457, N'2306affe-09eb-4b27-9542-8a5bab8b9d03', N'RIM 25 WG ', N'RIM 25 WG ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (458, N'33c88fbf-9ac3-43f2-8236-812ea88889bd', N'RIMURON 25 WG ', N'RIMURON 25 WG ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (459, N'2160612d-2876-4854-b97f-bd540eff2c82', N'RINCON 25 SG ', N'RINCON 25 SG ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (460, N'2d323ca2-4967-4b56-bfc7-4d21d14ee656', N'RINIDI WG ', N'RINIDI WG ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (461, N'9badaa76-a750-46c5-9ee4-1bbe2a91c8ca', N'RIVAL ', N'RIVAL ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (462, N'300376d1-f1b6-401e-988b-398ac24b751b', N'RIVAL DUO ', N'RIVAL DUO ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (463, N'163dd893-38b9-4f38-a8a1-81c4faf69376', N'RODEO PLUS ', N'RODEO PLUS ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (464, N'66023e4c-fcf5-43a0-a850-2c1c5326f2c9', N'ROUNDUP ENERGY ', N'ROUNDUP ENERGY ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (465, N'89cebe33-2b3c-49f3-917d-2e666fb83fab', N'ROUNDUP MAX ', N'ROUNDUP MAX ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (466, N'c7f6eed0-c786-4f00-91b1-3d8d48b0980c', N'ROUNDUP STAR ', N'ROUNDUP STAR ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (467, N'b53cbfb7-12bc-49f5-8e4c-f521efba25c2', N'ROYALTY ', N'ROYALTY ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (468, N'4b3e7a13-d0ff-4f52-b0fa-d1ed9ca8b88f', N'SACRON 45 DG ', N'SACRON 45 DG ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (469, N'7c4a4fb2-32c6-4306-8b41-fe8814c1116a', N'SAMSON 4 SC ', N'SAMSON 4 SC ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (470, N'0ba64c07-6121-4641-9f59-3d091c21a415', N'SAMSON EXTRA 6 OD ', N'SAMSON EXTRA 6 OD ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (471, N'44d747ef-d915-482a-a8b1-88d679868ac2', N'SANVINO ', N'SANVINO ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (472, N'98c55179-5f83-4b3c-9a18-b1b9594d9d37', N'SAPROL PROTI BOLEZNIM VRTNIC ', N'SAPROL PROTI BOLEZNIM VRTNIC ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (473, N'd5e47e04-9ace-4b4c-a3d9-a5dea3f228ad', N'SARACEN MAX ', N'SARACEN MAX ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (474, N'2b14eadf-1e18-4af6-a978-090497772d64', N'SAVVY ', N'SAVVY ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (475, N'779262a7-7195-46b7-9a6e-a06b04d30ef6', N'SCAB 480 SC ', N'SCAB 480 SC ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (476, N'f2df36d5-0427-429f-b178-75a163348d99', N'SCAB 80 WG ', N'SCAB 80 WG ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (477, N'38917000-1df3-4c85-b84a-0dbd4af1e008', N'SCALA ', N'SCALA ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (478, N'31547b89-1115-475f-82f4-2b7ff5a7e3f9', N'SCORE 250 EC ', N'SCORE 250 EC ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (479, N'4ea47342-8794-4e85-81b6-40b6e01d1947', N'SEKATOR OD ', N'SEKATOR OD ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (480, N'36a4918a-3c70-499c-b9af-29454c67e9a9', N'SEKATOR PLUS ', N'SEKATOR PLUS ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (481, N'8fde0acc-bb6e-4763-b987-45f352a5823f', N'SELECT SUPER ', N'SELECT SUPER ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (482, N'e6f56754-1970-413f-b972-4126e7c85a51', N'SemiosNET-Codling Moth ', N'SemiosNET-Codling Moth ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (483, N'344ce2e5-b4b0-4b2d-87dc-5e18d3f1f236', N'SENCOR SC 600 ', N'SENCOR SC 600 ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (484, N'cf18de0e-d9a5-446f-ae9b-637d959a3b46', N'SERCADIS ', N'SERCADIS ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (485, N'94e611c5-e33d-4dbd-86f3-dd64434c7d3b', N'SERCADIS PLUS ', N'SERCADIS PLUS ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (486, N'a6b89523-52b4-4dd2-b156-9ba040cf5a2b', N'SERENADE ASO ', N'SERENADE ASO ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (487, N'05b00978-34e9-4a10-9475-6e9b279cf665', N'SFINGA EXTRA WDG ', N'SFINGA EXTRA WDG ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (488, N'd97ed9f3-745f-4b56-9568-975e6e7e9d79', N'SHARPEN 33 EC ', N'SHARPEN 33 EC ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (489, N'536d3ec0-d082-4619-8ec1-d588ed4977b5', N'SHARPEN PLUS ', N'SHARPEN PLUS ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (490, N'18404365-b03d-4b19-a115-c0c8261e074a', N'SHIRLAN 500 SC ', N'SHIRLAN 500 SC ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (491, N'0356a15b-e61f-4984-b687-450f6f0fac8c', N'SHIRUDO (staro ime MASAI) ', N'SHIRUDO (staro ime MASAI) ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (492, N'c703a1ab-769c-4e98-b847-4aacecd61932', N'SHYFO ', N'SHYFO ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (493, N'28d1aa2c-a709-4f4c-a45a-880458d1a6fc', N'SIGNAL 300 ES ', N'SIGNAL 300 ES ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (494, N'23ca0aab-9c8a-4911-8783-8eb14100f416', N'SIGNUM ', N'SIGNUM ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (495, N'13213b12-fa5e-479f-b820-14494c9feddb', N'SILICOSEC ', N'SILICOSEC ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (496, N'f018add0-8030-436c-aa30-dbf71b2559ce', N'SILTRA XPRO ', N'SILTRA XPRO ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (497, N'338571e9-e37e-44e4-aac8-7d9684ef40f8', N'SINOPIA ', N'SINOPIA ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (498, N'1910ae99-7549-4233-8c51-fba310477b44', N'SIRENA ', N'SIRENA ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (499, N'9eb06fd1-fd61-43e0-9d3a-b2719f45e331', N'SISAM ', N'SISAM ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (500, N'b41d69c9-c773-4bfa-b74b-fc938c386061', N'SIVANTO PRIME ', N'SIVANTO PRIME ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (501, N'655fb91d-3494-4514-b83f-cc04066cf5bb', N'SMARTFRESH PROTABS ', N'SMARTFRESH PROTABS ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (502, N'0d12673e-136d-45b4-8fab-6bdeb5060c8c', N'SOILGUARD 0.5 GR ', N'SOILGUARD 0.5 GR ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (503, N'4d7b5fc3-12e7-4076-9de8-f7fd8a682726', N'SOLABIOL PROTI POLŽEM ', N'SOLABIOL PROTI POLŽEM ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (504, N'f407e416-b6f3-44a1-a260-54eaabc4ab63', N'SOLOFOL ', N'SOLOFOL ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (505, N'1fe89eab-175b-447d-8e86-93c382e44f45', N'SONATA ', N'SONATA ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (506, N'939fc1ac-008b-4a97-955b-cb5c96731b5d', N'SORIALE LX ', N'SORIALE LX ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (507, N'a9ff34ce-1853-4596-9054-55bbcff71321', N'SPANDIS ', N'SPANDIS ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (508, N'ce156bd8-d588-4a57-a10f-ca028a0c299f', N'SPIROX ', N'SPIROX ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (509, N'83746349-e665-4429-97ef-998d17e43a77', N'SPIROX D ', N'SPIROX D ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (510, N'1cc02fff-7614-4889-972a-e6178adb0b7c', N'STABILAN 750 SL ', N'STABILAN 750 SL ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (511, N'cea002b7-c729-4384-9750-da977f6926e3', N'STALLION SYNC TEC ', N'STALLION SYNC TEC ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (512, N'859fb076-6d03-4108-81ba-38128ae00d51', N'STARANE FORTE ', N'STARANE FORTE ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (513, N'59c3f9cb-eab5-4a51-89a1-87ba31604813', N'STOMP AQUA ', N'STOMP AQUA ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (514, N'1977d7a0-8fb1-4a75-84e7-97df01c0d348', N'STRETCH ', N'STRETCH ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (515, N'264f9583-809c-42d6-a128-6875c5920a54', N'STROBY WG ', N'STROBY WG ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (516, N'73e2a202-62dd-4046-8d50-952a33d9c90b', N'SUBSTRAL 2V1 ', N'SUBSTRAL 2V1 ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (517, N'269be919-cd95-40ff-8ebe-b957512a1d6b', N'SUBSTRAL 3V1 ', N'SUBSTRAL 3V1 ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (518, N'12d5953c-bac7-408a-a479-bf0c82b2e513', N'SUCCESSOR 600 ', N'SUCCESSOR 600 ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (519, N'f079fe40-4622-4e2d-b718-9381aa3bcbf2', N'SULFAR ', N'SULFAR ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (520, N'c9fa5bc1-4a54-4aa5-9fff-f7658c9eb225', N'SWITCH 62,5 WG ', N'SWITCH 62,5 WG ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (521, N'ee0ec936-1506-46ba-bcf1-fc912ccb224c', N'SYLLIT 400 SC ', N'SYLLIT 400 SC ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (522, N'6d5b5093-86f8-402b-97d6-50aafd345e2a', N'SYLLIT 544 SC ', N'SYLLIT 544 SC ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (523, N'b473e5ca-10cb-4092-8ec0-014dd5f6ed42', N'TAEGRO ', N'TAEGRO ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (524, N'1c658552-14c5-4bd4-859f-499ca3308067', N'TAJFUN 360 ', N'TAJFUN 360 ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (525, N'c73a3a3b-b5fa-42c8-8109-de0cd6b30f65', N'TALENDO ', N'TALENDO ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (526, N'f47acbfa-c29f-4488-8c6a-8e22b79d2753', N'TALENDO EXTRA ', N'TALENDO EXTRA ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (527, N'571420ea-dd62-4403-99ec-6b242964e51d', N'TALISMAN ', N'TALISMAN ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (528, N'77253efd-f007-4465-8f71-2304595ac78a', N'TARCZA 060 FS ', N'TARCZA 060 FS ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (529, N'549ba3e1-61ae-4269-874d-efb60e5f61f1', N'TARGA SUPER ', N'TARGA SUPER ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (530, N'10b6d317-8288-4f93-8a0b-90d1c5fe31ef', N'TAROT 25 WG ', N'TAROT 25 WG ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (531, N'feefb650-be50-497d-9675-a5b01eb91af6', N'TARTAROS ', N'TARTAROS ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (532, N'45ce68c4-c0b3-4fcf-9bbb-5fb494e9f335', N'TAZER 250 SC ', N'TAZER 250 SC ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (533, N'bc42c16a-2df9-4132-b6fc-a4db5487227a', N'TBM 75 WG ', N'TBM 75 WG ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (534, N'58fe24d2-4977-4350-959d-1d7df77aabb0', N'TEBKIN ', N'TEBKIN ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (535, N'85e23003-0b89-45be-8db7-f82a19b2cd7a', N'TEBSEME ', N'TEBSEME ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (536, N'1af85460-c42a-4dc5-9ee6-a0b6a4b53057', N'TEBUSHA 25% EW ', N'TEBUSHA 25% EW ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (537, N'2ceed58f-3f05-465d-ae88-9c99b72cc8fe', N'TEFLIX ', N'TEFLIX ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (538, N'08c6c375-36c7-4ab8-9131-3405350940a6', N'TELDOR SC 500 ', N'TELDOR SC 500 ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (539, N'6cf5b9f8-5799-4f2b-843b-45c9f2040dd3', N'TEMSA SC ', N'TEMSA SC ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (540, N'2309b6b3-8cbb-4108-95f8-bba9c6934080', N'TEPPEKI ', N'TEPPEKI ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (541, N'57dd9767-f957-4b76-b1f9-f00f74a9369b', N'TERIDOX 500 EC ', N'TERIDOX 500 EC ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (542, N'b2db5fe9-9cfb-4772-9c25-af694096c8ba', N'TERMINATOR PLUS INSEKTICID ZA RASTLINE ', N'TERMINATOR PLUS INSEKTICID ZA RASTLINE ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (543, N'873d7a99-3921-4393-abc9-1a1fe3c96ab1', N'TERMINATOR PLUS VABA ZA POLŽE ', N'TERMINATOR PLUS VABA ZA POLŽE ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (544, N'54da9fd5-b189-4eb0-8324-fac8400b9d1b', N'TERN ', N'TERN ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (545, N'831d33fb-1636-497a-b089-e78b47c75c35', N'THIOVIT JET ', N'THIOVIT JET ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (546, N'41b696d0-d8f8-496f-b5b4-fac8389d8faf', N'TOLUREX 50 SC ', N'TOLUREX 50 SC ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (547, N'7e60535a-7ee0-447b-9543-9ae5d7ab25b8', N'TOPAS 100 EC ', N'TOPAS 100 EC ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (548, N'34b877c3-e1c7-4dfc-8d63-e7c49b90be8d', N'TOUCHDOWN SYSTEM 4 ', N'TOUCHDOWN SYSTEM 4 ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (549, N'a205b623-2850-4523-9f11-a1f314734d76', N'TREPACH ', N'TREPACH ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (550, N'a27e5efa-ab97-44b6-9e6d-cac61bf5e8b7', N'TRIATHLON SPREJ ', N'TRIATHLON SPREJ ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (551, N'5f5f4511-f2ac-4d3b-b846-a0b37117c61b', N'TRICO ', N'TRICO ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (552, N'1133ae1f-f72d-47b4-957a-1eb6f8649526', N'TRIKA EXPERT ', N'TRIKA EXPERT ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (553, N'aaef86e7-b0ef-4d7f-b5bc-69546fe57bbd', N'TRINITY ', N'TRINITY ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (554, N'9fa0e654-4010-4af3-93ef-81b2bce5f034', N'TWINGO ', N'TWINGO ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (555, N'c77bc898-e5e2-4907-8120-0466fa07d4c6', N'U 46 M-FLUID ', N'U 46 M-FLUID ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (556, N'46813903-3a36-4828-9927-d1b61163e3a5', N'UNICORN DF ', N'UNICORN DF ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (557, N'2ae8220b-3088-4fdd-b5ac-8af5c3b99cbe', N'UNIVERSALIS ', N'UNIVERSALIS ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (558, N'bdd1745c-249b-4433-bbe3-a097a7fb21ff', N'UNIVERZALNI FUNGICID ', N'UNIVERZALNI FUNGICID ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (559, N'acceb821-33f4-4854-9905-88d2b850ad64', N'VALENTIA ', N'VALENTIA ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (560, N'9df4d62a-f821-46a7-a88c-7307eb1d0ad6', N'VALIS F ', N'VALIS F ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (561, N'd891313b-2ef2-4562-a26e-ff41ba07e9b0', N'VELOSTAR ', N'VELOSTAR ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (562, N'1ef34546-54f7-4489-b047-153a654febdb', N'VELUM PRIME ', N'VELUM PRIME ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (563, N'df16b82f-4c02-43d8-8587-0d805b7a37d9', N'VERBEN ', N'VERBEN ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (564, N'dfca85a4-fafd-4dc2-b531-62f538752e9e', N'VERTIMEC PRO ', N'VERTIMEC PRO ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (565, N'f05c0d3e-b71a-409e-aed1-f66156349309', N'VERTIPIN ', N'VERTIPIN ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (566, N'70dc5620-1bef-48f2-b68e-4ce188726bd1', N'VICTUS OD ', N'VICTUS OD ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (567, N'54e9aa38-ea5e-454b-8686-f03ce517d6c2', N'VIDERYO F ', N'VIDERYO F ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (568, N'ee1a1813-5c87-4da6-9545-55b62f6e7b1e', N'VINCYA F ', N'VINCYA F ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (569, N'8c3b9d66-a8fa-456b-864f-23c708ee6a81', N'VINDEX 80 WG ', N'VINDEX 80 WG ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (570, N'c4a917ae-d867-4bb5-92ca-023fe116d132', N'VINTEC ', N'VINTEC ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (571, N'4f5b4581-bb03-4af6-b7ee-173ba450a784', N'VITISAN ', N'VITISAN ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (572, N'a25eb4c7-96e0-42dd-98ce-65c57e58a5d1', N'VIVANDO ', N'VIVANDO ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (573, N'0ece1883-0266-45bf-8233-1df1654ed3db', N'VOLIAM ', N'VOLIAM ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (574, N'50e9253e-61e4-45f5-a0b2-1811f833fbed', N'VOTIVO FS 240 ', N'VOTIVO FS 240 ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (575, N'84d43272-47a3-4383-a9c6-3793e64e27f3', N'WINBY ', N'WINBY ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (576, N'ba5e4d26-c830-46ea-a7bf-e5e2fe8b826d', N'WISH TOP ', N'WISH TOP ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (577, N'110ed77a-0d35-4aa6-ba02-54d61253faa9', N'WÖBRA ', N'WÖBRA ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (578, N'13a9c3a5-99ea-4f10-9cf9-dd0a7b208ebd', N'WÜLFEL VABA ZA VOLUHARJA ', N'WÜLFEL VABA ZA VOLUHARJA ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (579, N'31c910a0-d12b-4e4b-946f-96f2c59a12f0', N'XANADU ', N'XANADU ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (580, N'fffd6f9e-4236-4b7c-a7ba-fb29891c0af0', N'XILON ', N'XILON ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (581, N'92302407-797d-41cc-95d3-c24c7d7d94f2', N'ZAFTRA AZT 250 SC ', N'ZAFTRA AZT 250 SC ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (582, N'a87a171c-73f5-46b8-92d6-6528e509ac13', N'ZAMZAR ', N'ZAMZAR ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (583, N'3cd25032-2ff3-428f-85f5-636e22399db6', N'ZANTARA ', N'ZANTARA ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (584, N'4373e9ab-4514-46cf-a7db-a5552ed52cb7', N'ZATO 50 WG ', N'ZATO 50 WG ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (585, N'd6578178-74b0-4708-9f4f-3df23d68f846', N'ZENBY ', N'ZENBY ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (586, N'09304194-50c0-46f4-b8fa-0da386b1a5a2', N'ZETROLA ', N'ZETROLA ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (587, N'3260030c-f20b-4016-82db-49ecd81a6901', N'ZIGNAL SUPER ', N'ZIGNAL SUPER ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (588, N'1172fdd1-383a-400f-89d5-4f25c497cd50', N'ZORVEC ENDAVIA ', N'ZORVEC ENDAVIA ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (589, N'a8684a54-df60-4cc3-9aae-f12215a95348', N'ZORVEC ZELAVIN ', N'ZORVEC ZELAVIN ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
INSERT [FieldBook].[pesticide] ([id], [uuid], [name], [name_en], [brand_name], [data], [status], [is_global], [farm_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (590, N'1a9276e9-034d-4af7-a8aa-d6febf77d031', N'ZOXIS 250 SC ', N'ZOXIS 250 SC ', NULL, NULL, 1, 1, NULL, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
GO
SET IDENTITY_INSERT [FieldBook].[pesticide] OFF

GO
INSERT [FieldBook].[task_type] ([id], [uuid], [name], [name_en], [system_type], [ploutos_type], [crop_status], [requires_time], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (1, N'6c43bcc2-1c14-4528-a12f-5da183d393f1', N'Oranje', N'Plowing', NULL, NULL, N'CROP_REMOVE', NULL, NULL, NULL, CAST(N'2024-07-03T14:40:14.250' AS DateTime), 3, 0, 1)
GO
INSERT [FieldBook].[task_type] ([id], [uuid], [name], [name_en], [system_type], [ploutos_type], [crop_status], [requires_time], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (2, N'b36559cc-3bb8-4b0e-b55b-7ae5215fcd2c', N'Setev/saditev/presajanje', N'Sowing/Planting/Transplanting', N'SOWING', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1)
GO
INSERT [FieldBook].[task_type] ([id], [uuid], [name], [name_en], [system_type], [ploutos_type], [crop_status], [requires_time], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (3, N'4fef2ea7-7d1d-4a15-865c-6f0e45878dac', N'Obdelava tal s pasivnimi oz. gnanimi stroji', N'Minimum/Conservation tillage', NULL, NULL, N'USER_DEFINED', NULL, NULL, NULL, NULL, NULL, 0, 1)
GO
INSERT [FieldBook].[task_type] ([id], [uuid], [name], [name_en], [system_type], [ploutos_type], [crop_status], [requires_time], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (4, N'4ed422b0-c8ef-43ec-b3d5-8e5b8c6ca713', N'Praha', N'Fallow land', NULL, NULL, N'CROP_REMOVE', 0, NULL, NULL, CAST(N'2024-08-06T08:27:26.403' AS DateTime), 1, 0, 1)
GO
INSERT [FieldBook].[task_type] ([id], [uuid], [name], [name_en], [system_type], [ploutos_type], [crop_status], [requires_time], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (5, N'72eb92fa-080f-4d66-a9f2-26607c8ec1af', N'Odstranitev invazivk (ITRV)', N'Removal of invasive plants', NULL, N'CropProtectionOperation', NULL, NULL, NULL, NULL, NULL, NULL, 0, 1)
GO
INSERT [FieldBook].[task_type] ([id], [uuid], [name], [name_en], [system_type], [ploutos_type], [crop_status], [requires_time], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (6, N'82724b8f-00f7-4efc-858a-3235d39735c4', N'Mehansko zatiranje plevelov', N'Mechanical weed control', NULL, N'CuttingOperation', NULL, 0, NULL, NULL, CAST(N'2024-08-06T09:02:37.110' AS DateTime), 1, 0, 1)
GO
INSERT [FieldBook].[task_type] ([id], [uuid], [name], [name_en], [system_type], [ploutos_type], [crop_status], [requires_time], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (7, N'890b28ea-0eca-45e2-ae24-4a3719f4b1e2', N'Spravilo, žetev', N'Harvest', N'HARVESTING', N'HarvestingOperation', NULL, NULL, NULL, NULL, NULL, NULL, 0, 1)
GO
INSERT [FieldBook].[task_type] ([id], [uuid], [name], [name_en], [system_type], [ploutos_type], [crop_status], [requires_time], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (8, N'4b5e906d-826e-48d1-872a-25a2d30d160f', N'Unicenje posevka brez uporabe FFS ', N'Crop destruction without herbicides', NULL, N'CropProtectionOperation', N'CROP_REMOVE', NULL, NULL, NULL, NULL, NULL, 0, 1)
GO
INSERT [FieldBook].[task_type] ([id], [uuid], [name], [name_en], [system_type], [ploutos_type], [crop_status], [requires_time], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (9, N'2a4ad821-67b9-4226-8629-94b8cf2f2c54', N'Košnja', N'Mowing', NULL, N'CuttingOperation', N'USER_DEFINED', 0, NULL, NULL, CAST(N'2024-08-06T08:28:17.400' AS DateTime), 1, 0, 1)
GO
INSERT [FieldBook].[task_type] ([id], [uuid], [name], [name_en], [system_type], [ploutos_type], [crop_status], [requires_time], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (10, N'c6aaceee-140f-4e14-9922-e6a53bf2092b', N'Paša', N'Grazing', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1)
GO
INSERT [FieldBook].[task_type] ([id], [uuid], [name], [name_en], [system_type], [ploutos_type], [crop_status], [requires_time], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (11, N'7c651404-2cbe-43c6-b9c3-a5cf3e62bb41', N'Druga delovna opravila', N'Other work tasks', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1)
GO
INSERT [FieldBook].[task_type] ([id], [uuid], [name], [name_en], [system_type], [ploutos_type], [crop_status], [requires_time], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (13, N'77eeb179-b0aa-4cc1-9978-e15bfeb3d292', N'Uporaba organskih in mineralnih gnojil', N'Use of organic and mineral fertilizers', N'FERTILIZING', N'FertilizationOperation', NULL, NULL, NULL, NULL, NULL, NULL, 0, 1)
GO
INSERT [FieldBook].[task_type] ([id], [uuid], [name], [name_en], [system_type], [ploutos_type], [crop_status], [requires_time], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (14, N'24e906be-7fa8-4052-aa21-39ba4e89258b', N'Uporaba FFS', N'Use of plant protection products (PPP)', N'PEST_CONTROL', N'CropProtectionOperation', NULL, 1, NULL, NULL, CAST(N'2024-07-16T14:07:35.280' AS DateTime), 1, 0, 1)
GO
INSERT [FieldBook].[task_type] ([id], [uuid], [name], [name_en], [system_type], [ploutos_type], [crop_status], [requires_time], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (15, N'05aa2d73-e330-4a23-a3e5-02abf91275e8', N'Obračanje', N'Turning', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1)
GO
INSERT [FieldBook].[task_type] ([id], [uuid], [name], [name_en], [system_type], [ploutos_type], [crop_status], [requires_time], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (16, N'9bebf49f-7c53-4a37-bdeb-55ffb6726881', N'Baliranje', N'Baling', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1)
GO
INSERT [FieldBook].[task_type] ([id], [uuid], [name], [name_en], [system_type], [ploutos_type], [crop_status], [requires_time], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (17, N'dca87677-534b-4e67-8707-95596e254e7e', N'Zgrabljanje', N'Raking', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1)
GO
INSERT [FieldBook].[task_type] ([id], [uuid], [name], [name_en], [system_type], [ploutos_type], [crop_status], [requires_time], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (18, N'84dc5743-e07d-437c-a49d-736e9d7ead62', N'Prevozi', N'Transport', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1)
GO
INSERT [FieldBook].[task_type] ([id], [uuid], [name], [name_en], [system_type], [ploutos_type], [crop_status], [requires_time], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (19, N'bb7ee784-711b-43ae-81a4-4fdc6d2c3be7', N'Tlačenje silosa', N'Silage compaction', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1)
GO
INSERT [FieldBook].[task_type] ([id], [uuid], [name], [name_en], [system_type], [ploutos_type], [crop_status], [requires_time], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (20, N'50f4c46a-f609-4627-9ab8-049d35e58e7f', N'Apnenje', N'Liming', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1)
GO
INSERT [FieldBook].[task_type] ([id], [uuid], [name], [name_en], [system_type], [ploutos_type], [crop_status], [requires_time], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (21, N'03711889-08d6-46fc-97b7-b8504002478e', N'kultiviranje z dognojevanjem', N'Cultivation with fertilization', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1)
GO
INSERT [FieldBook].[task_type] ([id], [uuid], [name], [name_en], [system_type], [ploutos_type], [crop_status], [requires_time], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (22, N'28c36410-2deb-4027-b577-73af803c3e69', N'Oblikovanje jarkov za šparglje', N'Shaping of asparagus beds', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1)
GO
INSERT [FieldBook].[task_type] ([id], [uuid], [name], [name_en], [system_type], [ploutos_type], [crop_status], [requires_time], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (23, N'cf6837f4-a3ed-41a2-8b55-11a72c8ebe68', N'Izdelava grebenov za šparglje', N'Making asparagus ridges', NULL, NULL, NULL, NULL, NULL, NULL, CAST(N'2024-05-27T14:36:37.137' AS DateTime), 1, 0, 1)
GO
SET IDENTITY_INSERT [FieldBook].[unit] ON 
GO
INSERT [FieldBook].[unit] ([id], [uuid], [name], [description], [description_en], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (1, NULL, N'kg', N'kilogrami', N'kilograms', NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[unit] ([id], [uuid], [name], [description], [description_en], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (2, NULL, N'm3', N'm3', N'cubic meters', NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[unit] ([id], [uuid], [name], [description], [description_en], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (3, NULL, N'l', N'l', N'liters', NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[unit] ([id], [uuid], [name], [description], [description_en], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (4, NULL, N'kg/ha', N'kg/ha', N'kilograms per hectare', NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[unit] ([id], [uuid], [name], [description], [description_en], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (5, NULL, N'l/ha', N'l/ha', N'liters per hectare', NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[unit] ([id], [uuid], [name], [description], [description_en], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (6, NULL, N'kW', N'kW', N'kilowatts', NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[unit] ([id], [uuid], [name], [description], [description_en], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (7, NULL, N'm', N'm', N'meters', NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[unit] ([id], [uuid], [name], [description], [description_en], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (8, NULL, N'cm', N'cm', N'centimeters', NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[unit] ([id], [uuid], [name], [description], [description_en], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (9, NULL, N'brazd', N'brazd', N'furrows', NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[unit] ([id], [uuid], [name], [description], [description_en], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (10, NULL, N't', N't', N'tons', NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[unit] ([id], [uuid], [name], [description], [description_en], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (11, NULL, N'KM', N'KM', N'Metric horsepower', NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[unit] ([id], [uuid], [name], [description], [description_en], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (12, NULL, N'kos', N'kos', N'pieces', NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[unit] ([id], [uuid], [name], [description], [description_en], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (13, NULL, N'vrst', N'vrst', N'row', NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[unit] ([id], [uuid], [name], [description], [description_en], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (14, NULL, N'nogac', N'nogac', N'stubble', NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[unit] ([id], [uuid], [name], [description], [description_en], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (15, NULL, N'kVA', N'kVA', N'kilovolt amperes', NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[unit] ([id], [uuid], [name], [description], [description_en], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (16, NULL, N'SE 25 MK', N'SE 25 MK', N'SE 25 MK', NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[unit] ([id], [uuid], [name], [description], [description_en], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (17, NULL, N'SE 50 MK', N'SE 50 MK', N'SE 50 MK', NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[unit] ([id], [uuid], [name], [description], [description_en], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (18, NULL, N'g', N'g', N'grams', NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[unit] ([id], [uuid], [name], [description], [description_en], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (19, NULL, N'ml', N'ml', N'mililiters', NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[unit] ([id], [uuid], [name], [description], [description_en], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (20, NULL, N'red', N'red', N'row', NULL, NULL, CAST(N'2024-07-05T13:20:10.920' AS DateTime), 3, 0, 1)
GO
SET IDENTITY_INSERT [FieldBook].[unit] OFF
GO
SET IDENTITY_INSERT [FieldBook].[unit_group] ON 
GO
INSERT [FieldBook].[unit_group] ([id], [unit_id], [group_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (1, 1, N'FERTILIZER', NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[unit_group] ([id], [unit_id], [group_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (2, 1, N'FERTILIZER_USAGE', NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[unit_group] ([id], [unit_id], [group_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (3, 1, N'MACHINERY_PERFORMANCE', NULL, NULL, NULL, NULL, 0, 1)
GO
INSERT [FieldBook].[unit_group] ([id], [unit_id], [group_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (4, 1, N'PESTICIDE', NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[unit_group] ([id], [unit_id], [group_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (5, 1, N'PESTICIDE_USAGE', NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[unit_group] ([id], [unit_id], [group_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (6, 2, N'FERTILIZER', NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[unit_group] ([id], [unit_id], [group_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (7, 2, N'FERTILIZER_USAGE', NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[unit_group] ([id], [unit_id], [group_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (8, 3, N'MACHINERY_PERFORMANCE', NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[unit_group] ([id], [unit_id], [group_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (9, 3, N'PESTICIDE', NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[unit_group] ([id], [unit_id], [group_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (10, 3, N'PESTICIDE_USAGE', NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[unit_group] ([id], [unit_id], [group_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (11, 4, N'FERTILIZER_USAGE', NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[unit_group] ([id], [unit_id], [group_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (12, 5, N'PESTICIDE_USAGE', NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[unit_group] ([id], [unit_id], [group_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (13, 6, N'MACHINERY_PERFORMANCE', NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[unit_group] ([id], [unit_id], [group_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (14, 7, N'MACHINERY_PERFORMANCE', NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[unit_group] ([id], [unit_id], [group_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (15, 8, N'MACHINERY_PERFORMANCE', NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[unit_group] ([id], [unit_id], [group_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (16, 9, N'MACHINERY_PERFORMANCE', NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[unit_group] ([id], [unit_id], [group_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (17, 10, N'MACHINERY_PERFORMANCE', NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[unit_group] ([id], [unit_id], [group_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (18, 11, N'MACHINERY_PERFORMANCE', NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[unit_group] ([id], [unit_id], [group_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (19, 12, N'FERTILIZER', NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[unit_group] ([id], [unit_id], [group_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (20, 12, N'FERTILIZER_USAGE', NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[unit_group] ([id], [unit_id], [group_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (21, 12, N'PESTICIDE', NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[unit_group] ([id], [unit_id], [group_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (22, 12, N'PESTICIDE_USAGE', NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[unit_group] ([id], [unit_id], [group_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (23, 13, N'MACHINERY_PERFORMANCE', NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[unit_group] ([id], [unit_id], [group_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (24, 14, N'MACHINERY_PERFORMANCE', NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[unit_group] ([id], [unit_id], [group_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (25, 15, N'MACHINERY_PERFORMANCE', NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[unit_group] ([id], [unit_id], [group_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (26, 16, N'MACHINERY_PERFORMANCE', NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[unit_group] ([id], [unit_id], [group_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (27, 17, N'MACHINERY_PERFORMANCE', NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[unit_group] ([id], [unit_id], [group_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (28, 20, N'MACHINERY_PERFORMANCE', NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [FieldBook].[unit_group] ([id], [unit_id], [group_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (29, 20, N'FERTILIZER_USAGE', CAST(N'2024-07-05T13:14:46.107' AS DateTime), 3, CAST(N'2024-07-05T13:15:23.250' AS DateTime), 3, 1, 0)
GO
INSERT [FieldBook].[unit_group] ([id], [unit_id], [group_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (30, 20, N'PESTICIDE_USAGE', CAST(N'2024-07-05T13:14:46.107' AS DateTime), 3, CAST(N'2024-07-05T13:15:23.250' AS DateTime), 3, 1, 0)
GO
INSERT [FieldBook].[unit_group] ([id], [unit_id], [group_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (31, 20, N'PESTICIDE', CAST(N'2024-07-05T13:14:46.107' AS DateTime), 3, CAST(N'2024-07-05T13:15:23.250' AS DateTime), 3, 1, 0)
GO
INSERT [FieldBook].[unit_group] ([id], [unit_id], [group_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (32, 20, N'PESTICIDE_USAGE', CAST(N'2024-07-05T13:19:55.347' AS DateTime), 3, CAST(N'2024-07-05T13:20:11.070' AS DateTime), 3, 1, 0)
GO
INSERT [FieldBook].[unit_group] ([id], [unit_id], [group_id], [created], [created_by], [modified], [modified_by], [is_deleted], [is_active]) VALUES (33, 20, N'PESTICIDE', CAST(N'2024-07-05T13:19:55.347' AS DateTime), 3, CAST(N'2024-07-05T13:20:11.070' AS DateTime), 3, 1, 0)
GO
SET IDENTITY_INSERT [FieldBook].[unit_group] OFF
GO
PRINT 'DATA INSERTED'
GO