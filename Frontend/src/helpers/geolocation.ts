const requestLocation = function (
  resolve: (data: { success: boolean; location?: GeolocationPosition }) => void
) {
  navigator.geolocation.getCurrentPosition(
    (location) => {
      resolve({ success: true, location });
    },
    () => {
      resolve({ success: false });
    },
    {
      enableHighAccuracy: true,
      timeout: 10000,
    }
  );
};

export default { requestLocation };
