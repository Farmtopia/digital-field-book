﻿using ITC.FieldBook.Application.Features.Users;
using ITC.FieldBook.Application.Infrastructure;
using ITC.FieldBook.Application.Infrastructure.Enums;
using ITC.FieldBook.Application.Infrastructure.Models;
using ITC.FieldBook.Application.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace ITC.FieldBook.Application.Features.Users
{
    public class AuthService
    {
        private UsersRepository _usersRepository;
        private IConfiguration _configuration;

        public AuthService(DbContext ctx, IConfiguration configuration)
        {
            _usersRepository = new UsersRepository(ctx);
            _configuration = configuration;
        }

        public string? GetToken(User user, Farm? farm)
        {
            var token = GenerateJSONWebToken(user, farm);
            return token;
        }

        private string GenerateJSONWebToken(User user, Farm? farm)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Jwt:Key"]));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var claims = new List<Claim> {
                new Claim(ClaimTypes.Name, user.Displayname ?? ""),
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString() ?? ""),
                new Claim(ClaimTypes.GivenName, user.Name ?? ""),
                new Claim(ClaimTypes.Surname, user.Surname ?? ""),
                new Claim(ClaimTypes.Email, user.Email ?? ""),
                new Claim(ClaimTypes.Role, user.Role ?? EnumRole.User),
                new Claim("Uuid", user.Uuid?.ToString() ?? ""),
                new Claim("FarmId", farm?.Id?.ToString() ?? ""),
            };

            var token = new JwtSecurityToken(_configuration["Jwt:Issuer"],
              _configuration["Jwt:Issuer"],
              claims,
              expires: DateTime.Now.AddDays(1),
              signingCredentials: credentials);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}
