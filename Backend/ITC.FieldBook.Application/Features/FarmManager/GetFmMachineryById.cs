﻿using ITC.FieldBook.Application.Infrastructure.Interfaces;
using ITC.FieldBook.Application.Infrastructure;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using ITC.FieldBook.Application.Infrastructure.Models;
using ITC.FieldBook.Application.Models;
using Microsoft.AspNetCore.Authorization;
using ITC.FieldBook.Application.Models.DTO;
using AutoMapper;
using ITC.FieldBook.Application.Features.Machinery;
using ITC.FieldBook.Application.Features.Fields;
using ITC.FieldBook.Application.Infrastructure.Tools;
using ITC.FieldBook.Application.Enums;
using System.ComponentModel.DataAnnotations;

namespace ITC.FieldBook.Application.Features.FarmManager
{
    public class GetFmMachineryByCodeController : ApiControllerBase
    {
        [HttpGet("farm-manager/machinery/get-by-code/{code}")]
        public Task<DbItem<FmMachinery>> GetFmMachineryByCode([FromRoute] string? code)
        {
            return Mediator.Send(new GetFmMachineryByCodeQuery { Code = code });
        }
    }

    public class GetFmMachineryByCodeQuery : IRequest<DbItem<FmMachinery>>
    {
        [Required]
        public string? Code { get; set; }
    }

    internal sealed class GetFmMachineryByCodeQueryHandler : IRequestHandler<GetFmMachineryByCodeQuery, DbItem<FmMachinery>>
    {
        private readonly IFarmManagerService _farmManagerService;

        public GetFmMachineryByCodeQueryHandler(IFarmManagerService farmManagerService)
        {
            _farmManagerService = farmManagerService;
        }

        public async Task<DbItem<FmMachinery>> Handle(GetFmMachineryByCodeQuery request, CancellationToken cancellationToken)
        {
            if(Globals.AppEnvironment != EnumAppEnvironment.DEFAULT)
                return new DbItem<FmMachinery>();

            var fmMachinery = await _farmManagerService.Instance.GetMachineryByCode(request.Code!);
            return new DbItem<FmMachinery> { Item = fmMachinery };
        }
    }
}
