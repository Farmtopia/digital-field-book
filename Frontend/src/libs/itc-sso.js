var appUuid;
var redirectToken;

var dev = false;
export const myApplicationsUrl = dev
  ? 'http://localhost:9000'
  : 'https://myapplications.dih-agrifood.com';

export const apiUrl = dev
  ? 'https://localhost:7147'
  : 'https://myapplications.dih-agrifood.com/api';

export const loginStatus = {
  NO_TOKEN: 'NO_TOKEN',
  INVALID_TOKEN: 'INVALID_TOKEN',
  NO_PERMISSION: 'NO_PERMISSION',
  ERROR: 'ERROR',
};

const getToken = async function (redirectTokenUuid) {
  var response = await fetch(
    `${apiUrl}/refresh-token?uuid=${redirectTokenUuid}`,
    {
      method: 'GET',
    }
  );

  var token = await response.text();
  return token;
};

const init = function (applicationUuid) {
  appUuid = applicationUuid?.toLowerCase();
  redirectToken = getUrlParam('redirect_token');

  var currentUrl = window.location.href;
  var newUrl = currentUrl.split('?')[0];
  window.history.replaceState({}, document.title, newUrl);
};

const login = function (callbackUrl) {
  var cbUrl = callbackUrl ?? window.location.href;
  window.location.href = `${myApplicationsUrl}/login?callbackUrl=${cbUrl}`;
};

const signUp = function () {
  window.open(`${myApplicationsUrl}/login?callbackUrl=${window.location.href}`);
};

const logout = function () {
  //localStorage.removeItem('itcJwtToken');
  //accessToken = null;
};

const checkLogin = async function () {
  if (!redirectToken) {
    return { accessToken: null, user: null, status: 'NO_TOKEN' };
  }

  var accessToken = await getToken(redirectToken);
  if (!accessToken) {
    return { accessToken: null, user: null, status: 'INVALID_TOKEN' };
  }

  try {
    var response = await fetch(`${apiUrl}/me`, {
      method: 'GET',
      //credentials: 'include',
      headers: {
        Authorization: `Bearer ${accessToken}`,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    });

    var user = await response.json();
    var hasPermission = user.applications?.some(
      (x) => x.uuid?.toLowerCase() == appUuid
    );

    if (!hasPermission) {
      return { accessToken: null, user, status: 'NO_PERMISSION' };
    }
    return { accessToken, user, status: 1 };
  } catch (e) {
    console.log('checkLogin Error', e);
    return { accessToken: null, user: null, status: 'ERROR' };
  }
};

const getUrlParam = function (name) {
  const queryString = window.location.search;
  const urlParams = new URLSearchParams(queryString);
  return urlParams.get(name);
};

export default {
  apiUrl,
  myApplicationsUrl,
  loginStatus,
  init,
  login,
  checkLogin,
  logout,
  signUp,
};
