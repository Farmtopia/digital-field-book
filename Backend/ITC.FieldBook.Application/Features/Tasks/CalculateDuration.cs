﻿using ITC.FieldBook.Application.Infrastructure.Interfaces;
using ITC.FieldBook.Application.Infrastructure;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata;
using Dapper;
using ITC.FieldBook.Application.Models;
using ITC.FieldBook.Application.Infrastructure.Models;
using ITC.FieldBook.Application.Features.Fields;
using ITC.FieldBook.Application.Enums;
using ITC.FieldBook.Application.Features.Units;
using System.ComponentModel.DataAnnotations;
using Azure.Core;
using Azure;
using Microsoft.Extensions.Azure;

namespace ITC.FieldBook.Application.Features.Tasks
{
    public class CalculateDurationController : ApiControllerBase
    {
        [HttpGet("tasks/fields/calculate-duration")]
        public Task<List<TaskDurationForFieldPlot>> CalculateDuration([FromQuery] CalculateDurationCommand command)
        {
            return Mediator.Send(command);
        }
    }

    public class CalculateDurationCommand : BaseDataQuery<List<TaskDurationForFieldPlot>>
    {
        [Required]
        public List<int>? FieldPlotIds { get; set; }

        [Required]
        public DateTime? DateFrom { get; set; }

        [Required]
        public DateTime? DateTo { get; set; }
    }


    internal sealed class CalculateDurationQueryHandler : IRequestHandler<CalculateDurationCommand, List<TaskDurationForFieldPlot>>
    {
        private readonly DbContext _ctx;
        private readonly IConfiguration _configuration;
        private readonly ICurrentUserService _currentUserService;

        public CalculateDurationQueryHandler(DbContext ctx, IConfiguration configuration, ICurrentUserService currentUserService)
        {
            _ctx = ctx;
            _configuration = configuration;
            _currentUserService = currentUserService;
        }

        public Task<List<TaskDurationForFieldPlot>> Handle(CalculateDurationCommand request, CancellationToken cancellationToken)
        {
            var user = _currentUserService.GetUser()!;
            var farmId = _currentUserService.FarmId!.Value;

            if (request.FieldPlotIds == null || !request.FieldPlotIds.Any())
                return Task.FromResult(new List<TaskDurationForFieldPlot>());

            var fieldsRepo = new FieldsRepository(_ctx);
            var fieldPlots = fieldsRepo.GetFieldPlots(farmId, fieldPlotIds: request.FieldPlotIds).Data.ToList();

            var durations = GetDurations(fieldPlots, request.DateFrom!.Value, request.DateTo!.Value);
            return Task.FromResult(durations);
        }
        public static List<TaskDurationForFieldPlot> GetDurations(List<FieldPlot> fieldPlots, DateTime dateFrom, DateTime dateTo)
        {
            var durations = new List<TaskDurationForFieldPlot>();
            var totalTime = (int)(dateTo - dateFrom).TotalMinutes;
            var totalArea = fieldPlots.Sum(x => x.Area ?? 0);

            if (totalTime > 0 && totalArea > 0)
            { 
                var rate = totalTime / totalArea;
                foreach (var fp in fieldPlots)
                {
                    durations.Add(new TaskDurationForFieldPlot
                    {
                        FieldPlotId = fp.Id,
                        DurationMinutes = (int)Math.Round((fp.Area ?? 0) * rate),
                    });
                }

                // If there is difference (because of decimals), add that to largest field in set
                var difference = totalTime - durations.Sum(x => x.DurationMinutes);
                if(difference != 0)
                {
                    var largestFieldPlot = fieldPlots.OrderByDescending(x=>x.Area).FirstOrDefault();
                    if (largestFieldPlot != null)
                    {
                        var largestDuration = durations.FirstOrDefault(x => x.FieldPlotId == largestFieldPlot.Id);
                        if (largestDuration != null)
                            largestDuration.DurationMinutes += difference;
                    }
                }
            }

            return durations;
        }

        /// <summary>
        /// If there are extra minutes
        /// </summary>
        /// <param name="estimates"></param>
        /// <param name="totalTime"></param>
        public static void AdjustDuration(List<TaskDurationForFieldPlot> estimates, double totalTime)
        {
            double roundedTotal = estimates.Sum(x=>x.DurationMinutes);
            double difference = totalTime - roundedTotal;

            if (difference != 0)
            {
                for (int i = 0; i < Math.Abs(difference); i++)
                {
                    int index = i % estimates.Count;
                    estimates.ElementAt(index).DurationMinutes += Math.Sign(difference);
                }
            }
        }
    }
}
