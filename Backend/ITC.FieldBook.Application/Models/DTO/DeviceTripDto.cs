﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace ITC.FieldBook.Application.Models.DTO
{
    public class DeviceTripDto
    {
        public int Id { get; set; }
        public int? DeviceId { get; set; }
        public int? GerkPid { get; set; }
        public DateTime? StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        public double? TotalMinutes { get; set; }
        public double? AverageVelocity { get; set; }
        public int? PointCount { get; set; }
        public string? BeaconId { get; set; }
        public string? BeaconName { get; set; }
        public string? GeoJSON { get; set; }

        public int? FieldId { get; set; }

        public string? FieldIdentifier { get; set; }

        public string? FieldName { get; set; }

        public int? MachineId { get; set; }
        public string? MachineName { get; set; }

        public int? AttachmentId { get; set; }
        public string? AttachmentName { get; set; }
    }
}
