﻿using ITC.FieldBook.Application.Infrastructure.Models;
using ITC.FieldBook.Application.Infrastructure;
using MediatR;
using Microsoft.AspNetCore.DataProtection.KeyManagement;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using ITC.FieldBook.Application.Models;
using ITC.FieldBook.Application.Infrastructure.Interfaces;

namespace ITC.FieldBook.Application.Features.Users.Settings
{
    public class SaveUserCropSettingController : ApiControllerBase
    {
        [HttpPost("user/settings/crop-settings")]
        public Task SaveUserCropSetting([FromBody] UserCropSetting data)
        {
            return Mediator.Send(new SaveUserCropSettingCommand { Data = data });
        }
    }

    public class SaveUserCropSettingCommand : IRequest
    {
        public UserCropSetting? Data { get; set; }
    }

    internal sealed class SaveUserCropSettingCommandHandler : IRequestHandler<SaveUserCropSettingCommand>
    {
        private readonly DbContext _ctx;
        private readonly ICurrentUserService _currentUserService;
        public SaveUserCropSettingCommandHandler(DbContext ctx, ICurrentUserService currentUserService)
        {
            _ctx = ctx;
            _currentUserService = currentUserService;
        }

        public Task Handle(SaveUserCropSettingCommand request, CancellationToken cancellationToken)
        {
            if (request.Data != null)
            {
                var data = request.Data;
                var user = _currentUserService.GetUser()!;

                data.UserId = user.Id;
                _ctx.Save(data, user: user);
            }

            return Task.CompletedTask;
        }
    }
}

