﻿using ITC.FieldBook.Application.Infrastructure.Interfaces;
using ITC.FieldBook.Application.Infrastructure;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata;
using Dapper;
using ITC.FieldBook.Application.Models;
using ITC.FieldBook.Application.Infrastructure.Models;

namespace ITC.FieldBook.Application.Features.Crops
{
    public class GetCropsController : ApiControllerBase
    {
        [HttpGet("crops")]
        public Task<DbData<Crop>> GetCrops()
        {
            return Mediator.Send(new GetCropsQuery { });
        }
    }

    public class GetCropsQuery : IRequest<DbData<Crop>>
    {
    }

    internal sealed class GetCropsQueryHandler : IRequestHandler<GetCropsQuery, DbData<Crop>>
    {
        private readonly DbContext _ctx;
        private readonly IConfiguration _configuration;
        private readonly ICurrentUserService _currentUserService;

        public GetCropsQueryHandler(DbContext ctx, IConfiguration configuration, ICurrentUserService currentUserService)
        {
            _ctx = ctx;
            _configuration = configuration;
            _currentUserService = currentUserService;
        }

        public Task<DbData<Crop>> Handle(GetCropsQuery request, CancellationToken cancellationToken)
        {
            var cropsRepository = new CropsRepository(_ctx);
            var data = cropsRepository.GetCrops();

            return Task.FromResult(data);
        }
    }
}
