﻿using ITC.FieldBook.Application.Infrastructure.Interfaces;
using ITC.FieldBook.Application.Infrastructure.Models;
using ITC.FieldBook.Application.Infrastructure.Tools;
using Microsoft.AspNetCore.DataProtection.KeyManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Dapper.SqlMapper;

namespace ITC.FieldBook.Application.Models
{
    public class Fertilizer : DbEntityWithAudit
    {
        public Fertilizer() : base("FieldBook.fertilizer") { }

        private int? _id;
        [DbColumn("id", IsKey = true, Identity = true)]
        public int? Id { get => _id; set { _id = value; RaisePropertyChanged(nameof(Id)); } }

        private string? _name;
        [DbColumn("name")]
        public string? Name { get => _name; set { _name = value; RaisePropertyChanged(nameof(Name)); } }

        private string? _nameEn;
        [DbColumn("name_en")]
        public string? NameEn { get => _nameEn; set { _nameEn = value; RaisePropertyChanged(nameof(NameEn)); } }

        private string? _brandName;
        [DbColumn("brand_name")]
        public string? BrandName { get => _brandName; set { _brandName = value; RaisePropertyChanged(nameof(BrandName)); } }

        private string? _type;
        [DbColumn("type")] public string? Type { get => _type; set { _type = value; RaisePropertyChanged(nameof(Type)); } }

        private int? _ratioN;
        [DbColumn("ratio_n")] public int? RatioN { get => _ratioN; set { _ratioN = value; RaisePropertyChanged(nameof(RatioN)); } }
            
        private int? _ratioP;
        [DbColumn("ratio_p")] public int? RatioP { get => _ratioP; set { _ratioP = value; RaisePropertyChanged(nameof(RatioP)); } }

        private int? _ratioK;
        [DbColumn("ratio_k")] public int? RatioK { get => _ratioK; set { _ratioK = value; RaisePropertyChanged(nameof(RatioK)); } }

        private string? _data;
        [DbColumn("data")]
        public string? Data { get => _data; set { _data = value; RaisePropertyChanged(nameof(Data)); } }

        private bool _isGlobal;
        [DbColumn("is_global")] public bool IsGlobal { get => _isGlobal; set { _isGlobal = value; RaisePropertyChanged(nameof(IsGlobal)); } }

        private int? _farmId;
        [DbColumn("farm_id")] public int? FarmId { get => _farmId; set { _farmId = value; RaisePropertyChanged(nameof(FarmId)); } }

        public string? FarmName { get; set; }

        public decimal? Stock { get; set; }

        public int? StockUnitId { get; set; }

        public string? StockUnit { get; set; }
    }
}
