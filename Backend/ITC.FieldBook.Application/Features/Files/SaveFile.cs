﻿using Azure.Storage.Blobs;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using ITC.FieldBook.Application.Infrastructure;
using ITC.FieldBook.Application.Infrastructure.Interfaces;
using ITC.FieldBook.Application.Infrastructure.Models;
using static Azure.Core.HttpHeader;

namespace ITC.FieldBook.Application.Features.Files
{
    public class SaveFileController : ApiControllerBase
    {
        [HttpPost("files")]
        public Task<List<SaveFileInfo>> SaveFile()
        {

            return Mediator.Send(new SaveFileCommand());
        }
    }

    public class SaveFileInfo
    {
        public int? Id { get; set; }

        public string? Displayname { get; set; }
    }

    public class SaveFileCommand : IRequest<List<SaveFileInfo>>
    {
        public string? Filename { get; set; }
        public byte[]? Bytes { get; set; }
    }

    internal sealed class SaveFileCommandHandler : IRequestHandler<SaveFileCommand, List<SaveFileInfo>>
    {
        private readonly DbContext _ctx;
        private readonly ICurrentUserService _currentUserService;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IFileService _fileService;

        public SaveFileCommandHandler(DbContext ctx, ICurrentUserService currentUserService, IHttpContextAccessor httpContextAccessor, IFileService fileService)
        {
            _ctx = ctx;
            _currentUserService = currentUserService;
            _httpContextAccessor = httpContextAccessor;
            _fileService = fileService;
        }

        public Task<List<SaveFileInfo>> Handle(SaveFileCommand request, CancellationToken cancellationToken)
        {
            var result = new List<int>();
            var user = _currentUserService.GetUser();

            // Request's .Form.Files property will
            // contain QUploader's files.
            var files = _httpContextAccessor!.HttpContext!.Request.Form.Files;
            var folder = user.Id!.Value.ToString();

            foreach (var file in files)
            {
                if (file == null || file.Length == 0)
                    continue;

                // Do something with the file.
                var fileName = file.FileName;
                using (var ms = new MemoryStream())
                {
                    file.CopyTo(ms);
                    var bytes = ms.ToArray();
                    var id = _fileService.Save(fileName, bytes, new SaveFileSettings { Folder = folder });
                    result.Add(id);
                }
            }

            var fileData = _fileService.Get(result.ToArray());

            return Task.FromResult(fileData.Select(x => new SaveFileInfo { Id = x.Id, Displayname = x.Displayname }).ToList());
        }
    }
}
