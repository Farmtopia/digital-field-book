﻿using Swashbuckle.AspNetCore.Annotations;
using System.Text.Json.Serialization;

namespace Farmtopia.DigitalFieldBook.API.Models.JSON_LD.Farms
{
    public class FarmsRoot
    {
        [SwaggerSchema(Nullable = false)]
        [JsonPropertyName("@context")]
        public FarmsContext Context { get; set; }

        [SwaggerSchema(Nullable = false)]
        [JsonPropertyName("@graph")]
        public virtual List<FarmsGraph> Graph { get; set; }
    }
}
