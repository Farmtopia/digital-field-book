﻿using Dapper;
using Farmtopia.DigitalFieldBook.API.Models;
using Farmtopia.DigitalFieldBook.API.Models.Providers;

namespace Farmtopia.DigitalFieldBook.API.Repository
{
    public class ProviderRepository
    {
        private DbContext _ctx;

        public ProviderRepository(DbContext ctx)
        {
            _ctx = ctx;
        }

        public Provider? GetProvider(int providerId)
        {
            using (var conn = _ctx.CreateConnection())
            {
                var data = conn.QuerySingleOrDefault<Provider>($@"
                    SELECT
                        p.id,
                        p.name,
                        p.directory_url,
                        p.secret_key
                    FROM Farmtopia.provider p
                    WHERE
                        id = @providerId
                        AND p.active = 1", new { providerId });

                return data;
            }
        }

        public void UpdateSecretKey(int providerId, string secretKey)
        {
            using (var conn = _ctx.CreateConnection())
            {
                var now = DateTime.Now;
                var data = conn.Execute($@"
                    UPDATE Farmtopia.provider
                    SET
                        secret_key = @secretKey,
                        modified = @now
                    WHERE 
                        id = @providerId", new { providerId, secretKey, now });
            }
        }

        public ProviderUser? GetUser(int providerId, int datasetId, string username)
        {
            using (var conn = _ctx.CreateConnection())
            {
                var data = conn.QuerySingleOrDefault<ProviderUser>($@"
                    SELECT
                        u.provider_id,
                        u.dataset_id,
                        u.username,
                        u.active
                    FROM Farmtopia.provider_user u
                    WHERE
                        u.provider_id = @providerId
                        AND u.dataset_id = @datasetId
                        AND u.username = @username", new { providerId, datasetId, username });

                return data;
            }
        }

        public void AddUser(int providerId, int datasetId, string username)
        {
            using (var conn = _ctx.CreateConnection())
            {
                var now = DateTime.Now;
                var data = conn.Execute($@"
                    INSERT INTO Farmtopia.provider_user(provider_id, dataset_id, username, active, created, modified)
                    VALUES(@providerId, @datasetId, @username, 1, @now, @now)", new { providerId, datasetId, username, now });
            }
        }

        public void UpdateUser(int providerId, int datasetId, string username)
        {
            using (var conn = _ctx.CreateConnection())
            {
                var now = DateTime.Now;
                var data = conn.Execute($@"
                    UPDATE Farmtopia.provider_user
                    SET
                        modified = @now
                    WHERE 
                        provider_id = @providerId
                        AND dataset_id = @datasetId
                        AND username = @username", new { providerId, datasetId, username, now });
            }
        }
    }
}
