﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITC.FieldBook.Application.Enums
{
    public static class EnumFieldEnvironmentType
    {
        public const string OUTDOOR_CULTIVATION = "OUTDOOR_CULTIVATION";
        public const string PROTECTED_ENVIRONMENT = "PROTECTED_ENVIRONMENT";
    }
}
