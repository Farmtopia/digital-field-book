﻿using ITC.FieldBook.Application.Infrastructure.Interfaces;
using ITC.FieldBook.Application.Infrastructure;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using ITC.FieldBook.Application.Infrastructure.Models;
using ITC.FieldBook.Application.Models;
using ITC.FieldBook.Application.Features.Fields;
using ITC.FieldBook.Application.Models.DTO;
using AutoMapper;

namespace ITC.FieldBook.Application.Features.Tasks
{
    public class GetTasksController : ApiControllerBase
    {
        [HttpGet("tasks")]
        public Task<DbData<TaskEntity>> GetTasks(int? fieldId, DateTime? dateFrom, DateTime? dateTo)
        {
            return Mediator.Send(new GetTasksQuery { FieldId = fieldId, DateFrom = dateFrom, DateTo = dateTo });
        }
    }

    public class GetTasksQuery : IRequest<DbData<TaskEntity>>
    {
        public int? FieldId { get; set; }

        public DateTime? DateFrom { get; set; }

        public DateTime? DateTo { get; set; }
    }

    internal sealed class GetTasksQueryHandler : IRequestHandler<GetTasksQuery, DbData<TaskEntity>>
    {
        private readonly DbContext _ctx;
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;
        private readonly ICurrentUserService _currentUserService;

        public GetTasksQueryHandler(DbContext ctx, IConfiguration configuration, ICurrentUserService currentUserService, IMapper mapper)
        {
            _ctx = ctx;
            _mapper = mapper;
            _configuration = configuration;
            _currentUserService = currentUserService;
        }

        public Task<DbData<TaskEntity>> Handle(GetTasksQuery request, CancellationToken cancellationToken)
        {
            var tasksRepository = new TasksRepository(_ctx);
            var fieldsRepository = new FieldsRepository(_ctx);

            var farmId = _currentUserService.FarmId!.Value;
            var data = tasksRepository.GetTasks(farmId, request.FieldId, request.DateFrom, request.DateTo);
            var taskFields = tasksRepository.GetTaskFields(farmId, data.Data.Select(x => x.Id!.Value).ToArray()).Data;
            
            if (data.Data.Any())
            {
                var minDate = data.Data.Min(x => x.DateFrom);
                var maxDate = data.Data.Max(x => x.DateFrom);
                var allFieldIds = taskFields.Select(x => x.FieldId!.Value).Distinct().ToList();
                var activePlots = minDate != null && maxDate != null ? fieldsRepository.GetActiveFieldPlots(farmId, minDate.Value, maxDate.Value, allFieldIds).Data : new List<FieldPlot>();
                var fields = fieldsRepository.GetFields(farmId, allFieldIds).Data;

                foreach (var d in data.Data)
                {
                    if (taskFields.Any())
                    {
                        d.TaskFields = taskFields.Where(x => x.TaskId == d.Id).OrderBy(x => x.FieldId == request.FieldId ? 0 : 1).ToList();
                        
                        if (d.TaskFields.Any())
                        {
                            d.Fields = new List<FieldDTO>();

                            // Add original fields
                            var fieldIds = d.TaskFields.Select(x => x.FieldId!.Value).Distinct().ToList();
                            foreach (var fieldId in fieldIds)
                            {
                                var field = fields.FirstOrDefault(x => x.Id == fieldId);
                                if(field !=null)
                                {
                                    d.Fields.Add(_mapper.Map<FieldDTO>(field));
                                }
                            }
                            
                            // Add active plots to fields
                            if (d.Fields.Any() && activePlots.Any())
                            {
                                foreach(var f in d.Fields)
                                {
                                    f.ActivePlots =  activePlots.Where(x => f.Id == x.FieldId && x.SowingDate <= d.DateFrom && (x.HarvestDate == null || x.HarvestDate > d.DateFrom)).ToList();
                                }
                            }
                                
                        }
                    }
                }
            }

            return Task.FromResult(data);
        }
    }
}
