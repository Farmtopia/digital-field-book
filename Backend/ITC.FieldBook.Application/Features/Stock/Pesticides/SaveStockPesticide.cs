﻿using ITC.FieldBook.Application.Infrastructure.Models;
using ITC.FieldBook.Application.Infrastructure;
using MediatR;
using Microsoft.AspNetCore.DataProtection.KeyManagement;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using ITC.FieldBook.Application.Models;
using ITC.FieldBook.Application.Infrastructure.Interfaces;
using ITC.FieldBook.Application.Features.Pesticides;

namespace ITC.FieldBook.Application.Features.Stock.Fertilizers
{
    public class SaveStockPesticideController : ApiControllerBase
    {
        [HttpPost("stock/pesticides")]
        public Task SaveStockPesticide([FromBody] StockPesticide data)
        {
            return Mediator.Send(new SaveStockPesticideCommand { Data = data });
        }
    }

    public class SaveStockPesticideCommand : IRequest
    {
        public StockPesticide? Data { get; set; }
    }

    internal sealed class SaveStockPesticideCommandHandler : IRequestHandler<SaveStockPesticideCommand>
    {

        private readonly DbContext _ctx;
        private readonly ICurrentUserService _currentUserService;

        public SaveStockPesticideCommandHandler(DbContext ctx, ICurrentUserService currentUserService)
        {
            _ctx = ctx;
            _currentUserService = currentUserService;
        }

        public Task Handle(SaveStockPesticideCommand request, CancellationToken cancellationToken)
        {
            if (request.Data != null)
            {
                var data = request.Data;
                var user = _currentUserService.GetUser()!;
                var stockRepo = new StockRepository(_ctx);

                data.FarmId = _currentUserService.FarmId!.Value;
                stockRepo.SaveStockPesticide(data, user: user);
            }

            return Task.CompletedTask;
        }
    }
}

