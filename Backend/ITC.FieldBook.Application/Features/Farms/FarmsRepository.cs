﻿using Azure.Core;
using ITC.FieldBook.Application.Infrastructure;
using ITC.FieldBook.Application.Infrastructure.Models;
using ITC.FieldBook.Application.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITC.FieldBook.Application.Features.Farms
{
    public class FarmsRepository
    {
        private readonly DbContext _ctx;

        public FarmsRepository(DbContext ctx)
        {
            _ctx = ctx;
        }

        public DbData<Farm> GetFarms(int? userId = null)
        {
            var data = _ctx.Get<Farm>($@"
                SELECT 
                    f.id,
                    f.name,
                    f.owner,
                    f.address,
                    f.post_code,
                    f.city,
                    f.identifier,
                    f.eco_farm,
                    f.thumb_file_id,
                    location.Lat AS latitude,
                    location.Long AS longitude
                FROM FieldBook.farm f
                WHERE
                    {DbContext.GetIsDeletedIsActiveCondition("f")}
                {(userId == null ? "" : $"AND f.id IN (SELECT farm_id FROM FieldBook.user_farm WHERE {DbContext.GetIsDeletedIsActiveCondition()} AND user_id = @userId)")}",
            new { userId });

            return data;
        }

        public DbItem<Farm> GetFarm(int farmId, int? userId = null)
        {
            var data = _ctx.GetItem<Farm>($@"
                SELECT TOP(1)
                    f.id,
                    f.name,
                    f.owner,
                    f.address,
                    f.post_code,
                    f.city,
                    f.identifier,
                    f.eco_farm,
                    f.thumb_file_id,
                    location.Lat AS latitude,
                    location.Long AS longitude
                FROM FieldBook.farm f
                WHERE
                    {DbContext.GetIsDeletedIsActiveCondition("f")}
                    AND f.id = @farmId
                    {(userId == null ? "" : $"AND EXISTS(SELECT TOP(1) 1 FROM FieldBook.user_farm WHERE {DbContext.GetIsDeletedIsActiveCondition()} AND farm_id = f.id AND user_id = @userId)")}",
            new { farmId, userId });

            return data;
        }


    }
}
