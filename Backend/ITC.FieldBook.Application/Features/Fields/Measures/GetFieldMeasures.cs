﻿using ITC.FieldBook.Application.Infrastructure.Interfaces;
using ITC.FieldBook.Application.Infrastructure;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata;
using Dapper;
using ITC.FieldBook.Application.Models;
using ITC.FieldBook.Application.Infrastructure.Models;
using Microsoft.VisualBasic;

namespace ITC.FieldBook.Application.Features.Fields.Measures
{
    public class GetFieldMeasuresController : ApiControllerBase
    {
        [HttpGet("fields/{fieldId}/measures")]
        public Task<DbData<FieldMeasure>> GetFieldMeasures([FromQuery]GetFieldMeasuresQuery query)
        {
            return Mediator.Send(query);
        }
    }

    public class GetFieldMeasuresQuery : IRequest<DbData<FieldMeasure>>
    {
        [FromRoute]
        public int FieldId { get; set; }

        public int FieldPlotId { get; set; }

        public int? Year { get; set; }
    }

    internal sealed class GetFieldMeasuresQueryHandler : IRequestHandler<GetFieldMeasuresQuery, DbData<FieldMeasure>>
    {
        private readonly DbContext _ctx;
        private readonly IConfiguration _configuration;
        private readonly ICurrentUserService _currentUserService;

        public GetFieldMeasuresQueryHandler(DbContext ctx, IConfiguration configuration, ICurrentUserService currentUserService)
        {
            _ctx = ctx;
            _configuration = configuration;
            _currentUserService = currentUserService;
        }

        public Task<DbData<FieldMeasure>> Handle(GetFieldMeasuresQuery request, CancellationToken cancellationToken)
        {
            var fieldsRepository = new FieldsRepository(_ctx);
            return Task.FromResult(fieldsRepository.GetFieldMeasures(_currentUserService.FarmId!.Value, request.FieldId, request.FieldPlotId, request.Year));
        }
    }
}
