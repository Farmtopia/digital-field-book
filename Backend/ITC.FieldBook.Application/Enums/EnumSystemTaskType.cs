﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITC.FieldBook.Application.Enums
{
    public static class EnumSystemTaskType
    {
        public const string FERTILIZING = "FERTILIZING";
        public const string HARVESTING = "HARVESTING";
        public const string PEST_CONTROL = "PEST_CONTROL";
        public const string SOWING = "SOWING";
    }
}
